1
00:00:06,072 --> 00:00:08,140
<i> ¶¶</i>

2
00:00:18,485 --> 00:00:20,552
I realize that this
is the last thing
you want to hear,

3
00:00:20,554 --> 00:00:24,356
but... we're about
to walk into a deal--

4
00:00:24,358 --> 00:00:26,458
I know what
we're walking into.

5
00:00:29,195 --> 00:00:32,264
Well, that's comforting.

6
00:00:32,266 --> 00:00:34,133
There's no forecast
for infected.

7
00:00:34,135 --> 00:00:36,101
What happened
wasn't our fault.

8
00:00:36,103 --> 00:00:39,671
We hand over what we have,
hope the invitation stands.

9
00:00:39,673 --> 00:00:43,409
Ah.
And if he refuses?

10
00:00:46,413 --> 00:00:48,347
We have ofelia.

11
00:01:05,765 --> 00:01:07,833
- ( Groans )
- Ofelia?

12
00:01:10,303 --> 00:01:12,404
- ( Banging )
- Hey, stop the truck.
Stop the truck!

13
00:01:12,406 --> 00:01:15,641
Ofelia!
Hey, stop the truck!

14
00:01:16,609 --> 00:01:18,710
Ah!
( Coughing )

15
00:01:20,447 --> 00:01:23,282
( Groans )

16
00:01:23,284 --> 00:01:25,551
- Jesus Christ, are you okay?
- I'm okay. I'm all right.

17
00:01:25,553 --> 00:01:29,188
Let's go.
I just lost my grip.
I lost my grip.

18
00:01:29,190 --> 00:01:31,290
Did you hit your head?
Let me see.

19
00:01:31,292 --> 00:01:32,758
I'm fine.
Let's just go.

20
00:01:34,427 --> 00:01:37,596
Madison, let's go.

21
00:01:39,232 --> 00:01:41,500
Madison, please,
let's just go back.

22
00:01:41,502 --> 00:01:44,236
- Okay?
- It's okay, ofelia.

23
00:01:44,238 --> 00:01:46,672
Let's you and me talk
for a second, okay?

24
00:01:57,450 --> 00:02:00,586
I want you to take me
to my father.

25
00:02:00,588 --> 00:02:02,254
You hear me?

26
00:02:03,523 --> 00:02:05,824
Give me your word,
Madison.

27
00:02:07,527 --> 00:02:11,296
I will.
I promise.

28
00:02:32,519 --> 00:02:34,586
<i> ( Insects chirping )</i>

29
00:02:56,543 --> 00:02:58,677
<i>( Car approaching )</i>

30
00:02:59,746 --> 00:03:01,947
<i> ¶¶</i>

31
00:03:06,553 --> 00:03:10,422
<i> ( car doors open, close )</i>

32
00:03:13,693 --> 00:03:16,228
<i> ( Metal clinking )</i>

33
00:03:30,977 --> 00:03:32,978
Alicia:
<i> Get in your car</i>
<i> and get out of here,</i>

34
00:03:32,980 --> 00:03:34,780
<i> or I will kill you!</i>

35
00:03:38,618 --> 00:03:41,486
- Nick?
- Saw your car.

36
00:03:44,924 --> 00:03:47,793
Alicia,
this is crazy.

37
00:03:47,795 --> 00:03:50,562
I've seen crazy.
This is kind of nice.

38
00:03:51,965 --> 00:03:53,832
This is disgusting,
though.

39
00:03:53,834 --> 00:03:56,268
You have no idea
what it's like
being out here.

40
00:03:56,270 --> 00:03:57,536
You seemed
to manage all right.

41
00:03:57,538 --> 00:03:59,605
There is safety
in numbers, Alicia.

42
00:03:59,607 --> 00:04:01,506
- You know that.
-<i> I don't know that.</i>

43
00:04:01,508 --> 00:04:04,643
I almost died yesterday
surrounded by people.

44
00:04:04,645 --> 00:04:07,713
Nick, is this you talking
or is it mom?

45
00:04:07,715 --> 00:04:09,581
Surviving is killing me.

46
00:04:13,486 --> 00:04:15,854
She's gonna lose her shit,
i come back without you.

47
00:04:18,691 --> 00:04:21,860
I know what
you're trying to do.

48
00:04:21,862 --> 00:04:23,895
It's been riding you
ever since you got
to the ranch.

49
00:04:23,897 --> 00:04:25,597
You're trying to atone.

50
00:04:28,534 --> 00:04:29,801
Don't.

51
00:04:32,305 --> 00:04:33,972
I forgive you.

52
00:04:36,976 --> 00:04:39,544
You don't owe me.

53
00:04:39,546 --> 00:04:41,546
You don't owe mom.

54
00:04:41,548 --> 00:04:45,050
And it might
feel like that,
but you don't.

55
00:04:45,052 --> 00:04:47,653
You deserve to carve
what you can out
of this life.

56
00:04:54,661 --> 00:04:56,895
- ( People chattering )
-<i> ( Radio chatter )</i>

57
00:04:56,897 --> 00:05:01,400
Man over P.A.:
<i> No parking in the white,</i>
<i> red, or drop zone.</i>

58
00:05:01,402 --> 00:05:03,535
<i> Any vehicle parked</i>
<i> in these zones</i>

59
00:05:03,537 --> 00:05:06,371
<i> will be confiscated</i>
<i> and sold for parts.</i>

60
00:05:07,907 --> 00:05:09,775
Be careful.

61
00:05:09,777 --> 00:05:11,877
- ( Groans )
- Okay, let's get you
out of that shirt.

62
00:05:11,879 --> 00:05:13,345
Victor:
<i>Madison.</i>

63
00:05:14,547 --> 00:05:16,081
If they so much
as think ofelia's sick,

64
00:05:16,083 --> 00:05:17,783
they'll kill her
where she stands.

65
00:05:17,785 --> 00:05:19,685
We're early.
Salazar won't be
here for hours.

66
00:05:19,687 --> 00:05:21,386
She needs a bed
and medicine for the pain.

67
00:05:21,388 --> 00:05:22,821
You got a better idea?

68
00:05:24,390 --> 00:05:26,725
Smile. Your life
depends on it.

69
00:05:30,463 --> 00:05:32,864
( Chatter )

70
00:05:32,866 --> 00:05:34,599
( Guard speaking Spanish )

71
00:05:34,601 --> 00:05:37,002
Hand over
your weapons now.

72
00:05:37,004 --> 00:05:38,737
<i> ( Radio chatter )</i>

73
00:05:45,945 --> 00:05:48,980
- No receipt, no weapons.
- Yeah, I know how it works.

74
00:05:48,982 --> 00:05:50,382
These are for trade.

75
00:05:53,653 --> 00:05:56,688
Six m4s, five people.

76
00:05:56,690 --> 00:05:59,891
That's 500 credits
minus entry--

77
00:06:02,061 --> 00:06:04,496
Johnny,<i> pendejo.</i>

78
00:06:05,965 --> 00:06:08,567
I told you to fix
the exchange rates.

79
00:06:10,036 --> 00:06:11,670
( Stifled groan )

80
00:06:13,373 --> 00:06:16,041
<i> ( Sounds echoing )</i>

81
00:06:21,948 --> 00:06:25,016
- Woman:<i> That comes out</i>
<i> to 330 credits minus--</i>
- hey, hey, hey.

82
00:06:25,018 --> 00:06:28,120
- That's 200 less.
- Sorry, last week's rates.

83
00:06:28,122 --> 00:06:29,688
We'll take it.

84
00:06:29,690 --> 00:06:31,022
Hey!
( Clicks tongue )

85
00:06:44,871 --> 00:06:45,937
Enjoy.

86
00:07:00,153 --> 00:07:02,020
She's burning up.

87
00:07:02,022 --> 00:07:04,189
All right, stay with her.
I'll find something
for the pain.

88
00:07:06,459 --> 00:07:07,926
Taqa.

89
00:07:09,962 --> 00:07:11,630
Stay awake.

90
00:07:17,470 --> 00:07:19,604
Hey. This is a waste.

91
00:07:19,606 --> 00:07:22,174
- Excuse me?
- Tragic, sure.

92
00:07:22,176 --> 00:07:25,210
But you just gave away
half our rifles,
and for what?

93
00:07:25,212 --> 00:07:27,012
<i>To buy ofelia</i>
<i>a kiss good-bye?</i>

94
00:07:27,014 --> 00:07:28,547
For Christ's sake,
Madison, think.

95
00:07:28,549 --> 00:07:30,449
- Consider your moves.
- We're out of moves.

96
00:07:30,451 --> 00:07:32,884
We're out of guns.
We got no home.

97
00:07:32,886 --> 00:07:35,220
We rely on the kindness
of strangers now, Victor.

98
00:07:35,222 --> 00:07:37,689
I gave my word
that I would take
ofelia to her father

99
00:07:37,691 --> 00:07:39,191
- and I intend to keep that.
- How commendable.

100
00:07:39,193 --> 00:07:41,760
There's no plan b!
I can only face this.

101
00:07:41,762 --> 00:07:43,195
Our better angels
are all dead,

102
00:07:43,197 --> 00:07:44,996
and so is ofelia.

103
00:07:44,998 --> 00:07:46,665
<i> Your word isn't worth</i>
<i> a damn whether or not</i>

104
00:07:46,667 --> 00:07:48,500
<i> she makes it</i>
<i> another hour or two.</i>

105
00:07:48,502 --> 00:07:51,203
The only thing worth
anything anymore
are resources.

106
00:07:51,205 --> 00:07:54,039
You keep squandering
what little we've got.

107
00:07:54,041 --> 00:07:55,507
You expect charity
from a butcher,

108
00:07:55,509 --> 00:07:58,477
he will not
offer you that.

109
00:07:58,479 --> 00:08:00,479
Are you afraid of him?

110
00:08:00,481 --> 00:08:02,214
<i>Is that it?</i>

111
00:08:03,716 --> 00:08:05,250
<i> Fine,</i>
<i> we'll wait and see</i>

112
00:08:05,252 --> 00:08:07,152
what Daniel does
when you show up
to pay for water

113
00:08:07,154 --> 00:08:11,256
you<i> don't</i> need
with a dead daughter
and empty hands!

114
00:08:11,258 --> 00:08:13,825
<i> ¶¶</i>

115
00:08:23,836 --> 00:08:26,137
-<i> ( birds chirping )</i>
- Nick: Last chance.

116
00:08:28,708 --> 00:08:31,009
- Please?
- It's not.

117
00:08:31,011 --> 00:08:33,512
I know where you'll be,
and if I get into trouble,

118
00:08:33,514 --> 00:08:34,980
I'll come and find you.

119
00:08:36,582 --> 00:08:38,884
I'd prefer it
if you didn't die.

120
00:08:38,886 --> 00:08:40,752
I won't.

121
00:08:48,628 --> 00:08:50,896
I promise.

122
00:09:02,842 --> 00:09:04,075
Okay.

123
00:09:07,680 --> 00:09:08,880
( Engine starts )

124
00:09:10,149 --> 00:09:11,883
<i> ( Car door closes )</i>

125
00:09:28,100 --> 00:09:30,936
Why does everybody
hate you, Nick?

126
00:09:30,938 --> 00:09:34,339
It sounds like
you were a real treat
back in the day.

127
00:09:37,777 --> 00:09:40,745
Old news
and not your business.

128
00:09:41,948 --> 00:09:43,248
Besides, Alicia's wrong.

129
00:09:43,250 --> 00:09:45,617
That's not why
i stayed at the ranch.

130
00:09:45,619 --> 00:09:49,654
- No? Why did you stay?
- Travis was dead.

131
00:09:50,957 --> 00:09:52,791
And Luciana was sick.

132
00:09:52,793 --> 00:09:56,094
- But my mom
was falling apart.
- ( Laughs )

133
00:09:56,096 --> 00:09:58,163
- Ah...
- So, I had
to keep it together.

134
00:09:58,165 --> 00:10:00,765
( Chuckles )
It's bullshit.
It's bullshit.

135
00:10:02,268 --> 00:10:04,936
Why don't you tell me
why I stayed?

136
00:10:04,938 --> 00:10:08,306
All right,
you stayed at the ranch
because you love me.

137
00:10:08,308 --> 00:10:11,242
- It's all right.
- ( Scoffs )

138
00:10:13,279 --> 00:10:16,214
Troy: You and me,
we're more alike
than you think.

139
00:10:16,216 --> 00:10:20,952
Black sheep,
children of violence.

140
00:10:20,954 --> 00:10:23,355
It's just you wish
you weren't.

141
00:10:23,357 --> 00:10:26,124
<i> ¶¶</i>

142
00:10:30,963 --> 00:10:33,865
- ( shallow breathing )
-<i> ( Door opens )</i>

143
00:10:33,867 --> 00:10:36,701
Hey, I got these
for the pain and fever.

144
00:10:36,703 --> 00:10:39,070
Electrolytes
for the dehydration.

145
00:10:41,941 --> 00:10:43,174
Ofelia.

146
00:10:53,252 --> 00:10:54,919
( Coughing )

147
00:10:56,155 --> 00:10:58,690
She doesn't
deserve this.

148
00:10:58,692 --> 00:11:00,725
Daniel doesn't
deserve this.

149
00:11:03,729 --> 00:11:08,099
I just wanna
give her some peace
before she dies.

150
00:11:08,101 --> 00:11:11,736
Maybe strand's right,
maybe Daniel's gonna
kill us all.

151
00:11:16,275 --> 00:11:20,412
Yesterday,
i lost everything
I've ever known.

152
00:11:20,414 --> 00:11:24,282
My home, my friends,

153
00:11:24,284 --> 00:11:26,851
the bones
of my forefathers

154
00:11:26,853 --> 00:11:29,354
scattered in a field
with no meaning.

155
00:11:30,389 --> 00:11:32,190
<i>I was raised on loss.</i>

156
00:11:32,192 --> 00:11:34,793
I thought
i was good at it.

157
00:11:36,128 --> 00:11:38,396
But there is
such a thing as too much.

158
00:11:42,802 --> 00:11:45,837
Ofelia asked to see
her father one last time.

159
00:11:46,972 --> 00:11:49,741
You're honoring
that wish,

160
00:11:49,743 --> 00:11:51,876
<i>which is</i>
<i>all you can do.</i>

161
00:11:53,412 --> 00:11:56,047
Return his daughter
for him to bury.

162
00:11:58,417 --> 00:12:01,920
Put an end to this,
even if it does get us
all killed.

163
00:12:06,892 --> 00:12:08,960
Thank you, taqa.

164
00:12:11,997 --> 00:12:14,833
<i> ¶¶</i>

165
00:12:18,904 --> 00:12:20,972
<i> ( bird screeching )</i>

166
00:12:29,515 --> 00:12:31,783
<i> ( Bell jingles )</i>

167
00:12:31,785 --> 00:12:33,852
<i> ( Insects chirping )</i>

168
00:12:36,388 --> 00:12:38,990
<i> - ( Bell jingles )</i>
<i> - ( Door closes )</i>

169
00:12:38,992 --> 00:12:41,059
( Flies buzzing )

170
00:12:46,332 --> 00:12:48,466
( Buzzing continues )

171
00:12:56,509 --> 00:12:58,810
( Flies buzzing )

172
00:13:22,067 --> 00:13:24,068
<i> ( Clattering )</i>

173
00:13:24,070 --> 00:13:26,171
<i> ¶¶</i>

174
00:13:38,818 --> 00:13:40,852
oh, yeah.

175
00:13:45,224 --> 00:13:46,357
( Growling )

176
00:13:53,098 --> 00:13:55,600
( Grunts )

177
00:13:55,602 --> 00:13:58,102
( Panting )

178
00:14:01,307 --> 00:14:03,875
<i>( Infected snarling )</i>

179
00:14:28,133 --> 00:14:30,201
( Snarling )

180
00:14:34,406 --> 00:14:38,076
( Balls clattering )

181
00:14:48,387 --> 00:14:51,256
( Growling )

182
00:15:14,346 --> 00:15:17,148
<i> - ( Door opens )</i>
<i> - ( Bell jingles )</i>

183
00:15:17,150 --> 00:15:19,117
( Snarling )

184
00:15:20,920 --> 00:15:22,387
( Panting )

185
00:15:35,968 --> 00:15:37,969
<i> ( Bell jingles )</i>

186
00:15:40,272 --> 00:15:42,006
( Grunting )

187
00:15:43,275 --> 00:15:44,509
<i> - ( Pickax thuds )</i>
<i> - ( Grunting )</i>

188
00:15:49,581 --> 00:15:51,649
( Grunts )

189
00:16:23,582 --> 00:16:26,250
( Grunts )

190
00:16:26,252 --> 00:16:29,120
<i> ¶¶</i>

191
00:16:46,772 --> 00:16:49,040
( panting )

192
00:16:51,744 --> 00:16:54,012
<i> ( Distant chatter )</i>

193
00:16:59,218 --> 00:17:00,785
( Nick whispers )
Mom?

194
00:17:02,654 --> 00:17:04,522
¶ Hey, mom? ¶

195
00:17:08,560 --> 00:17:10,762
- ofelia, Jesus.
- It's okay.

196
00:17:10,764 --> 00:17:12,563
She's just asleep.

197
00:17:13,632 --> 00:17:16,768
I saw Walker.
He told me.

198
00:17:18,504 --> 00:17:22,774
- Where's your sister?
- Well, we found her.

199
00:17:22,776 --> 00:17:26,310
She was okay.
She was happy, actually.

200
00:17:26,312 --> 00:17:30,581
Um, look, I tried
to get her to come back,

201
00:17:30,583 --> 00:17:33,284
but she knows
where to find us.

202
00:17:34,553 --> 00:17:37,789
-<i> I'm sorry.</i>
- Okay.

203
00:17:39,058 --> 00:17:40,491
Okay.

204
00:17:41,427 --> 00:17:43,127
Where's Troy?

205
00:17:43,129 --> 00:17:46,097
He's at the bar,
freaked out by
all the Mexicans.

206
00:17:46,099 --> 00:17:50,435
Oh, well, that's good,
'cause we--

207
00:17:50,437 --> 00:17:53,337
the more people we have,
then the safer...

208
00:18:01,747 --> 00:18:04,515
( Crying )
Oh, god.

209
00:18:06,718 --> 00:18:08,386
<i> ( Sniffles )</i>

210
00:18:10,789 --> 00:18:13,791
( Sighs )

211
00:18:13,793 --> 00:18:15,460
God.

212
00:18:16,662 --> 00:18:18,429
It's okay.

213
00:18:20,265 --> 00:18:22,633
- It's okay.
- ( Crying continues )

214
00:18:26,538 --> 00:18:28,206
( Sniffles )

215
00:18:33,679 --> 00:18:36,214
Thank you
for being here.

216
00:18:37,716 --> 00:18:39,784
I don't know what
i would do without you.

217
00:18:42,387 --> 00:18:45,123
Come on, you need to get
some fresh air.

218
00:18:45,125 --> 00:18:47,558
- I'll look after ofelia.
- Okay.

219
00:18:47,560 --> 00:18:50,862
- It's all right.
- You take this.

220
00:18:59,705 --> 00:19:02,874
- ( Sighs )
-<i> ( Door opens )</i>

221
00:19:04,843 --> 00:19:06,511
<i> ( Door closes )</i>

222
00:19:13,252 --> 00:19:15,820
( Exhales )

223
00:19:18,657 --> 00:19:21,392
<i> ¶¶</i>

224
00:19:52,624 --> 00:19:54,692
<i> ( birds chirping )</i>

225
00:20:02,234 --> 00:20:05,303
<i> ¶¶</i>

226
00:20:52,584 --> 00:20:53,751
( grunts )

227
00:20:56,955 --> 00:20:58,589
Bitch,
you touch my shit,

228
00:20:58,591 --> 00:21:01,392
<i> I will crack you open</i>
<i> like a coconut.</i>

229
00:21:05,464 --> 00:21:07,598
Those are my potatoes.

230
00:21:07,600 --> 00:21:10,368
Why the hell
are your potatoes
in my car?

231
00:21:10,370 --> 00:21:13,037
You took them
from the restaurant.

232
00:21:14,940 --> 00:21:17,041
Got a limited supply
and I'm not about
to let them go.

233
00:21:17,043 --> 00:21:18,943
Give them back
and I'll be on my way.

234
00:21:20,879 --> 00:21:23,347
Sounds like a conflict
of interest to me.

235
00:21:41,400 --> 00:21:44,368
And after
you shoot my tank,

236
00:21:44,370 --> 00:21:45,936
then what
are you gonna do?

237
00:21:45,938 --> 00:21:49,006
Why you gotta make trouble
when there isn't any?

238
00:21:57,449 --> 00:21:58,916
We can share them.

239
00:22:01,820 --> 00:22:05,856
I'm tired and hungry,
and I'd really prefer
not to kill you.

240
00:22:05,858 --> 00:22:08,526
-<i> ( Sizzling )</i>
- ( Chatter )

241
00:22:12,497 --> 00:22:15,099
( Clears throat )
What is this?

242
00:22:15,101 --> 00:22:17,568
( Speaking Spanish )

243
00:22:17,570 --> 00:22:19,437
No, i-- I know
what quesadilla is.

244
00:22:19,439 --> 00:22:21,505
I'm asking what's<i> sesos?</i>

245
00:22:21,507 --> 00:22:24,075
( Speaking Spanish )

246
00:22:25,677 --> 00:22:29,347
( Speaking Spanish )

247
00:22:29,349 --> 00:22:32,650
Oh, no, no.
I-- I don't drink.

248
00:22:32,652 --> 00:22:34,852
- Look, I don't drink.
- What do you mean
you don't drink?

249
00:22:34,854 --> 00:22:37,054
- I mean I don't drink.
- I got a head start,

250
00:22:37,056 --> 00:22:39,423
so you got
some catching up to do.

251
00:22:42,994 --> 00:22:44,929
- Really?
-<i> Amiga...</i>

252
00:22:44,931 --> 00:22:47,498
I don't-- I'm not
gonna need this.

253
00:22:49,768 --> 00:22:53,437
( Speaking Spanish )

254
00:22:54,539 --> 00:22:56,040
...uppers?

255
00:22:56,042 --> 00:22:59,009
You're looking
for<i> El matadero.</i>

256
00:22:59,011 --> 00:23:02,380
- The slaughterhouse.
-<i> ¿qué?</i>

257
00:23:02,382 --> 00:23:04,048
You have to ask
for<i> El matarife.</i>

258
00:23:04,050 --> 00:23:05,883
-<i> El matarife.</i>
- No, it's all right.

259
00:23:05,885 --> 00:23:07,885
- Okay,<i> gracias.</i>
- It's not a good idea, Nick.

260
00:23:07,887 --> 00:23:10,621
It's not a...

261
00:23:10,623 --> 00:23:13,858
Well, I saved your life.
You owe me.

262
00:23:13,860 --> 00:23:16,026
<i> ( Music playing )</i>

263
00:23:19,064 --> 00:23:22,466
Drink your milk.
( Inhales, exhales )

264
00:23:22,468 --> 00:23:25,603
Okay, okay.
Okay.

265
00:23:25,605 --> 00:23:27,671
Here you go.

266
00:23:27,673 --> 00:23:30,107
Here you go.

267
00:23:30,109 --> 00:23:33,110
<i> - ( Music playing )</i>
<i> - ( Chatter )</i>

268
00:23:33,112 --> 00:23:35,546
<i> ( Giggling, panting )</i>

269
00:23:36,481 --> 00:23:37,748
( Speaking Spanish )

270
00:23:38,917 --> 00:23:42,953
<i> ( Chatter continues )</i>

271
00:23:56,701 --> 00:23:59,036
( Speaking Spanish )

272
00:24:00,705 --> 00:24:04,508
<i> ( Woman speaking Spanish,</i>
<i> moaning )</i>

273
00:24:10,582 --> 00:24:13,918
( Grunting )

274
00:24:13,920 --> 00:24:17,655
This is about the time
you realize how deep
in the shit you are.

275
00:24:17,657 --> 00:24:21,892
And here I was,
thinking we were friends.

276
00:24:21,894 --> 00:24:23,461
I want to talk
to proctor John.

277
00:24:23,463 --> 00:24:25,596
( All laughing )

278
00:24:25,598 --> 00:24:28,966
Yeah, don't we all.

279
00:24:28,968 --> 00:24:33,804
No, I think it's better
if you stay here with us.

280
00:24:33,806 --> 00:24:35,906
I've got something
that he wants.

281
00:24:35,908 --> 00:24:38,676
<i> Something that</i>
<i> I can give him.</i>

282
00:24:40,846 --> 00:24:43,914
He's gonna want
to hear this.

283
00:24:43,916 --> 00:24:47,518
( Echoing )
Ofelia, honey.

284
00:24:47,520 --> 00:24:48,853
It's time.

285
00:24:48,855 --> 00:24:51,088
Madison's voice:
<i> Ofelia.</i>

286
00:24:51,090 --> 00:24:53,791
-<i> Ofelia, honey.</i>
- Walker: Come on, ofelia.

287
00:24:53,793 --> 00:24:55,759
Madison:
<i>It's time.</i>

288
00:25:00,999 --> 00:25:03,534
It's best if we go alone.

289
00:25:04,970 --> 00:25:06,237
I'm coming with you.

290
00:25:08,974 --> 00:25:11,709
I don't know
how he'll respond
to seeing you there.

291
00:25:15,947 --> 00:25:17,715
Please?

292
00:25:20,986 --> 00:25:23,687
It's okay, taqa.

293
00:25:25,624 --> 00:25:27,825
<i> ¶¶</i>

294
00:25:55,186 --> 00:25:59,723
it's okay, I got you.
It's okay.

295
00:25:59,725 --> 00:26:02,026
Over here.

296
00:26:02,028 --> 00:26:04,194
Easy.

297
00:26:04,196 --> 00:26:06,230
Oh, okay.

298
00:26:07,599 --> 00:26:09,934
( Shivering )
I'm freezing.

299
00:26:20,178 --> 00:26:22,212
<i> ¶¶</i>

300
00:26:22,214 --> 00:26:25,716
if I turn,
you put me down.

301
00:26:26,952 --> 00:26:29,587
I don't want him
to see me like that.

302
00:26:29,589 --> 00:26:32,122
I don't want him
to-- to do it, okay?

303
00:26:32,124 --> 00:26:34,792
- You do it.
- It's not gonna
come to that.

304
00:26:34,794 --> 00:26:36,226
He's gonna be here
any minute.

305
00:26:36,228 --> 00:26:40,798
Please, please,
please, do it?

306
00:26:41,766 --> 00:26:43,100
Okay.

307
00:26:47,272 --> 00:26:49,006
Tell him...

308
00:26:54,679 --> 00:26:59,116
Tell him I was
really looking forward
to getting to know him.

309
00:27:00,719 --> 00:27:05,356
Just tell him--
just-- just...

310
00:27:09,194 --> 00:27:11,228
You spend your whole life
lying to your kids

311
00:27:11,230 --> 00:27:13,897
because you think
you can protect them.

312
00:27:15,100 --> 00:27:19,036
Somehow, someway,
they know.

313
00:27:20,805 --> 00:27:24,174
They know who you are,
they know what you did.

314
00:27:25,243 --> 00:27:27,111
They know you.

315
00:27:29,247 --> 00:27:31,181
It's in the blood.

316
00:27:35,387 --> 00:27:38,288
You knew your father.

317
00:27:38,290 --> 00:27:41,291
Don't think
for one second
you didn't.

318
00:27:45,196 --> 00:27:47,431
Thank you.

319
00:27:47,433 --> 00:27:50,034
Thank you, Madison.

320
00:27:52,070 --> 00:27:54,271
( Sighs )

321
00:27:58,043 --> 00:28:00,944
<i>( Car approaching )</i>

322
00:28:06,217 --> 00:28:09,153
Okay, he's here.

323
00:28:09,155 --> 00:28:10,988
No, no. No!

324
00:28:10,990 --> 00:28:12,256
No!

325
00:28:14,259 --> 00:28:16,760
- Wake up, please.
-<i> ( Car doors open )</i>

326
00:28:16,762 --> 00:28:18,395
- Please, ofelia.
-<i> ( Car doors close )</i>

327
00:28:18,397 --> 00:28:20,230
Oh, god.

328
00:28:21,166 --> 00:28:22,833
Oh...

329
00:28:24,035 --> 00:28:25,836
Oh, god.

330
00:28:32,477 --> 00:28:35,479
- Daniel, wait, wait!
- Where's ofelia?

331
00:28:35,481 --> 00:28:36,914
Listen to me.

332
00:28:36,916 --> 00:28:38,782
Stop, please,
listen to me.

333
00:28:38,784 --> 00:28:41,185
Just listen
for a minute.
I gotta...

334
00:28:41,187 --> 00:28:43,787
Listen to me.
No.

335
00:28:45,757 --> 00:28:47,891
<i> ¶¶</i>

336
00:28:52,831 --> 00:28:54,264
ofelia?

337
00:28:57,202 --> 00:28:58,836
<i> Ofelia?</i>

338
00:29:01,973 --> 00:29:03,474
Ofelia?

339
00:29:06,444 --> 00:29:08,779
Ofelia?

340
00:29:30,435 --> 00:29:33,937
- What kind of sick joke
is this?!
- Daniel.

341
00:29:33,939 --> 00:29:35,539
What happened?

342
00:29:35,541 --> 00:29:40,377
- Answer me!
- Daniel, god, I'm so sorry.

343
00:29:40,379 --> 00:29:44,214
<i>I didn't--</i>
<i>just the ranch</i>
<i>was-- was overrun.</i>

344
00:29:45,250 --> 00:29:47,851
Ofelia, she saved--
she saved us,

345
00:29:47,853 --> 00:29:49,920
-<i> but she got bit.</i>
- ( Sobbing )

346
00:29:49,922 --> 00:29:51,955
<i>I'm so sorry.</i>

347
00:29:51,957 --> 00:29:55,058
I can't--
i can't even imagine.

348
00:29:56,828 --> 00:29:58,929
<i>I'm so sorry.</i>

349
00:29:58,931 --> 00:30:00,497
Please.

350
00:30:03,835 --> 00:30:08,105
( Speaking Spanish )

351
00:30:14,312 --> 00:30:17,281
- I'm so sorry.
- Get away from us...

352
00:30:17,283 --> 00:30:19,183
Before I kill you.

353
00:30:48,313 --> 00:30:49,580
( Gunshot )

354
00:30:49,582 --> 00:30:51,849
( Sighs )

355
00:30:57,422 --> 00:30:59,590
<i> ( Chatter echoing )</i>

356
00:31:32,590 --> 00:31:35,125
- Nick:<i> Perdóne.</i>
-<i> ( Woman cackling )</i>

357
00:31:38,396 --> 00:31:41,265
<i> ¿Dónde está</i>
<i> El matarife?</i>

358
00:31:41,267 --> 00:31:44,001
<i> Adentro.</i>

359
00:31:44,003 --> 00:31:45,469
<i> Gracias.</i>

360
00:31:45,471 --> 00:31:48,038
<i> ( Echoing voices</i>
<i> continue )</i>

361
00:31:48,040 --> 00:31:51,108
<i> "Anemone" playing</i>

362
00:31:59,384 --> 00:32:02,486
<i> ¶ I ¶</i>

363
00:32:02,488 --> 00:32:06,323
<i> ¶ I think</i>
<i> I know how I feel ¶</i>

364
00:32:10,428 --> 00:32:14,932
<i> ¶ 'cause I ¶</i>

365
00:32:14,934 --> 00:32:17,167
<i> ¶ I only play it</i>
<i> for real... ¶</i>

366
00:32:17,169 --> 00:32:19,002
<i> Buenas noches.</i>

367
00:32:21,973 --> 00:32:26,310
Welcome to<i> El matadero,</i>
my fellow Americans.

368
00:32:26,312 --> 00:32:29,146
How can I be
of assistance?

369
00:32:29,148 --> 00:32:33,650
Well, I was
told you're the man
with the things.

370
00:32:33,652 --> 00:32:36,253
You were told correct.

371
00:32:36,255 --> 00:32:37,721
What sort of things
are you looking for?

372
00:32:37,723 --> 00:32:41,525
- All the things.
- Hey, easy, easy.

373
00:32:41,527 --> 00:32:43,660
<i> ( Men laughing,</i>
<i> echoing )</i>

374
00:32:46,998 --> 00:32:52,135
We have<i> heroína,</i>
<i> morfina, cocaína,</i>

375
00:32:52,137 --> 00:32:54,237
-<i> anfetamina...</i>
- Uh...

376
00:32:54,239 --> 00:32:55,973
<i> ...codeína.</i>

377
00:32:55,975 --> 00:32:57,541
Can't go down,
we need to go up.

378
00:32:58,576 --> 00:33:02,145
Do you really want up?

379
00:33:15,059 --> 00:33:17,461
Let's just get what you need
and let's get out of here.

380
00:33:22,767 --> 00:33:24,501
Okay.

381
00:33:24,503 --> 00:33:28,171
<i> ( Cackling echoes )</i>

382
00:33:31,542 --> 00:33:36,146
This is locus coeruleus.

383
00:33:36,148 --> 00:33:37,414
What the hell is that?

384
00:33:37,416 --> 00:33:42,519
It's the nucleus
from the brain stem.

385
00:33:42,521 --> 00:33:45,655
Pure adrenaline.

386
00:33:45,657 --> 00:33:47,724
As up as up gets.

387
00:33:47,726 --> 00:33:50,394
Enough to wake the dead.

388
00:33:50,396 --> 00:33:53,530
How much does
a hunk of brain stem
cost these days?

389
00:33:53,532 --> 00:33:58,035
For you, my friend,
it's free.

390
00:33:59,570 --> 00:34:01,371
<i> Nada es gratis.</i>

391
00:34:01,373 --> 00:34:03,173
Whose glands are these?

392
00:34:04,108 --> 00:34:06,076
Shall we say pigs?

393
00:34:06,078 --> 00:34:07,511
( Laughs )

394
00:34:07,513 --> 00:34:10,714
Okay, come on,
let's go, let's go.

395
00:34:13,451 --> 00:34:15,385
Let's eat pigs.

396
00:34:18,756 --> 00:34:22,159
<i> ( Laughter and chatter</i>
<i> echoing )</i>

397
00:34:25,396 --> 00:34:27,197
-<i> ( Glass thuds )</i>
- Mm.

398
00:34:27,199 --> 00:34:30,367
<i> ¶ I wanna know</i>
<i> how it feels ¶</i>

399
00:34:35,206 --> 00:34:37,307
<i> ¶ 'cause I ¶</i>

400
00:34:38,709 --> 00:34:42,712
<i> ¶ I only play it</i>
<i> for real... ¶</i>

401
00:34:45,183 --> 00:34:48,485
<i> ( laughter echoing )</i>

402
00:34:51,122 --> 00:34:56,226
<i> ¶ Instead you're</i>
<i> dragging me down... ¶</i>

403
00:34:56,228 --> 00:34:59,229
( laughs )

404
00:34:59,231 --> 00:35:01,565
Alicia:
What do you do
with the teeth?

405
00:35:03,768 --> 00:35:05,836
I saw you
with the clippers.

406
00:35:13,111 --> 00:35:16,646
They buy gold in bulk
at the post.

407
00:35:16,648 --> 00:35:19,649
Some of the Mexicans
will sell the teeth

408
00:35:19,651 --> 00:35:22,452
and the fingers
for charms.

409
00:35:22,454 --> 00:35:25,222
Protection from the biters,
that sort of shit.

410
00:35:26,357 --> 00:35:27,824
Here.

411
00:35:29,393 --> 00:35:31,528
Thanks.

412
00:35:31,530 --> 00:35:34,397
You look like
you could use
some protection.

413
00:35:38,903 --> 00:35:42,606
That is disgusting,
no offense.

414
00:35:45,543 --> 00:35:47,410
None taken.

415
00:35:50,148 --> 00:35:53,517
Where you headed
if you're too good
for good luck?

416
00:35:54,719 --> 00:35:57,888
I got a tip on a place
in the low desert.

417
00:36:00,892 --> 00:36:04,895
You're gonna be passing
ghost town after ghost town.

418
00:36:04,897 --> 00:36:07,931
All picked clean.

419
00:36:07,933 --> 00:36:10,567
These potatoes
are a gift.

420
00:36:16,440 --> 00:36:18,275
I'll find a way.

421
00:36:18,277 --> 00:36:20,911
Will you, now?

422
00:36:20,913 --> 00:36:22,279
( Scoffs )

423
00:36:25,449 --> 00:36:29,419
I either find a way
to do this alone

424
00:36:29,421 --> 00:36:30,921
or I die.

425
00:36:33,758 --> 00:36:38,395
And that right there
is why I don't keep company.

426
00:36:38,397 --> 00:36:40,697
When we're done here,
I'm gonna take my things
and go.

427
00:36:40,699 --> 00:36:43,567
- No offense.
- None taken.

428
00:36:45,636 --> 00:36:48,672
The problem
ain't the killing,

429
00:36:48,674 --> 00:36:50,707
it's the friends.

430
00:36:53,678 --> 00:36:55,879
This is the life.

431
00:36:57,315 --> 00:36:59,549
You ain't gonna find
a way out.

432
00:36:59,551 --> 00:37:03,320
Not north, not south,
not anywhere else.

433
00:37:04,889 --> 00:37:08,525
You grow accustomed
to the killing,

434
00:37:08,527 --> 00:37:10,694
you'll get by.

435
00:37:12,396 --> 00:37:14,598
Who gets used to this?

436
00:37:18,736 --> 00:37:21,738
If a woman sets
her mind to it,

437
00:37:21,740 --> 00:37:24,574
she can get used
to anything.

438
00:37:27,712 --> 00:37:29,546
<i> ( People chattering )</i>

439
00:37:29,548 --> 00:37:31,348
<i> ¶¶</i>

440
00:37:31,350 --> 00:37:33,883
( panting )

441
00:37:37,722 --> 00:37:40,857
- ( Grunts )
- Nick!

442
00:37:41,892 --> 00:37:44,327
Wait up!

443
00:37:44,329 --> 00:37:46,563
( Grunts )
Nick, Nick,
you're gonna--

444
00:37:46,565 --> 00:37:48,365
- ( echoing ) Hey.
- ( Echoing ) What?

445
00:37:48,367 --> 00:37:50,433
I'm gonna have a--
i having a heart attack.

446
00:37:50,435 --> 00:37:52,302
No, you don't
fight it, man.

447
00:37:52,304 --> 00:37:53,737
<i> It makes it worse.</i>
<i> Just ride with it.</i>

448
00:37:53,739 --> 00:37:55,405
What do you mean,
"ride with it"?

449
00:37:55,407 --> 00:37:57,774
This is just like
another Tuesday
for you, huh?

450
00:37:57,776 --> 00:37:59,409
You know,
the sad thing, Troy,

451
00:37:59,411 --> 00:38:01,911
is you will never
feel freer or more yourself

452
00:38:01,913 --> 00:38:03,780
than when it's
5:00 in the morning

453
00:38:03,782 --> 00:38:07,450
and the sun's about to rise
and you're out of your mind.

454
00:38:07,452 --> 00:38:10,420
- ( Laughs ) Oh, god.
- Everything else is just lunch.

455
00:38:10,422 --> 00:38:12,889
- Yo, Nick? Nick?
- And people's disappointment.

456
00:38:12,891 --> 00:38:14,524
- ( Snarling )
- Troy:<i> Nick, wait up,</i>
<i> wait up.</i>

457
00:38:14,526 --> 00:38:17,727
Hey, n-Nick, Nick, Nick.
Wait, wait, wait.

458
00:38:17,729 --> 00:38:20,764
- ( Infected snarling )
- You wanna try something?

459
00:38:20,766 --> 00:38:22,766
You wanna try something?
Hey, you wanna try something?

460
00:38:22,768 --> 00:38:25,035
- Try something?
- ( Snarling )

461
00:38:27,838 --> 00:38:30,807
Whoa, Nick!
Nick!

462
00:38:30,809 --> 00:38:32,375
<i>Nick, what are</i>
<i>you doing?</i>

463
00:38:34,879 --> 00:38:37,580
Wipe it on thick,
as much as you can.

464
00:38:38,883 --> 00:38:40,617
<i> You gotta walk quiet.</i>

465
00:38:40,619 --> 00:38:42,719
Stare at the space
between atoms.

466
00:38:45,556 --> 00:38:48,091
Troy:
Nick? Nick!

467
00:38:54,098 --> 00:38:57,033
( Snarling )

468
00:38:57,035 --> 00:38:59,569
( Whispering )
Nick! Nick!

469
00:38:59,571 --> 00:39:01,771
<i> ¶¶</i>

470
00:39:36,006 --> 00:39:37,574
I can't go back.

471
00:39:39,410 --> 00:39:42,779
- I can't go back with her.
- Shh, okay. Shh.

472
00:39:42,781 --> 00:39:46,683
- Shh, shh.
- I can't go back
with her.

473
00:39:46,685 --> 00:39:49,886
( Shushing )

474
00:39:49,888 --> 00:39:51,988
<i> ¶¶</i>

475
00:40:03,000 --> 00:40:05,902
<i> ( distant chatter )</i>

476
00:40:14,145 --> 00:40:18,615
<i> ( Door opens, closes )</i>

477
00:40:33,531 --> 00:40:35,198
( Sighs )

478
00:40:36,767 --> 00:40:40,670
I've been
walking all night
with my daughter.

479
00:40:40,672 --> 00:40:43,072
I carried her out
to a-- an olive tree

480
00:40:43,074 --> 00:40:46,042
and buried her
with my hands.

481
00:40:50,748 --> 00:40:52,816
She saved Alicia's life.

482
00:40:54,852 --> 00:40:57,854
Alicia, where is she?

483
00:41:00,024 --> 00:41:03,693
She left.
On her own now.

484
00:41:07,465 --> 00:41:10,934
Had enough
of all this,
i suppose.

485
00:41:10,936 --> 00:41:12,936
Can you blame her?

486
00:41:20,544 --> 00:41:22,946
You were with ofelia
when she died?

487
00:41:25,850 --> 00:41:27,750
Did she say anything?

488
00:41:33,958 --> 00:41:36,559
She said to tell you
that she loved you.

489
00:41:51,709 --> 00:41:53,576
She said she was
looking forward

490
00:41:53,578 --> 00:41:55,278
to getting to know you.

491
00:41:58,215 --> 00:42:01,751
I told her
she already did.

492
00:42:01,753 --> 00:42:03,520
- That you were--
- she never did know the man

493
00:42:03,522 --> 00:42:06,055
that brought her
to the United States.

494
00:42:06,057 --> 00:42:10,527
She saw him once
for a moment,

495
00:42:10,529 --> 00:42:12,195
and it frightened her.

496
00:42:14,131 --> 00:42:15,965
( Sighs )

497
00:42:19,036 --> 00:42:20,904
( Exhales )

498
00:42:23,040 --> 00:42:27,210
Well, get your things ready,
Madison Clark.

499
00:42:27,212 --> 00:42:30,113
This is no place for you.

500
00:42:32,249 --> 00:42:33,983
I'd like to bring
the others.

501
00:42:36,253 --> 00:42:37,987
If that's all right.

502
00:42:47,064 --> 00:42:49,132
<i> ¶¶</i>

503
00:43:04,148 --> 00:43:05,848
( sighs )

504
00:43:08,218 --> 00:43:09,953
( Bucket clatters )

505
00:43:11,322 --> 00:43:13,590
<i> ( Birds chirping )</i>

506
00:43:13,592 --> 00:43:15,158
Alicia:
<i> Thought you'd</i>
<i> already gone.</i>

507
00:43:16,327 --> 00:43:17,961
Was about to.

508
00:43:17,963 --> 00:43:19,596
Wouldn't have found me
if this pile of junk

509
00:43:19,598 --> 00:43:21,264
were worth a damn.

510
00:43:24,134 --> 00:43:25,969
Where you headed?

511
00:43:25,971 --> 00:43:27,937
( Sighs )

512
00:43:31,208 --> 00:43:33,977
Guess some more killing.

513
00:43:33,979 --> 00:43:35,979
Those teeth ain't
gonna pull themselves.

514
00:43:35,981 --> 00:43:39,282
- Need help?
-<i> ( Infected growling )</i>

515
00:43:39,284 --> 00:43:41,351
I thought we said
no friends.

516
00:43:41,353 --> 00:43:43,686
Just the killing, then?

517
00:43:47,224 --> 00:43:49,726
Ever think about
selling your hair?

518
00:43:50,861 --> 00:43:52,729
Don't even think
about it.

519
00:43:52,731 --> 00:43:54,797
<i> ¶¶</i>

520
00:43:59,069 --> 00:44:01,204
<i> ( distant chatter</i>
<i> over P.A. )</i>

521
00:44:02,740 --> 00:44:04,741
<i> ¶¶</i>

522
00:44:16,320 --> 00:44:17,687
heading back to the dam.

523
00:44:19,857 --> 00:44:21,724
Uh, Troy and I,
we were thinking

524
00:44:21,726 --> 00:44:23,993
of maybe staying behind
a couple of days.

525
00:44:25,362 --> 00:44:27,163
It's a good place
to have a toehold

526
00:44:27,165 --> 00:44:29,365
for when you come back
and need to trade.

527
00:44:33,904 --> 00:44:36,205
Well, good for me, then.

528
00:44:40,310 --> 00:44:41,978
Yeah.

529
00:44:41,980 --> 00:44:44,213
We'll secure
more provisions
for the dam

530
00:44:44,215 --> 00:44:45,882
when you need it.

531
00:44:49,920 --> 00:44:51,954
You do what you need to.

532
00:44:53,824 --> 00:44:55,792
You know
where to find us.

533
00:44:57,361 --> 00:45:00,096
<i> - ( Engine starts )</i>
<i> - ( Doors close )</i>

534
00:45:03,367 --> 00:45:05,835
<i> ( Engine starts )</i>

535
00:45:07,905 --> 00:45:09,372
You ready?

536
00:45:09,374 --> 00:45:12,108
Yeah. You?

537
00:45:12,110 --> 00:45:14,110
Always.

