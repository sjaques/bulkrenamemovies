1
00:00:18,554 --> 00:00:21,453
Ik weet dat dit het laatste is
wat je wil horen, maar...

2
00:00:23,267 --> 00:00:26,270
we begeven ons in een...
-Ik weet waarin we ons begeven.

3
00:00:29,273 --> 00:00:30,733
Nou...

4
00:00:30,900 --> 00:00:32,276
dat is een pak van m'n hart.

5
00:00:32,443 --> 00:00:35,988
Ge�nfecteerden zijn niet te
voorspellen. Het kwam niet door ons.

6
00:00:36,155 --> 00:00:39,492
We geven alles af en hopen
dat de uitnodiging nog steeds geldt.

7
00:00:41,994 --> 00:00:43,079
En als hij weigert?

8
00:00:46,707 --> 00:00:47,792
We hebben Ofelia.

9
00:01:06,018 --> 00:01:06,936
Ofelia?

10
00:01:10,481 --> 00:01:12,441
H�, stop even.

11
00:01:13,818 --> 00:01:15,736
H�, stop.

12
00:01:23,411 --> 00:01:25,621
Jezus. Gaat het?
-Ja, niks aan de hand.

13
00:01:26,247 --> 00:01:28,666
Kom, we gaan.
Ik verloor gewoon m'n grip.

14
00:01:29,500 --> 00:01:31,127
Heb je je hoofd gestoten?

15
00:01:31,293 --> 00:01:32,878
Niks aan de hand. Kom nou maar.

16
00:01:34,547 --> 00:01:35,339
Madison...

17
00:01:36,757 --> 00:01:37,758
laten we nou gaan.

18
00:01:39,260 --> 00:01:40,761
Alsjeblieft, laten we gaan.

19
00:01:41,721 --> 00:01:43,264
Ok�?
-Het komt goed, Ofelia.

20
00:01:44,348 --> 00:01:46,183
Ik wil heel even met je praten, ok�?

21
00:01:57,695 --> 00:01:59,280
Breng me naar m'n vader.

22
00:02:00,865 --> 00:02:01,866
Hoor je me?

23
00:02:03,743 --> 00:02:05,202
Beloof het me, Madison.

24
00:02:07,830 --> 00:02:08,831
Dat zal ik doen.

25
00:02:09,874 --> 00:02:10,875
Ik beloof het.

26
00:03:31,288 --> 00:03:33,082
Stap in en maak dat je wegkomt...

27
00:03:33,249 --> 00:03:34,417
of ik schiet je dood.

28
00:03:38,921 --> 00:03:40,798
Nick?
-Ik zag je auto.

29
00:03:45,219 --> 00:03:47,013
Alicia, dit is gestoord.

30
00:03:48,014 --> 00:03:50,599
Ik weet wat gestoord is.
Dit is best fijn.

31
00:03:52,393 --> 00:03:53,936
Maar dit is smerig.

32
00:03:54,103 --> 00:03:56,230
Je hebt geen idee
hoe het is hierbuiten.

33
00:03:56,397 --> 00:03:59,650
Jij redt je wel, zo te zien.
-In een groep ben je veilig.

34
00:03:59,817 --> 00:04:01,527
Dat weet je best.
-Nee.

35
00:04:01,694 --> 00:04:04,739
Ik ben bijna doodgegaan,
omringd door mensen.

36
00:04:04,905 --> 00:04:06,615
Is dit jouw mening of die van mam?

37
00:04:07,908 --> 00:04:09,618
Overleven wordt m'n dood.

38
00:04:13,622 --> 00:04:15,958
Ze wordt gek
als ik terugkom zonder jou.

39
00:04:18,919 --> 00:04:20,379
Ik weet wat je wil.

40
00:04:22,214 --> 00:04:25,343
Je wil het al sinds je op de ranch
kwam. Je wil het goedmaken.

41
00:04:28,721 --> 00:04:29,889
Dat hoeft niet.

42
00:04:32,600 --> 00:04:33,601
Ik vergeef je.

43
00:04:37,188 --> 00:04:38,773
Je bent me niks verschuldigd.

44
00:04:39,815 --> 00:04:40,816
En mam ook niet.

45
00:04:41,692 --> 00:04:43,402
Ook al voelt het wel zo.

46
00:04:45,321 --> 00:04:47,698
Je verdient het
om iets uit je leven te halen.

47
00:05:08,511 --> 00:05:09,512
Voorzichtig.

48
00:05:10,262 --> 00:05:11,681
Trek dat shirt maar uit.

49
00:05:12,181 --> 00:05:13,182
Madison.

50
00:05:14,600 --> 00:05:17,770
Als ze denken dat Ofelia ziek is,
doden ze haar direct.

51
00:05:17,937 --> 00:05:19,814
Salazar blijft nog uren weg.

52
00:05:19,981 --> 00:05:23,234
Ze heeft een bed en pijnstillers
nodig. Weet jij iets beters?

53
00:05:24,485 --> 00:05:26,320
Lachen. Je leven hangt ervan af.

54
00:05:34,870 --> 00:05:36,288
Geef je wapens af.

55
00:05:46,173 --> 00:05:49,093
Geen bon, geen wapens.
-Ik weet hoe het werkt.

56
00:05:49,260 --> 00:05:50,344
We komen ze ruilen.

57
00:05:53,931 --> 00:05:56,309
Zes M4's, vijf mensen.

58
00:05:56,934 --> 00:05:59,979
Dat is dan 500 credits,
min de toegang.

59
00:06:02,398 --> 00:06:04,483
Verdomme, Johnny.

60
00:06:06,319 --> 00:06:08,571
Je zou de wisselkoers aanpassen.

61
00:06:22,376 --> 00:06:24,545
Dat is dan 330 credits...

62
00:06:24,712 --> 00:06:26,172
H�, dat is 200 minder.

63
00:06:26,339 --> 00:06:28,716
Sorry, dat was de oude koers.
-Prima.

64
00:06:45,191 --> 00:06:46,025
Veel plezier.

65
00:07:00,414 --> 00:07:01,666
Ze heeft hoge koorts.

66
00:07:01,832 --> 00:07:04,126
Blijf bij haar.
Ik zoek iets tegen de pijn.

67
00:07:07,046 --> 00:07:07,838
Taqa.

68
00:07:10,174 --> 00:07:11,133
Wakker blijven.

69
00:07:17,556 --> 00:07:19,600
H�. Dit is verspilde moeite.

70
00:07:20,184 --> 00:07:22,186
Pardon?
-Ok�, het is tragisch.

71
00:07:22,353 --> 00:07:25,022
Je hebt de helft
van onze wapens weggegeven...

72
00:07:25,189 --> 00:07:28,192
voor 'n afscheidskus voor Ofelia.
Denk nou na, Madison.

73
00:07:28,359 --> 00:07:30,403
Is dit je keuze?
-We hebben geen keuze.

74
00:07:30,569 --> 00:07:32,905
We hebben geen wapens
en geen thuis meer.

75
00:07:33,072 --> 00:07:35,366
We zijn nu afhankelijk van vreemden.

76
00:07:35,533 --> 00:07:38,661
Ik heb Ofelia beloofd
om haar naar haar vader te brengen.

77
00:07:38,828 --> 00:07:40,663
Nobel.
-Er is geen alternatief.

78
00:07:40,830 --> 00:07:41,789
Ik moet dit doen.

79
00:07:41,956 --> 00:07:43,916
Onze goede engelen zijn dood...

80
00:07:44,083 --> 00:07:45,084
en Ofelia ook.

81
00:07:45,251 --> 00:07:48,295
Je woord is waardeloos,
of ze nou nog ��n of twee uur leeft.

82
00:07:48,462 --> 00:07:51,340
Alleen middelen zijn
nog iets waard...

83
00:07:51,507 --> 00:07:53,509
en je bent ze aan het verspillen.

84
00:07:54,260 --> 00:07:57,179
Je moet geen liefdadigheid verwachten
van een slager.

85
00:07:58,764 --> 00:07:59,890
Ben je bang voor hem?

86
00:08:00,725 --> 00:08:01,726
Is dat het?

87
00:08:03,769 --> 00:08:05,980
Best, dan zien we wel
wat Daniel doet...

88
00:08:06,147 --> 00:08:08,190
als je nutteloos water wil kopen...

89
00:08:08,357 --> 00:08:10,818
met een dode dochter en lege handen.

90
00:08:25,206 --> 00:08:26,207
Laatste kans.

91
00:08:28,835 --> 00:08:29,836
Alsjeblieft.

92
00:08:30,003 --> 00:08:31,046
Dat is het niet.

93
00:08:31,212 --> 00:08:35,008
Ik weet waar jullie zijn. Als het
misgaat, kom ik naar jullie toe.

94
00:08:36,760 --> 00:08:38,928
Ik heb liever dat je niet doodgaat.

95
00:08:39,095 --> 00:08:40,096
Ik ga niet dood.

96
00:08:48,730 --> 00:08:49,731
Dat beloof ik.

97
00:09:28,311 --> 00:09:30,105
Waarom haat iedereen jou?

98
00:09:31,356 --> 00:09:34,067
Zo te horen
was je vroeger heel populair.

99
00:09:37,862 --> 00:09:40,240
Dat was toen.
En het gaat je niks aan.

100
00:09:42,117 --> 00:09:45,537
En Alicia heeft het mis. Het is niet
de reden dat ik ben gebleven.

101
00:09:45,704 --> 00:09:47,414
Nee? Wat dan wel?

102
00:09:48,164 --> 00:09:49,249
Travis was dood.

103
00:09:51,126 --> 00:09:52,752
En Luciana was ziek.

104
00:09:53,420 --> 00:09:55,588
Maar m'n moeder trok het niet meer.

105
00:09:56,756 --> 00:09:58,216
Dus ik moest kalm blijven.

106
00:09:58,383 --> 00:10:00,218
Wat een gelul.

107
00:10:02,762 --> 00:10:04,973
Waarom ben ik dan gebleven
volgens jou?

108
00:10:05,140 --> 00:10:07,267
Omdat je van me houdt.

109
00:10:08,476 --> 00:10:09,477
Het geeft niet.

110
00:10:13,481 --> 00:10:15,692
We lijken meer op elkaar
dan je denkt.

111
00:10:16,860 --> 00:10:18,194
Zwarte schapen.

112
00:10:18,361 --> 00:10:19,738
Gewelddadige kinderen.

113
00:10:21,239 --> 00:10:22,991
Alleen wou jij dat je dat niet was.

114
00:10:34,044 --> 00:10:36,338
Dit is tegen de pijn en de koorts.

115
00:10:36,838 --> 00:10:38,882
En elektrolyten tegen de uitdroging.

116
00:10:56,316 --> 00:10:57,651
Ze verdient dit niet.

117
00:10:58,610 --> 00:10:59,903
En Daniel ook niet.

118
00:11:03,823 --> 00:11:06,284
Ik wil haar gewoon
vredig laten sterven.

119
00:11:08,286 --> 00:11:11,665
Misschien gaat Daniel ons
inderdaad allemaal vermoorden.

120
00:11:16,586 --> 00:11:18,672
Gisteren ben ik alles kwijtgeraakt.

121
00:11:20,632 --> 00:11:21,633
M'n thuis.

122
00:11:22,133 --> 00:11:23,134
M'n vrienden.

123
00:11:24,719 --> 00:11:28,807
De botten van m'n voorouders,
zinloos verspreid in een veld.

124
00:11:30,725 --> 00:11:32,811
Ik ben opgegroeid met verlies.

125
00:11:32,978 --> 00:11:34,479
Ik dacht dat ik er goed in was.

126
00:11:36,231 --> 00:11:37,941
Maar dit is te veel.

127
00:11:42,904 --> 00:11:45,281
Ofelia wilde haar vader
nog ��n keer zien.

128
00:11:47,033 --> 00:11:48,326
Jij vervult die wens.

129
00:11:49,869 --> 00:11:51,037
Meer kun je niet doen.

130
00:11:53,581 --> 00:11:55,375
Zo kan hij z'n dochter begraven.

131
00:11:58,753 --> 00:12:01,172
Ook al wordt het onze dood.

132
00:12:07,137 --> 00:12:08,221
Dank je, Taqa.

133
00:17:09,064 --> 00:17:11,858
Ofelia. Jezus.
-Rustig maar. Ze slaapt.

134
00:17:13,818 --> 00:17:15,487
Walker heeft het me verteld.

135
00:17:19,157 --> 00:17:20,241
Waar is je zus?

136
00:17:20,742 --> 00:17:22,369
We hebben haar gevonden.

137
00:17:23,078 --> 00:17:25,372
Het gaat goed met haar.
Ze is gelukkig.

138
00:17:27,999 --> 00:17:30,585
Ik heb haar gevraagd
om terug te komen...

139
00:17:30,752 --> 00:17:33,254
maar ze weet waar we zijn.

140
00:17:34,839 --> 00:17:35,840
Het spijt me.

141
00:17:42,055 --> 00:17:43,056
Waar is Troy?

142
00:17:43,723 --> 00:17:46,017
Aan de bar, bang voor alle Mexicanen.

143
00:17:46,184 --> 00:17:47,811
Dat is mooi.

144
00:17:50,730 --> 00:17:53,233
Hoe meer mensen we hebben,
hoe veiliger...

145
00:18:16,923 --> 00:18:17,924
Stil maar.

146
00:18:20,468 --> 00:18:21,469
Stil maar.

147
00:18:33,940 --> 00:18:35,483
Bedankt dat je er bent.

148
00:18:38,069 --> 00:18:40,530
Ik weet niet
wat ik zonder jou zou moeten.

149
00:18:42,532 --> 00:18:44,492
Kom, je hebt frisse lucht nodig.

150
00:18:45,285 --> 00:18:46,536
Ik let wel op Ofelia.

151
00:18:47,120 --> 00:18:48,288
Ok�.
-Kom maar.

152
00:18:49,414 --> 00:18:50,415
Hier.

153
00:20:57,249 --> 00:20:58,584
Afblijven, trut.

154
00:20:58,751 --> 00:21:00,836
Ik splijt je open als een kokosnoot.

155
00:21:05,549 --> 00:21:07,009
Dat zijn mijn aardappels.

156
00:21:08,135 --> 00:21:10,262
Wat doen jouw aardappels
in mijn auto?

157
00:21:11,180 --> 00:21:13,891
Je hebt ze meegenomen
uit het restaurant.

158
00:21:15,100 --> 00:21:16,894
De voorraad is beperkt.

159
00:21:17,061 --> 00:21:18,520
Geef ze terug, dan ga ik weg.

160
00:21:20,898 --> 00:21:23,233
Dat klinkt
als conflicterende belangen.

161
00:21:41,502 --> 00:21:44,296
En als je op m'n tank
hebt geschoten...

162
00:21:44,463 --> 00:21:45,589
wat ga je dan doen?

163
00:21:46,757 --> 00:21:48,550
Waarom zouden we ruziemaken?

164
00:21:57,434 --> 00:21:58,560
We kunnen ze delen.

165
00:22:02,398 --> 00:22:05,275
Ik ben moe en ik heb honger
en ik dood je liever niet.

166
00:22:13,701 --> 00:22:14,702
Wat is dit?

167
00:22:17,538 --> 00:22:19,248
Ik weet wat een quesadilla is.

168
00:22:19,415 --> 00:22:21,417
Wat is 'sesos'?

169
00:22:29,383 --> 00:22:30,926
Nee, ik drink niet.

170
00:22:32,928 --> 00:22:34,847
Nick, ik drink niet.
-Hoe bedoel je?

171
00:22:35,222 --> 00:22:36,223
Dat ik niet drink.

172
00:22:36,390 --> 00:22:38,767
Ik ben al begonnen,
dus je moet even inhalen.

173
00:22:43,230 --> 00:22:44,315
Serieus?

174
00:22:45,065 --> 00:22:47,026
Nee, dat heb ik niet nodig.

175
00:22:49,945 --> 00:22:51,238
Waar vind ik...

176
00:22:54,283 --> 00:22:55,075
pepmiddelen?

177
00:22:56,285 --> 00:22:57,911
Zoek je El Matadero?

178
00:22:59,163 --> 00:23:00,122
Het Slachthuis?

179
00:23:02,416 --> 00:23:04,209
Vraag naar El Matarife.

180
00:23:04,376 --> 00:23:05,878
El Matarife.
-Hoeft niet.

181
00:23:06,045 --> 00:23:08,005
Gracias.
-Dat is geen goed idee.

182
00:23:08,172 --> 00:23:09,131
Het is geen...

183
00:23:10,758 --> 00:23:13,719
Ik heb je leven gered.
Je staat bij me in het krijt.

184
00:23:19,308 --> 00:23:20,392
Drink je melk op.

185
00:23:25,773 --> 00:23:26,774
Daar gaat ie.

186
00:23:28,067 --> 00:23:29,068
Daar gaat ie.

187
00:24:14,029 --> 00:24:17,574
Het is tijd dat je beseft
hoe diep je in de stront zit.

188
00:24:18,325 --> 00:24:20,452
En ik maar denken
dat we vrienden waren.

189
00:24:21,996 --> 00:24:23,872
Ik wil opzichter John spreken.

190
00:24:25,833 --> 00:24:27,376
Ja, wie niet?

191
00:24:30,754 --> 00:24:33,632
Je kunt beter bij ons blijven.

192
00:24:33,799 --> 00:24:35,467
Ik heb iets dat hij wil hebben.

193
00:24:36,051 --> 00:24:37,803
Iets dat ik hem kan geven.

194
00:24:41,098 --> 00:24:42,641
Dit wil hij horen.

195
00:24:44,810 --> 00:24:46,520
Ofelia, lieverd.

196
00:24:47,688 --> 00:24:48,939
Het is tijd.

197
00:24:51,317 --> 00:24:52,860
Ofelia, lieverd.

198
00:24:53,027 --> 00:24:54,778
Kom, Ofelia.
-Het is tijd.

199
00:25:01,118 --> 00:25:02,619
We kunnen beter alleen gaan.

200
00:25:05,247 --> 00:25:06,290
Ik ga mee.

201
00:25:09,126 --> 00:25:11,253
Ik weet niet
hoe hij op jou zal reageren.

202
00:25:16,175 --> 00:25:17,176
Alsjeblieft.

203
00:25:21,180 --> 00:25:22,264
Laat ons maar.

204
00:25:55,381 --> 00:25:56,882
Ik heb je.

205
00:25:57,716 --> 00:25:58,717
Kom maar.

206
00:26:00,052 --> 00:26:01,053
Hier zo.

207
00:26:02,304 --> 00:26:03,305
Rustig aan.

208
00:26:07,893 --> 00:26:08,894
Ik heb het ijskoud.

209
00:26:22,491 --> 00:26:25,035
Als ik verander, moet je me doden.

210
00:26:27,037 --> 00:26:28,706
Ik wil niet dat hij me zo ziet.

211
00:26:29,498 --> 00:26:31,542
Ik wil niet dat hij het moet doen.

212
00:26:32,418 --> 00:26:34,670
Jij moet het doen.
-Zo ver komt het niet.

213
00:26:34,837 --> 00:26:37,006
Hij kan elk moment komen.
-Alsjeblieft.

214
00:26:37,506 --> 00:26:38,590
Alsjeblieft.

215
00:26:38,757 --> 00:26:40,718
Alsjeblieft. Doe het.

216
00:26:47,558 --> 00:26:48,559
Zeg maar...

217
00:26:54,732 --> 00:26:58,444
Zeg maar dat ik er echt naar uitkeek
om hem te leren kennen.

218
00:27:01,113 --> 00:27:02,114
Zeg maar...

219
00:27:02,698 --> 00:27:04,450
Zeg...

220
00:27:09,121 --> 00:27:11,248
Je liegt constant
tegen je kinderen...

221
00:27:11,415 --> 00:27:13,667
omdat je denkt
dat je ze kunt beschermen.

222
00:27:15,294 --> 00:27:16,587
Maar toch...

223
00:27:17,921 --> 00:27:19,006
weten ze het.

224
00:27:20,883 --> 00:27:23,385
Ze weten wie je bent
en wat je hebt gedaan.

225
00:27:25,471 --> 00:27:26,472
Ze kennen je.

226
00:27:29,475 --> 00:27:30,559
Het zit in hun bloed.

227
00:27:35,689 --> 00:27:36,899
Je kende je vader.

228
00:27:38,567 --> 00:27:40,444
Geloof dat maar.

229
00:27:45,449 --> 00:27:46,450
Dank je.

230
00:28:07,805 --> 00:28:08,847
Daar is hij.

231
00:28:14,520 --> 00:28:15,604
Word nou wakker.

232
00:28:16,730 --> 00:28:17,731
Alsjeblieft.

233
00:28:32,663 --> 00:28:34,873
Daniel, wacht.
-Waar is Ofelia?

234
00:28:35,833 --> 00:28:38,252
Luister naar me. Alsjeblieft.

235
00:28:38,961 --> 00:28:40,129
Luister heel even.

236
00:28:41,672 --> 00:28:43,048
Luister naar me. Nee.

237
00:29:30,679 --> 00:29:32,348
Wat is dit voor zieke grap?

238
00:29:34,016 --> 00:29:35,017
Wat is er gebeurd?

239
00:29:35,768 --> 00:29:36,852
Zeg op.

240
00:29:37,019 --> 00:29:39,813
Daniel, nee. Het spijt me ontzettend.

241
00:29:40,606 --> 00:29:42,816
Ik heb niet... De ranch werd...

242
00:29:42,983 --> 00:29:44,193
onder de voet gelopen.

243
00:29:45,402 --> 00:29:48,489
Ofelia heeft ons gered,
maar ze is gebeten.

244
00:29:48,656 --> 00:29:51,033
Het spijt me ontzettend.

245
00:29:51,992 --> 00:29:54,495
Ik kan het me niet eens voorstellen.

246
00:29:56,955 --> 00:29:58,040
Het spijt me zo.

247
00:29:58,957 --> 00:29:59,917
Alsjeblieft.

248
00:30:06,382 --> 00:30:07,549
Dit is de prijs.

249
00:30:14,390 --> 00:30:16,767
Het spijt me echt.
-Ga weg.

250
00:30:17,393 --> 00:30:18,519
Voor ik je vermoord.

251
00:32:22,076 --> 00:32:25,370
Welkom bij El Matadero,
beste mede-Amerikanen.

252
00:32:26,538 --> 00:32:28,290
Waarmee kan ik jullie helpen?

253
00:32:29,208 --> 00:32:31,502
Ik hoorde
dat jij de man met de dingen bent.

254
00:32:34,004 --> 00:32:35,297
Dat heb je goed gehoord.

255
00:32:36,507 --> 00:32:38,967
Wat voor dingen zoek je?
-Alle dingen.

256
00:32:40,010 --> 00:32:41,595
Rustig aan.

257
00:32:47,101 --> 00:32:49,561
We hebben hero�ne...

258
00:32:49,728 --> 00:32:51,313
morfine, coca�ne...

259
00:32:52,231 --> 00:32:53,232
amfetamine...

260
00:32:54,399 --> 00:32:55,442
code�ne.

261
00:32:55,609 --> 00:32:57,611
Nee, we willen een oppepper.

262
00:32:58,862 --> 00:33:01,615
Wil je echt een oppepper?

263
00:33:15,129 --> 00:33:17,506
Koop wat je nodig hebt,
dan smeren we hem.

264
00:33:31,854 --> 00:33:35,732
Dit is locus caeruleus.

265
00:33:36,233 --> 00:33:37,443
Wat is dat in godsnaam?

266
00:33:37,943 --> 00:33:41,905
Een hoofdkern in de hersenstam.

267
00:33:42,823 --> 00:33:44,867
Pure adrenaline.

268
00:33:45,868 --> 00:33:47,828
Een sterkere oppepper is er niet.

269
00:33:48,287 --> 00:33:50,414
Genoeg om de doden
tot leven te wekken.

270
00:33:50,873 --> 00:33:54,168
Hoeveel kost een stuk hersenstam
tegenwoordig?

271
00:33:54,334 --> 00:33:55,836
Voor jou, beste vriend...

272
00:33:56,962 --> 00:33:57,963
is het gratis.

273
00:34:01,759 --> 00:34:03,135
Van wie zijn deze klieren?

274
00:34:04,261 --> 00:34:06,096
Zullen we 'varkens' zeggen?

275
00:34:07,681 --> 00:34:10,225
Ok�. Kom, we gaan.

276
00:34:13,687 --> 00:34:15,105
Laten we 'varkens' zeggen.

277
00:35:00,067 --> 00:35:01,318
Wat doe je met de tanden?

278
00:35:04,113 --> 00:35:05,447
Ik zag je tang.

279
00:35:13,288 --> 00:35:15,332
Op de post kopen ze goud in bulk.

280
00:35:16,959 --> 00:35:21,672
Sommige Mexicanen verkopen de tanden
en de vingers voor talismans.

281
00:35:22,673 --> 00:35:24,758
Als bescherming tegen de bijters.

282
00:35:27,261 --> 00:35:28,095
Hier.

283
00:35:31,849 --> 00:35:34,393
Zo te zien kun je wel
wat bescherming gebruiken.

284
00:35:39,231 --> 00:35:40,357
Dit is smerig.

285
00:35:41,692 --> 00:35:42,651
Sorry.

286
00:35:45,779 --> 00:35:46,780
Geeft niks.

287
00:35:50,325 --> 00:35:52,828
Waar ga je naartoe
als je geen geluk nodig hebt?

288
00:35:54,747 --> 00:35:57,249
Ik ben getipt
over een plek in de lage woestijn.

289
00:35:58,333 --> 00:35:59,334
Water, wild.

290
00:36:01,086 --> 00:36:03,964
Dan kom je
door de ene spookstad na de andere.

291
00:36:05,215 --> 00:36:06,592
Er is niks meer te halen.

292
00:36:08,302 --> 00:36:10,220
Deze aardappels zijn een geschenk.

293
00:36:16,477 --> 00:36:17,770
Ik vind wel een manier.

294
00:36:18,604 --> 00:36:19,688
Is dat zo?

295
00:36:25,819 --> 00:36:27,613
Of ik doe dit in m'n eentje...

296
00:36:29,615 --> 00:36:30,616
of ik ga dood.

297
00:36:33,994 --> 00:36:36,830
En daarom hoef ik dus
geen gezelschap.

298
00:36:38,540 --> 00:36:41,960
Als we klaar zijn, pak ik m'n spullen
en vertrek ik. Sorry.

299
00:36:42,127 --> 00:36:43,128
Geeft niks.

300
00:36:45,964 --> 00:36:47,758
Het doden is niet het probleem.

301
00:36:48,842 --> 00:36:50,636
De vrienden zijn het probleem.

302
00:36:53,847 --> 00:36:55,307
Dit is het leven.

303
00:36:57,434 --> 00:36:58,811
Je vindt geen uitweg.

304
00:36:59,686 --> 00:37:02,689
Niet in het noorden,
niet in het zuiden, nergens.

305
00:37:05,150 --> 00:37:07,111
Het doden went.

306
00:37:08,862 --> 00:37:09,863
Je redt je wel.

307
00:37:12,699 --> 00:37:14,076
Wie went hier nou aan?

308
00:37:18,997 --> 00:37:21,041
Als een vrouw echt wil...

309
00:37:22,000 --> 00:37:23,752
kan ze overal aan wennen.

310
00:37:42,104 --> 00:37:43,063
Wacht.

311
00:37:45,774 --> 00:37:47,317
Nick, je krijgt...

312
00:37:48,485 --> 00:37:50,404
Ik krijg een hartaanval.

313
00:37:50,571 --> 00:37:53,782
Verzet je nou niet.
Dat maakt het erger. Ga erin mee.

314
00:37:53,949 --> 00:37:55,367
Hoe bedoel je, ga erin mee?

315
00:37:55,534 --> 00:37:57,953
Dit is hartstikke normaal
voor jou, h�?

316
00:37:58,120 --> 00:37:59,413
Het trieste is...

317
00:37:59,580 --> 00:38:01,999
dat je je nooit vrijer
of meer jezelf voelt...

318
00:38:02,166 --> 00:38:05,127
dan wanneer het
vijf uur 's ochtends is...

319
00:38:05,294 --> 00:38:06,712
en je hartstikke high bent.

320
00:38:09,631 --> 00:38:11,508
De rest is bijzaak.

321
00:38:11,675 --> 00:38:14,136
En de teleurstelling van anderen.
-Wacht.

322
00:38:15,888 --> 00:38:17,681
Nick, wacht.

323
00:38:19,892 --> 00:38:23,020
Wil je iets uitproberen?

324
00:38:31,278 --> 00:38:32,321
Nick, wat...

325
00:38:35,074 --> 00:38:36,867
Smeer het zo dik mogelijk op je.

326
00:38:39,078 --> 00:38:40,621
Je moet stilletjes lopen.

327
00:38:40,788 --> 00:38:42,873
Staar naar de ruimte
tussen de atomen.

328
00:39:36,343 --> 00:39:37,803
Ik kan niet teruggaan.

329
00:39:39,471 --> 00:39:42,099
Ik kan niet teruggaan met haar.

330
00:39:44,935 --> 00:39:46,645
Ik kan niet teruggaan met haar.

331
00:40:37,029 --> 00:40:39,448
Ik heb de hele nacht
met m'n dochter gelopen.

332
00:40:40,657 --> 00:40:43,035
Ik heb haar
naar een olijfboom gedragen...

333
00:40:43,202 --> 00:40:45,579
en haar met m'n eigen handen
begraven.

334
00:40:51,001 --> 00:40:52,461
Ze heeft Alicia gered.

335
00:40:55,089 --> 00:40:57,216
Alicia. Waar is ze?

336
00:41:00,260 --> 00:41:01,261
Ze is vertrokken.

337
00:41:02,554 --> 00:41:03,680
Ze loopt alleen rond.

338
00:41:07,601 --> 00:41:09,520
Ze had er vast genoeg van.

339
00:41:11,230 --> 00:41:12,314
Dat snap ik wel.

340
00:41:20,531 --> 00:41:22,282
Jij was bij Ofelia toen ze stierf.

341
00:41:25,953 --> 00:41:27,287
Heeft ze nog iets gezegd?

342
00:41:34,253 --> 00:41:35,838
Dat ze van je hield.

343
00:41:51,895 --> 00:41:54,815
Dat ze ernaar uitkeek
om je te leren kennen.

344
00:41:58,569 --> 00:42:00,070
Ik zei dat ze je al kende.

345
00:42:02,364 --> 00:42:05,284
Ze kende de man niet
die haar naar de VS heeft gebracht.

346
00:42:06,285 --> 00:42:08,829
Ze heeft hem ��n keer gezien.

347
00:42:08,996 --> 00:42:11,707
Heel eventjes maar.
En hij maakte haar bang.

348
00:42:23,343 --> 00:42:26,472
Pak je spullen, Madison Clark.

349
00:42:27,514 --> 00:42:28,849
Dit is geen plek voor jou.

350
00:42:32,561 --> 00:42:34,813
Ik wil de anderen graag
hierheen halen.

351
00:42:36,482 --> 00:42:37,566
Als het mag.

352
00:43:13,727 --> 00:43:15,229
Ik dacht dat je al weg was.

353
00:43:16,563 --> 00:43:17,564
Ik wilde net gaan.

354
00:43:18,232 --> 00:43:20,901
Maar dit onding heeft het begeven.

355
00:43:24,571 --> 00:43:25,572
Waar ga je naartoe?

356
00:43:31,453 --> 00:43:32,621
Ik ga nog wat doden.

357
00:43:34,248 --> 00:43:35,999
Iemand moet die tanden trekken.

358
00:43:37,042 --> 00:43:38,043
Heb je hulp nodig?

359
00:43:40,087 --> 00:43:42,089
Geen vrienden, hadden we toch gezegd?

360
00:43:42,256 --> 00:43:43,340
Alleen doden dan?

361
00:43:47,344 --> 00:43:49,430
Ooit overwogen
om je haar te verkopen?

362
00:43:51,056 --> 00:43:52,182
Waag het niet.

363
00:44:16,457 --> 00:44:17,624
Ik ga terug naar de dam.

364
00:44:20,294 --> 00:44:24,006
Troy en ik willen misschien
een paar dagen blijven.

365
00:44:25,591 --> 00:44:29,261
Het is handig om voet aan de grond
te hebben als je straks wil handelen.

366
00:44:34,057 --> 00:44:35,225
Nou, dat is fijn.

367
00:44:42,065 --> 00:44:45,069
We zorgen wel voor wat voorraad
voor bij de dam.

368
00:44:50,115 --> 00:44:51,492
Doe maar wat je moet doen.

369
00:44:54,036 --> 00:44:55,496
Jullie weten ons te vinden.

370
00:45:08,092 --> 00:45:09,093
Ben je klaar?

371
00:45:09,635 --> 00:45:10,803
Ja. Jij?

372
00:45:12,346 --> 00:45:13,570
Altijd.

