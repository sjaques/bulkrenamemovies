1
00:01:14,065 --> 00:01:19,097
Bedankt dat je vannacht de wacht hield.
- Ik doe het graag.

2
00:01:21,297 --> 00:01:24,331
Jij mag vanavond de wacht houden.

3
00:01:28,637 --> 00:01:32,238
Gaan we vandaag winnen?
- Zeker wel.

4
00:01:33,277 --> 00:01:37,641
We hebben batterijen voor de walkie nodig.
Dan werkt het weer.

5
00:02:13,398 --> 00:02:16,514
Waarom lach je?

6
00:02:16,516 --> 00:02:20,083
Zijn een paar dagen weg
en we hebben alleen maar...

7
00:02:20,085 --> 00:02:24,087
twee wapens, ingedeukte blikken bonen
en een paar rugbyshirts.

8
00:02:24,089 --> 00:02:26,389
Ja.

9
00:02:27,135 --> 00:02:29,401
We gaan de strijd aan.

10
00:02:30,186 --> 00:02:31,861
Het is beter.

11
00:02:43,107 --> 00:02:48,110
Als ik nog ��n woord over Fat Joey hoor,
sla ik iemands tanden eruit.

12
00:02:48,112 --> 00:02:51,547
Gelukkig geen extra lading meer.
Geen mascotte-onzin meer.

13
00:03:00,290 --> 00:03:03,824
Ze houden wel van pretzels.
- En batterijen.

14
00:03:07,830 --> 00:03:11,331
Twee dagen, we moeten teruggaan.

15
00:03:14,970 --> 00:03:17,504
Rick...

16
00:03:17,506 --> 00:03:20,940
Nog anderhalve dag.
Vandaag en morgen.

17
00:03:23,279 --> 00:03:26,445
We kunnen er later weer naartoe gaan.
We moeten nu terug.

18
00:03:26,447 --> 00:03:31,016
Nog iets meer.
- Het gaat goed.

19
00:03:31,018 --> 00:03:33,678
We hoeven ze nu niet te vinden.

20
00:03:34,773 --> 00:03:38,491
Ik weet het.
Nog iets meer, goed?

21
00:03:44,331 --> 00:03:46,364
Goed.

22
00:04:32,148 --> 00:04:38,477
Quality over Quantity (QoQ) Releases
The Walking Dead S07E12: Say Yes

23
00:04:49,199 --> 00:04:53,701
Ik kwam net langs om dat te doen.
Denise leerde het mij.

24
00:04:55,571 --> 00:04:57,205
Ben al klaar.

25
00:04:58,275 --> 00:04:59,981
Wat?

26
00:05:00,443 --> 00:05:02,877
Niets.

27
00:05:06,082 --> 00:05:08,416
Het zal werken.

28
00:05:08,818 --> 00:05:11,084
Wat betekent dat eigenlijk, Tara?

29
00:05:11,086 --> 00:05:14,421
Dat we nu de aantallen hebben.
We hebben alleen wapens nodig.

30
00:05:14,423 --> 00:05:17,157
Weet je waar we vandaag
zoveel wapens kunnen vinden?

31
00:05:17,159 --> 00:05:20,638
Deze week, dit jaar?

32
00:05:21,163 --> 00:05:23,873
Nee, dat weet je niet.

33
00:05:24,765 --> 00:05:27,150
Ik kan niet wachten.

34
00:05:28,536 --> 00:05:31,733
Ik weet niet hoe het met jou zit,
maar ik kan het niet.

35
00:05:31,940 --> 00:05:36,008
We zullen de wapens vinden.
En we zullen vechten.

36
00:05:36,010 --> 00:05:40,397
Het zal niet makkelijk zijn.
Dat was het ook niet bij de buitenpost.

37
00:05:42,716 --> 00:05:45,750
Misschien kun je dit voor hen bewaren.

38
00:05:50,924 --> 00:05:55,316
Hier.
Het vermindert je litteken.

39
00:06:00,066 --> 00:06:02,132
Houd het maar.

40
00:06:02,134 --> 00:06:05,336
We hebben wapens nodig.
En ik zal ze vinden.

41
00:07:17,539 --> 00:07:20,581
Wat was het?
- Een hert.

42
00:07:20,582 --> 00:07:24,809
Ik ben je een hert verschuldigd.

43
00:07:25,212 --> 00:07:28,594
Van eerder, toen ze buiten het hek stonden.

44
00:07:28,682 --> 00:07:30,565
Dat ben je inderdaad.

45
00:07:41,428 --> 00:07:43,142
Zie je dat?

46
00:07:46,132 --> 00:07:48,432
Ja.

47
00:08:16,927 --> 00:08:19,730
Voor zulke wapens zijn we hier.

48
00:08:47,824 --> 00:08:51,913
Er is hier iets goed misgegaan.

49
00:08:52,028 --> 00:08:53,803
Een lange tijd geleden.

50
00:08:53,804 --> 00:08:57,336
Deze zien er serieus uit.

51
00:08:57,366 --> 00:09:00,534
Misschien zijn hier ook serieuze wapens.

52
00:09:08,009 --> 00:09:11,384
Laten we het beter bekijken.
- Ja.

53
00:10:22,079 --> 00:10:23,833
Verdomme.

54
00:10:29,553 --> 00:10:31,246
Kijk uit waar je loopt.

55
00:10:31,854 --> 00:10:35,227
Misschien moeten we hier niet te lang blijven.

56
00:10:35,228 --> 00:10:38,224
Eens kijken wat we kunnen ontdekken.

57
00:10:55,144 --> 00:11:01,214
Soldaten, burgers, al die kogels.
Er was een gevecht.

58
00:11:01,216 --> 00:11:05,648
Misschien waren het de walkers.
Misschien andere mensen.

59
00:11:05,819 --> 00:11:08,744
Ze dragen hun wapens nog.

60
00:11:09,823 --> 00:11:11,843
Ik denk dat dit het is.

61
00:11:12,760 --> 00:11:14,914
Dat denk ik ook.

62
00:11:16,130 --> 00:11:17,929
Denk je dat het ons lukt?

63
00:11:49,995 --> 00:11:51,795
Ja, dat kunnen we.

64
00:12:21,726 --> 00:12:25,061
Gaat het?
- Ja en met jou?

65
00:12:25,063 --> 00:12:27,129
Ja.

66
00:12:27,131 --> 00:12:30,466
Dit is een teken, nietwaar?

67
00:12:33,170 --> 00:12:35,417
Dit is het, dat moet wel.

68
00:12:36,940 --> 00:12:40,576
Dat is het zeker.

69
00:12:45,081 --> 00:12:48,949
Ze zijn klaar.
Laten we gaan eten.

70
00:13:18,879 --> 00:13:21,166
Ik vond geen wapens.

71
00:13:21,882 --> 00:13:23,849
Mocht jij je het afvragen.

72
00:13:23,851 --> 00:13:27,720
Ik was de hele dag weg
en heb niets gevonden.

73
00:13:27,722 --> 00:13:30,122
Geen echte.

74
00:13:31,859 --> 00:13:34,444
Maar ik had een pistool.

75
00:13:35,395 --> 00:13:38,410
Ik ging het gebruiken om Negan te doden.

76
00:13:39,032 --> 00:13:41,566
En als ik het had gedaan,
zoals ik had gepland...

77
00:13:41,568 --> 00:13:43,802
zou Negan nu dood zijn.

78
00:13:43,804 --> 00:13:48,169
Misschien was ik dan nu dood,
maar wie kan dat iets schelen?

79
00:13:48,742 --> 00:13:52,248
Eugene zou hier nog steeds zijn.

80
00:13:52,249 --> 00:13:55,245
Olivia zou nog leven.

81
00:13:55,247 --> 00:13:58,048
Spencer zou nog leven.

82
00:13:58,050 --> 00:14:01,418
En nu zijn zij weg en ik ben hier...

83
00:14:01,420 --> 00:14:05,589
omdat ik stom genoeg was
om naar jou te luisteren.

84
00:14:05,591 --> 00:14:07,644
Maar je luisterde.

85
00:14:09,060 --> 00:14:10,824
En je deed het.

86
00:14:14,265 --> 00:14:18,100
Je staat daar en vertelt mensen
over hun levens.

87
00:14:20,238 --> 00:14:22,104
Alleen als ze hierheen komen.

88
00:14:24,075 --> 00:14:27,748
Net zoals jij nu doet.

89
00:14:28,044 --> 00:14:30,720
Je weet helemaal niets.

90
00:14:32,048 --> 00:14:36,385
Je hebt gelijk. Ik weet niets.

91
00:14:38,221 --> 00:14:41,657
Ik zei dat je niet hoorde te sterven...

92
00:14:42,259 --> 00:14:46,735
en dat je niet moest doen
wat je van plan was...

93
00:14:46,929 --> 00:14:51,545
omdat we je nodig hadden,
we hebben je nog steeds nodig.

94
00:14:51,801 --> 00:14:54,289
Zelfs een dwaas als ik kan dat zien.

95
00:14:54,637 --> 00:14:56,737
Het is makkelijker om dood te zijn...

96
00:14:56,739 --> 00:15:00,206
en als het mijn fout is dat je nog leeft,
moet ik daarmee leven.

97
00:15:01,139 --> 00:15:03,242
Ik besloot mij ermee te bemoeien.

98
00:15:03,244 --> 00:15:06,045
Maar ik dacht dat dat het juiste was...

99
00:15:06,047 --> 00:15:10,249
en ik wist dat er veel op het spel stond.

100
00:15:10,251 --> 00:15:13,787
Je kunt het mij kwalijk nemen dat je nog leeft...

101
00:15:13,789 --> 00:15:16,889
maar wat ga je er nu mee doen?

102
00:15:16,891 --> 00:15:21,313
Hoe laat je de dingen die moeten gebeuren,
gebeuren?

103
00:15:22,195 --> 00:15:26,498
Alles is mogelijk, zolang je hart nog klopt.

104
00:15:30,103 --> 00:15:32,985
Zeker meer dan schreeuwen tegen een dwaas.

105
00:16:06,538 --> 00:16:09,037
Wat?

106
00:16:15,947 --> 00:16:20,356
Hoe is het?
- Heerlijk.

107
00:16:25,355 --> 00:16:27,655
Dus morgen...

108
00:16:27,657 --> 00:16:30,505
gaan we naar buiten en halen de wapens.

109
00:16:31,294 --> 00:16:35,075
We laten Jadis en haar mensen
samen met ons vechten.

110
00:16:35,164 --> 00:16:38,232
We doden Negan en ieder ander om te winnen.

111
00:16:39,870 --> 00:16:42,151
Wat gebeurt er daarna?

112
00:16:43,973 --> 00:16:47,953
Daarna, gaan we door.
- Ja, maar...

113
00:16:49,277 --> 00:16:52,278
Negan heeft op zijn manier de leiding.

114
00:16:52,280 --> 00:16:55,883
Wij moeten het van hem overnemen
als hij dood is.

115
00:16:58,086 --> 00:17:01,086
Al die gemeenschappen...

116
00:17:01,088 --> 00:17:04,256
ze kunnen er samen uitkomen,
hoe ze alles moeten runnen.

117
00:17:04,258 --> 00:17:07,426
Maar iemand moet dat laten gebeuren.

118
00:17:07,428 --> 00:17:10,062
Er moet iemand zijn die hen zal begeleiden.

119
00:17:12,466 --> 00:17:14,261
Jij zou dat moeten zijn.

120
00:17:17,470 --> 00:17:20,563
Ik niet.
- Waarom niet?

121
00:17:20,908 --> 00:17:22,306
Je kunt het.

122
00:17:23,197 --> 00:17:25,309
Je bent er goed in.

123
00:17:25,311 --> 00:17:27,779
Als het iets is wat je wilde.

124
00:17:27,781 --> 00:17:31,358
Ik denk niet dat ik dat wil.

125
00:17:34,987 --> 00:17:39,302
Maar wij twee�n...

126
00:17:40,426 --> 00:17:43,176
om samen de leiding te nemen...

127
00:17:44,396 --> 00:17:48,298
dat wil ik wel.

128
00:17:48,300 --> 00:17:52,068
Als het iets is wat jij wilt.

129
00:17:54,606 --> 00:17:56,415
Ja.

130
00:18:16,159 --> 00:18:18,739
Ik eet nog vijf van deze maaltijden, goed?

131
00:18:20,430 --> 00:18:22,093
Weet je...

132
00:18:23,433 --> 00:18:26,368
het hoeft morgen niet klaar te zijn.

133
00:18:26,903 --> 00:18:29,610
We kunnen de tijd nemen.

134
00:18:29,872 --> 00:18:33,474
Als we er een paar dagen bijnemen,
is dat goed.

135
00:18:33,476 --> 00:18:36,870
Het is hier leeg.

136
00:18:37,113 --> 00:18:38,749
Het is goed afgesloten.

137
00:18:40,983 --> 00:18:44,075
We hebben de enige ingang gevonden, dus...

138
00:18:46,187 --> 00:18:49,179
We moeten teruggaan.

139
00:18:49,524 --> 00:18:54,581
Als we de wapens meenemen,
maakt het niet uit als het iets langer duurt.

140
00:18:54,863 --> 00:19:00,465
Wacht, ik wilde je dit laten zien.

141
00:19:00,467 --> 00:19:06,258
Het is chili, macaroni met kaas...

142
00:19:07,474 --> 00:19:09,375
ineen.

143
00:19:09,376 --> 00:19:10,575
Kom op.

144
00:20:12,053 --> 00:20:13,687
Sorry.

145
00:20:15,558 --> 00:20:18,157
Ik dacht even na.

146
00:20:21,763 --> 00:20:26,669
Wil je dit?
Gefeliciteerd.

147
00:20:27,035 --> 00:20:28,467
Voor jou.

148
00:20:28,469 --> 00:20:32,405
Iemand gaf het me,
iemand die me heeft geholpen.

149
00:20:32,407 --> 00:20:35,774
Ze riskeerde haar leven voor mij.

150
00:20:35,776 --> 00:20:40,311
Ze redde mijn leven.
Het hoefde niet, maar ze deed het.

151
00:20:40,313 --> 00:20:44,149
Maar dat betekent wat, dat moet wel.

152
00:20:46,119 --> 00:20:49,755
Als ik wat zeg...

153
00:20:49,757 --> 00:20:51,789
zal Rick daar niet weggaan zonder wapens.

154
00:20:51,791 --> 00:20:57,695
Als we dichtbij genoeg kunnen komen,
zullen ze schieten en dan schieten wij terug.

155
00:21:01,133 --> 00:21:03,734
Ze zouden samen met ons moeten vechten.

156
00:21:03,736 --> 00:21:09,940
De Verlossers hebben hun vaders,
zoons en broers gedood.

157
00:21:09,942 --> 00:21:13,409
Ze willen niet met ons vechten.
Ze verschuilen zich liever.

158
00:21:15,914 --> 00:21:21,651
Dus vechten ze gewoon met ons.

159
00:21:21,653 --> 00:21:25,621
Als ik iedereen over Oceanside vertel,
gaat dat gebeuren.

160
00:21:31,729 --> 00:21:34,663
Wat maakt ons leven meer waard,
als die van hen?

161
00:21:38,836 --> 00:21:43,471
Omdat wij de mensen willen tegenhouden
die ons pijn willen doen...

162
00:21:43,473 --> 00:21:46,173
die andere mensen pijn zullen doen?

163
00:21:57,120 --> 00:21:58,920
Verdomme.

164
00:22:33,387 --> 00:22:34,886
Kijk.

165
00:22:34,888 --> 00:22:38,023
De walkers in het gras,
ze komen niet dichterbij.

166
00:22:41,128 --> 00:22:46,330
We moeten degenen die hier zijn uitschakelen
en 't gat dichten, dan pakken we de rest langzaam.

167
00:22:49,402 --> 00:22:51,301
Zeven, acht...

168
00:22:52,872 --> 00:22:55,706
negen...

169
00:22:55,708 --> 00:22:59,009
Ik kan het blokkeren met die auto.

170
00:22:59,011 --> 00:23:02,011
Ik pak die onderweg.
En jij pakt de rest.

171
00:23:03,515 --> 00:23:05,248
Jij laat er voor mij acht achter?

172
00:23:08,119 --> 00:23:13,834
We kunnen ze neerschieten, maar dat trekt aandacht,
het moet stil gebeuren met het zwaard.

173
00:23:15,560 --> 00:23:18,090
Acht is een eitje voor je.

174
00:24:57,322 --> 00:25:00,323
Je hebt je acht walkers.
Ik kan duwen.

175
00:25:00,325 --> 00:25:01,825
Ik ook.

176
00:25:01,827 --> 00:25:04,161
Zorg jij maar dat je ons naar dat gat toe stuurt.

177
00:25:09,533 --> 00:25:10,900
De remmen doen het niet.

178
00:25:36,026 --> 00:25:39,027
Alles in orde, Michonne?

179
00:25:39,029 --> 00:25:40,628
Ja.

180
00:25:40,630 --> 00:25:41,995
En met jou?

181
00:25:43,232 --> 00:25:45,265
Ja.

182
00:25:45,267 --> 00:25:48,401
Volgens mij hebben we het verkeerd ingeschat.

183
00:25:48,403 --> 00:25:51,438
Denk je dat, of weet je dat?

184
00:25:53,943 --> 00:25:55,108
Ik weet het.

185
00:25:57,746 --> 00:25:59,712
Het was een goed plan.

186
00:25:59,714 --> 00:26:01,780
Het was een geweldig plan.

187
00:26:31,880 --> 00:26:33,980
We doen het hier.

188
00:26:39,654 --> 00:26:41,587
Het houdt het niet.

189
00:26:46,227 --> 00:26:48,527
Daar gaat hij.

190
00:27:29,134 --> 00:27:33,136
Verspreid ze in kleinere groepen.
De barri�re houdt ze misschien tegen.

191
00:27:33,138 --> 00:27:35,138
Neem jij de glijbaan,
dan neem ik het reuzenrad.

192
00:27:35,140 --> 00:27:37,947
Of we gaan gewoon.
- Wil je weg?

193
00:27:37,950 --> 00:27:40,742
Nah, we doen het gewoon.
- Dat weet ik wel zeker.

194
00:28:40,135 --> 00:28:41,601
Hoe gaat het daar?

195
00:28:41,603 --> 00:28:45,637
Nog acht. En daar?

196
00:28:48,875 --> 00:28:50,308
Tien.

197
00:31:44,140 --> 00:31:48,142
Ik heb het geprobeerd,
maar je hebt er nog ��n te goed van me.

198
00:33:47,192 --> 00:33:50,359
We hadden nog een paar dagen weg
kunnen blijven.

199
00:33:50,361 --> 00:33:53,296
Maar dat had ik niks gevonden.

200
00:34:02,940 --> 00:34:05,040
Ik heb nog niet geslapen.

201
00:34:09,309 --> 00:34:11,814
Ik dacht aan wat we hebben verloren.

202
00:34:15,419 --> 00:34:19,220
Ik dacht aan mijn vrienden.

203
00:34:19,222 --> 00:34:22,356
Glenn heeft me gered.

204
00:34:22,358 --> 00:34:24,525
In het begin.

205
00:34:24,527 --> 00:34:28,596
En ik kon hem niet redden.

206
00:34:28,598 --> 00:34:30,865
Het is normaal.

207
00:34:30,867 --> 00:34:33,433
Dat weet ik.

208
00:34:33,435 --> 00:34:36,884
Je eraan vastklampen.

209
00:34:37,628 --> 00:34:42,609
We zijn erdoorheen gekomen.
Hier wordt het niet beter van.

210
00:34:42,611 --> 00:34:44,677
Het spijt me, Rick.

211
00:34:51,714 --> 00:34:54,068
We zullen met ze vechten.

212
00:34:55,751 --> 00:34:58,158
Dat gaat er gebeuren.

213
00:35:00,222 --> 00:35:02,388
En we zullen mensen verliezen...

214
00:35:02,390 --> 00:35:04,625
heel veel misschien, misschien elkaar.

215
00:35:09,463 --> 00:35:13,772
Zelfs dan, is het 't nog waard.

216
00:35:15,069 --> 00:35:17,436
Toen ik dacht...

217
00:35:23,210 --> 00:35:25,605
Ik wil je niet verliezen.

218
00:35:27,071 --> 00:35:33,449
Jij vroeg me wat voor leven we hadden
als we ons zouden overgeven.

219
00:35:35,122 --> 00:35:39,791
Het was geen leven.

220
00:35:39,793 --> 00:35:41,767
Wat we daar gedaan hebben...

221
00:35:42,478 --> 00:35:45,072
wat we nu doen...

222
00:35:45,931 --> 00:35:50,000
een toekomst cre�ren voor Judith
en voor Glenn en Maggie's baby...

223
00:35:50,002 --> 00:35:55,573
het gevecht aangaan, dat is leven.
Dat heb je me laten zien.

224
00:35:56,475 --> 00:35:58,641
Je kunt me verliezen.

225
00:35:58,643 --> 00:36:00,609
Nee.
- Dat kun je wel.

226
00:36:00,611 --> 00:36:03,880
Ik kan jou verliezen.

227
00:36:03,882 --> 00:36:07,984
We kunnen onze vrienden verliezen, onze dierbaren.

228
00:36:08,987 --> 00:36:11,620
Het gaat niet meer om ons.

229
00:36:11,622 --> 00:36:14,169
Maar om de toekomst.

230
00:36:16,026 --> 00:36:22,130
En als ik het niet haal, moet jij het overnemen,
want dat kun je.

231
00:36:25,235 --> 00:36:26,864
Maar...

232
00:36:28,371 --> 00:36:31,372
hoe weet je dat zo zeker?

233
00:36:31,374 --> 00:36:33,654
Omdat...

234
00:36:34,077 --> 00:36:37,343
jij me hier naartoe leidde.

235
00:36:45,855 --> 00:36:48,589
Doen ze het allemaal?

236
00:36:48,591 --> 00:36:50,623
Zover ik weet wel.

237
00:36:52,194 --> 00:36:56,096
Het moet schoongemaakt worden.
We hebben spullen gevonden.

238
00:36:56,098 --> 00:36:58,031
Dat moeten wij doen?

239
00:36:58,033 --> 00:37:02,401
We hebben al wat schoongemaakt en geolied.
Jullie doen de rest, of we doen het samen.

240
00:37:02,403 --> 00:37:04,771
Ja, maar werken ze?

241
00:37:04,773 --> 00:37:07,472
Je kunt er een paar uitproberen als je wilt.

242
00:37:07,474 --> 00:37:09,775
Hoeveel?
- 63.

243
00:37:09,777 --> 00:37:11,243
We hebben een inventaris gemaakt.

244
00:37:11,245 --> 00:37:13,645
Nee.

245
00:37:13,647 --> 00:37:15,580
Bedoel je de inventaris?

246
00:37:17,017 --> 00:37:19,417
Het is niet genoeg.
- Waar heb je het over?

247
00:37:19,419 --> 00:37:21,686
Je vroeg veel wapens.
Hier zijn ze.

248
00:37:21,688 --> 00:37:24,956
Genoeg voor jouw gevecht.
Wij hebben het dubbele nodig.

249
00:37:24,958 --> 00:37:26,523
Ik wil twee keer zoveel.

250
00:37:26,525 --> 00:37:29,160
We hebben onze tijd genoeg verspild.
We nemen het mee terug.

251
00:37:29,162 --> 00:37:30,628
Nee.

252
00:37:30,630 --> 00:37:34,198
De wapens zijn van ons.
De deal gaat nog steeds door.

253
00:37:37,637 --> 00:37:39,870
Niet alles.

254
00:37:39,872 --> 00:37:42,739
We houden er tien voor onszelf,
om er meer te vinden.

255
00:37:42,741 --> 00:37:44,040
Vijf.

256
00:37:44,042 --> 00:37:45,408
Tien.
- Zes.

257
00:37:45,410 --> 00:37:47,144
Tien.
- Negen.

258
00:37:47,146 --> 00:37:48,912
En de kat wil ik terug.

259
00:37:53,085 --> 00:37:57,820
20. De kat hou ik.
We halen de wapens en vechten samen.

260
00:37:57,822 --> 00:38:00,455
Zeg dat je akkoord ga.

261
00:38:03,761 --> 00:38:05,627
Ja.

262
00:38:05,629 --> 00:38:07,629
We zullen snel vechten.

263
00:38:33,155 --> 00:38:35,422
Je hebt een paar extra dagen...

264
00:38:35,424 --> 00:38:38,658
voor wat er hierna komt.

265
00:38:42,497 --> 00:38:43,897
Een paar extra dagen?

266
00:38:43,899 --> 00:38:48,968
Klopt. We zullen meer vinden,
we komen er wel uit.

267
00:38:48,970 --> 00:38:51,337
Binnenkort.

268
00:38:51,339 --> 00:38:53,973
Over een paar dagen.

269
00:39:21,034 --> 00:39:23,133
Alles goed?

270
00:39:23,135 --> 00:39:28,005
Ja, ik was op zoek naar jou.
Wat is er aan de hand?

271
00:39:28,007 --> 00:39:29,773
Heb je Rosita gezien?

272
00:39:29,775 --> 00:39:32,895
Ze was er vanochtend niet
bij de wisseling van de wacht.

273
00:39:33,740 --> 00:39:36,362
Ze is vast op zoek naar meer.

274
00:39:41,686 --> 00:39:45,448
Waarom kwam je langs?

275
00:39:49,560 --> 00:39:52,080
Ik moet je wat vertellen.

276
00:40:16,652 --> 00:40:18,419
Hoi.

277
00:40:25,395 --> 00:40:26,928
Ben je hier...

278
00:40:26,930 --> 00:40:29,130
Ik ben hier omdat ik je hulp nodig heb.

279
00:40:38,907 --> 00:40:41,063
Op ��n voorwaarde:

280
00:40:42,911 --> 00:40:44,798
Ik schiet.

281
00:40:48,282 --> 00:40:50,215
Goed.

282
00:40:54,322 --> 00:40:57,572
Heb je explosieven meegenomen van de brug?
- Ik heb erover nagedacht.

283
00:40:57,709 --> 00:41:01,178
Ik wilde niet dat iemand
erachter kwam en me zou tegenhouden.

284
00:41:01,181 --> 00:41:02,907
Ik heb wat beters.

285
00:41:17,777 --> 00:41:19,810
Zodat je kunt schieten.

286
00:41:28,653 --> 00:41:30,887
Rick en Michonne hebben wapens gevonden.

287
00:41:35,194 --> 00:41:38,461
Maar ze zijn niet klaar om te gaan.
- Dat gaat niet.

288
00:41:38,463 --> 00:41:41,330
Ze hebben meer wapens, mensen
en meer tijd nodig...

289
00:41:41,332 --> 00:41:43,946
meer excuses.

290
00:41:44,668 --> 00:41:46,400
Ik heb alles onthouden.

291
00:41:46,403 --> 00:41:49,468
Daryl en Carl hebben Rick alles verteld over
wat er bij Negan gebeurd.

292
00:41:50,008 --> 00:41:53,909
Jezus heeft een kaart
voor me gemaakt van de buitenkant.

293
00:41:53,911 --> 00:41:56,077
Het is ver weg. Goed beveiligd.

294
00:41:56,079 --> 00:41:58,446
We zijn maar met z'n twee�n.
- Trek je je terug?

295
00:41:58,448 --> 00:42:00,548
Ik ben klaar om hem te vermoorden.

296
00:42:00,550 --> 00:42:03,084
Maar ik moet zeker weten
dat jij weet dat dit betekent.

297
00:42:03,086 --> 00:42:04,352
Weet jij dat?

298
00:42:06,723 --> 00:42:08,853
Ze mogen ons niet levend krijgen.

299
00:42:11,028 --> 00:42:13,260
Als ze dat doen, geven we ze iets.

300
00:42:13,262 --> 00:42:16,363
Een enkele reis voor ons beiden.

301
00:42:18,901 --> 00:42:20,635
Als het wij beiden zijn.

302
00:42:22,505 --> 00:42:23,970
Dat is het.

303
00:42:24,424 --> 00:42:27,939
Vertaling:
Quality over Quantity (QoQ) Releases

304
00:42:27,940 --> 00:42:31,440
Download deze ondertitel op:
- www.OpenSubtitles.org -

