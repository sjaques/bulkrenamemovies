1
00:00:00,027 --> 00:00:02,963
<i>Wat voorafging:</i>

2
00:00:02,965 --> 00:00:04,406
Ik wil alleen Negan.

3
00:00:05,133 --> 00:00:08,368
Met ��n kogel lukt 't niet.
Misschien dat je hem uitschakelt.

4
00:00:08,470 --> 00:00:10,136
Maak dan een kogel.

5
00:00:10,239 --> 00:00:12,539
Alles wat we hebben,
hebben we uit gevechten.

6
00:00:12,641 --> 00:00:15,709
Als we hun regels volgen,
hebben we een soort leven.

7
00:00:15,811 --> 00:00:19,045
Je leidde ons allemaal naar het beloofde land,
nietwaar Rick?

8
00:00:19,147 --> 00:00:20,814
Hier zijn we dan.

9
00:00:20,916 --> 00:00:23,350
Ik vind het hier leuk.

10
00:00:23,452 --> 00:00:25,318
De Verlossers kunnen best redelijk zijn.

11
00:01:31,640 --> 00:01:32,699
Maggie.

12
00:01:35,924 --> 00:01:42,262
Ik weet dat mensen aardige dingen
over jou en Sasha zeggen.

13
00:01:44,833 --> 00:01:47,934
Is dat zo?
- Ja.

14
00:01:47,936 --> 00:01:50,470
Over die ene avond...

15
00:01:50,472 --> 00:01:55,041
dat ze denken dat jullie deze plek redden.

16
00:01:55,043 --> 00:01:57,817
Laat het niet naar je hoofd stijgen.

17
00:01:58,547 --> 00:02:01,170
Laat het je niet dwarszitten.

18
00:02:02,618 --> 00:02:05,089
Daar lijkt het wel op.

19
00:02:15,030 --> 00:02:17,110
Wat doe je daarboven?

20
00:02:19,868 --> 00:02:21,201
Ga je dat opeten?

21
00:02:22,404 --> 00:02:23,803
Ja.

22
00:02:25,207 --> 00:02:28,642
Kerel, je weet dat ze zwanger is.

23
00:02:54,036 --> 00:02:57,837
Altijd tegen de aderen in.

24
00:02:57,839 --> 00:03:01,808
Altijd tegen de aderen in.

25
00:03:39,715 --> 00:03:41,448
Dat ruikt goed.

26
00:03:48,488 --> 00:03:49,488
Wil je proeven?

27
00:04:04,998 --> 00:04:07,665
Hier is zijn verdomde limonade.

28
00:04:07,667 --> 00:04:09,467
Ik had nog wat in mijn keuken.

29
00:04:11,871 --> 00:04:13,437
Denis vond het lekker.

30
00:04:18,378 --> 00:04:21,279
Dank je.

31
00:04:24,517 --> 00:04:28,185
Ga naar huis.
Laat mij het overnemen.

32
00:04:30,189 --> 00:04:33,458
Nee, het lukt wel.

33
00:04:34,861 --> 00:04:37,609
Ik zei tegen Rick dat ik op Judith zou letten.

34
00:04:38,898 --> 00:04:40,398
Ik ga dat doen.

35
00:04:45,972 --> 00:04:47,605
Je moet gaan.

36
00:04:49,309 --> 00:04:50,841
We gaan zitten.

37
00:05:19,305 --> 00:05:20,905
De tafel moet anders worden gedekt.

38
00:05:46,299 --> 00:05:51,769
Vandaag en alleen vandaag, h�?
- Ja.

39
00:05:55,608 --> 00:06:00,711
Al die kogelgaten,
het zal snel vol met water zitten.

40
00:06:05,651 --> 00:06:08,819
Misschien halen we het naar de kano.

41
00:06:08,821 --> 00:06:10,354
Misschien.

42
00:06:12,825 --> 00:06:14,325
Als je hier wilt blijven...

43
00:06:16,129 --> 00:06:18,162
Ik doe net alsof jij dat niet zei.

44
00:06:32,945 --> 00:06:36,848
Ik ga dit doen. Wij allebei.

45
00:07:02,942 --> 00:07:04,842
Ik wacht niet meer op je vader.

46
00:07:04,844 --> 00:07:06,917
Ik weet niet waar hij is...

47
00:07:08,514 --> 00:07:10,381
maar Lucille...

48
00:07:14,821 --> 00:07:16,020
heeft honger.

49
00:07:21,527 --> 00:07:24,895
Carl, geef de broodjes.

50
00:07:29,335 --> 00:07:30,434
Alsjeblieft.

51
00:08:05,296 --> 00:08:10,657
Quality over Quantity (QoQ) Releases
The Walking Dead S07E08: Hearts Still Beating

52
00:08:26,107 --> 00:08:28,808
Kijk dat eens.

53
00:08:28,810 --> 00:08:30,943
Zo makkelijk is het.

54
00:08:30,945 --> 00:08:34,313
Het is gewoon een transactie.

55
00:08:34,315 --> 00:08:37,383
Je ziet er slecht uit,
wat betekent dat je ervoor hebt gevochten.

56
00:08:37,385 --> 00:08:38,851
Je deed je taak.

57
00:08:40,622 --> 00:08:42,688
Je snapt het en dat vind ik leuk.

58
00:08:42,690 --> 00:08:44,690
Negan vindt dat leuk.

59
00:08:48,162 --> 00:08:50,129
Dat is goed om te horen.

60
00:08:51,833 --> 00:08:55,325
Als je het goed speelt,
laat ik je misschien zien waar we wonen.

61
00:08:56,337 --> 00:08:58,204
Misschien trakteer ik je dan op een broodje.

62
00:09:04,879 --> 00:09:06,488
Zeg maar wanneer.

63
00:09:08,549 --> 00:09:10,549
Kijk je graag, matje?

64
00:09:10,551 --> 00:09:12,551
Ja.

65
00:09:12,553 --> 00:09:14,186
Ik bedoel...

66
00:09:14,188 --> 00:09:17,223
Je moet hier nu weg.

67
00:09:17,225 --> 00:09:18,424
Nu.

68
00:09:55,330 --> 00:09:56,330
Morgan.

69
00:10:00,168 --> 00:10:02,335
Het is wat fruit uit de tuin van het Koninkrijk.

70
00:10:02,337 --> 00:10:05,371
Nectarines, appels.

71
00:10:05,373 --> 00:10:07,440
Dank je. Ik ben al voorzien.

72
00:10:07,442 --> 00:10:11,344
Ik weet dat je voor jezelf kan zorgen,
maar verse producten is niet iets...

73
00:10:11,346 --> 00:10:12,346
Echt?

74
00:10:13,948 --> 00:10:15,514
Ik ben al voorzien.

75
00:10:18,987 --> 00:10:20,786
Ezekiel?

76
00:10:20,788 --> 00:10:25,458
Blijkbaar geloven mensen mij niet
als ik zeg dat ik alleen wil zijn.

77
00:10:25,460 --> 00:10:28,861
Ik probeerde je alleen te laten
en dat zal ik ook doen.

78
00:10:28,863 --> 00:10:30,632
Maar jij riep mij hierheen.

79
00:10:32,166 --> 00:10:33,299
Waarom?

80
00:10:33,301 --> 00:10:35,841
Hoe gaat het met je?
- Goed.

81
00:10:37,105 --> 00:10:39,772
Goed, dan kun je nu gaan.

82
00:10:42,877 --> 00:10:46,612
Ik denk dat je soft wordt.
- Ik denk dat je weggaat.

83
00:10:55,957 --> 00:10:59,792
Hallo, Carol.
Sorry dat ik je lastig val.

84
00:10:59,794 --> 00:11:02,428
Morgan, ik verwachtte jou hier niet te zien.

85
00:11:02,430 --> 00:11:06,665
Het is goed dat je er bent.
Eigenlijk wilde ik met jullie allebei praten.

86
00:11:06,667 --> 00:11:08,234
Het is belangrijk.

87
00:11:13,007 --> 00:11:14,431
Bijna bij de kano.

88
00:11:18,479 --> 00:11:21,313
Het water vult de boot wel snel.
- Ik weet het.

89
00:11:31,099 --> 00:11:32,458
Rick, links.

90
00:12:03,491 --> 00:12:05,124
We zijn er bijna.

91
00:12:21,476 --> 00:12:23,476
Nee.

92
00:12:29,150 --> 00:12:33,255
Houd vol.

93
00:12:33,256 --> 00:12:37,123
Blijf in de kano.
Ik kan...

94
00:12:38,526 --> 00:12:40,626
Aaron.

95
00:12:58,045 --> 00:12:59,145
Ik ben in orde.

96
00:12:59,147 --> 00:13:01,013
Het gaat goed.

97
00:13:03,017 --> 00:13:06,018
Ik ben in orde.

98
00:13:37,318 --> 00:13:39,919
<i>Blijf niet tegen de muur aan staan.</i>

99
00:13:39,921 --> 00:13:41,086
<i>Kijk uit.</i>

100
00:13:43,624 --> 00:13:46,625
<i>Verdomme.</i>

101
00:13:49,797 --> 00:13:51,530
<i>Haal een mop en een ander vat.</i>

102
00:13:51,532 --> 00:13:53,432
<i>Houden we deze rotzooi?</i>

103
00:13:53,434 --> 00:13:55,801
<i>We hoeven het niet te eten.</i>

104
00:13:55,803 --> 00:13:57,603
<i>Hij haat augurken.</i>

105
00:14:06,981 --> 00:14:08,347
<i>Er zitten haken aan.</i>

106
00:14:08,349 --> 00:14:10,115
<i>Waarom heeft hij het niet vastgebonden?</i>

107
00:14:10,117 --> 00:14:12,651
<i>Zei het al twee keer.
Hij luistert gewoon niet.</i>

108
00:14:37,925 --> 00:14:42,046
<b>GEFELICITEERD MET JE OVERWINNING,
MAAR JE HEBT NOG STEEDS VERLOREN</b>

109
00:14:45,653 --> 00:14:48,220
Een slechte verliezer.

110
00:14:50,558 --> 00:14:53,025
Het enige wat hij tekort kwam
was munitie.

111
00:14:54,862 --> 00:14:56,562
We moeten dit nu terugbrengen.

112
00:14:59,033 --> 00:15:02,835
Hij heeft wel peddels.

113
00:15:02,837 --> 00:15:06,522
Nu er wind staat, drijven we zo naar de kust toe.

114
00:15:10,544 --> 00:15:12,611
<i>Breng deze rotzooi terug.</i>

115
00:15:12,613 --> 00:15:14,513
<i>Kijk dit keer uit waar je loopt.</i>

116
00:15:14,515 --> 00:15:17,650
<i>Laat hem met rust.</i>

117
00:15:33,567 --> 00:15:35,513
Eerder...

118
00:15:36,671 --> 00:15:39,713
bedoelde ik niet dat je het niet zou kunnen doen.

119
00:15:41,042 --> 00:15:43,035
Het is gewoon...

120
00:15:44,712 --> 00:15:47,713
om zo ver weg te gaan,
zoveel te riskeren...

121
00:15:47,715 --> 00:15:49,014
om dingen voor hen te halen...

122
00:15:49,016 --> 00:15:51,016
mensen zijn het er niet mee eens.

123
00:15:51,018 --> 00:15:53,618
Ik zou het je niet kwalijk nemen,
als je het niet zou doen.

124
00:15:58,059 --> 00:15:59,885
Ik was daar.

125
00:16:00,961 --> 00:16:02,889
Ik zag wat er op de weg gebeurde.

126
00:16:05,633 --> 00:16:07,891
Wat wij doen,
houdt de mensen in leven.

127
00:16:09,070 --> 00:16:10,873
We moeten dat doen...

128
00:16:11,439 --> 00:16:13,663
het maakt niet uit wat er met ons gebeurt.

129
00:16:14,442 --> 00:16:16,575
Michonne denkt niet dat dit een leven is.

130
00:16:20,090 --> 00:16:23,148
Je toewijden aan zo'n keuze...

131
00:16:23,150 --> 00:16:26,885
na hoe wij vrij leefden...

132
00:16:26,887 --> 00:16:27,986
Ik snap het.

133
00:16:30,057 --> 00:16:31,532
Het is moeilijk.

134
00:16:32,493 --> 00:16:33,759
Je geeft alles op...

135
00:16:33,761 --> 00:16:36,136
net zoals je eigen leven.

136
00:16:39,066 --> 00:16:41,503
Maar of je hart slaat, of niet.

137
00:16:42,937 --> 00:16:44,977
Je geliefden hart slaat, of ze doen dat niet.

138
00:16:49,610 --> 00:16:53,250
We nemen aan wat ze ons geven,
zodat we kunnen leven.

139
00:17:28,586 --> 00:17:31,020
Hoe lang ben je al bij Negan?

140
00:17:37,763 --> 00:17:39,496
Waarom was je daar alleen?

141
00:17:42,434 --> 00:17:43,434
Vertel het me.

142
00:17:55,547 --> 00:17:58,782
Je denkt dat praten
de uitkomst niet zal veranderen.

143
00:17:58,784 --> 00:18:01,084
Maar je kent mij niet.

144
00:18:05,257 --> 00:18:07,023
Laten we kijken wat er gebeurt.

145
00:18:23,975 --> 00:18:26,269
Ik vermoord hem vandaag niet.

146
00:18:27,646 --> 00:18:30,580
Ik zal een manier vinden om te winnen.

147
00:18:33,752 --> 00:18:36,085
Ik ga mijn uitkomst veranderen.

148
00:18:37,989 --> 00:18:40,290
Daarom ben ik hier alleen.

149
00:18:48,567 --> 00:18:50,409
Is dat een appeltaart?

150
00:18:52,804 --> 00:18:54,470
Hoe deed je dat?

151
00:18:54,472 --> 00:18:56,272
Ik rook het toen je aan kwam lopen.

152
00:18:57,475 --> 00:18:59,008
Heb je het gebakken?

153
00:18:59,010 --> 00:19:01,685
Een man gaf het aan ons
als een bedankje.

154
00:19:02,047 --> 00:19:04,747
Zijn dochter zei dat jij
de president van Hilltop moet worden.

155
00:19:04,749 --> 00:19:07,217
Maggie voor President.

156
00:19:12,991 --> 00:19:15,491
Wil je een bord?

157
00:19:15,493 --> 00:19:17,460
Nee, hoor.

158
00:19:19,431 --> 00:19:21,097
Heeft Jesus jou die gegeven?

159
00:19:22,267 --> 00:19:24,367
Is hij hier nog?

160
00:19:24,369 --> 00:19:27,741
Ik wil wat dingen aan zijn lijst toevoegen
voordat hij gaat.

161
00:19:28,874 --> 00:19:31,774
De kinderen hebben pennen en potloden nodig.

162
00:19:31,776 --> 00:19:33,142
Ben je al president?

163
00:19:33,679 --> 00:19:35,612
Ik heb alleen maar met de mensen gesproken.

164
00:19:35,614 --> 00:19:37,655
Jesus is vanmorgen al vertrokken.

165
00:19:37,683 --> 00:19:40,417
Hij zei dat ik het tegen je moest zeggen,
maar ik was 't vergeten.

166
00:19:40,886 --> 00:19:43,914
Volgende keer dan.

167
00:19:43,955 --> 00:19:45,188
Ik ga melk halen.

168
00:19:45,190 --> 00:19:46,856
Laat mij maar.
- Nee, het lukt wel.

169
00:19:55,367 --> 00:19:56,566
Wat?

170
00:19:56,568 --> 00:19:59,669
Je liegt tegen Maggie over Jesus.

171
00:20:02,173 --> 00:20:04,340
Waarom zeg je dat?

172
00:20:04,342 --> 00:20:07,253
Een meisje die in het huis woont.
Ze vertelde me alles over iedereen.

173
00:20:07,312 --> 00:20:11,080
Ze zei dat Jesus een runner is
en dat hij gisteren is vertrokken...

174
00:20:11,082 --> 00:20:12,482
niet vanmorgen.

175
00:20:14,152 --> 00:20:17,320
Het is voor haar eigen bestwil.
- Waarom?

176
00:20:17,322 --> 00:20:21,557
Omdat ik iets moet doen
en zij mij wil helpen.

177
00:20:23,762 --> 00:20:25,561
Je wilt Negan doden.

178
00:20:30,869 --> 00:20:32,405
En als je hulp hebt?

179
00:20:32,471 --> 00:20:37,440
Als we hulp hadden, heel veel mensen,
dan zou ik het aan haar durven te vertellen...

180
00:20:37,442 --> 00:20:40,009
omdat ze dan weet
dat zij het dan niet hoeft te doen.

181
00:20:40,011 --> 00:20:42,208
Maar als alleen ik het ben...
- Dat is niet zo.

182
00:20:42,363 --> 00:20:45,515
Jij en Maggie zijn niet de enigen
die Negan uit willen schakelen.

183
00:20:45,517 --> 00:20:47,650
Nee, Enid. Dat gaat niet gebeuren.

184
00:20:47,652 --> 00:20:50,253
Als je om haar geeft,
zeg je niks tegen haar.

185
00:20:52,457 --> 00:20:54,290
We moeten haar veilig houden.

186
00:20:54,292 --> 00:20:58,227
Jij moet haar veilig houden.

187
00:21:00,799 --> 00:21:02,999
Je staat er niet alleen voor.

188
00:21:06,137 --> 00:21:08,141
Daar lijkt het wel op.

189
00:21:10,275 --> 00:21:12,942
Nadat de wereld viel,
kwamen er veel plekken.

190
00:21:12,944 --> 00:21:16,535
Misschien komen jullie van ��n van die plekken.

191
00:21:18,083 --> 00:21:21,322
De meeste gemeenschappen en kampen
zijn nu verdwenen.

192
00:21:23,655 --> 00:21:25,588
Ik verloor mensen.

193
00:21:25,590 --> 00:21:29,665
Ik verloor het vertrouwen in de mensen.

194
00:21:30,729 --> 00:21:33,296
Maar toen vond ik het Koninkrijk.

195
00:21:33,298 --> 00:21:35,476
Ik ontmoette Ezekiel.

196
00:21:36,768 --> 00:21:38,267
Ik zag wat hij had opgebouwd.

197
00:21:38,269 --> 00:21:41,070
Maar nu denk ik dat wat hij heeft opgebouwd,
bedreigd wordt.

198
00:21:41,072 --> 00:21:42,338
De Verlossers.

199
00:21:42,340 --> 00:21:44,356
De Verlossers.

200
00:21:46,111 --> 00:21:47,245
Een paar maanden geleden...

201
00:21:47,246 --> 00:21:50,600
ontmoetten Ezekiel en een paar bewakers
een groep in het bos.

202
00:21:51,349 --> 00:21:55,898
De Verlossers herkende Ezekiels vaardigheden
en Ezekiel wilde niet vechten...

203
00:21:55,954 --> 00:21:57,253
dus sloten ze een deal.

204
00:21:57,255 --> 00:22:00,823
In ruil voor eten en goederen,
raakt niemand gewond...

205
00:22:00,825 --> 00:22:03,493
en zij zouden nooit
naar het Koninkrijk toe komen.

206
00:22:03,495 --> 00:22:07,830
En heel weinig van ons weten ervan.
- Wat heeft dit met mij te maken?

207
00:22:07,832 --> 00:22:09,732
Ik weet dat Ezekiel jou mag.

208
00:22:09,734 --> 00:22:14,103
Ik weet ook dat hij jou vertrouwt
en daarom ben ik hier.

209
00:22:14,105 --> 00:22:16,973
Je moet mij helpen om hem ergens
van te overtuigen.

210
00:22:16,975 --> 00:22:19,542
Nu is er vrede tussen ons en de Verlossers...

211
00:22:19,544 --> 00:22:22,045
maar vroeg of laat gaat er iets mis.

212
00:22:22,047 --> 00:22:27,116
Misschien leveren we te weinig,
of misschien kijken we hun verkeerd aan...

213
00:22:27,118 --> 00:22:31,521
of misschien houden zij zich
niet meer aan de afspraak.

214
00:22:32,857 --> 00:22:35,425
De dingen zullen slecht gaan.

215
00:22:35,427 --> 00:22:36,859
En wanneer dat gebeurt...

216
00:22:40,031 --> 00:22:42,398
zal het Koninkrijk vallen.

217
00:22:46,137 --> 00:22:50,697
Ik had een gezin.
Ik verloor ze aan deze wereld.

218
00:22:51,976 --> 00:22:54,487
Ik zag ze sterven.

219
00:22:54,979 --> 00:22:59,182
Ik vrees dat als we nu niets doen...

220
00:22:59,184 --> 00:23:02,151
dat we niet alleen mensen verliezen,
we verliezen alles.

221
00:23:02,153 --> 00:23:05,521
Ik weet wat de Verlossers zijn
en ik weet wat ze doen.

222
00:23:05,523 --> 00:23:08,858
Ik weet dat ze niet te vertrouwen zijn.

223
00:23:12,163 --> 00:23:13,663
Ik denk dat jij dat ook weet.

224
00:23:15,767 --> 00:23:18,367
Wat vraag je nu precies?

225
00:23:20,371 --> 00:23:22,390
Ik vraag je om Ezekiel ervan te overtuigen...

226
00:23:22,474 --> 00:23:26,342
om de Verlossers aan te vallen,
als eerste toe te slaan...

227
00:23:26,344 --> 00:23:27,543
en ze te vernietigen.

228
00:23:35,564 --> 00:23:40,034
Is dat voor jou of voor Negan?

229
00:23:43,172 --> 00:23:44,705
Het is voor hem.

230
00:23:46,742 --> 00:23:49,076
Hoe ga je het doen?

231
00:23:49,078 --> 00:23:51,762
Ik zal de trekker overhalen.

232
00:23:52,381 --> 00:23:56,614
Ze zullen je doden.
- Zolang hij maar als eerste gaat.

233
00:23:57,687 --> 00:24:00,528
Waarom moet jij sterven?

234
00:24:00,529 --> 00:24:02,690
Omdat hij dat ook moet.

235
00:24:07,930 --> 00:24:10,064
Ik ben het ermee eens.

236
00:24:10,066 --> 00:24:11,398
Maar...

237
00:24:16,772 --> 00:24:18,372
Waarom jij?

238
00:24:22,245 --> 00:24:23,444
Er is...

239
00:24:25,047 --> 00:24:27,362
Je hoeft niet tegen mij te liegen...

240
00:24:29,252 --> 00:24:32,253
als dit ons laatste gesprek is.

241
00:24:36,659 --> 00:24:39,703
Als Abraham nog leefde, konden we vechten.

242
00:24:42,298 --> 00:24:44,264
Als Glenn er nog was...

243
00:24:45,601 --> 00:24:48,697
had Maggie's kind een vader.

244
00:24:51,173 --> 00:24:54,309
Michonne en Carl kunnen vechten.

245
00:24:55,011 --> 00:24:57,371
Ze hebben Rick.

246
00:24:58,014 --> 00:25:01,213
Aaron heeft Eric.

247
00:25:01,951 --> 00:25:04,950
Eugene weet dingen.

248
00:25:05,955 --> 00:25:07,823
Daryl is sterk.

249
00:25:10,693 --> 00:25:12,493
En Sasha?

250
00:25:26,609 --> 00:25:28,208
Kijk me aan, Rosita.

251
00:25:34,483 --> 00:25:36,483
Jij hoeft het niet te zijn.

252
00:25:37,653 --> 00:25:41,158
Niemand hoefde het te zijn.

253
00:25:42,658 --> 00:25:44,577
We winnen...

254
00:25:45,027 --> 00:25:48,052
maar we moeten het juiste moment afwachten...

255
00:25:48,331 --> 00:25:53,300
of het samen cre�ren.

256
00:25:54,270 --> 00:25:57,004
En jij bent daar een deel van.

257
00:25:59,709 --> 00:26:02,242
Doe dit niet.

258
00:26:04,513 --> 00:26:08,716
We hebben je nodig.

259
00:27:06,509 --> 00:27:08,842
Ik weet dat ze met velen zijn.

260
00:27:08,844 --> 00:27:11,645
Ik denk met veel meer dan in het Koninkrijk.

261
00:27:11,647 --> 00:27:15,249
Het verrassingselement is onze enige kans.
We moeten eerst toeslaan...

262
00:27:15,251 --> 00:27:18,252
en we moeten het nu doen,
nu we nog steeds in het voordeel zijn.

263
00:27:20,589 --> 00:27:25,459
Carol, ik denk dat je nooit deel uitmaakte
van geweld en gevechten.

264
00:27:26,629 --> 00:27:28,062
Je hebt het verkeerd.

265
00:27:28,064 --> 00:27:29,763
Je hebt het helemaal verkeerd.

266
00:27:29,765 --> 00:27:32,499
Ze is waarschijnlijk
de meest capabele vechter in deze kamer.

267
00:27:32,501 --> 00:27:34,568
Dan is het tijd om te vechten.

268
00:27:36,672 --> 00:27:38,172
Nee.

269
00:27:38,174 --> 00:27:40,974
Dit is iets waar ik geen deel van uitmaak.

270
00:27:40,976 --> 00:27:45,314
Je hoeft niet te vechten. Alleen Ezekiel ervan te
overtuigen dat 't Koninkrijk het gevecht aangaat.

271
00:27:45,315 --> 00:27:47,915
Je begrijpt me niet.

272
00:27:47,917 --> 00:27:51,485
Ik wilde niet dat jij,
Ezekiel of Morgan hierheen zouden komen.

273
00:27:51,487 --> 00:27:56,123
Ik wil niets te maken hebben
met jullie levens of doden.

274
00:27:56,125 --> 00:27:58,492
Ik wil alleen met rust worden gelaten.

275
00:28:03,999 --> 00:28:07,501
Mensen zullen sterven, heel veel mensen.

276
00:28:07,503 --> 00:28:11,138
Je doodde een man.
- Ik nam een leven om een leven te redden.

277
00:28:11,140 --> 00:28:13,440
Het is precies hetzelfde.

278
00:28:13,442 --> 00:28:16,962
Wij nemen hun levens, om de onze te redden.
- Je weet niet of dat zal gebeuren.

279
00:28:17,146 --> 00:28:19,507
Je zal ooit moeten kiezen
om iemand te doden, Morgan...

280
00:28:19,622 --> 00:28:22,361
want zo erg wordt het.

281
00:28:24,320 --> 00:28:28,522
Waarom kies je nu niet,
voordat je iemand verliest waar je om geeft?

282
00:28:28,524 --> 00:28:32,493
Het is nu vrede.
Ik wil dat niet veranderen.

283
00:28:32,495 --> 00:28:35,863
Misschien kunnen we op ze bouwen.
- Niet met die mensen.

284
00:28:35,865 --> 00:28:40,300
Als ze zich tegen ons keren, en dat gaat gebeuren,
kleeft dat bloed aan jouw handen.

285
00:28:46,342 --> 00:28:48,342
Misschien zijn jullie dat wel gewend.

286
00:28:52,948 --> 00:28:54,148
Je moet ook gaan.

287
00:28:54,150 --> 00:28:56,216
Misschien wel.

288
00:28:58,354 --> 00:29:03,423
Ik wil hier niemand meer zien
of weet waar ik ben.

289
00:29:03,425 --> 00:29:07,594
Als je iemand ziet die we kennen,
zeg dat ik weg ben.

290
00:29:07,596 --> 00:29:09,263
Doe dat voor mij, alsjeblieft.

291
00:29:09,265 --> 00:29:11,803
Het was nooit de bedoeling dat je me zou zien.

292
00:29:59,513 --> 00:30:01,346
Hallo.

293
00:30:02,716 --> 00:30:04,316
Hallo.

294
00:30:07,621 --> 00:30:09,354
Hallo.

295
00:31:32,873 --> 00:31:34,539
Afspraakje?

296
00:31:38,112 --> 00:31:41,513
Ik sta op goede voet met ze.
Ik maak daar gebruik van.

297
00:31:41,515 --> 00:31:43,081
Hoe dan?

298
00:31:46,220 --> 00:31:49,988
Om dichter bij ze te komen,
in ieder geval proberen.

299
00:31:49,990 --> 00:31:55,327
Als dat lukt, na een paar maanden of jaren
kunnen we wat proberen.

300
00:31:57,097 --> 00:32:01,300
Ik denk dat mijn moeder dat zou doen,
dus dat ga ik doen.

301
00:32:09,076 --> 00:32:14,479
Wat het ook was,
ik dacht dat het goed was tussen ons...

302
00:32:14,481 --> 00:32:19,397
tenminste een begin.
Waarom heb je het ge�indigd?

303
00:32:20,587 --> 00:32:22,746
Waarom ben je er eigenlijk mee begonnen?

304
00:32:26,360 --> 00:32:28,587
Ik dacht niet aan die ellende.

305
00:32:30,764 --> 00:32:34,299
Je ziet er leuk uit, lang.

306
00:32:34,301 --> 00:32:38,003
Soms ben je lief.

307
00:32:38,005 --> 00:32:39,892
Dus ik heb je gebruikt.

308
00:32:47,548 --> 00:32:49,676
Het spijt me daarvoor.

309
00:32:52,820 --> 00:32:57,289
Kom je straks ook naar het diner?

310
00:32:57,291 --> 00:32:59,291
Ik bedoel alleen maar eten.

311
00:32:59,293 --> 00:33:01,068
Ik hoef er niks voor terug.

312
00:33:04,898 --> 00:33:06,531
Goed.

313
00:33:29,923 --> 00:33:31,189
Wat krijgen we nou?

314
00:33:39,433 --> 00:33:42,367
Niks aan de hand.

315
00:33:42,369 --> 00:33:48,607
Je kunt die kant oplopen, maat,
ik zal niemand wat zeggen.

316
00:33:48,609 --> 00:33:50,742
Ik had daar ook moeten zijn...

317
00:33:50,744 --> 00:33:55,773
ik probeer hier ook uit te komen,
net als jij.

318
00:33:57,584 --> 00:33:59,217
Alsjeblieft.

319
00:34:01,373 --> 00:34:02,793
Daryl.

320
00:34:30,417 --> 00:34:33,885
Het gaat er niet om,
om te overleven.

321
00:34:33,887 --> 00:34:35,754
Maar om alles te krijgen.

322
00:34:38,459 --> 00:34:41,062
Ik heb de sleutel. Laten we gaan.

323
00:34:42,830 --> 00:34:45,464
Nee, ik wil alleen met hem praten.
- Ik zei 'nee.'

324
00:34:45,466 --> 00:34:47,933
Gedraag je niet als een eikel, Arat.

325
00:34:47,935 --> 00:34:50,035
Laat die man erlangs.

326
00:34:56,610 --> 00:34:59,911
Jezus. Is dat voor mij?

327
00:35:00,969 --> 00:35:05,460
We hebben elkaar nog niet ontmoet.
Ik ben Spencer Monroe.

328
00:35:06,653 --> 00:35:08,605
Hallo.

329
00:35:20,345 --> 00:35:21,444
Dat is Negan.

330
00:35:34,292 --> 00:35:37,193
Is hij daar?

331
00:35:37,195 --> 00:35:38,461
Ja.

332
00:35:38,463 --> 00:35:41,130
We zijn allemaal Negan.

333
00:35:43,802 --> 00:35:45,568
Wat je ook wilt doen, zal niet lukken.

334
00:35:46,050 --> 00:35:49,639
Maar je hebt nog steeds keuzes.

335
00:35:49,641 --> 00:35:51,707
Ga naar huis.

336
00:35:51,709 --> 00:35:57,079
Verbrand deze auto, rijd hem in het meer,
laat het verdwijnen.

337
00:35:58,516 --> 00:36:00,949
Er ligt een demper in het handschoenenkastje.

338
00:37:03,781 --> 00:37:06,916
Waar is hij?
- Negan?

339
00:37:06,918 --> 00:37:10,453
Hij zit in je huis, eikel.
Hij wacht op jou.

340
00:37:20,265 --> 00:37:21,564
Kom je zomaar aanwaaien?

341
00:37:21,566 --> 00:37:27,937
We wachten al uren om te kijken
wat je voor ons hebt.

342
00:37:27,939 --> 00:37:30,273
Waarom kijken we niet eerst?

343
00:37:33,745 --> 00:37:39,649
Leidingwater, airco,
een cadeau voor het nieuwe huis?

344
00:37:39,651 --> 00:37:45,021
Dat doet het hem.
Ik heb hier mijn eigen vakantiehuisje.

345
00:37:45,023 --> 00:37:47,023
Daar drinken we op.

346
00:37:53,598 --> 00:37:56,832
Dat is lekker.

347
00:37:56,834 --> 00:37:59,702
Wat we nog missen is een pooltafel.

348
00:37:59,704 --> 00:38:04,507
Niets beter als een potje 8-ball.

349
00:38:04,509 --> 00:38:06,943
Het huis aan de overkant
heeft er ��n in de garage.

350
00:38:06,945 --> 00:38:13,282
Spencer, jij wordt zo mijn nieuwe beste vriend.

351
00:38:14,719 --> 00:38:17,019
Het is een mooie dag...

352
00:38:17,021 --> 00:38:21,857
te mooi om in zo'n garage te zitten,
denk je ook niet?

353
00:38:23,528 --> 00:38:26,537
Ik heb een beter idee.

354
00:38:35,506 --> 00:38:36,739
Niet slecht.

355
00:38:36,741 --> 00:38:40,810
We zijn ook ver gegaan.

356
00:38:40,812 --> 00:38:43,479
Wat is dit?

357
00:38:43,481 --> 00:38:46,482
''Gefeliciteerd met je overwinning,
maar je hebt nog steeds verloren.''

358
00:38:51,889 --> 00:38:54,090
Heb je een liefdesbrief achtergelaten voor ons?

359
00:38:54,092 --> 00:38:57,556
Nee, ik...

360
00:38:58,516 --> 00:39:00,930
We zouden waarschijnlijk...

361
00:39:02,100 --> 00:39:03,866
Zei jij nou waarschijnlijk?

362
00:39:07,905 --> 00:39:11,440
Dat hebben we niet gedaan.
- Het gaat niet om dat klotebriefje.

363
00:39:11,442 --> 00:39:13,609
Niet doen.

364
00:39:13,611 --> 00:39:15,745
Loop gewoon naar Negan, Rick.

365
00:39:23,421 --> 00:39:27,923
Maar je vriend hier,
die hoeft nergens heen.

366
00:39:28,926 --> 00:39:30,226
Achteruit.

367
00:39:32,797 --> 00:39:35,131
Ik zou dit nooit met Rick doen.

368
00:39:35,133 --> 00:39:38,467
Hij zou daar alleen met zijn voorhoofd fronsen...

369
00:39:38,469 --> 00:39:42,438
en me met die ellendige ogen aankijken.

370
00:39:42,440 --> 00:39:46,442
Daar kwam ik eigenlijk voor.

371
00:39:46,444 --> 00:39:47,943
Ik wil met je praten over Rick.

372
00:39:54,318 --> 00:39:59,321
Goed, praat dan met me, Spencer.
Vertel me over Rick.

373
00:40:07,198 --> 00:40:12,401
Ik begrijp wat je hier probeert te doen,
wat je probeert op te bouwen.

374
00:40:12,403 --> 00:40:16,005
Niet dat ik het eens ben, met de manier hoe,
maar ik snap het.

375
00:40:16,007 --> 00:40:20,910
Je bouwt een netwerk om je heen.
Je laat ze bijdragen, zodat het beter wordt.

376
00:40:20,912 --> 00:40:23,779
Klinkt logisch.

377
00:40:23,781 --> 00:40:29,485
Maar je moet weten dat
Rick niet goed kan samenwerken.

378
00:40:33,458 --> 00:40:34,990
Is dat zo?

379
00:40:38,529 --> 00:40:42,198
Rick was hier niet de oorspronkelijke leider.
Dat was mijn moeder.

380
00:40:42,200 --> 00:40:45,468
Ze deed het echt goed.

381
00:40:45,470 --> 00:40:49,705
Toen stierf ze,
niet lang nadat Rick hier kwam...

382
00:40:49,707 --> 00:40:52,274
net als mijn broer en mijn vader.

383
00:40:52,276 --> 00:40:56,712
Dus alles was hier koek en ei,
voor hoelang, jaren?

384
00:40:56,714 --> 00:41:01,717
En Rick kwam hier,
en ineens was je een wees.

385
00:41:01,719 --> 00:41:05,888
Dat is het treurigste verhaal
wat ik ooit heb gehoord.

386
00:41:05,890 --> 00:41:10,459
Goed voor jou dat hij de leiding niet meer heeft.
- Dat doet er niet toe.

387
00:41:10,461 --> 00:41:12,294
Zijn ego loopt uit de hand.

388
00:41:12,296 --> 00:41:18,300
Hij zal een manier vinden om het te verpesten,
op zijn manier het over te nemen.

389
00:41:18,302 --> 00:41:22,037
Dat deed hij ook met mijn moeder.
En dat doet hij nog eens.

390
00:41:30,581 --> 00:41:33,549
En wat denk jij wat daar aan gedaan moet worden?

391
00:41:34,685 --> 00:41:37,553
Ik ben de zoon van mijn moeder.

392
00:41:37,555 --> 00:41:41,223
Ik kan de leider worden die zij was.

393
00:41:41,225 --> 00:41:43,859
Dat heeft deze plek nodig.
Dat is wat jij nodig hebt.

394
00:41:46,097 --> 00:41:50,199
Dus jij zegt dat ik jou de leiding moet geven?

395
00:41:51,235 --> 00:41:53,335
Daar zouden we allemaal beter van worden.

396
00:41:57,375 --> 00:41:58,941
Het komt goed met je.

397
00:42:01,078 --> 00:42:04,079
Zo is het genoeg.
Hij weet het nu wel.

398
00:42:13,457 --> 00:42:16,959
Mijn hart klopt nog, toch?

399
00:42:21,732 --> 00:42:25,846
Weet je, ik zit te denken, Spencer.

400
00:42:25,849 --> 00:42:31,759
Hoe Rick mij bedreigde om me te doden,
hoe hij me haat.

401
00:42:32,009 --> 00:42:35,244
Maar hij is daar nu...

402
00:42:35,246 --> 00:42:38,047
spullen aan het regelen om zeker te zijn...

403
00:42:38,049 --> 00:42:42,918
dat ik hier niemand wat doe.

404
00:42:42,920 --> 00:42:48,572
Hij heeft zijn haat ingeslikt
en regelt het gewoon.

405
00:42:50,962 --> 00:42:52,695
Dan moet je ook lef hebben.

406
00:43:00,271 --> 00:43:02,304
En toen kwam jij...

407
00:43:04,775 --> 00:43:09,411
het mannetje wat wachtte tot Rick weg was,
zodat hij naar me kon komen om me over te halen...

408
00:43:09,413 --> 00:43:13,949
om het vieze werk te doen,
zodat hij Rick zijn plek in kon nemen.

409
00:43:13,951 --> 00:43:15,851
Dus ik ga het toch vragen:

410
00:43:15,853 --> 00:43:21,690
als je de boel over wil nemen,
waarom dood je Rick dan niet zelf...

411
00:43:21,692 --> 00:43:25,780
en neem je de boel niet gewoon over?
- Nee...

412
00:43:26,430 --> 00:43:30,634
Weet je wat ik denk?
Want ik weet het wel.

413
00:43:33,371 --> 00:43:38,117
Omdat je geen lef hebt.

414
00:43:59,297 --> 00:44:02,965
Hoe beschamend.

415
00:44:03,968 --> 00:44:08,504
Daar is het.
Het zat er toch in. Je hebt wel lef.

416
00:44:08,506 --> 00:44:12,474
Ik heb het nog nooit zo mis gehad.

417
00:44:32,096 --> 00:44:38,500
En nu mag iemand deze rotzooi opruimen.

418
00:44:48,379 --> 00:44:51,547
Iemand die het spelletje af wil maken?

419
00:44:51,549 --> 00:44:52,915
Alsjeblieft.

420
00:44:54,585 --> 00:44:56,885
Iemand?

421
00:44:58,189 --> 00:45:00,089
Iemand?

422
00:45:01,625 --> 00:45:04,259
Alsjeblieft.

423
00:45:04,261 --> 00:45:05,794
Ik was aan het winnen.

424
00:45:20,260 --> 00:45:22,627
Wat is dit dan?

425
00:45:24,002 --> 00:45:26,231
Wilde je me vermoorden?

426
00:45:26,233 --> 00:45:27,832
Je hebt Lucille geraakt.

427
00:45:27,834 --> 00:45:29,567
Ze stond in de weg.

428
00:45:41,481 --> 00:45:43,982
Wat is dit?

429
00:45:46,849 --> 00:45:48,954
Wat is dit?

430
00:45:48,956 --> 00:45:51,957
Is deze jongen zelfgemaakt?

431
00:45:51,959 --> 00:45:54,793
Moet je dat gezicht zien.

432
00:45:54,795 --> 00:45:59,664
Dit is zelfgemaakt.
Denk je dat ik dom ben, schat...

433
00:45:59,666 --> 00:46:04,669
je hebt echt wat vindingrijkheid laten zien.

434
00:46:04,671 --> 00:46:10,698
Arat, laat dat mes maar over
die meid haar gezicht gaan.

435
00:46:13,080 --> 00:46:18,450
Lucille is prachtig, goed gesneden,
ze zal nooit meer hetzelfde zijn...

436
00:46:18,452 --> 00:46:20,480
dus waarom die van jou wel?

437
00:46:22,456 --> 00:46:24,171
Tenzij...

438
00:46:25,959 --> 00:46:30,829
Tenzij jij me zegt wie deze heeft gemaakt.

439
00:46:30,831 --> 00:46:33,331
Dat heb ik gedaan.

440
00:46:33,333 --> 00:46:35,133
Ik heb het gemaakt.

441
00:46:35,135 --> 00:46:39,637
Kijk, nu denk ik dat je liegt.

442
00:46:39,639 --> 00:46:42,440
En lieg je nu tegen me?

443
00:46:44,878 --> 00:46:47,379
Jammer dit.

444
00:46:47,381 --> 00:46:50,637
Arat zal dat lekkere bekkie
van je open moeten snijden.

445
00:46:51,618 --> 00:46:53,318
We doen het nog eens.

446
00:46:53,320 --> 00:46:55,153
Ik was het.

447
00:46:58,225 --> 00:47:01,159
Jij bent echt een stoere meid.

448
00:47:03,864 --> 00:47:05,263
Goed.

449
00:47:06,967 --> 00:47:09,234
Wat jij wilt.

450
00:47:09,236 --> 00:47:10,236
Arat...

451
00:47:11,505 --> 00:47:12,971
Dood iemand.

452
00:47:14,574 --> 00:47:16,741
Nee, ik was het.

453
00:47:37,631 --> 00:47:39,731
We hadden een afspraak.

454
00:47:40,901 --> 00:47:44,002
Kijk, daar hebben we Rick.

455
00:47:44,004 --> 00:47:48,006
Door je mensen moet ik blijven schreeuwen
en ben ik mijn stem haast kwijt.

456
00:47:49,409 --> 00:47:52,877
Rick, wat dacht je van een bedankje?

457
00:47:55,248 --> 00:47:59,250
Ik bedoel, we zijn deze relatie begonnen...

458
00:47:59,252 --> 00:48:02,754
toen ik al je vrienden total loss sloeg...

459
00:48:02,756 --> 00:48:08,059
en we hebben nooit elkaars haar gevlochten,
of geheimpjes gedeeld...

460
00:48:08,061 --> 00:48:10,562
maar wat dacht je van een beetje waardering?

461
00:48:10,564 --> 00:48:16,701
Ik ben door mijn knie�n gegaan
om te laten zien hoe redelijk ik ben.

462
00:48:16,703 --> 00:48:18,636
Je zoon...

463
00:48:18,638 --> 00:48:24,042
had zich verstopt in een van mijn trucks,
en maaide een paar mannen neer...

464
00:48:24,044 --> 00:48:27,912
en ik heb hem thuisgebracht,
veilig en wel...

465
00:48:27,914 --> 00:48:30,862
en hij heeft spaghetti gekregen.

466
00:48:35,455 --> 00:48:38,590
E�n van je andere mensen...

467
00:48:38,592 --> 00:48:43,094
hij wilde dat ik je vermoordde,
en hem de leiding zou geven.

468
00:48:43,096 --> 00:48:46,898
Ik heb hem uitgeschakeld, voor jou.

469
00:48:46,900 --> 00:48:50,635
En die andere...

470
00:48:50,637 --> 00:48:55,140
zij schoot op Lucille,
omdat ze mij wilde doden...

471
00:48:55,142 --> 00:48:57,665
dus ik zorgde dat je een mond
minder hoeft te voeren.

472
00:48:57,668 --> 00:49:02,514
En als ik naar haar kijk,
heeft die mond flinke schade aangebracht.

473
00:49:02,516 --> 00:49:04,115
Ikzelf...

474
00:49:04,117 --> 00:49:07,619
had ik haar niet uitgekozen,
maar Arat...

475
00:49:07,621 --> 00:49:09,738
ze vertrouwde haar niet.

476
00:49:18,198 --> 00:49:21,933
Je zooi wacht op je bij het hek.
Ga gewoon weg.

477
00:49:23,537 --> 00:49:26,004
Tuurlijk, Rick...

478
00:49:26,006 --> 00:49:32,410
nadat ik de gozer of griet heb gevonden
die deze kogel heeft gemaakt.

479
00:49:33,513 --> 00:49:35,211
Arat?

480
00:49:38,218 --> 00:49:40,051
Ik heb hem gemaakt.

481
00:49:40,053 --> 00:49:42,487
Nee, dat is niet zo.

482
00:49:50,897 --> 00:49:52,564
Ik heb hem gemaakt.

483
00:49:54,568 --> 00:49:56,201
Ik was het.

484
00:49:56,203 --> 00:49:58,236
Jij?

485
00:50:00,907 --> 00:50:06,744
Het heeft een behuizing met vier gaten nodig,
poeder, en een trechter...

486
00:50:06,746 --> 00:50:08,079
Kop dicht.

487
00:50:09,683 --> 00:50:12,150
Ik geloof je.

488
00:50:25,198 --> 00:50:29,701
Lucille, geef me kracht.

489
00:50:34,174 --> 00:50:40,078
Ik zal je ontdoen van je kogelmaker, Rick...

490
00:50:40,080 --> 00:50:44,515
dat samen met wat er bij het hek ligt.

491
00:50:44,517 --> 00:50:49,420
En wat je ook bij elkaar gezocht hebt,
het zal niet genoeg zijn...

492
00:50:49,422 --> 00:50:55,760
want je hebt nog steeds een flink gat,
na vandaag.

493
00:51:00,100 --> 00:51:01,299
We gaan weg.

494
00:51:06,273 --> 00:51:07,905
Neem mij mee, alsjeblieft.

495
00:51:09,643 --> 00:51:12,188
Rick, ik zal er niet om liegen...

496
00:51:12,212 --> 00:51:15,966
je keuken is echt een teringzooi.

497
00:51:17,951 --> 00:51:19,817
Ik zie je de volgende keer wel weer.

498
00:52:18,498 --> 00:52:20,365
Carl zei dat je hier was.

499
00:52:47,594 --> 00:52:49,928
Ik heb gevonden wat ik zocht.

500
00:52:55,569 --> 00:52:59,871
Ik wilde samen met jou en Aaron gaan...

501
00:52:59,873 --> 00:53:01,906
maar ik kon het niet.

502
00:53:03,009 --> 00:53:06,344
Ik moest mijn eigen weg gaan.

503
00:53:06,346 --> 00:53:08,446
Maar toen ik het gevonden had...

504
00:53:08,448 --> 00:53:14,085
realiseerde ik...

505
00:53:14,087 --> 00:53:18,289
dat het niet op mijn manier gaat.

506
00:53:18,291 --> 00:53:22,794
Die van ons, jou en mij.

507
00:53:29,870 --> 00:53:33,071
Er zijn er meer, meer als we dachten.

508
00:53:33,073 --> 00:53:37,642
We zijn gewoon in de minderheid.

509
00:53:37,644 --> 00:53:43,548
Maar dat verandert niet hoe ik me voel,
want het verandert niets aan deze situatie.

510
00:53:43,550 --> 00:53:46,384
We leven nog steeds, Rick.

511
00:53:46,386 --> 00:53:48,586
Er is zoveel gebeurd...

512
00:53:48,588 --> 00:53:50,955
zoveel wat we hebben meegemaakt.

513
00:53:50,957 --> 00:53:52,423
En...

514
00:53:52,425 --> 00:53:55,393
en ondanks of misschien daardoor...

515
00:53:55,395 --> 00:53:57,362
hebben we het gedaan.

516
00:53:58,798 --> 00:54:03,801
We zijn hier nog steeds, wij twee�n.

517
00:54:03,803 --> 00:54:07,672
We staan nog steeds overeind,
en dat zal zo blijven.

518
00:54:07,674 --> 00:54:11,476
Dus wat doen we daarmee?

519
00:54:11,478 --> 00:54:16,447
Hoe zorgen we ervoor dat het wat betekent?

520
00:54:17,027 --> 00:54:23,121
Wij zijn degenen die dingen voor elkaar krijgen.
Dat zijn jouw woorden.

521
00:54:23,123 --> 00:54:25,189
Waar zijn degenen die leven.

522
00:54:30,196 --> 00:54:33,631
Daarom moeten we vechten...

523
00:54:33,633 --> 00:54:39,770
niet voor ons, maar voor Judith, Carl...

524
00:54:40,507 --> 00:54:43,608
voor Alexandria, voor de Hilltop...

525
00:54:43,610 --> 00:54:48,780
voor ons allemaal.

526
00:54:48,782 --> 00:54:50,715
We kunnen ze aan, Rick.

527
00:54:50,717 --> 00:54:54,852
We vinden een manier om ze te verslaan.

528
00:54:54,854 --> 00:54:58,489
We kunnen het.

529
00:54:58,491 --> 00:55:00,158
Maar...

530
00:55:03,129 --> 00:55:05,964
Maar...

531
00:55:05,966 --> 00:55:09,166
Alleen als...

532
00:55:10,013 --> 00:55:14,174
wij dit doen.

533
00:55:20,547 --> 00:55:23,181
Ja, dat weet ik nu.

534
00:55:30,256 --> 00:55:32,256
Dat weet ik nu.

535
00:56:40,427 --> 00:56:44,295
Sasha. Enid.

536
00:56:58,278 --> 00:57:01,646
Ben je in orde?
- Ja.

537
00:57:01,648 --> 00:57:05,116
Met de baby gaat het ook goed,
wij allemaal.

538
00:57:06,753 --> 00:57:08,519
Je had gelijk...

539
00:57:08,521 --> 00:57:12,623
vanaf het begin al.

540
00:57:12,625 --> 00:57:17,028
Je zei dat we ons klaar moesten maken
om te vechten.

541
00:57:17,030 --> 00:57:19,363
Ik luisterde niet, ik kon het niet.

542
00:57:22,268 --> 00:57:24,769
Maar nu wel.

543
00:59:36,389 --> 00:59:42,394
Vertaling:
Quality over Quantity (QoQ) Releases

544
01:00:45,016 --> 01:00:48,490
Download deze ondertitel op:
- www.OpenSubtitles.org -

