﻿1
00:05:24,445 --> 00:05:26,178
Hi.

2
00:05:29,917 --> 00:05:31,217
Are you here...

3
00:05:31,252 --> 00:05:33,134
I'm here because I need your help.

4
00:06:15,760 --> 00:06:19,881
Sync & corrections by honeybunny & kinglouisxx
<font color="#ec14bd">www.addic7ed.com</font>

5
00:06:23,201 --> 00:06:25,502
Sorry for taking over your trailer.

6
00:06:25,538 --> 00:06:26,804
Oh.

7
00:06:26,839 --> 00:06:28,349
I grew up with a lot of people around.

8
00:06:28,704 --> 00:06:30,004
I'm used to it.

9
00:06:30,072 --> 00:06:31,806
Big family?

10
00:06:31,874 --> 00:06:34,708
Uh... group home.

11
00:06:36,503 --> 00:06:38,471
This isn't like that, though.

12
00:06:38,598 --> 00:06:40,483
Not the bad parts, at least.

13
00:06:41,562 --> 00:06:43,874
For the first time,
I feel like I belong.

14
00:06:45,398 --> 00:06:47,965
Trying to make sure you
and Sasha became a part of this

15
00:06:47,990 --> 00:06:50,162
made me a part of this.

16
00:06:51,131 --> 00:06:52,976
When I was first here...

17
00:06:53,303 --> 00:06:54,700
I was never here.

18
00:06:54,920 --> 00:06:56,516
I, um...

19
00:06:56,597 --> 00:06:59,558
I always found it hard
getting close to anyone...

20
00:07:00,089 --> 00:07:02,504
neighbors, friends...

21
00:07:03,589 --> 00:07:05,256
boyfriends.

22
00:07:10,528 --> 00:07:12,653
You should try it sometime.

23
00:07:13,301 --> 00:07:15,399
Even if it doesn't last.

24
00:07:18,004 --> 00:07:21,563
I'm gonna talk to our blacksmith
about making more spears.

25
00:07:21,841 --> 00:07:25,086
Maybe we can trade with
the Kingdom for some body armor.

26
00:07:25,611 --> 00:07:28,546
Thing is, what we really need
is riot gear.

27
00:08:08,909 --> 00:08:10,275
I was...

28
00:08:10,655 --> 00:08:14,525
looking for something to read
the other day.

29
00:08:19,478 --> 00:08:21,277
You can have the bullets.

30
00:08:22,591 --> 00:08:23,852
I didn't know you had a gun.

31
00:08:24,114 --> 00:08:25,191
I didn't.

32
00:08:26,461 --> 00:08:27,819
I do now.

33
00:08:29,388 --> 00:08:31,721
Sasha, don't go... not yet.

34
00:08:36,080 --> 00:08:38,447
Rosita didn't come here to train people.

35
00:08:38,762 --> 00:08:40,415
You're both going after Negan.

36
00:08:41,735 --> 00:08:43,952
But you can't do that without people.

37
00:08:44,333 --> 00:08:46,033
A lot of people.

38
00:08:46,235 --> 00:08:48,123
We've talked about this.

39
00:08:48,790 --> 00:08:50,692
I know what you think.

40
00:08:51,056 --> 00:08:52,928
And I appreciate that.

41
00:08:53,407 --> 00:08:55,696
But I'm not gonna change my mind.

42
00:08:56,433 --> 00:08:57,814
She's not.

43
00:08:59,506 --> 00:09:02,147
Does Maggie know Rosita's here yet?

44
00:09:02,842 --> 00:09:05,607
Uh, I-I-I don't think so.

45
00:09:05,998 --> 00:09:07,432
I didn't...

46
00:09:08,738 --> 00:09:10,446
You should tell her.

47
00:09:10,680 --> 00:09:12,013
About all of it.

48
00:09:12,048 --> 00:09:14,516
No. Not yet.

49
00:09:14,584 --> 00:09:16,317
Sh...

50
00:09:16,386 --> 00:09:18,686
I'm still... I'm still getting ready.

51
00:09:20,352 --> 00:09:23,043
And the thing is, Rosita is going,

52
00:09:23,129 --> 00:09:24,270
with or without me.

53
00:09:24,363 --> 00:09:25,693
So it should be with me.

54
00:09:25,729 --> 00:09:27,210
- Then I'll go, too.
- Me, too.

55
00:09:27,235 --> 00:09:28,630
No.

56
00:09:28,836 --> 00:09:31,781
The Hilltop has to be ready
for what happens after.

57
00:09:32,280 --> 00:09:33,846
Maggie needs you.

58
00:09:35,306 --> 00:09:37,171
She needs you, too.

59
00:09:38,384 --> 00:09:40,197
Not anymore.

60
00:09:41,540 --> 00:09:43,478
She has everyone else,

61
00:09:44,122 --> 00:09:45,887
and they have her.

62
00:09:53,224 --> 00:09:54,918
You can stay.

63
00:09:55,460 --> 00:09:57,028
I know you can.

64
00:09:58,098 --> 00:10:03,231
But I know you won't and she
won't, but I wish you would.

65
00:10:03,841 --> 00:10:07,036
'Cause it's a long life,
and then it isn't.

66
00:10:07,871 --> 00:10:10,087
You can take anything else you need.

67
00:10:10,532 --> 00:10:13,809
But you and Rosita
need to talk to Maggie.

68
00:10:14,064 --> 00:10:15,711
You owe her that much.

69
00:10:31,111 --> 00:10:32,673
Listen...

70
00:10:33,576 --> 00:10:35,209
Enid...

71
00:10:36,002 --> 00:10:38,042
Maggie trusts you.

72
00:10:39,813 --> 00:10:43,182
You have to protect her, no matter what.

73
00:10:44,025 --> 00:10:46,418
She's the future of this place.

74
00:10:46,704 --> 00:10:48,270
I know it.

75
00:10:51,017 --> 00:10:52,783
So are you.

76
00:10:58,912 --> 00:11:01,126
Hold on to this for me.

77
00:11:03,465 --> 00:11:05,031
It's for the baby.

78
00:11:07,579 --> 00:11:10,297
Maybe you can work on it while I'm gone.

79
00:11:13,144 --> 00:11:14,543
Okay.

80
00:11:17,524 --> 00:11:19,141
Sasha...

81
00:11:19,358 --> 00:11:20,958
Yeah.

82
00:11:25,969 --> 00:11:27,269
In 10 minutes,

83
00:11:27,294 --> 00:11:29,027
I'm gonna tell Maggie what's going on.

84
00:11:31,059 --> 00:11:32,942
It's up to you
what you want to do with that.

85
00:11:36,114 --> 00:11:38,205
I'm doing what you asked.

86
00:11:59,157 --> 00:12:00,724
The Saviors are coming!

87
00:12:00,753 --> 00:12:02,413
The Saviors are coming!

88
00:12:02,438 --> 00:12:03,352
We have to get Maggie.

89
00:12:03,377 --> 00:12:04,728
- Where is she?
- Come on!

90
00:12:12,264 --> 00:12:13,456
Where do we hide?

91
00:12:13,498 --> 00:12:15,561
We're not hiding.

92
00:12:21,579 --> 00:12:22,834
All right, get in.

93
00:12:22,928 --> 00:12:24,467
It goes to the other side.

94
00:12:24,545 --> 00:12:26,627
We got to get out before Maggie does.

95
00:12:38,758 --> 00:12:40,748
We'll never make it in time.

96
00:12:41,065 --> 00:12:42,990
Come on!

97
00:12:50,675 --> 00:12:53,186
Just stay down there.
I'll keep them away.

98
00:12:53,226 --> 00:12:55,616
They aren't the same ones
who came to Alexandria.

99
00:13:26,828 --> 00:13:29,819
Simon! Hello.

100
00:13:29,844 --> 00:13:33,483
Right back at you, Gregory. Hello.

101
00:13:34,924 --> 00:13:36,869
Ahh.

102
00:13:37,526 --> 00:13:40,126
We weren't, uh,
expecting you back so soon.

103
00:13:40,549 --> 00:13:42,462
You want to talk in my office?

104
00:13:42,681 --> 00:13:44,939
I got a bottle of gin my people found.

105
00:13:45,067 --> 00:13:46,867
You're a gin man, right?

106
00:13:47,025 --> 00:13:49,302
Oh, I'm a man of...

107
00:13:49,392 --> 00:13:51,984
shifting specifics of tastes
in transition.

108
00:13:52,009 --> 00:13:54,007
I'm into tequila now.

109
00:13:55,234 --> 00:13:57,933
Añejo. Reposado.

110
00:13:57,958 --> 00:13:59,749
Sipping, mixing...

111
00:14:00,343 --> 00:14:01,903
Un mundo de sabor.

112
00:14:03,296 --> 00:14:05,239
Gin seems like turpentine.

113
00:14:06,154 --> 00:14:08,833
It happened quick with a gimlet
the other night.

114
00:14:09,903 --> 00:14:12,158
I'm an unusual kind of creature.

115
00:14:12,483 --> 00:14:13,982
You know what?

116
00:14:14,200 --> 00:14:15,304
I respect that.

117
00:14:15,615 --> 00:14:17,415
Yeah. Tequila.

118
00:14:18,021 --> 00:14:20,135
Mental note. Locked.

119
00:14:20,494 --> 00:14:21,869
Great.

120
00:14:22,014 --> 00:14:23,768
Oh, no need to talk.

121
00:14:23,862 --> 00:14:26,016
This is hopefully a quick visit.

122
00:14:26,084 --> 00:14:27,450
Oh.

123
00:14:27,486 --> 00:14:29,586
Well, what can I do for you?

124
00:14:30,232 --> 00:14:31,721
That's an excellent question.

125
00:14:31,927 --> 00:14:34,324
Alas, the answer's not so excellent.

126
00:14:34,716 --> 00:14:37,365
You have a certain somebody that, uh...

127
00:14:37,474 --> 00:14:39,195
now needs to be
Negan's certain somebody,

128
00:14:39,231 --> 00:14:43,133
so what you can do for me
is point me in their direction.

129
00:14:46,476 --> 00:14:48,132
So, who is it?

130
00:14:48,405 --> 00:14:51,158
You get your pointy finger up,
and I'll tells ya.

131
00:14:58,793 --> 00:15:00,218
Daryl.

132
00:15:03,242 --> 00:15:04,804
Daryl.

133
00:15:05,639 --> 00:15:07,306
Daryl.

134
00:15:21,286 --> 00:15:23,250
Did you dig that tunnel?

135
00:15:23,512 --> 00:15:25,412
Maggie thought
we needed a quick way out.

136
00:15:25,652 --> 00:15:26,918
So I made one.

137
00:15:27,057 --> 00:15:29,546
Well, we're out. So now what?

138
00:15:30,949 --> 00:15:32,482
You know.

139
00:15:55,070 --> 00:15:57,029
If this one starts,
I'll siphon some gas.

140
00:15:57,132 --> 00:15:58,132
I just got to find...

141
00:15:58,201 --> 00:15:59,368
There's an empty water bottle.

142
00:16:02,418 --> 00:16:04,611
I thought this was the one, but...

143
00:16:14,741 --> 00:16:16,308
Like it?

144
00:16:16,481 --> 00:16:17,884
I made it.

145
00:16:17,981 --> 00:16:19,575
Car's dead.

146
00:17:07,180 --> 00:17:09,339
Where'd you learn to disarm bombs?

147
00:17:11,509 --> 00:17:13,198
Were you in the Army?

148
00:17:13,553 --> 00:17:15,582
Someone I knew.

149
00:17:16,936 --> 00:17:18,371
Not him.

150
00:17:18,825 --> 00:17:20,653
Listen, I'm not here to play
"Get to Know You,"

151
00:17:20,740 --> 00:17:22,910
so either we talk about
the mission or we don't.

152
00:17:29,369 --> 00:17:30,676
Okay.

153
00:17:38,736 --> 00:17:40,369
Jesus said there's some buildings,

154
00:17:40,514 --> 00:17:41,946
two, three, four stories,

155
00:17:41,990 --> 00:17:43,923
just outside the sanctuary
to the east of it.

156
00:17:44,084 --> 00:17:45,256
At least one of them

157
00:17:45,281 --> 00:17:47,032
should have a line of sight
to the rear courtyard,

158
00:17:47,220 --> 00:17:49,196
past the fence with the walkers on it.

159
00:17:49,556 --> 00:17:52,223
We hole up, we wait,
we take a shot from there.

160
00:17:53,540 --> 00:17:55,469
If we have to get closer,

161
00:17:55,704 --> 00:17:57,203
there's some weak spots in the perimeter

162
00:17:57,228 --> 00:17:59,321
we might still be able
to take advantage of,

163
00:17:59,708 --> 00:18:00,940
but if we can,

164
00:18:01,125 --> 00:18:04,025
I say we stay outside, do it clean.

165
00:18:04,737 --> 00:18:06,862
Maybe make it out of there alive.

166
00:18:17,488 --> 00:18:18,921
You see a problem with that?

167
00:18:18,998 --> 00:18:21,333
I want to make sure we get him.
That means we go in.

168
00:18:21,358 --> 00:18:23,723
If we shoot from a distance and miss,
we don't get another chance.

169
00:18:23,748 --> 00:18:24,614
I won't miss.

170
00:18:24,639 --> 00:18:26,692
Even if you shoot and graze him,
he doesn't die.

171
00:18:26,717 --> 00:18:28,245
I won't miss.

172
00:18:28,270 --> 00:18:29,970
If we go in and get caught...

173
00:18:29,995 --> 00:18:31,161
I won't get caught.

174
00:18:31,426 --> 00:18:32,992
What the hell's wrong with you?

175
00:18:34,438 --> 00:18:36,431
They take us, we don't get another try.

176
00:18:37,108 --> 00:18:38,841
Because they'll kill us.

177
00:18:40,374 --> 00:18:42,010
If we're out here, we're alive.

178
00:18:42,202 --> 00:18:43,812
We get another go.

179
00:18:59,745 --> 00:19:01,830
Nothing is wrong with me, Sasha.

180
00:19:02,033 --> 00:19:03,830
I'm gonna make sure he's dead.

181
00:19:04,706 --> 00:19:06,344
Look, if this is too much for you,

182
00:19:06,377 --> 00:19:08,044
you want to bail, now's your chance.

183
00:19:08,354 --> 00:19:09,508
I'm not going anywhere.

184
00:19:09,533 --> 00:19:11,873
- Well, I'm done talking about it.
- Good.

185
00:19:11,908 --> 00:19:13,107
Maybe you'll think about it.

186
00:19:14,355 --> 00:19:15,369
Maybe you'll change your mind

187
00:19:15,395 --> 00:19:18,177
about what it takes
to get what you want.

188
00:19:27,522 --> 00:19:28,790
Hi!

189
00:19:28,865 --> 00:19:31,723
Uh, I've got fresh veggies...

190
00:19:31,748 --> 00:19:34,998
Stop. They're vegetables.

191
00:19:35,146 --> 00:19:36,659
Use the whole word.

192
00:19:36,799 --> 00:19:38,099
We have time.

193
00:19:38,275 --> 00:19:39,534
Uh...

194
00:19:39,676 --> 00:19:41,073
Okay.

195
00:19:41,176 --> 00:19:44,778
I have these vegetables
they told me to bring over,

196
00:19:44,813 --> 00:19:48,015
uh, and the basket's pretty heavy.

197
00:19:48,309 --> 00:19:50,027
For me, I mean.

198
00:19:50,152 --> 00:19:51,934
Probably not you. Uh, here.

199
00:19:51,959 --> 00:19:53,014
Load them in the truck,

200
00:19:53,092 --> 00:19:55,209
and, uh, if you meet me by the garden,

201
00:19:55,234 --> 00:19:57,038
- I can get you the rest...
- Stop.

202
00:19:57,960 --> 00:20:01,595
I don't know who you think I am
or who we are.

203
00:20:01,983 --> 00:20:04,164
Load them yourself. I'm busy.

204
00:20:04,295 --> 00:20:06,888
Oh. Sorry. I'm sorry.

205
00:20:06,913 --> 00:20:10,704
Girl, pick that shit up
right now and scram.

206
00:20:14,506 --> 00:20:16,039
And I'll take that.

207
00:20:18,551 --> 00:20:20,010
Now.

208
00:20:20,262 --> 00:20:22,616
Don't make me cut it off you, girl.

209
00:21:31,341 --> 00:21:33,576
Dr. Harlan Carson, correct?

210
00:21:35,277 --> 00:21:36,796
Do you need a checkup?

211
00:21:37,179 --> 00:21:40,515
Probably. It's been a few
years, but I'm not here for that.

212
00:21:43,827 --> 00:21:45,444
We already gave you as much
medicine as we could spare

213
00:21:45,469 --> 00:21:47,903
- the last time you were here.
- Yes, you did.

214
00:21:48,096 --> 00:21:50,616
But we're not here to take more of that.

215
00:21:51,038 --> 00:21:52,398
Congratulations!

216
00:21:52,507 --> 00:21:54,390
You're moving up in the world.

217
00:21:54,610 --> 00:21:56,543
Negan wants you where he is.

218
00:21:56,727 --> 00:21:58,827
You're an important fellow, Harlan.

219
00:22:00,546 --> 00:22:02,146
You're coming with us.

220
00:22:03,516 --> 00:22:07,585
Look, I... I have patients here
that need me.

221
00:22:07,909 --> 00:22:10,109
Why does Negan need two doctors?

222
00:22:18,012 --> 00:22:21,547
Well, I think my look may have
conveyed the information.

223
00:22:21,870 --> 00:22:24,232
People say I have an expressive face.

224
00:22:24,361 --> 00:22:26,895
But if not, let me repeat your question.

225
00:22:26,920 --> 00:22:31,063
Why do we need two doctors?
Answer... we don't.

226
00:22:36,137 --> 00:22:37,470
So...

227
00:22:38,810 --> 00:22:40,397
my brother pissed someone off,

228
00:22:40,646 --> 00:22:42,007
get himself killed?

229
00:22:42,363 --> 00:22:44,096
Something like that.

230
00:22:44,560 --> 00:22:46,645
Very, very ugly stuff.

231
00:22:46,960 --> 00:22:48,793
My condolences.

232
00:22:50,175 --> 00:22:51,884
But it's time.

233
00:22:53,866 --> 00:22:55,866
All of my charts are in order.

234
00:22:56,043 --> 00:22:57,324
You need to make arrangements

235
00:22:57,349 --> 00:22:58,815
for some kind of medical care here,

236
00:22:58,934 --> 00:23:00,833
especially for my ongoing patients.

237
00:23:01,386 --> 00:23:02,591
I will.

238
00:23:04,985 --> 00:23:07,042
- I need to pack.
- You don't.

239
00:23:07,214 --> 00:23:09,947
We have everything you need,
and what we don't, we'll get.

240
00:23:10,237 --> 00:23:11,913
Hey, you like ice cream?

241
00:23:12,331 --> 00:23:13,998
We have ice cream.

242
00:23:14,229 --> 00:23:17,515
We have a lady
that makes cardamom gelato.

243
00:23:17,699 --> 00:23:19,065
Shit you not.

244
00:23:19,234 --> 00:23:22,054
Oh! Almost forgot. Set that down.

245
00:23:24,020 --> 00:23:25,806
Well, fair is fair.

246
00:23:27,258 --> 00:23:29,136
We're not leaving you high and dry

247
00:23:29,161 --> 00:23:31,161
when it comes to medical solutions.

248
00:23:31,295 --> 00:23:32,594
Go on.

249
00:23:49,478 --> 00:23:51,747
Simon, a word?

250
00:23:52,570 --> 00:23:54,307
Mm.

251
00:23:56,377 --> 00:23:59,682
Look, I think I've proven

252
00:23:59,707 --> 00:24:01,832
that I can work with Negan.

253
00:24:02,056 --> 00:24:03,770
I'm a can-do guy,

254
00:24:04,434 --> 00:24:07,128
but I got to hang on
to my peoples' trust.

255
00:24:07,153 --> 00:24:09,689
Otherwise, you don't know
who might take over.

256
00:24:10,220 --> 00:24:12,814
You know, someone else
might not be so cooperative.

257
00:24:13,345 --> 00:24:15,838
They might have crazy ideas.

258
00:24:18,439 --> 00:24:21,022
Who might take over?

259
00:24:21,047 --> 00:24:23,661
- What crazy ideas?
- I don't know.

260
00:24:23,686 --> 00:24:26,064
I'm just considering
worst-case scenarios.

261
00:24:26,315 --> 00:24:28,134
You know, what I'm saying is,

262
00:24:28,166 --> 00:24:32,178
I know human nature,
and it isn't inconceivable.

263
00:24:33,998 --> 00:24:36,700
- I hear your concern.
- Mm.

264
00:24:37,293 --> 00:24:39,388
I'm not insensitive to it.

265
00:24:39,413 --> 00:24:41,340
- Mm.
- Right?

266
00:24:43,684 --> 00:24:47,340
If you're having problems
of that nature...

267
00:24:48,418 --> 00:24:50,609
come to see me.

268
00:24:51,047 --> 00:24:52,447
Anytime.

269
00:24:52,730 --> 00:24:54,238
I don't know where to go.

270
00:24:56,371 --> 00:25:00,321
I'll arrange it so that...

271
00:25:02,892 --> 00:25:05,593
...you just tell the guard
who you are...

272
00:25:05,715 --> 00:25:06,961
Mm-hmm.

273
00:25:07,443 --> 00:25:09,380
...and he'll let you right in.

274
00:25:13,143 --> 00:25:15,536
Long as there's no shenanigans afoot.

275
00:25:16,768 --> 00:25:18,372
There will be none.

276
00:25:21,838 --> 00:25:24,218
I just... I just
would like you to consider...

277
00:25:24,335 --> 00:25:25,901
Wonderful.

278
00:25:27,783 --> 00:25:29,116
Hmm?

279
00:25:30,374 --> 00:25:32,895
We'll crack open some tequila.

280
00:25:33,679 --> 00:25:35,312
We'll talk.

281
00:25:35,942 --> 00:25:37,965
We'll work it out.

282
00:25:38,926 --> 00:25:40,254
You're a good producer for us.

283
00:25:40,279 --> 00:25:42,526
- I don't want you to be so stressed.
- Yeah.

284
00:25:43,686 --> 00:25:45,776
I'd be a lot less stressed
if you'd leave my doctor.

285
00:25:45,801 --> 00:25:47,434
Gregory...

286
00:25:47,837 --> 00:25:49,970
Got it. Okay.

287
00:25:50,300 --> 00:25:52,133
I won't ask again.

288
00:26:51,368 --> 00:26:53,906
You were gonna kill that guy.

289
00:26:54,836 --> 00:26:56,062
He was gonna find us.

290
00:26:56,322 --> 00:26:59,637
He wasn't, and he didn't.

291
00:27:02,275 --> 00:27:04,208
He deserved to die.

292
00:27:05,915 --> 00:27:07,581
Ever since you got here,

293
00:27:07,705 --> 00:27:09,533
you haven't said a word to me.

294
00:27:12,094 --> 00:27:13,822
Would you look at me?

295
00:27:15,181 --> 00:27:16,823
Please?

296
00:27:23,054 --> 00:27:24,884
Daryl...

297
00:27:28,854 --> 00:27:30,442
I'm sorry.

298
00:27:33,046 --> 00:27:34,792
I'm sorry.

299
00:27:44,400 --> 00:27:46,233
It wasn't your fault.

300
00:27:47,409 --> 00:27:49,717
It was.

301
00:27:50,840 --> 00:27:52,306
No.

302
00:27:52,607 --> 00:27:54,349
It wasn't.

303
00:27:55,467 --> 00:27:58,243
You're one of the good things
in this world.

304
00:27:59,141 --> 00:28:01,071
That's what Glenn thought.

305
00:28:01,916 --> 00:28:03,485
And he would know,

306
00:28:04,079 --> 00:28:06,274
'cause he was
one of the good things, too.

307
00:28:09,355 --> 00:28:11,394
And, uh...

308
00:28:13,464 --> 00:28:15,659
I wanted to kill that guy, too.

309
00:28:17,066 --> 00:28:20,300
I wanted to string them all up
and watch them die.

310
00:28:25,378 --> 00:28:27,274
But we have to win.

311
00:28:36,722 --> 00:28:38,504
Help me win.

312
00:29:49,944 --> 00:29:52,040
This'll work.

313
00:30:33,029 --> 00:30:34,686
Now.

314
00:31:03,629 --> 00:31:06,077
Get in!

315
00:32:23,057 --> 00:32:24,743
Anything?

316
00:32:25,533 --> 00:32:27,385
Negan still hasn't come out.

317
00:32:27,598 --> 00:32:29,364
But Eugene's out there.

318
00:32:30,664 --> 00:32:31,984
Eugene?

319
00:32:32,378 --> 00:32:33,744
It looks like...

320
00:32:34,030 --> 00:32:36,278
he's ordering people around.

321
00:32:38,179 --> 00:32:40,106
Playing some angle.

322
00:32:44,116 --> 00:32:45,813
Will you teach me those knots?

323
00:32:46,775 --> 00:32:48,918
I know a bunch, but not those.

324
00:32:49,983 --> 00:32:51,749
Won't matter soon.

325
00:32:54,160 --> 00:32:55,546
It might.

326
00:33:06,358 --> 00:33:07,749
Yeah, I'll show you.

327
00:33:09,186 --> 00:33:11,128
All right, here's the first one.

328
00:33:12,757 --> 00:33:14,918
Cross it over your hand like that.

329
00:33:15,365 --> 00:33:19,157
Then you take the top loop,
bring it down, up,

330
00:33:19,376 --> 00:33:22,079
and back through, then...

331
00:33:23,952 --> 00:33:25,351
Got it?

332
00:33:25,475 --> 00:33:27,151
Let me try.

333
00:33:52,067 --> 00:33:53,510
You know...

334
00:33:54,479 --> 00:33:57,218
we got lucky, having you with us.

335
00:33:58,874 --> 00:34:00,804
You know how to do everything.

336
00:34:18,534 --> 00:34:20,136
Johnny.

337
00:34:21,026 --> 00:34:22,987
That's who taught me about bombs.

338
00:34:23,792 --> 00:34:25,762
Survivalist, prepper-type shit.

339
00:34:28,424 --> 00:34:31,375
Marcus taught me about cars.
He was a mechanic...

340
00:34:31,727 --> 00:34:33,494
and a wannabe stunt driver.

341
00:34:34,972 --> 00:34:36,772
And an asshole.

342
00:34:37,482 --> 00:34:39,896
Knot-tying was from Chaser.

343
00:34:41,478 --> 00:34:43,263
Yeah, that wasn't his real name.

344
00:34:45,857 --> 00:34:47,657
There were others.

345
00:34:49,058 --> 00:34:51,222
Were those people you lost
on the way to D.C.?

346
00:34:51,308 --> 00:34:53,093
No, not them.

347
00:34:53,582 --> 00:34:56,029
A lot of guys wanted to protect me,

348
00:34:56,633 --> 00:34:59,781
like there was no way I could
know how to take care of myself.

349
00:35:01,103 --> 00:35:02,302
And I didn't.

350
00:35:03,672 --> 00:35:05,839
And I hated the way that felt.

351
00:35:07,727 --> 00:35:09,460
So I rolled with it.

352
00:35:10,306 --> 00:35:12,205
They didn't even notice I was picking up

353
00:35:12,230 --> 00:35:14,564
everything they knew how to do
and doing it better.

354
00:35:18,758 --> 00:35:21,312
Then I'd outgrow them and bounce.

355
00:35:26,218 --> 00:35:28,605
The sex was just for fun.

356
00:35:30,144 --> 00:35:31,652
When the world's over,

357
00:35:31,973 --> 00:35:34,574
everyone should be
getting their rocks off.

358
00:35:40,873 --> 00:35:42,890
Was that how it was with him?

359
00:35:45,628 --> 00:35:47,339
Abraham?

360
00:35:51,682 --> 00:35:53,072
No.

361
00:35:56,448 --> 00:35:59,565
I fell in with him because he
saw I could handle my shit.

362
00:36:00,451 --> 00:36:02,385
And I never looked back.

363
00:36:09,238 --> 00:36:11,176
Well, no, I did.

364
00:36:13,176 --> 00:36:14,840
When it was over.

365
00:36:17,946 --> 00:36:19,628
Yeah.

366
00:36:26,384 --> 00:36:28,946
You know the thing that killed me?

367
00:36:30,664 --> 00:36:31,999
When we got to Alexandria,

368
00:36:32,024 --> 00:36:34,121
I was acting like
I was all good 'cause he wasn't.

369
00:36:36,875 --> 00:36:39,359
Then, the son of a bitch
was actually all good.

370
00:36:41,792 --> 00:36:44,010
And I couldn't figure this one out.

371
00:36:45,471 --> 00:36:47,384
Living there,
feeling like we had made it,

372
00:36:47,409 --> 00:36:49,679
I couldn't pick that up.

373
00:36:49,848 --> 00:36:52,085
Then, when he bounced, I...

374
00:36:56,844 --> 00:36:59,007
...I thought I hated you.

375
00:37:02,244 --> 00:37:05,823
But maybe I just hated that he
figured out his shit first.

376
00:37:13,834 --> 00:37:16,217
I've never told anyone
any of that stuff.

377
00:37:17,421 --> 00:37:19,600
Who I was, what I did...

378
00:37:21,667 --> 00:37:23,307
I'm glad...

379
00:37:24,112 --> 00:37:26,487
that you felt like...

380
00:37:27,276 --> 00:37:29,245
you could.

381
00:37:35,422 --> 00:37:38,023
I was stupid to waste so much time.

382
00:37:47,916 --> 00:37:49,727
Now I'll never get to tell Abraham

383
00:37:49,768 --> 00:37:51,517
I'm happy he was happy.

384
00:38:02,276 --> 00:38:03,919
Were you happy?

385
00:38:10,914 --> 00:38:12,685
I was.

386
00:38:21,246 --> 00:38:23,213
It wasn't his time.

387
00:38:24,904 --> 00:38:27,557
Abraham would've wanted
to go out fighting.

388
00:38:32,243 --> 00:38:35,509
And that asshole with the bat
took that away, too.

389
00:38:38,743 --> 00:38:42,100
To go out with a point to going out.

390
00:38:52,344 --> 00:38:54,579
I guess we all want that.

391
00:39:01,125 --> 00:39:03,078
No matter how it all goes down,

392
00:39:03,723 --> 00:39:05,273
I got your back.

393
00:39:06,476 --> 00:39:08,656
And I've got yours.

394
00:39:14,518 --> 00:39:16,323
You think that prick's out there yet?

395
00:39:16,460 --> 00:39:17,726
Hmm.

396
00:39:39,614 --> 00:39:42,706
Oh... shit.

397
00:39:42,893 --> 00:39:44,025
What?

398
00:39:44,050 --> 00:39:46,800
Dr. Carson, Maggie's doctor.

399
00:39:47,064 --> 00:39:48,891
They brought him here.

400
00:39:56,595 --> 00:39:58,462
Negan's out.

401
00:40:08,061 --> 00:40:10,233
Oh, he's too close.

402
00:40:11,748 --> 00:40:14,061
Ah, I can't get a clean shot.

403
00:40:16,343 --> 00:40:18,042
Turn on the walkie.

404
00:40:27,213 --> 00:40:30,126
This is Dr. Eugene Porter,
Chief Engineer, speaking.

405
00:40:30,228 --> 00:40:31,923
I'm gonna need a dozen more walkers,

406
00:40:32,032 --> 00:40:34,057
a.k.a. "dead ones"
in the local parlance,

407
00:40:34,082 --> 00:40:37,057
for expanding the fence protocol PDQ.

408
00:40:37,196 --> 00:40:39,011
Actually, PFQ.

409
00:40:39,285 --> 00:40:42,053
This is per Negan, who I also am,

410
00:40:42,078 --> 00:40:44,106
even if it takes all night
to procure them,

411
00:40:44,225 --> 00:40:46,201
so I need able-bodied persons out there

412
00:40:46,277 --> 00:40:47,560
in the surrounding blocks of buildings

413
00:40:47,663 --> 00:40:49,450
snoopin' and snatchin', stat.

414
00:40:49,647 --> 00:40:51,948
Negan will be indisposed in the boudoir,

415
00:40:52,016 --> 00:40:53,516
so in the meantime,

416
00:40:53,675 --> 00:40:56,409
any questions should be
directed to yours truly.

417
00:40:56,675 --> 00:40:59,534
Dr. Eugene Porter, Chief Engineer,

418
00:40:59,613 --> 00:41:03,140
also known as Negan, who I am.

419
00:41:04,495 --> 00:41:06,468
Change of plans.

420
00:41:11,369 --> 00:41:13,279
So we go in.

421
00:41:15,326 --> 00:41:17,037
I'm ready.

422
00:41:34,213 --> 00:41:35,679
Come in.

423
00:41:40,138 --> 00:41:41,770
Freddie said you wanted to see me?

424
00:41:41,878 --> 00:41:43,784
Yes, have a seat.

425
00:41:49,946 --> 00:41:52,480
I noticed you've been slacking
on your scavenging

426
00:41:52,531 --> 00:41:53,998
and recruiting lately.

427
00:41:54,066 --> 00:41:58,016
And there are too many people
in your trailer.

428
00:41:58,187 --> 00:41:59,753
Fire hazard.

429
00:41:59,938 --> 00:42:03,789
Plenty of room here at
Barrington House for our guests.

430
00:42:03,898 --> 00:42:05,665
Speaking of which,

431
00:42:05,789 --> 00:42:09,079
I have job assignments
for the newcomers.

432
00:42:09,799 --> 00:42:11,507
People are gonna stay,
they're gonna chip in.

433
00:42:11,532 --> 00:42:13,626
No more free rides on the teat.

434
00:42:13,746 --> 00:42:15,751
You let them take Dr. Carson.

435
00:42:15,887 --> 00:42:17,970
Surprised you didn't kneel again.

436
00:42:18,156 --> 00:42:19,597
You must be really worried

437
00:42:19,622 --> 00:42:21,465
if you're trying to split everyone up.

438
00:42:23,598 --> 00:42:25,161
Shouldn't talk to me like that.

439
00:42:25,845 --> 00:42:27,179
Who knows what might happen

440
00:42:27,204 --> 00:42:29,602
with all these Saviors coming around?

441
00:42:30,454 --> 00:42:32,315
Did you just threaten me?

442
00:42:32,477 --> 00:42:34,860
I was being sarcastic, okay?

443
00:42:35,016 --> 00:42:37,948
- No, I think you just threatened me.
- Think what you want.

444
00:42:38,082 --> 00:42:40,772
You know, what I'm saying is,
I look out for my friends,

445
00:42:40,808 --> 00:42:44,345
and I realized... we're not friends.

446
00:42:44,821 --> 00:42:47,087
Come in.

447
00:42:48,949 --> 00:42:51,259
Ah, wonderful... tequila.

448
00:42:51,845 --> 00:42:54,019
Crack that open for me, would you?

449
00:42:54,499 --> 00:42:55,799
You may go now.

450
00:42:56,094 --> 00:42:58,276
Show him out, will you, Kal?

451
00:43:13,381 --> 00:43:14,497
Everything okay?

452
00:43:14,600 --> 00:43:16,333
Where's Sasha and Rosita?

453
00:43:20,980 --> 00:43:22,505
You see that over there?

454
00:43:22,671 --> 00:43:25,529
That is a half-assed attempt
at a safety protocol.

455
00:43:25,785 --> 00:43:28,208
We need a full-assed, full-court press.

456
00:43:28,233 --> 00:43:31,152
That stuff out front with the spikes
and the chains and the heads,

457
00:43:31,184 --> 00:43:32,208
that's good for the optics.

458
00:43:32,233 --> 00:43:33,786
It sends a message to would-be intruders

459
00:43:33,811 --> 00:43:35,929
of the "Don't Mess with Texas" variety.

460
00:43:37,515 --> 00:43:40,474
But we need to drill down
on actual safety protocol,

461
00:43:40,921 --> 00:43:43,054
especially at points
that have proven to be janky.

462
00:43:43,079 --> 00:43:44,378
Well, yeah.

463
00:43:44,570 --> 00:43:46,589
That's why I'm on guard duty now.

464
00:43:46,739 --> 00:43:49,057
No offense to that last guy,

465
00:43:49,159 --> 00:43:50,564
but he could hardly handle a gun.

466
00:43:54,108 --> 00:43:56,842
Get up. You're getting out.

467
00:43:59,503 --> 00:44:02,675
Eugene, get up. We're breaking you out.

468
00:44:05,789 --> 00:44:07,456
Eugene!

469
00:44:10,163 --> 00:44:11,555
No.

470
00:44:12,045 --> 00:44:13,860
I'm not going with you.

471
00:44:14,035 --> 00:44:15,268
What?

472
00:44:16,853 --> 00:44:19,710
I didn't ask you to come, so go.

473
00:44:22,060 --> 00:44:24,234
People got to be en route.

474
00:44:35,541 --> 00:44:39,117
That weak, lying sack of shit.

475
00:44:42,083 --> 00:44:44,442
This is it. I'm going in.

476
00:44:46,806 --> 00:44:48,239
Hold on.

477
00:44:48,489 --> 00:44:49,803
This is gonna take a second.

478
00:44:49,904 --> 00:44:52,139
More could be coming back.
Keep an eye out.

479
00:44:59,977 --> 00:45:01,908
Just another second.

480
00:45:09,120 --> 00:45:11,112
What the hell are you doing?

481
00:45:11,237 --> 00:45:12,498
Go.

482
00:45:12,682 --> 00:45:14,290
It's not your time.

483
00:45:14,854 --> 00:45:17,221
There's got to be a point to it, right?

484
00:45:18,112 --> 00:45:19,570
They need you.

485
00:45:57,746 --> 00:45:59,091
Damn it.

486
00:45:59,633 --> 00:46:01,367
Damn it!

487
00:46:10,017 --> 00:46:15,017
Sync & corrections by honeybunny & kinglouisxx
<font color="#ec14bd">www.addic7ed.com</font>

