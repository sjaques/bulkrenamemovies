1
00:00:35,000 --> 00:00:37,868
They ...
They have guns.

2
00:00:37,870 --> 00:00:39,202
A lot of them.

3
00:00:39,204 --> 00:00:40,537
I saw it.

4
00:00:40,539 --> 00:00:41,672
What?

5
00:00:41,674 --> 00:00:43,106
That group.

6
00:00:43,108 --> 00:00:46,376
Um, the women I met.

7
00:00:46,378 --> 00:00:50,013
They have an armory.
They have guns.

8
00:00:50,015 --> 00:00:52,316
Why didn't you tell us before now?

9
00:00:52,318 --> 00:00:54,618
I made a promise, Rick.

10
00:01:00,326 --> 00:01:02,793
Do you ever think
about who you've killed?

11
00:01:07,666 --> 00:01:09,967
Yeah.

12
00:01:17,576 --> 00:01:20,143
Are you ready if this goes south?

13
00:01:20,145 --> 00:01:21,945
It won't.

14
00:01:21,947 --> 00:01:24,348
If it does, you don't need to feel bad.

15
00:01:24,350 --> 00:01:25,882
I<i> do</i> feel bad.

16
00:01:25,884 --> 00:01:26,917
I will.

17
00:01:26,919 --> 00:01:28,318
Tara...

18
00:01:28,507 --> 00:01:30,726
you don't have to.

19
00:02:16,778 --> 00:02:21,219
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

20
00:02:24,600 --> 00:02:26,507
You need to cover this with a tarp.

21
00:02:26,639 --> 00:02:29,173
- A tarp?
- Tarp.

22
00:02:29,375 --> 00:02:30,574
Why?

23
00:02:30,576 --> 00:02:32,609
Think about it.

24
00:02:32,611 --> 00:02:34,278
Okay. Just tell me.

25
00:02:36,582 --> 00:02:38,215
We want warmth

26
00:02:38,217 --> 00:02:40,317
so they'll put out shoots
before the last freeze.

27
00:02:40,319 --> 00:02:42,853
That way, we pull back the tarp,
they die naturally,

28
00:02:42,855 --> 00:02:45,088
saves on all the endless weeding.

29
00:02:45,090 --> 00:02:47,090
Get to tarping.

30
00:02:47,092 --> 00:02:48,659
You got it, boss lady.

31
00:02:48,661 --> 00:02:51,795
Uh, wait.
Where are you going?

32
00:02:51,797 --> 00:02:53,730
I, uh ...
I'm not a farmer.

33
00:02:53,732 --> 00:02:55,265
I need some help here.

34
00:02:55,267 --> 00:02:56,934
No, you don't.

35
00:02:56,936 --> 00:02:58,936
I saw a wild blueberry bush
growing outside.

36
00:02:58,938 --> 00:03:00,337
If it's young enough,

37
00:03:00,339 --> 00:03:02,039
I'm gonna pull it up
and replant it here.

38
00:03:02,041 --> 00:03:04,808
Good one will produce for over 40 years.

39
00:03:04,810 --> 00:03:07,311
We got to start acting
like we'll be around that long.

40
00:03:08,914 --> 00:03:11,415
Cool. Yeah.

41
00:03:11,417 --> 00:03:13,750
Just tarp.
Don't touch.

42
00:03:13,752 --> 00:03:15,852
I'll see how you do after.

43
00:03:27,633 --> 00:03:30,534
Once you're up, watch for the signal.

44
00:03:32,004 --> 00:03:33,971
What time does the clock start?

45
00:03:33,973 --> 00:03:36,006
Soon as she's in.

46
00:03:38,844 --> 00:03:41,612
You good?

47
00:03:41,614 --> 00:03:44,114
I'm good enough.

48
00:04:03,068 --> 00:04:06,870
I'm glad you came.
I know it's been hard.

49
00:04:06,872 --> 00:04:08,839
- I know you don't want this.
- <i>Didn't</i> want this,

50
00:04:08,841 --> 00:04:10,707
but I'm starting to understand
what's on the menu now.

51
00:04:10,709 --> 00:04:12,142
We can hide like these people,

52
00:04:12,144 --> 00:04:14,044
we can keep trying to serve The Saviors,

53
00:04:14,046 --> 00:04:15,979
and I can watch you get
randomly beaten every so often,

54
00:04:15,981 --> 00:04:17,047
or else we can fight.

55
00:04:17,049 --> 00:04:18,849
So, here we are.

56
00:04:21,020 --> 00:04:24,421
And being with you, well...

57
00:04:24,423 --> 00:04:26,790
it makes abject terror tolerable.

58
00:04:32,097 --> 00:04:35,432
I should've tried harder
to stop Rosita and Sasha.

59
00:04:35,434 --> 00:04:37,868
If they'd have just waited
one more day...

60
00:04:37,870 --> 00:04:39,736
Well, Sasha's a good shot,

61
00:04:39,738 --> 00:04:43,674
and Rosita knows
how to take care of herself.

62
00:04:43,676 --> 00:04:46,777
Probably back at Hilltop right now.

63
00:04:46,779 --> 00:04:49,680
Least I hope so.
We're gonna need 'em.

64
00:04:49,682 --> 00:04:51,715
There's a whole lot of people
still got to die.

65
00:05:07,633 --> 00:05:09,733
Hey, there.

66
00:05:09,735 --> 00:05:12,402
You okay?

67
00:05:12,404 --> 00:05:15,605
Can I get some water?

68
00:05:26,618 --> 00:05:29,319
I should introduce myself.

69
00:05:29,321 --> 00:05:32,055
I'm David.

70
00:05:32,057 --> 00:05:36,860
I wonder if you remember me
from last night.

71
00:05:36,862 --> 00:05:39,162
Do you?

72
00:05:39,164 --> 00:05:41,565
No.

73
00:05:43,769 --> 00:05:46,136
Well, I can forgive that.

74
00:05:46,138 --> 00:05:48,472
Last night was kind of a shitstorm.

75
00:05:48,474 --> 00:05:52,442
I was the guy that gave the rope
to the guys that tied you up.

76
00:05:52,444 --> 00:05:56,146
I always keep some close.

77
00:05:56,148 --> 00:05:59,182
There's just all kinds
of fun and interesting things

78
00:05:59,184 --> 00:06:03,820
you can do with rope.

79
00:06:03,822 --> 00:06:06,790
God, you're pretty.

80
00:06:12,698 --> 00:06:15,298
If I give you some water,

81
00:06:15,300 --> 00:06:19,870
which is not something
I'm supposed to do,

82
00:06:19,872 --> 00:06:22,973
will you do something for me?

83
00:06:22,975 --> 00:06:26,476
Something you're not supposed to do?

84
00:06:26,478 --> 00:06:28,345
Will you?

85
00:06:40,626 --> 00:06:44,895
Tell me how thirsty you are.

86
00:06:48,567 --> 00:06:51,168
Go to hell.

87
00:06:55,841 --> 00:06:59,042
Oh, shit.

88
00:07:02,614 --> 00:07:05,615
Fighting's just gonna
make it last longer.

89
00:07:08,053 --> 00:07:12,556
Which, for the record, is fine by me.

90
00:07:12,558 --> 00:07:14,724
David!

91
00:07:16,195 --> 00:07:18,228
What the hell are you doing in here?

92
00:07:18,230 --> 00:07:19,763
Negan, sir ...

93
00:07:19,765 --> 00:07:22,332
Do you really think
I need you to answer that?

94
00:07:22,334 --> 00:07:25,068
I can see that you're trying
to rape this woman.

95
00:07:25,070 --> 00:07:29,573
You were trying to rape
this woman, weren't you?

96
00:07:37,249 --> 00:07:40,283
This is some unacceptable behavior.

97
00:07:40,285 --> 00:07:43,253
Rape is against the rules here.

98
00:07:43,255 --> 00:07:46,289
I wouldn't wanna be somewhere
where it wasn't.

99
00:07:46,291 --> 00:07:52,562
Someone in charge who let
something like that fly...

100
00:07:52,564 --> 00:07:55,532
Whew.

101
00:08:00,629 --> 00:08:02,111
David.

102
00:08:03,505 --> 00:08:06,302
You really crossed a line here.

103
00:08:07,579 --> 00:08:09,646
I'm sorry, sir.

104
00:08:16,255 --> 00:08:18,579
You know what?

105
00:08:18,724 --> 00:08:21,157
I do not accept your apology.

106
00:08:29,601 --> 00:08:31,589
Hey!

107
00:08:32,738 --> 00:08:34,503
Get, uh...

108
00:08:36,167 --> 00:08:37,971
Sasha.

109
00:08:39,460 --> 00:08:42,101
That is a beautiful name.

110
00:08:43,982 --> 00:08:47,233
Get Sasha here
a new T-shirt.

111
00:08:53,692 --> 00:08:55,625
I'm sorry you had to see that.

112
00:08:55,627 --> 00:08:58,194
Sorry 'bout the rope, too.

113
00:08:58,196 --> 00:08:59,763
Probably overkill,

114
00:08:59,765 --> 00:09:04,401
but you did cause
one hell of a fracas last night.

115
00:09:09,474 --> 00:09:13,209
Oh, shit.

116
00:09:14,546 --> 00:09:17,347
I remember you.

117
00:09:17,349 --> 00:09:20,483
Yeah.

118
00:09:20,485 --> 00:09:22,519
You were there.

119
00:09:25,023 --> 00:09:28,725
Oh, hell.
I get it now.

120
00:09:28,727 --> 00:09:30,894
I got to hand it to you.

121
00:09:30,896 --> 00:09:34,164
You've got some beach-ball-sized
lady nuts on you,

122
00:09:34,166 --> 00:09:36,733
coming in all kamikaze like that.

123
00:09:36,735 --> 00:09:38,335
Big question here,

124
00:09:38,337 --> 00:09:43,340
and I need the truth on this one ...

125
00:09:43,342 --> 00:09:46,743
Did Rick put you up to this?

126
00:09:46,745 --> 00:09:50,580
Rick?

127
00:09:50,582 --> 00:09:52,816
Your bitch?

128
00:09:52,818 --> 00:09:53,917
No.

129
00:09:55,851 --> 00:09:58,986
Either way, you must've thought
it was gonna be the end,

130
00:09:58,988 --> 00:10:02,022
coming in on your lonesome like that.

131
00:10:02,024 --> 00:10:04,858
But that's not the way it's got to go.

132
00:10:04,860 --> 00:10:06,460
Unh-unh.
Just the opposite.

133
00:10:06,462 --> 00:10:08,495
See, this...

134
00:10:08,497 --> 00:10:11,999
Well, this could be the beginning.

135
00:10:22,044 --> 00:10:26,446
This knife is yours now.

136
00:10:26,448 --> 00:10:29,616
You can try to use it, take me out,

137
00:10:29,618 --> 00:10:32,819
but considering I am standing above you,

138
00:10:32,821 --> 00:10:35,155
holding a baseball bat,

139
00:10:35,157 --> 00:10:37,858
that doesn't seem real smart.

140
00:10:37,860 --> 00:10:40,661
Now, you can use it to slit your wrists,

141
00:10:40,663 --> 00:10:43,463
which would be a damn shame,
but I get it.

142
00:10:43,465 --> 00:10:46,433
You're obviously not
on the shiny side of the street,

143
00:10:46,435 --> 00:10:48,802
given recent events.

144
00:10:48,804 --> 00:10:51,872
Now, you can sit there and do nothin',

145
00:10:51,874 --> 00:10:56,243
wait for ol' David to come back
to life and eat your face,

146
00:10:56,245 --> 00:10:58,612
also a damn shame and kinda nuts,

147
00:10:58,614 --> 00:11:01,181
but, hell, to each their own.

148
00:11:01,183 --> 00:11:08,722
Or you can use that blade
and stop ol' Rapey Davey

149
00:11:08,724 --> 00:11:12,693
from becoming
Dead-Alive Rapey Davey,

150
00:11:12,695 --> 00:11:17,597
save yourself, join the cause.

151
00:11:20,035 --> 00:11:22,736
I know what I'd do.

152
00:11:24,006 --> 00:11:26,239
What?

153
00:11:26,241 --> 00:11:28,875
I'm a man short.

154
00:11:28,877 --> 00:11:31,445
Hell, you can't really call
this piece of shit a man,

155
00:11:31,447 --> 00:11:33,046
but still, I'm short,

156
00:11:33,048 --> 00:11:35,749
and you got those
beach-ball-sized lady nuts,

157
00:11:35,751 --> 00:11:39,720
and I wanna harness
the heat comin' off of 'em.

158
00:11:39,722 --> 00:11:44,191
You can help me run this place one day,

159
00:11:44,248 --> 00:11:48,272
all of us together, following the rules,

160
00:11:48,297 --> 00:11:50,130
workin' on the same side of things.

161
00:11:50,132 --> 00:11:52,799
That's all this was ever about.

162
00:11:52,801 --> 00:11:56,066
And it still can be for you.

163
00:11:56,864 --> 00:12:00,785
I know it's hard to picture,
considering what I did.

164
00:12:01,867 --> 00:12:06,397
But, Sasha, we all got shit to get over.

165
00:12:13,693 --> 00:12:16,127
Take some time.
Think about it.

166
00:12:16,152 --> 00:12:18,832
Whatever you decide, so it shall be.

167
00:12:18,857 --> 00:12:20,730
No pressure.

168
00:12:21,342 --> 00:12:24,202
And, again,
I am sorry you had to see that,

169
00:12:24,227 --> 00:12:28,053
even though I know
you have seen some things.

170
00:12:29,273 --> 00:12:31,631
I just want you to understand...

171
00:12:33,142 --> 00:12:35,397
we are not monsters.

172
00:13:17,434 --> 00:13:20,168
What did they do to you?

173
00:13:23,273 --> 00:13:25,907
They're keepin' me safe.

174
00:13:29,112 --> 00:13:31,813
I brought you some creature comforts.

175
00:13:34,517 --> 00:13:37,652
Full disclosure, the pillow
is not hypoallergenic.

176
00:13:39,489 --> 00:13:40,989
There was some debate

177
00:13:40,991 --> 00:13:43,091
as to whether or not
I should come in here.

178
00:13:43,093 --> 00:13:44,626
They said in the event
of a hostage situation

179
00:13:44,628 --> 00:13:45,994
with the pig sticker

180
00:13:45,996 --> 00:13:48,129
that I would fully be on my own.

181
00:13:48,131 --> 00:13:51,032
Just wanted to share that.

182
00:13:55,372 --> 00:13:58,673
Negan relayed to me his offer to you.

183
00:13:58,675 --> 00:14:01,142
It's my opinion that
you should accept said offer,

184
00:14:01,144 --> 00:14:03,344
just as I did.

185
00:14:03,346 --> 00:14:04,979
I know it's a hold-your-nose
situation,

186
00:14:04,981 --> 00:14:06,814
but taking up with the management here

187
00:14:06,816 --> 00:14:09,317
was indeed the correct select.

188
00:14:12,656 --> 00:14:15,189
You know, I believed I could be brave,

189
00:14:15,191 --> 00:14:17,392
that I could be a survivor
like Abraham was,

190
00:14:17,394 --> 00:14:21,796
and ridin' that RV
into the sunset was...

191
00:14:21,798 --> 00:14:25,533
some of the greatest 37 minutes
of my existence.

192
00:14:25,535 --> 00:14:28,836
But I know now that
I was full-tilt delusional.

193
00:14:31,641 --> 00:14:34,609
I have never in my life been as scared

194
00:14:34,611 --> 00:14:37,278
as I was that night in the woods,

195
00:14:37,280 --> 00:14:39,547
kit to the grit,

196
00:14:39,549 --> 00:14:41,716
fully believing my number's
about to be called,

197
00:14:41,718 --> 00:14:44,085
then seeing it happen
to someone brave...

198
00:14:45,789 --> 00:14:48,723
...to a survivor.

199
00:14:48,725 --> 00:14:50,358
And then
the pants-pissing terror

200
00:14:50,360 --> 00:14:53,461
of the rinse and repeat
of that very same event ...

201
00:14:53,463 --> 00:14:54,662
I just...

202
00:14:56,733 --> 00:14:58,433
I couldn't.

203
00:15:01,938 --> 00:15:05,606
Being here means
I'll never have to again.

204
00:15:05,608 --> 00:15:09,544
And neither will you
if you just say "yes."

205
00:15:09,546 --> 00:15:12,680
Please say "yes."

206
00:15:15,218 --> 00:15:17,085
He would want you to.

207
00:15:24,394 --> 00:15:26,527
He wouldn't.

208
00:15:26,529 --> 00:15:29,063
I do.

209
00:15:29,065 --> 00:15:31,899
We're still here.

210
00:15:31,901 --> 00:15:33,768
Things have changed.
We can change with 'em.

211
00:15:33,770 --> 00:15:35,703
We have to.

212
00:15:40,276 --> 00:15:41,976
Go.

213
00:15:54,758 --> 00:15:56,424
He's, uh, starting to...

214
00:15:56,426 --> 00:15:58,392
Please.

215
00:15:59,863 --> 00:16:01,729
Go.

216
00:16:52,115 --> 00:16:55,650
Don't think I've seen you
outside the walls.

217
00:16:55,652 --> 00:16:59,020
Well, I built these walls to use them.

218
00:16:59,022 --> 00:17:02,557
If I want greenery, I eat a salad.

219
00:17:02,559 --> 00:17:04,192
Then why are you here?

220
00:17:04,194 --> 00:17:06,360
I was hoping we could talk.

221
00:17:06,362 --> 00:17:08,196
Then I saw you come out here alone.

222
00:17:08,198 --> 00:17:10,031
I was concerned.

223
00:17:10,033 --> 00:17:12,366
Where are your friends?

224
00:17:14,204 --> 00:17:17,471
They went out.

225
00:17:17,473 --> 00:17:20,174
They're looking for something they need.

226
00:17:20,176 --> 00:17:22,243
Without you?

227
00:17:22,245 --> 00:17:27,682
Now, I'm just wondering ...
Why you still here, Maggie?

228
00:17:27,684 --> 00:17:29,150
Hmm?

229
00:17:29,152 --> 00:17:30,518
With Carson gone...

230
00:17:30,520 --> 00:17:33,921
I heard you were gonna get him back.

231
00:17:33,923 --> 00:17:35,756
That not true?

232
00:17:37,227 --> 00:17:39,093
Was it just something you said?

233
00:17:39,095 --> 00:17:41,195
Maggie, you've got to start trusting me.

234
00:17:41,197 --> 00:17:43,431
You know, people are tense,
they're worried.

235
00:17:43,433 --> 00:17:46,033
Everybody'd be a hell of a lot better

236
00:17:46,035 --> 00:17:50,204
if ... if we could present
a-a united front.

237
00:17:50,206 --> 00:17:53,608
It would be a lot easier if we
actually were a united front.

238
00:17:53,610 --> 00:17:55,509
You're right.

239
00:17:55,511 --> 00:17:59,280
I need to be more open
to working together.

240
00:17:59,282 --> 00:18:01,449
I apologize.

241
00:18:01,451 --> 00:18:03,417
I never intended for things between us

242
00:18:03,419 --> 00:18:06,053
to come to this point.

243
00:18:06,055 --> 00:18:07,955
I appreciate that.

244
00:18:10,560 --> 00:18:12,126
It's never too late to change.

245
00:18:12,128 --> 00:18:13,394
You know that?

246
00:18:18,434 --> 00:18:21,535
If you're serious, I'll come by
later and we can talk more.

247
00:18:21,537 --> 00:18:23,070
Fine.

248
00:18:23,072 --> 00:18:26,407
Great.

249
00:18:30,880 --> 00:18:33,814
I thought this'd just take a second.

250
00:18:33,816 --> 00:18:36,817
Would you mind keeping
an eye out while I finish up?

251
00:18:36,819 --> 00:18:38,185
Could you do that?

252
00:18:38,187 --> 00:18:39,587
Happy to.

253
00:19:25,768 --> 00:19:27,034
Oh!

254
00:19:27,036 --> 00:19:28,803
We got one.

255
00:19:28,805 --> 00:19:30,504
I'll do it.

256
00:19:30,506 --> 00:19:34,508
Uh, uh... I'm here
to protect the pregnant lady.

257
00:19:34,510 --> 00:19:36,243
Don't need the pregnant lady
to protect me.

258
00:19:38,114 --> 00:19:39,981
Have you ever killed one before?

259
00:19:39,983 --> 00:19:41,849
I wouldn't be here if I hadn't.

260
00:19:46,990 --> 00:19:49,023
It's fine.
I got it.

261
00:19:49,025 --> 00:19:50,224
No.

262
00:20:00,303 --> 00:20:02,870
Oh, Jesus.

263
00:20:02,872 --> 00:20:06,273
Fine. Do it.

264
00:20:20,690 --> 00:20:22,890
Maggie!

265
00:20:25,995 --> 00:20:28,029
Please help me!

266
00:20:38,408 --> 00:20:40,541
You okay?

267
00:20:50,353 --> 00:20:53,254
He hasn't killed one before.

268
00:20:53,256 --> 00:20:54,789
He's learning.

269
00:20:54,791 --> 00:20:56,791
That's not what he told us.

270
00:21:02,598 --> 00:21:04,565
Thanks for the help.

271
00:21:18,481 --> 00:21:22,049
Ooh. You catch that
yourself?

272
00:21:22,051 --> 00:21:23,818
- Shit, yeah.
- Rachel.

273
00:21:23,820 --> 00:21:25,986
I mean, yeah.

274
00:21:25,988 --> 00:21:27,288
Gonna go clean it.

275
00:21:27,290 --> 00:21:28,589
Okay.

276
00:21:38,234 --> 00:21:39,934
Hi.

277
00:21:48,112 --> 00:21:50,380
Stay quiet

278
00:21:50,382 --> 00:21:52,062
and put your hands on your head.

279
00:21:52,182 --> 00:21:54,885
We should've just killed you
in the village.

280
00:21:57,243 --> 00:21:59,076
You're gonna be glad you didn't.

281
00:21:59,078 --> 00:22:00,611
You're not alone, are you?

282
00:22:00,613 --> 00:22:01,745
Sit.

283
00:22:01,747 --> 00:22:02,913
What do you want?

284
00:22:02,915 --> 00:22:04,448
I want you to sit.

285
00:22:13,526 --> 00:22:15,559
Hey, Cyndie.

286
00:22:19,598 --> 00:22:21,198
You promised.

287
00:22:21,200 --> 00:22:23,367
Yeah. I did.

288
00:22:23,369 --> 00:22:25,169
Put your hands on your head.

289
00:22:33,479 --> 00:22:35,145
I didn't have a choice.

290
00:22:35,147 --> 00:22:38,015
Why are you here?

291
00:22:38,017 --> 00:22:40,451
I said I didn't have a choice.

292
00:22:40,453 --> 00:22:42,553
But you do, both of you do.

293
00:22:42,555 --> 00:22:43,887
My friends are out there right now,

294
00:22:43,889 --> 00:22:45,055
and they're gonna take this place,

295
00:22:45,057 --> 00:22:46,623
hopefully without firing a shot.

296
00:22:46,625 --> 00:22:48,892
What? You're ... You're gonna
"take this place"?

297
00:22:48,894 --> 00:22:49,893
What are you talking about?

298
00:22:49,895 --> 00:22:51,228
We need your guns.

299
00:22:51,230 --> 00:22:52,696
What?

300
00:22:52,698 --> 00:22:54,198
We're gonna fight The Saviors.

301
00:22:54,200 --> 00:22:55,799
- You should join us.
- No.

302
00:22:55,801 --> 00:22:57,501
The Saviors killed your fathers,

303
00:22:57,503 --> 00:23:00,337
your brothers, husbands, sons,
and you ran from them.

304
00:23:00,339 --> 00:23:02,306
I thought we got rid of them.

305
00:23:02,308 --> 00:23:04,141
Beatrice said there was more
out there, and she was right.

306
00:23:04,143 --> 00:23:05,509
They came back.

307
00:23:05,511 --> 00:23:08,412
They killed my friends.

308
00:23:08,414 --> 00:23:10,380
They killed my girlfriend.

309
00:23:10,382 --> 00:23:12,916
They took us over.
They took everything from us.

310
00:23:12,918 --> 00:23:16,220
We do whatever
The Saviors tell us to do,

311
00:23:16,222 --> 00:23:18,021
and they think we're still
doing that, but we're not.

312
00:23:18,023 --> 00:23:19,389
We're gonna fight them.

313
00:23:19,391 --> 00:23:21,492
And we have other communities beside us,

314
00:23:21,494 --> 00:23:23,393
and with Oceanside,
we would have an army.

315
00:23:23,395 --> 00:23:25,329
So, that's it?

316
00:23:25,331 --> 00:23:27,831
"Thanks for the guns.
You should join us"?

317
00:23:27,833 --> 00:23:29,900
If we fight them together,
we can beat them.

318
00:23:29,902 --> 00:23:31,335
We have to try.

319
00:23:31,337 --> 00:23:33,670
They'll win, Tara.
I've seen it. So have you.

320
00:23:33,672 --> 00:23:36,173
Just talk to the leader
of my group, Rick.

321
00:23:36,175 --> 00:23:38,041
Just talk, and we don't
have to take over this place.

322
00:23:38,043 --> 00:23:39,376
We don't want to.

323
00:23:39,378 --> 00:23:40,677
I can give them the signal to stop this,

324
00:23:40,679 --> 00:23:42,779
but you have to tell me right now.

325
00:23:44,884 --> 00:23:47,551
Listen. We're not
just hiding in the woods.

326
00:23:47,553 --> 00:23:50,220
We're doing something.
We're not giving up.

327
00:23:50,222 --> 00:23:52,890
I didn't want to break
my promise, Cyndie, okay?

328
00:23:52,892 --> 00:23:56,293
But the world can belong
to good people, to fair people,

329
00:23:56,295 --> 00:23:58,562
if we're all just brave enough to try.

330
00:23:58,564 --> 00:23:59,730
We are not good,

331
00:23:59,732 --> 00:24:01,331
and we are not brave.

332
00:24:01,333 --> 00:24:02,633
Neither you nor me.

333
00:24:02,635 --> 00:24:04,001
She is.

334
00:24:04,003 --> 00:24:05,469
That's why we're in this mess.

335
00:24:06,872 --> 00:24:08,338
Look, Tara, if you take our guns,

336
00:24:08,340 --> 00:24:09,940
you might as well be killing us all.

337
00:24:09,942 --> 00:24:11,141
That's what you'll be doing.

338
00:24:11,143 --> 00:24:12,809
We should talk to them.

339
00:24:12,811 --> 00:24:14,211
No.

340
00:24:14,213 --> 00:24:15,812
We can try to stop this.

341
00:24:15,814 --> 00:24:17,281
No, Cyndie.

342
00:24:20,953 --> 00:24:22,920
It doesn't matter.

343
00:24:24,256 --> 00:24:25,722
You can't.

344
00:24:30,496 --> 00:24:32,930
Go! Go!

345
00:24:36,202 --> 00:24:37,968
The arsenal!

346
00:24:53,018 --> 00:24:55,352
On the ground. Now.

347
00:24:55,354 --> 00:24:58,355
Those hands ...
Put them on your head.

348
00:25:01,694 --> 00:25:03,694
Please.

349
00:25:22,047 --> 00:25:24,214
Your people killing my people?

350
00:25:24,216 --> 00:25:26,483
The bombs were outside the walls.

351
00:25:26,485 --> 00:25:28,318
You could've stopped this.

352
00:25:28,320 --> 00:25:29,519
Look, I hope everyone is okay.

353
00:25:29,521 --> 00:25:30,554
I do.

354
00:25:30,556 --> 00:25:31,688
They better be.

355
00:25:31,690 --> 00:25:33,357
Stand up.

356
00:25:33,359 --> 00:25:34,891
It's time to move.

357
00:25:40,199 --> 00:25:41,431
It's my legs.

358
00:25:41,433 --> 00:25:43,166
Stop, stop.
Back up. Back up.

359
00:25:43,168 --> 00:25:44,668
Cyndie, stop!

360
00:25:51,277 --> 00:25:52,709
- It's empty.
- What?

361
00:25:52,711 --> 00:25:54,611
It's unloaded.

362
00:25:54,613 --> 00:25:57,481
We didn't come here to hurt anybody.

363
00:25:57,483 --> 00:26:00,250
Cyndie, give it here.

364
00:26:00,252 --> 00:26:02,486
Now.

365
00:26:02,488 --> 00:26:05,555
Natania, just talk to Rick.

366
00:26:05,557 --> 00:26:07,357
Shut up.

367
00:26:10,396 --> 00:26:11,928
Everybody down!

368
00:26:11,930 --> 00:26:13,130
Hands on your heads.

369
00:26:13,132 --> 00:26:14,398
Everybody stay calm.

370
00:26:14,400 --> 00:26:15,932
We don't want anyone to get hurt.

371
00:26:15,934 --> 00:26:17,668
Stay down and listen to what we say.

372
00:26:17,670 --> 00:26:21,305
We want this to go as simply
and as peacefully as possible.

373
00:26:21,307 --> 00:26:23,273
All of you can make it that way.

374
00:26:23,275 --> 00:26:25,609
Get down over there.
Keep quiet.

375
00:26:28,147 --> 00:26:30,580
Now, we made a lot of noise.

376
00:26:30,582 --> 00:26:33,383
We want to wrap this up quick
so you can send people

377
00:26:33,385 --> 00:26:35,686
to redirect anything coming this way.

378
00:26:35,688 --> 00:26:38,221
Tara said your forests
are relatively clear,

379
00:26:38,223 --> 00:26:39,856
so we won't take any chances.

380
00:26:39,858 --> 00:26:44,361
No one needs to get hurt.

381
00:26:44,363 --> 00:26:47,798
This is just about
what you have, what we need.

382
00:26:47,800 --> 00:26:50,033
Nobody's taking anything.

383
00:26:50,035 --> 00:26:54,971
You need to let everyone go
and leave right now.

384
00:26:54,973 --> 00:26:56,807
Just walk away or this one dies.

385
00:26:56,809 --> 00:26:59,876
Yeah, we'll leave you alone.

386
00:26:59,878 --> 00:27:01,745
But we're taking your weapons with us.

387
00:27:01,747 --> 00:27:03,146
That's not gonna change.

388
00:27:03,148 --> 00:27:06,483
It's Natania, right?

389
00:27:06,485 --> 00:27:11,288
Put the gun down, and let's talk
about what we<i> can</i> change.

390
00:27:11,290 --> 00:27:12,322
No.

391
00:27:12,324 --> 00:27:13,657
Leave right now.

392
00:27:15,227 --> 00:27:18,428
Michonne, don't!

393
00:27:18,430 --> 00:27:21,164
We just wanna be left alone.

394
00:27:21,166 --> 00:27:23,266
Yeah, we'll leave you alone.

395
00:27:23,268 --> 00:27:25,736
Just let go of her.
Now.

396
00:27:25,738 --> 00:27:29,573
Or we'll kill you.

397
00:27:29,575 --> 00:27:31,141
None of us want that.

398
00:27:31,143 --> 00:27:33,176
They want us to fight The Saviors.

399
00:27:36,181 --> 00:27:38,448
We tried that.
We lost. Too much.

400
00:27:38,450 --> 00:27:40,150
We're not gonna lose anymore ...

401
00:27:40,152 --> 00:27:42,986
not our guns, not our safety,

402
00:27:42,988 --> 00:27:44,454
not after everything
we've done to get here.

403
00:27:44,456 --> 00:27:46,857
We're gonna win ...

404
00:27:46,859 --> 00:27:49,960
with your guns,
with or without your help.

405
00:27:55,033 --> 00:27:58,101
Natania, put the gun down.

406
00:27:58,103 --> 00:28:00,203
You kill me, and you die.

407
00:28:00,205 --> 00:28:03,140
And my people take the guns
and nothing changes.

408
00:28:07,479 --> 00:28:09,546
Maybe we should try.

409
00:28:12,184 --> 00:28:13,950
Grandma, stop.

410
00:28:13,952 --> 00:28:15,619
It's over.

411
00:28:15,621 --> 00:28:16,720
Just talk to them, okay?

412
00:28:16,722 --> 00:28:19,322
It's not over!

413
00:28:19,324 --> 00:28:21,591
They've forgotten.

414
00:28:21,593 --> 00:28:23,160
You've all forgotten.

415
00:28:23,162 --> 00:28:26,062
Some of you actually want to fight them?

416
00:28:26,064 --> 00:28:27,531
After everything?

417
00:28:27,533 --> 00:28:30,300
We can lose our guns,

418
00:28:30,302 --> 00:28:32,969
but us leaving this place to fight?

419
00:28:32,971 --> 00:28:34,471
After everything?

420
00:28:34,473 --> 00:28:36,139
I have to remind you!

421
00:28:36,141 --> 00:28:37,641
Yes.

422
00:28:37,643 --> 00:28:40,177
I am gonna do this,
and then I'm gonna die.

423
00:28:40,179 --> 00:28:42,546
But it's that important.

424
00:28:42,548 --> 00:28:43,980
This is your life, all of you.

425
00:28:43,982 --> 00:28:45,849
Remember what it looks like.

426
00:28:45,851 --> 00:28:48,084
Remember what they did to us!

427
00:28:48,086 --> 00:28:49,319
You need to see this.

428
00:28:49,321 --> 00:28:51,054
Open your eyes!

429
00:28:51,056 --> 00:28:52,722
Rick! Walkers!

430
00:28:58,564 --> 00:28:59,930
Everybody up!

431
00:28:59,932 --> 00:29:01,865
Get the children behind us!

432
00:29:01,867 --> 00:29:03,099
They're coming.

433
00:29:03,101 --> 00:29:04,401
First shift, join them on the line.

434
00:29:04,403 --> 00:29:06,736
Knives out.
Dead only. Dead only!

435
00:29:06,738 --> 00:29:08,538
Don't go anywhere.

436
00:29:08,540 --> 00:29:12,108
Everyone, shots
within 10 feet of the line.

437
00:29:12,110 --> 00:29:13,510
That's it.

438
00:29:24,356 --> 00:29:25,722
Now!

439
00:30:32,691 --> 00:30:37,427
No. We're not
fighting them with you.

440
00:30:37,429 --> 00:30:40,830
So take your damn guns and go.

441
00:31:08,554 --> 00:31:11,155
You win.

442
00:31:11,157 --> 00:31:13,157
No.

443
00:31:13,159 --> 00:31:16,427
<i>You</i> win.

444
00:31:19,032 --> 00:31:21,432
Get some people in here.
Clean this up.

445
00:31:33,880 --> 00:31:35,780
What?

446
00:31:35,782 --> 00:31:37,181
You didn't actually think

447
00:31:37,183 --> 00:31:40,084
I was gonna let you keep this, did you?

448
00:31:41,921 --> 00:31:43,854
You still got a ways to go

449
00:31:43,856 --> 00:31:47,691
before I'll believe
you're fully on board here.

450
00:31:47,693 --> 00:31:50,194
Baby steps.

451
00:31:50,196 --> 00:31:53,397
It is absolutely not personal.

452
00:31:53,399 --> 00:31:55,266
If the situation was reversed,

453
00:31:55,268 --> 00:31:58,569
I'm sure you wouldn't
believe me, either.

454
00:31:58,571 --> 00:32:00,871
Still...

455
00:32:00,873 --> 00:32:04,241
the way you saved yourself,

456
00:32:04,243 --> 00:32:07,812
the way you surrendered
to me just now ...

457
00:32:07,814 --> 00:32:09,713
That is a damn good start.

458
00:32:09,715 --> 00:32:14,885
But we got to work
on some things, Sasha.

459
00:32:14,887 --> 00:32:20,558
But I promise you,
I will try to make it fun.

460
00:32:20,560 --> 00:32:23,828
Cards on the table...

461
00:32:23,830 --> 00:32:25,596
a little birdie told me

462
00:32:25,598 --> 00:32:29,900
that Rick and the rest of your
people are up to no good.

463
00:32:29,902 --> 00:32:32,837
And, well, that needs to change,

464
00:32:32,839 --> 00:32:35,639
and you're gonna help me change it.

465
00:32:35,641 --> 00:32:38,642
We're gonna find us
a win-win.

466
00:32:38,644 --> 00:32:39,844
How?

467
00:32:39,846 --> 00:32:41,145
What are you ...

468
00:32:41,147 --> 00:32:42,580
Hey, I'm gonna get you some stuff

469
00:32:42,582 --> 00:32:44,181
to make you more comfortable,

470
00:32:44,183 --> 00:32:47,051
let you have a taste of
what it is we have to offer.

471
00:32:47,053 --> 00:32:48,619
Spend the night enjoying it,

472
00:32:48,621 --> 00:32:51,122
'cause tomorrow...

473
00:32:51,124 --> 00:32:55,159
is gonna be a big day.

474
00:34:02,428 --> 00:34:03,994
Kal!

475
00:34:12,538 --> 00:34:14,104
Yeah?

476
00:34:14,106 --> 00:34:17,107
I'm gonna need you
to drive me somewhere.

477
00:34:17,109 --> 00:34:19,276
You're gonna want to pack a bag.

478
00:34:24,283 --> 00:34:26,417
Hello?

479
00:34:26,419 --> 00:34:28,519
It's Eugene.

480
00:34:28,521 --> 00:34:32,423
Word in the halls is that
you're all-in with things here.

481
00:34:32,425 --> 00:34:34,225
It may not seem like it now,

482
00:34:34,227 --> 00:34:35,559
but I assure you,

483
00:34:35,561 --> 00:34:37,928
you, too, made the correct select.

484
00:34:41,567 --> 00:34:44,068
I was wrong.

485
00:34:45,771 --> 00:34:47,605
I can't.

486
00:34:49,642 --> 00:34:52,676
I won't.

487
00:34:52,678 --> 00:34:54,912
I've been sitting here,

488
00:34:54,914 --> 00:34:58,482
trying to figure out how I could,

489
00:34:58,484 --> 00:35:01,485
with what happened,

490
00:35:01,487 --> 00:35:03,187
with our friends still out there,

491
00:35:03,189 --> 00:35:05,756
still being hurt by them.

492
00:35:05,758 --> 00:35:09,293
I don't know how I could've said that.

493
00:35:09,295 --> 00:35:14,431
And now... I am trapped.

494
00:35:14,433 --> 00:35:16,000
You have to help me.

495
00:35:16,002 --> 00:35:17,401
I will.

496
00:35:17,403 --> 00:35:19,069
I'll help you get used to the idea

497
00:35:19,071 --> 00:35:22,439
and illustrate the equation
and demonstrate why ...

498
00:35:22,441 --> 00:35:24,475
It was a mistake.

499
00:35:24,477 --> 00:35:26,010
I was ready.

500
00:35:26,012 --> 00:35:29,580
I knew, if I didn't kill him last night,

501
00:35:29,582 --> 00:35:31,348
it'd be over.

502
00:35:31,350 --> 00:35:34,151
And I didn't, so it is.

503
00:35:34,153 --> 00:35:36,787
It has to be.

504
00:35:38,257 --> 00:35:40,357
You have to get me something ...

505
00:35:40,359 --> 00:35:44,228
a knife, a gun, a razor blade.

506
00:35:44,230 --> 00:35:45,996
For Negan?

507
00:35:45,998 --> 00:35:47,197
No!

508
00:35:48,868 --> 00:35:53,137
One way or another, he is gonna
use me to hurt my friends ...

509
00:35:53,139 --> 00:35:57,007
<i>our</i> friends.

510
00:35:57,009 --> 00:35:59,143
And there is nothing I can do about it

511
00:35:59,145 --> 00:36:02,513
except make sure I'm not alive
to give him the chance.

512
00:36:02,515 --> 00:36:06,483
If he has me, he will hurt them.

513
00:36:06,485 --> 00:36:08,786
I have to die.

514
00:36:08,788 --> 00:36:10,020
It's the only way.

515
00:36:10,022 --> 00:36:11,522
I-I don't ...
I-I don't think that ...

516
00:36:11,524 --> 00:36:13,524
I know you won't do anything
to stop them.

517
00:36:13,526 --> 00:36:14,525
I know that.

518
00:36:14,527 --> 00:36:18,829
But, please, Eugene...

519
00:36:18,831 --> 00:36:22,433
don't let them use me
to hurt our people.

520
00:36:22,435 --> 00:36:23,867
Please, you have to give me

521
00:36:23,869 --> 00:36:29,606
something to stop this ...
a gun, a knife, glass.

522
00:36:29,608 --> 00:36:31,041
I don't care.

523
00:36:31,043 --> 00:36:33,243
I have to die.

524
00:36:33,245 --> 00:36:34,978
Please.

525
00:36:39,051 --> 00:36:40,484
I...

526
00:36:40,486 --> 00:36:42,720
I will consider it.

527
00:36:49,328 --> 00:36:51,261
Thank you.

528
00:37:21,809 --> 00:37:23,910
We don't need all this, do we?

529
00:37:26,381 --> 00:37:29,215
Yeah. Yeah, we do.

530
00:37:33,555 --> 00:37:37,256
It's not just the ones I killed.

531
00:37:37,258 --> 00:37:39,759
I think about the people
I<i> didn't</i> kill, too.

532
00:37:49,804 --> 00:37:54,574
We're gonna bring them back
when it's all over.

533
00:37:54,576 --> 00:37:56,909
I-I want to go with you.

534
00:37:56,911 --> 00:37:58,311
Some of us do, but not all of us,

535
00:37:58,313 --> 00:38:00,313
and it has to be all of us.

536
00:38:00,315 --> 00:38:02,248
My grandmother thinks
you'll all be dead.

537
00:38:02,250 --> 00:38:05,117
Yeah, well, your grandmother's
wrong about a lot of things.

538
00:38:05,119 --> 00:38:06,419
Where is she?

539
00:38:06,421 --> 00:38:07,753
She didn't want to see this.

540
00:38:07,755 --> 00:38:09,956
She's lying down.
I hit her pretty hard.

541
00:38:09,958 --> 00:38:12,458
Thanks for saving my life before.

542
00:38:12,460 --> 00:38:14,293
And the other time.

543
00:38:14,295 --> 00:38:15,394
Oh, and then the other time.

544
00:38:15,396 --> 00:38:17,797
Maybe today.

545
00:38:17,799 --> 00:38:20,132
Thanks.

546
00:38:20,134 --> 00:38:22,635
Hey.

547
00:38:22,637 --> 00:38:24,470
Thank you.

548
00:38:24,472 --> 00:38:26,172
For what you're doing.

549
00:38:32,113 --> 00:38:34,080
You're not leaving us any?

550
00:38:34,082 --> 00:38:35,982
Nope.
See ya later, Rachel.

551
00:38:40,922 --> 00:38:42,955
You okay?

552
00:38:42,957 --> 00:38:44,957
Yeah.

553
00:38:44,959 --> 00:38:46,800
You're right.

554
00:38:46,825 --> 00:38:49,292
I don't have to feel bad.

555
00:39:10,517 --> 00:39:11,716
Sasha?

556
00:39:11,718 --> 00:39:13,084
I'm here.

557
00:39:17,958 --> 00:39:21,259
I've given our last exchange
a good, hard pro/con think.

558
00:39:24,698 --> 00:39:27,432
I've decided to grant your request.

559
00:39:31,684 --> 00:39:33,934
This will do it.

560
00:39:39,980 --> 00:39:44,649
It's a poison, homemade by yours truly.

561
00:39:44,651 --> 00:39:46,284
It will bring about certain death

562
00:39:46,286 --> 00:39:47,886
in approximately 20 to 30 minutes

563
00:39:47,888 --> 00:39:50,455
upon the moment of ingestion.

564
00:39:50,457 --> 00:39:52,858
I would like to believe
that it's painless.

565
00:39:52,860 --> 00:39:54,559
I am not ...
I'm not certain.

566
00:39:54,561 --> 00:39:57,062
I made it at the behest
of others for similar reasons,

567
00:39:57,064 --> 00:39:58,330
only it came to pass that

568
00:39:58,332 --> 00:39:59,531
they were feeding me the bum's steer,

569
00:39:59,533 --> 00:40:01,032
so I cut bait.

570
00:40:03,604 --> 00:40:05,303
The difference here is that

571
00:40:05,305 --> 00:40:07,372
you and I have been
long-time traveling companions,

572
00:40:07,374 --> 00:40:09,774
and you have proven yourself
to be honest, brave,

573
00:40:09,776 --> 00:40:11,743
and fully self-aware.

574
00:40:11,745 --> 00:40:14,579
I'm certain I owe you for things.

575
00:40:14,581 --> 00:40:16,548
You asked me for a way out.

576
00:40:16,550 --> 00:40:18,517
I had access to one.

577
00:40:20,988 --> 00:40:24,189
For the record, I absolutely
do not want you to go.

578
00:40:26,226 --> 00:40:29,828
But it sounds to me like you
happen to be already gone.

579
00:41:15,042 --> 00:41:16,741
Hey, are you okay?

580
00:41:16,743 --> 00:41:18,610
Where's Sasha?

581
00:41:22,883 --> 00:41:24,983
There's someone here.

582
00:41:47,007 --> 00:41:49,107
Whoa. Whoa.

583
00:41:49,109 --> 00:41:51,676
Whoa, whoa, whoa.
Whoa, whoa, whoa, whoa!

584
00:41:51,678 --> 00:41:53,745
Slow down. Come on.
Whoa!

585
00:41:53,747 --> 00:41:55,914
He says he wants to help us.

586
00:42:02,860 --> 00:42:04,689
That true?

587
00:42:06,593 --> 00:42:08,360
You want to help?

588
00:42:08,829 --> 00:42:10,528
I do.

589
00:42:26,179 --> 00:42:29,714
Okay.

590
00:42:33,487 --> 00:42:36,488
Get on your knees.

591
00:42:36,608 --> 00:42:41,608
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

