1
00:00:34,958 --> 00:00:37,826
Ze hebben wapens.

2
00:00:37,828 --> 00:00:40,495
Heel veel wapens. Ik zag het.

3
00:00:40,497 --> 00:00:43,064
Wat?
- Die groep.

4
00:00:44,175 --> 00:00:46,334
De vrouw die ik ontmoette.

5
00:00:46,336 --> 00:00:49,971
Ze hebben een arsenaal, wapens.

6
00:00:49,973 --> 00:00:52,274
Waarom vertelde je het niet eerder?

7
00:00:52,276 --> 00:00:54,576
Ik beloofde het, Rick.

8
00:00:59,984 --> 00:01:02,751
Denk je weleens aan degenen die je hebt vermoord?

9
00:01:07,624 --> 00:01:09,925
Ja.

10
00:01:17,534 --> 00:01:20,101
Ben je er klaar voor, als dit misloopt?

11
00:01:20,103 --> 00:01:21,903
Dat gebeurt niet.

12
00:01:21,905 --> 00:01:24,306
Zowel, dan hoef jij je
er niet slecht over te voelen.

13
00:01:24,308 --> 00:01:26,875
Dat voel ik me dan wel.

14
00:01:26,877 --> 00:01:31,179
Tara, dat is niet nodig.

15
00:02:16,511 --> 00:02:22,178
Quality over Quantity (QoQ) Releases
The Walking Dead S07E15: Something They Need

16
00:02:24,358 --> 00:02:26,265
Je moet dit bedekken met een zeildoek.

17
00:02:26,397 --> 00:02:28,931
Een zeildoek?
- Ja.

18
00:02:28,933 --> 00:02:32,167
Waarom?
- Denk erover na.

19
00:02:32,169 --> 00:02:33,836
Vertel het mij.

20
00:02:36,140 --> 00:02:39,875
We willen warmte, zodat er scheuten
aan groeien voordat het gaat vriezen.

21
00:02:39,877 --> 00:02:44,646
Als we dan het zeildoek erop drukken,
sterven ze natuurlijk en hoeven we niet te wieden.

22
00:02:44,648 --> 00:02:48,217
Aan de slag.
- Begrepen, bazin.

23
00:02:49,152 --> 00:02:54,823
Waar ga je heen?
Ik ben geen boer, ik heb hier hulp bij nodig.

24
00:02:54,825 --> 00:02:59,139
Nee hoor.
Ik zag buiten een bosbessenstruik.

25
00:02:59,140 --> 00:03:01,597
Als het jong genoeg is,
dan kan ik het hier herplanten.

26
00:03:01,599 --> 00:03:06,869
Een goede zal 40 jaar lang meegaan.
We moeten doen alsof we er ook zolang zullen zijn.

27
00:03:08,472 --> 00:03:10,973
Is goed.

28
00:03:10,975 --> 00:03:15,410
Alleen het zeildoek, niet aanraken.
Ik kom straks wel kijken.

29
00:03:27,191 --> 00:03:30,092
Zodra je boven bent, wacht je op het signaal.

30
00:03:31,562 --> 00:03:35,564
Wanneer gaat de tijd in?
- Zodra ze daarbinnen is.

31
00:03:38,402 --> 00:03:40,259
Gaat het goed?

32
00:03:41,172 --> 00:03:43,672
Goed genoeg.

33
00:04:02,626 --> 00:04:06,841
Ik ben blij dat je bent gekomen.
Ik weet dat het moeilijk is.

34
00:04:06,842 --> 00:04:10,265
Ik weet dat je dit niet wilt.
- Wilde dit niet, maar ik weet wat we riskeren.

35
00:04:10,267 --> 00:04:13,602
We kunnen ons verbergen of de Verlossers dienen...

36
00:04:13,604 --> 00:04:16,605
en ik kan toekijken hoe je vaak geslagen wordt
of we kunnen terugvechten.

37
00:04:16,607 --> 00:04:18,407
Dus hier zijn we.

38
00:04:20,578 --> 00:04:26,348
En om bij jou te zijn,
maakt dit het een stuk makkelijker.

39
00:04:31,655 --> 00:04:34,990
Ik had beter moeten proberen
om Rosita en Sasha tegen te houden.

40
00:04:34,992 --> 00:04:37,426
Als ze nog een dag langer hadden gewacht...

41
00:04:37,428 --> 00:04:43,232
Sasha kan goed schieten
en Rosita kan voor haarzelf zorgen.

42
00:04:43,234 --> 00:04:46,335
Zijn waarschijnlijk alweer bij de Hilltop.

43
00:04:46,337 --> 00:04:51,712
Laten we het hopen. We hebben ze nodig.
Er zullen nog steeds veel mensen sterven.

44
00:05:07,191 --> 00:05:11,054
Hallo daar. Gaat het?

45
00:05:11,962 --> 00:05:15,163
Mag ik een beetje water?

46
00:05:26,176 --> 00:05:28,877
Ik moet mij even voorstellen.

47
00:05:28,879 --> 00:05:31,613
Ik ben David.

48
00:05:31,615 --> 00:05:34,940
Ik vraag me af of jij je mij nog herinnert.

49
00:05:36,420 --> 00:05:37,859
Doe je dat?

50
00:05:38,722 --> 00:05:41,123
Nee.

51
00:05:43,327 --> 00:05:48,030
Dat kan ik je vergeven.
Gisteren ging het er heftig aan toe.

52
00:05:48,032 --> 00:05:52,000
Ik was degene die het touw gaf
aan degenen die jou vastbonden.

53
00:05:52,002 --> 00:05:55,704
Ik heb er altijd ��n bij me.

54
00:05:55,706 --> 00:06:00,035
Je kunt allerlei leuke en interessante dingen...

55
00:06:00,036 --> 00:06:03,378
met een touw doen.

56
00:06:03,380 --> 00:06:06,348
Wat ben je mooi.

57
00:06:12,256 --> 00:06:14,856
Als ik jou water geef...

58
00:06:14,858 --> 00:06:19,107
wat ik eigenlijk niet mag doen...

59
00:06:19,430 --> 00:06:22,190
wil jij dan iets voor mij doen?

60
00:06:22,533 --> 00:06:26,034
Iets wat jij niet hoort te doen.

61
00:06:26,036 --> 00:06:27,903
Doe je dat?

62
00:06:40,184 --> 00:06:44,453
Zeg me hoeveel dorst je hebt.

63
00:06:48,125 --> 00:06:50,726
Loop naar de hel.

64
00:07:02,172 --> 00:07:05,173
Door je te verzetten, duurt het alleen maar langer.

65
00:07:07,611 --> 00:07:12,114
Wat voor mij prima is.

66
00:07:12,116 --> 00:07:14,282
David.

67
00:07:15,753 --> 00:07:19,321
Wat doe jij hier?
- Negan, meneer.

68
00:07:19,323 --> 00:07:21,890
Denk je echt dat ik wil dat je antwoord geeft?

69
00:07:21,892 --> 00:07:25,108
Ik zie dat je deze vrouw wil verkrachten.

70
00:07:25,109 --> 00:07:29,131
Je probeerde haar te verkrachten, is het niet?

71
00:07:36,807 --> 00:07:42,811
Dit is onacceptabel gedrag.
Verkrachting is hier verboden.

72
00:07:42,813 --> 00:07:45,847
Ik zou niet ergens willen zijn,
waar dat niet zo is.

73
00:07:45,849 --> 00:07:52,120
Iemand die de leiding heeft
en dat zou toestaan...

74
00:08:00,164 --> 00:08:02,764
David.

75
00:08:02,766 --> 00:08:07,135
Je bent nu echt te ver gegaan.

76
00:08:07,137 --> 00:08:09,204
Het spijt me, meneer.

77
00:08:15,813 --> 00:08:21,015
Weet je wat?
Ik accepteer jouw excuses niet.

78
00:08:32,296 --> 00:08:35,340
Haal voor...

79
00:08:35,532 --> 00:08:38,667
Sasha.

80
00:08:38,669 --> 00:08:41,837
Dat is een mooie naam.

81
00:08:43,540 --> 00:08:47,342
Haal voor Sasha een nieuw T-shirt.

82
00:08:53,250 --> 00:08:57,752
Sorry dat je dat moest zien.
Het spijt me ook van het touw.

83
00:08:57,754 --> 00:09:03,959
Waarschijnlijk overdreven,
maar je veroorzaakte hier wel een chaos.

84
00:09:14,104 --> 00:09:16,905
Ik herinner mij jou.

85
00:09:20,043 --> 00:09:22,077
Je was erbij.

86
00:09:24,581 --> 00:09:28,283
Nu snap ik het.

87
00:09:28,285 --> 00:09:30,452
Ik moet het je nageven.

88
00:09:30,454 --> 00:09:36,291
Je hebt wel lef om hier zo naar binnen te stormen.

89
00:09:36,293 --> 00:09:40,798
De grote vraag is
en ik wil de waarheid horen...

90
00:09:42,900 --> 00:09:46,301
Heeft Rick je dit opgedragen?

91
00:09:46,303 --> 00:09:50,138
Rick?

92
00:09:50,140 --> 00:09:52,374
Jouw teef?

93
00:09:52,376 --> 00:09:53,475
Nee.

94
00:09:55,409 --> 00:09:58,544
Hoe dan ook, je dacht dat dit het einde was.

95
00:09:58,546 --> 00:10:01,580
Als je hier alleen naartoe komt.

96
00:10:01,582 --> 00:10:04,416
Maar zo zal het niet gaan.

97
00:10:04,418 --> 00:10:06,018
Juist het tegendeel.

98
00:10:06,020 --> 00:10:11,557
Want dit...
Dit kan het begin zijn.

99
00:10:21,602 --> 00:10:26,004
Dit mes is nu van jou.

100
00:10:26,006 --> 00:10:29,174
Je kunt ermee proberen mij te doden...

101
00:10:29,176 --> 00:10:34,713
maar gezien ik boven je sta
met een honkbalknuppel...

102
00:10:34,715 --> 00:10:37,416
lijkt dat niet echt slim.

103
00:10:37,418 --> 00:10:43,021
Je kunt er je polsen mee doorsnijden,
wat heel jammer zou zijn, maar ik snap het wel.

104
00:10:43,023 --> 00:10:48,360
Gezien de recente gebeurtenissen,
zit je hier niet echt veilig.

105
00:10:48,362 --> 00:10:51,430
Je kunt blijven zitten en niets doen...

106
00:10:51,432 --> 00:10:55,801
wachten tot oude David tot leven komt
en je wilt opeten...

107
00:10:55,803 --> 00:11:00,739
dat zou ook zonde zijn en een beetje dom,
maar ieder hun eigen ding.

108
00:11:00,741 --> 00:11:02,224
Of...

109
00:11:04,043 --> 00:11:08,437
je gebruikt dat mes om te voorkomen
dat oude Rapey Davey...

110
00:11:08,438 --> 00:11:12,251
wandelende dode Rapey Davey wordt.

111
00:11:12,253 --> 00:11:17,155
Red jezelf, sluit je bij ons aan.

112
00:11:19,593 --> 00:11:22,294
Ik weet wat ik zou doen.

113
00:11:23,564 --> 00:11:25,797
Wat?

114
00:11:25,799 --> 00:11:28,433
Ik kom een man te kort.

115
00:11:28,435 --> 00:11:32,604
Je kunt dat stuk ongeluk geen man noemen,
maar toch kom ik er ��n te kort.

116
00:11:32,606 --> 00:11:39,078
En jij hebt lef en daar wil ik gebruik van maken.

117
00:11:39,280 --> 00:11:43,749
Je kunt mij helpen om ooit
deze plek te runnen.

118
00:11:43,751 --> 00:11:49,688
Wij allemaal samen, volgen de regels,
werken allemaal aan dezelfde kant.

119
00:11:49,690 --> 00:11:52,357
Daar ging het altijd al om.

120
00:11:52,359 --> 00:11:56,662
Dat kan het nog steeds voor jou zijn.

121
00:11:56,664 --> 00:12:01,767
Ik weet dat jij je dat moeilijk kunt voorstellen,
na wat ik net deed.

122
00:12:01,769 --> 00:12:06,772
Sasha,
we hebben allemaal slechte dingen gedaan.

123
00:12:13,447 --> 00:12:15,881
Neem je tijd, denk erover na.

124
00:12:15,883 --> 00:12:21,219
Wat je ook beslist, dat zal er gebeuren.
Geen druk.

125
00:12:21,221 --> 00:12:24,156
Nogmaals, het spijt me dat je dat moest zien.

126
00:12:24,158 --> 00:12:28,432
Ook al weet ik dat je veel hebt gezien.

127
00:12:29,363 --> 00:12:31,706
Ik wil dat je begrijpt...

128
00:12:32,700 --> 00:12:35,267
dat we geen monsters zijn.

129
00:13:17,392 --> 00:13:20,126
Wat hebben ze met je gedaan?

130
00:13:23,231 --> 00:13:25,865
Ze houden mij veilig.

131
00:13:29,070 --> 00:13:31,771
Ik heb wat troost meegenomen.

132
00:13:34,475 --> 00:13:37,610
Eerlijk gezegd, dit kussen is niet hypoallergeen.

133
00:13:39,447 --> 00:13:43,049
Er was een discussie
of ik wel of niet hier naartoe zou komen.

134
00:13:43,051 --> 00:13:48,087
Ze zeiden dat als ik gegijzeld word,
dan sta ik er alleen voor.

135
00:13:48,089 --> 00:13:50,990
Dat wilde ik even delen.

136
00:13:55,330 --> 00:13:58,631
Negan heeft mij zijn aanbod aan jou verteld.

137
00:13:58,633 --> 00:14:03,302
Ik denk dat je zijn aanbod moet accepteren,
net zoals ik dat deed.

138
00:14:03,304 --> 00:14:04,937
Ik weet dat het moeilijk is...

139
00:14:04,939 --> 00:14:09,275
maar samenwerken met het management
was inderdaad de juiste beslissing.

140
00:14:12,614 --> 00:14:17,350
Ik geloofde dat ik dapper kon zijn,
dat ik net zoals Abraham een overlever zou worden.

141
00:14:17,352 --> 00:14:21,754
En de zonsondergang tegemoet rijden...

142
00:14:21,756 --> 00:14:25,491
waren de beste 37 minuten van mijn bestaan.

143
00:14:25,493 --> 00:14:28,794
Maar ik weet nu dat ik niet helder nadacht.

144
00:14:31,599 --> 00:14:37,236
Ik ben nog nooit zo bang in mijn leven geweest
als die nacht in het bos...

145
00:14:37,238 --> 00:14:41,674
op mijn knie�n en wist zeker
dat ik gedood zou worden...

146
00:14:41,676 --> 00:14:44,043
en dan zien dat iemand die heel dapper is...

147
00:14:45,747 --> 00:14:48,681
bij een overlever.

148
00:14:48,683 --> 00:14:54,620
En toen weer de angst
omdat het weer opnieuw gebeurde...

149
00:14:56,691 --> 00:14:58,391
Ik kon het niet.

150
00:15:01,896 --> 00:15:05,564
Door hier te zijn betekent dat
ik het nooit meer hoef mee te maken.

151
00:15:05,566 --> 00:15:09,502
Jij ook niet als je ja zegt.

152
00:15:09,504 --> 00:15:12,638
Zeg alsjeblieft ja.

153
00:15:15,176 --> 00:15:17,043
Hij zou dat willen.

154
00:15:24,352 --> 00:15:29,021
Hij zou dat niet doen.
Ik wel.

155
00:15:29,023 --> 00:15:31,857
Wij zijn hier nog.

156
00:15:31,859 --> 00:15:35,661
De dingen zijn veranderd.
We kunnen met hen veranderen. Dat moet wel.

157
00:15:40,234 --> 00:15:41,934
Ga weg.

158
00:15:54,716 --> 00:15:58,350
Hij begint...
- Alsjeblieft.

159
00:15:59,821 --> 00:16:01,687
Ga weg.

160
00:16:52,073 --> 00:16:55,608
Volgens mij ben je nog
nooit buiten de muren geweest.

161
00:16:55,610 --> 00:16:58,978
Ik bouwde die muren om ze te gebruiken.

162
00:16:58,980 --> 00:17:02,515
Als ik groen wil, dan eet ik een salade.

163
00:17:02,517 --> 00:17:06,318
Waarom ben je dan hier?
- Ik hoopte dat we kunnen praten.

164
00:17:06,320 --> 00:17:09,989
Toen zag ik je hier alleen naartoe gaan.
Ik maakte mij zorgen.

165
00:17:09,991 --> 00:17:12,324
Waar zijn je vrienden?

166
00:17:13,862 --> 00:17:15,249
Ze gingen weg.

167
00:17:17,431 --> 00:17:20,132
Ze zoeken naar iets wat ze nodig hebben.

168
00:17:20,134 --> 00:17:22,201
Zonder jou.

169
00:17:22,203 --> 00:17:27,640
Ik vraag het me af...
Waarom ben je hier nog, Maggie?

170
00:17:28,810 --> 00:17:30,203
Nu Carson weg is...

171
00:17:30,278 --> 00:17:35,714
Ik hoorde dat je hem terug gaat halen.
Is dat niet waar?

172
00:17:36,885 --> 00:17:38,338
Of heb je dat alleen maar gezegd?

173
00:17:38,353 --> 00:17:41,153
Maggie, je moet me vertrouwen.

174
00:17:41,155 --> 00:17:43,389
Mensen zijn gespannen, maken zich zorgen.

175
00:17:43,391 --> 00:17:45,991
Iedereen zou zich beter voelen...

176
00:17:45,993 --> 00:17:50,162
als we een verenigd front zouden vertegenwoordigen.

177
00:17:50,164 --> 00:17:53,566
Het zou een stuk makkelijker zijn
als we dat echt waren.

178
00:17:53,568 --> 00:17:55,467
Je hebt gelijk.

179
00:17:55,469 --> 00:17:59,238
Ik moet er open voor staan om samen te werken.

180
00:17:59,240 --> 00:18:01,407
Mijn excuses.

181
00:18:01,409 --> 00:18:06,011
Het was nooit mijn bedoeling
dat het zo ver zou komen.

182
00:18:06,013 --> 00:18:07,913
Dat waardeer ik.

183
00:18:10,518 --> 00:18:13,352
Het is nooit te laat om te veranderen.

184
00:18:18,392 --> 00:18:21,493
Als je het meent, kom ik later wel langs
en praten we erover.

185
00:18:21,495 --> 00:18:23,962
Goed.

186
00:18:30,838 --> 00:18:33,772
Ik dacht dat dit maar even duurde.

187
00:18:33,774 --> 00:18:37,316
Wil je de wacht houden, terwijl ik dit afmaak?
Kun je dat doen?

188
00:18:37,492 --> 00:18:39,545
Graag zelfs.

189
00:19:26,994 --> 00:19:30,462
Daar is er ��n.
- Ik doe het wel.

190
00:19:30,464 --> 00:19:36,401
Ik ben hier om de zwangere dame te beschermen.
Niet andersom.

191
00:19:38,072 --> 00:19:41,807
Heb je er al eentje eerder gedood?
- Anders zou ik hier niet zijn.

192
00:19:46,748 --> 00:19:50,182
Het is goed, ik doe het wel.
- Nee.

193
00:20:00,261 --> 00:20:02,828
Jezus.

194
00:20:02,830 --> 00:20:06,231
Goed, doe het.

195
00:20:20,648 --> 00:20:22,848
Maggie.

196
00:20:25,953 --> 00:20:27,987
Help me, alsjeblieft.

197
00:20:38,366 --> 00:20:40,499
Gaat het wel?

198
00:20:50,311 --> 00:20:54,747
Hij heeft er nog nooit ��n gedood.
Hij oefent het.

199
00:20:54,749 --> 00:20:56,749
Tegen ons zei hij wat anders.

200
00:21:02,556 --> 00:21:04,523
Bedankt voor je hulp.

201
00:21:18,439 --> 00:21:23,776
Heb je die zelf gevangen?
- Ja, verdorie.

202
00:21:23,778 --> 00:21:25,944
Ik bedoel ja.

203
00:21:25,946 --> 00:21:27,246
Ik ga het schoonmaken.

204
00:21:38,192 --> 00:21:39,892
Hoi.

205
00:21:48,270 --> 00:21:52,220
Wees stil en doe je handen op je hoofd.

206
00:21:52,340 --> 00:21:55,043
We hadden je in het dorp moeten doden.

207
00:21:57,401 --> 00:21:59,234
Je zult blij zijn dat je dat niet deed.

208
00:21:59,236 --> 00:22:00,769
Je bent niet alleen, is het niet?

209
00:22:00,771 --> 00:22:01,903
Ga zitten.

210
00:22:01,905 --> 00:22:04,606
Wat wil je?
- Ik wil dat je gaat zitten.

211
00:22:13,684 --> 00:22:15,717
Cyndie.

212
00:22:19,756 --> 00:22:23,525
Je beloofde het.
- Ja, dat deed ik.

213
00:22:23,527 --> 00:22:25,327
Doe je handen op je hoofd.

214
00:22:33,637 --> 00:22:35,303
Ik had geen keus.

215
00:22:35,305 --> 00:22:36,812
Waarom ben je hier?

216
00:22:38,781 --> 00:22:42,711
Ik zei dat ik geen keus had.
Maar jullie wel.

217
00:22:42,713 --> 00:22:46,781
Mijn vrienden zijn er, en die nemen
deze plek over, hopelijk zonder slag of stoot.

218
00:22:46,783 --> 00:22:49,050
Ga je het hier overnemen?

219
00:22:49,052 --> 00:22:51,386
Waar heb je het over?
- We willen jullie wapens.

220
00:22:52,556 --> 00:22:55,957
We gaan de strijd aan met de Verlossers.
Voeg je bij ons.

221
00:22:55,959 --> 00:23:00,495
De Verlossers hebben jullie dierbaren vermoord,
en jullie zijn op de vlucht voor ze.

222
00:23:00,497 --> 00:23:04,299
Ik dacht dat we van ze af waren.
Beatrice zei dat er meer waren, en ze had gelijk.

223
00:23:04,301 --> 00:23:08,570
Ze zijn teruggekomen.
Ze hebben mijn vrienden vermoord.

224
00:23:08,572 --> 00:23:10,538
Mijn vriendin.

225
00:23:10,540 --> 00:23:13,074
Ze hebben alles van ons afgepakt.

226
00:23:13,076 --> 00:23:16,378
We doen alles wat de Verlossers zeggen
wat we moeten doen...

227
00:23:16,380 --> 00:23:19,547
en ze denken dat we er niks tegen doen,
maar we gaan de strijd aan.

228
00:23:19,549 --> 00:23:23,551
We hebben andere gemeenschappen naast ons,
en met de Oceanside hebben we een leger.

229
00:23:23,553 --> 00:23:25,487
Dat was het dus?

230
00:23:25,489 --> 00:23:27,989
"Bedankt voor de wapens,
voeg je nu bij ons"?

231
00:23:27,991 --> 00:23:31,493
Samen kunnen we ze verslaan.
We moeten het proberen.

232
00:23:31,495 --> 00:23:33,828
Ze zullen winnen, Tara.
Ik heb het gezien en jij ook.

233
00:23:33,830 --> 00:23:36,331
Praat met de leider van mijn groep, Rick.

234
00:23:36,333 --> 00:23:39,534
Praat met hem en we hoeven het hier niet
over te nemen. We willen het niet.

235
00:23:39,536 --> 00:23:42,937
Ik kan het teken geven dat het nu stopt,
maar dan moet je het nu zeggen.

236
00:23:45,042 --> 00:23:50,378
We verstoppen ons niet in de bossen.
We doen wat en geven niet op.

237
00:23:50,380 --> 00:23:53,048
Ik wilde mijn belofte niet verbreken, Cyndie.

238
00:23:53,050 --> 00:23:56,451
Maar de wereld behoort toe aan
goede en eerlijke mensen...

239
00:23:56,453 --> 00:23:58,720
als we allemaal dapper genoeg om 't te proberen.

240
00:23:58,722 --> 00:24:02,791
We zijn niet goed of dapper.
Geen van beiden niet.

241
00:24:02,793 --> 00:24:03,807
Zij wel.

242
00:24:03,961 --> 00:24:05,710
Daarom zitten we in deze rotzooi.

243
00:24:07,030 --> 00:24:10,098
Als je onze wapens meeneemt, Tara,
gaan we er allemaal aan.

244
00:24:10,100 --> 00:24:11,299
Dat doe je nu.

245
00:24:11,301 --> 00:24:14,369
We kunnen met ze praten.
- Nee.

246
00:24:14,371 --> 00:24:17,439
Proberen om dit te stoppen.
- Nee, Cyndie.

247
00:24:21,111 --> 00:24:23,078
Het maakt niks meer uit.

248
00:24:24,414 --> 00:24:25,880
Te laat.

249
00:24:30,654 --> 00:24:33,088
Rennen.

250
00:24:36,360 --> 00:24:38,126
De wapenruimte.

251
00:24:53,176 --> 00:24:55,510
Op de grond.

252
00:24:55,512 --> 00:24:58,513
Handen op jullie hoofd.

253
00:25:01,852 --> 00:25:03,852
Alsjeblieft.

254
00:25:22,205 --> 00:25:24,372
Gaan jouw mensen de mijne vermoorden?

255
00:25:24,374 --> 00:25:28,476
De bommen waren buiten de muur.
Je kunt dit stoppen.

256
00:25:28,478 --> 00:25:31,846
Ik hoop dat iedereen ongedeerd is, echt.
- Dat hoop ik maar.

257
00:25:31,848 --> 00:25:35,049
Sta op. We gaan.

258
00:25:40,357 --> 00:25:42,419
Mijn been.
- Achteruit.

259
00:25:42,426 --> 00:25:44,348
Stop, Cyndie.

260
00:25:51,435 --> 00:25:52,867
Hij is leeg.

261
00:25:52,869 --> 00:25:54,769
Niet geladen.

262
00:25:54,771 --> 00:25:57,639
We zijn hier niet om iemand wat aan te doen.

263
00:25:57,641 --> 00:26:00,408
Geef aan mij, Cyndie.

264
00:26:00,410 --> 00:26:02,644
Nu.

265
00:26:02,646 --> 00:26:05,713
Natania, praat met Rick.

266
00:26:05,715 --> 00:26:07,515
Kop dicht.

267
00:26:10,554 --> 00:26:13,288
Op de grond.
- Handen op jullie hoofd.

268
00:26:13,290 --> 00:26:16,090
Blijf kalm, we willen niet
dat er iemand gewond raakt.

269
00:26:16,092 --> 00:26:17,826
Blijf laag, en doe wat we zeggen.

270
00:26:17,828 --> 00:26:21,463
We willen dit zo vredig mogelijk doen.

271
00:26:21,465 --> 00:26:23,431
Het ligt allemaal aan jullie.

272
00:26:23,433 --> 00:26:25,767
Ga daar heen.
En hou je rustig.

273
00:26:28,305 --> 00:26:30,738
We hebben een hoop herrie gemaakt.

274
00:26:30,740 --> 00:26:35,844
We willen dit snel doen, dan kun je mensen
sturen voor alles wat deze kant op komt.

275
00:26:35,846 --> 00:26:40,014
Tara zei dat de bossen veilig waren,
dus we nemen geen risico.

276
00:26:40,016 --> 00:26:44,519
Niemand hoeft gewond te raken.

277
00:26:44,521 --> 00:26:47,956
Dit gaat om wat jullie hebben,
wat wij nodig hebben.

278
00:26:47,958 --> 00:26:50,191
Niemand neemt iets.

279
00:26:50,193 --> 00:26:55,129
Laat iedereen vrij, en ga weg.

280
00:26:55,131 --> 00:26:56,965
Loop gewoon weg, of zij sterft.

281
00:26:56,967 --> 00:27:01,903
We laten je met rust.
Maar we nemen jullie wapens mee.

282
00:27:01,905 --> 00:27:03,304
Daar verandert niks aan.

283
00:27:03,306 --> 00:27:06,641
Natania, toch?

284
00:27:06,643 --> 00:27:11,446
Doe het pistool weg,
dan gaan we praten over wat er kan veranderen.

285
00:27:11,448 --> 00:27:13,815
Nee. Ik wil dat je nu gaat.

286
00:27:15,385 --> 00:27:18,586
Niet doen, Michonne.

287
00:27:18,588 --> 00:27:23,424
We willen met rust gelaten worden.
- Dat doen we.

288
00:27:23,426 --> 00:27:25,894
Maar laat haar nu gaan.

289
00:27:25,896 --> 00:27:29,731
Anders ga jij eraan.

290
00:27:29,733 --> 00:27:31,299
Niemand wil dat.

291
00:27:31,301 --> 00:27:33,772
Ze willen dat we de strijd aangaan
met de Verlossers.

292
00:27:36,339 --> 00:27:38,606
Dat hebben we geprobeerd.
En verloren. Te vaak.

293
00:27:38,608 --> 00:27:42,704
Ik wil niets meer verliezen,
geen wapens of onze veiligheid...

294
00:27:42,746 --> 00:27:44,846
na alles wat we hebben gedaan om hier te komen.

295
00:27:45,035 --> 00:27:47,015
We zullen winnen...

296
00:27:47,017 --> 00:27:50,118
met jullie wapens,
met of zonder jullie hulp.

297
00:27:55,191 --> 00:27:58,259
Natania, doe het pistool weg.

298
00:27:58,261 --> 00:28:00,361
Als je me doodt, ga je eraan.

299
00:28:00,363 --> 00:28:03,298
Dan pakken mijn mensen de wapens,
en verandert er niks.

300
00:28:07,637 --> 00:28:09,704
Misschien moeten we het proberen.

301
00:28:12,342 --> 00:28:14,108
Stop, oma.

302
00:28:14,110 --> 00:28:16,878
Het is voorbij.
Laten we met ze praten.

303
00:28:16,880 --> 00:28:19,480
Het is niet voorbij.

304
00:28:19,482 --> 00:28:21,749
Ze zijn het vergeten.

305
00:28:21,751 --> 00:28:23,318
Jullie zijn het allemaal vergeten.

306
00:28:23,320 --> 00:28:27,689
Wil iemand wel de strijd met ze aangaan?
Na alles wat er is gebeurd?

307
00:28:27,691 --> 00:28:30,458
We kunnen onze wapens verliezen...

308
00:28:30,460 --> 00:28:33,127
maar om deze plek te verlaten om te vechten?

309
00:28:33,129 --> 00:28:36,297
Moet ik na alles wat er is gebeurd
jullie eraan herinneren?

310
00:28:36,299 --> 00:28:40,335
Ja, ik doe dit en daarna sterf ik.

311
00:28:40,337 --> 00:28:44,138
Het is z� belangrijk.
Het gaat om het leven van jullie allemaal.

312
00:28:44,140 --> 00:28:48,242
Onthoud hoe het eruit ziet,
wat ze ons hebben aangedaan.

313
00:28:48,244 --> 00:28:50,607
<i>Jullie moeten dat zien.
Open jullie ogen.</i>

314
00:28:50,821 --> 00:28:52,921
Rick, Walkers.

315
00:28:58,722 --> 00:29:02,969
Iedereen opstaan. De kinderen achter ons.
Ze komen eraan.

316
00:29:03,059 --> 00:29:06,694
Eerste rij.
Messen gereed, alleen de doden.

317
00:29:06,896 --> 00:29:08,696
Ga nergens heen.

318
00:29:08,698 --> 00:29:12,266
Iedereen schieten als ze op drie meter
van de lijn zijn.

319
00:29:12,268 --> 00:29:13,668
Zo ja.

320
00:29:24,514 --> 00:29:25,880
Nu.

321
00:30:32,849 --> 00:30:37,585
Nee, we vechten niet met jullie mee.

322
00:30:37,587 --> 00:30:40,988
Pak de wapens en ga.

323
00:31:08,612 --> 00:31:11,213
Jij hebt gewonnen.

324
00:31:11,215 --> 00:31:13,215
Nee.

325
00:31:13,217 --> 00:31:16,485
Jij hebt gewonnen.

326
00:31:19,090 --> 00:31:21,490
Laat iemand dit opruimen.

327
00:31:33,938 --> 00:31:35,838
Wat?

328
00:31:35,840 --> 00:31:40,142
Je dacht toch niet dat je dit mocht houden?

329
00:31:41,979 --> 00:31:47,749
Je hebt nog een weg te gaan
voordat ik geloof dat je meewerkt.

330
00:31:47,751 --> 00:31:50,252
Babystapjes.

331
00:31:50,254 --> 00:31:53,455
En dat is niet persoonlijk bedoeld.

332
00:31:53,457 --> 00:31:58,627
Als de situatie omgekeerd was,
zou je me ook niet geloven.

333
00:31:58,629 --> 00:32:00,929
Toch...

334
00:32:00,931 --> 00:32:04,299
de manier waarop jij jezelf gered hebt...

335
00:32:04,301 --> 00:32:07,870
hoe jij je net aan me overgaf...

336
00:32:07,872 --> 00:32:10,315
Dat was een verdomd goed begin.

337
00:32:10,318 --> 00:32:14,943
Maar we moeten nog aan wat dingen werken, Sasha.

338
00:32:14,945 --> 00:32:20,616
Maar ik beloof dat ik het leuk zal maken.

339
00:32:20,618 --> 00:32:23,886
De kaarten op de tafel...

340
00:32:23,888 --> 00:32:29,958
een klein vogeltje zei tegen me dat
Rick en de rest niet veel goeds van plan zijn.

341
00:32:29,960 --> 00:32:35,697
En dat moet veranderen,
en jij gaat me daarmee helpen.

342
00:32:35,699 --> 00:32:41,203
We maken er een win-winsituatie van.
- Hoe? Wat ben je...

343
00:32:41,205 --> 00:32:44,239
Ik regel wat spullen
zodat je je gemakkelijker voelt...

344
00:32:44,241 --> 00:32:47,109
we laten je proeven van wat wij te bieden hebben.

345
00:32:47,111 --> 00:32:51,180
Geniet er vannacht van, want morgen...

346
00:32:51,182 --> 00:32:55,217
gaat een grote dag worden.

347
00:34:02,486 --> 00:34:04,052
Kal.

348
00:34:14,164 --> 00:34:17,165
Ik wil dat je me ergens naartoe brengt.

349
00:34:17,167 --> 00:34:19,334
Pak maar een koffer in.

350
00:34:26,477 --> 00:34:28,577
Ik ben het, Eugene.

351
00:34:28,579 --> 00:34:32,481
Er wordt in de hal gezegd
dat jij je bij ons aansluit.

352
00:34:32,483 --> 00:34:34,832
Het lijkt misschien niet zo...

353
00:34:34,835 --> 00:34:37,986
maar ik verzeker je,
dat je zorgvuldig gekozen hebt.

354
00:34:41,625 --> 00:34:44,126
<i>Ik had het mis.</i>

355
00:34:45,829 --> 00:34:47,663
<i>Ik kan het niet.</i>

356
00:34:49,700 --> 00:34:52,734
<i>Ik wil het niet.</i>

357
00:34:52,736 --> 00:34:58,540
<i>Ik zit hier om erachter te komen
hoe ik het wel kan...</i>

358
00:34:58,542 --> 00:35:01,543
<i>met wat er gebeurd is...</i>

359
00:35:01,545 --> 00:35:05,814
<i>met onze vrienden die zij nog steeds pijn doen.</i>

360
00:35:05,816 --> 00:35:09,351
<i>Geen idee waarom ik dat kon zeggen.</i>

361
00:35:09,780 --> 00:35:16,058
<i>En nu zit ik opgesloten.
Je moet me helpen.</i>

362
00:35:16,060 --> 00:35:17,459
Dat wil ik.

363
00:35:17,461 --> 00:35:19,577
Ik help je om aan het idee te wennen...

364
00:35:19,580 --> 00:35:23,257
en verduidelijk de vergelijking
en demonstreert waarom...

365
00:35:23,260 --> 00:35:26,484
<i>Het was een fout.
Ik was er klaar voor.</i>

366
00:35:26,487 --> 00:35:31,406
<i>Ik wist, als ik hem niet gisteravond
zou vermoorden dat het voorbij zou zijn.</i>

367
00:35:31,408 --> 00:35:34,209
<i>En ik heb het niet gedaan...</i>

368
00:35:34,211 --> 00:35:36,845
<i>Het moet gedaan worden.</i>

369
00:35:38,315 --> 00:35:40,415
<i>Je moet wat voor me regelen...</i>

370
00:35:40,417 --> 00:35:44,286
<i>een mes, een pistool of een scheermes.</i>

371
00:35:44,288 --> 00:35:46,054
Voor Negan?

372
00:35:46,056 --> 00:35:47,255
<i>Nee.</i>

373
00:35:48,926 --> 00:35:53,195
<i>Op een of andere manier gaat hij mij gebruiken
om mijn vrienden pijn te doen...</i>

374
00:35:53,197 --> 00:35:57,065
<i>onze vrienden.</i>

375
00:35:57,067 --> 00:35:59,605
En ik kan er niks aan doen...

376
00:35:59,608 --> 00:36:02,571
<i>behalve dat ik niet meer leef
en hem de kans kan geven.</i>

377
00:36:02,573 --> 00:36:06,541
<i>Als hij mij heeft kan hij ze pijn doen.</i>

378
00:36:06,543 --> 00:36:08,844
<i>Ik moet sterven.</i>

379
00:36:08,846 --> 00:36:10,078
<i>Het is de enige manier.</i>

380
00:36:11,582 --> 00:36:14,583
<i>Ik weet dat jij niks zal doen
om hem te stoppen. Dat weet ik.</i>

381
00:36:14,585 --> 00:36:18,887
<i>Maar alsjeblieft, Eugene...</i>

382
00:36:18,889 --> 00:36:22,491
<i>laat ze mij niet gebruiken om mijn
mensen pijn te doen.</i>

383
00:36:22,493 --> 00:36:25,980
<i>Je moet me wat geven om dit te stoppen...</i>

384
00:36:25,983 --> 00:36:29,456
<i>een pistool, mes, een glas.</i>

385
00:36:29,666 --> 00:36:33,301
<i>Het maakt me niet uit.
Ik moet sterven.</i>

386
00:36:33,303 --> 00:36:35,036
<i>Alsjeblieft.</i>

387
00:36:40,544 --> 00:36:42,778
Ik zal het overwegen.

388
00:36:49,386 --> 00:36:51,319
Bedankt.

389
00:37:21,867 --> 00:37:23,968
We hebben dit toch niet allemaal nodig?

390
00:37:26,439 --> 00:37:29,273
Jawel.

391
00:37:33,613 --> 00:37:37,314
Het gaat niet om de mensen die ik vermoord heb.

392
00:37:37,316 --> 00:37:39,817
Ik denk ook aan de mensen
die ik niet heb vermoord.

393
00:37:49,862 --> 00:37:52,986
We brengen ze terug als het allemaal voorbij is.

394
00:37:54,634 --> 00:37:56,967
Ik wil met je mee.

395
00:37:56,969 --> 00:38:00,371
Sommigen willen ook, maar niet allemaal,
maar we moeten allemaal.

396
00:38:00,373 --> 00:38:02,306
Mijn oma denkt dat jullie allemaal eraan gaan.

397
00:38:02,308 --> 00:38:06,477
Je oma heeft het vaker fout.
Waar is ze?

398
00:38:06,479 --> 00:38:10,014
Ze wilde dit niet zien, ze ligt te slapen.
Ik had haar hard geraakt.

399
00:38:10,016 --> 00:38:14,351
Bedankt voor het redden van mijn leven.
Ook die andere keer.

400
00:38:14,353 --> 00:38:17,855
O, en die andere keer ook.
Misschien vandaag.

401
00:38:17,857 --> 00:38:20,190
Bedankt.

402
00:38:22,695 --> 00:38:24,528
Bedankt.

403
00:38:24,530 --> 00:38:26,230
Voor wat jij hebt gedaan.

404
00:38:32,171 --> 00:38:34,138
Laat je niks voor ons achter?

405
00:38:34,140 --> 00:38:36,040
Nee. Zie je later, Rachel.

406
00:38:40,980 --> 00:38:43,013
Gaat het?

407
00:38:45,017 --> 00:38:49,486
Je hebt gelijk.
Ik hoef me niet slecht te voelen.

408
00:39:11,576 --> 00:39:12,942
Ik ben hier.

409
00:39:17,816 --> 00:39:21,329
Ik heb eens goed nagedacht
over ons laatste gesprek.

410
00:39:24,556 --> 00:39:27,290
Ik heb besloten om je verzoek in te willigen.

411
00:39:31,296 --> 00:39:34,064
Hier kun je wat mee.

412
00:39:40,268 --> 00:39:44,507
Het is vergif, gemaakt door ondertekende.

413
00:39:44,509 --> 00:39:50,313
Je zult ongeveer binnen 20 tot 30 minuten sterven,
na het moment van inname.

414
00:39:50,949 --> 00:39:54,417
Ik denk dat het pijnloos is,
maar weet het niet zeker.

415
00:39:54,419 --> 00:39:56,920
Ik maakte het voor iemand anders
voor dezelfde reden...

416
00:39:56,922 --> 00:40:00,890
maar het bleek dat ze mij gebruikten,
dus deed ik het niet.

417
00:40:03,870 --> 00:40:07,538
Het verschil is
dat wij samen veel hebben meegemaakt...

418
00:40:07,541 --> 00:40:11,601
en je hebt bewezen dat je eerlijk, dapper
en verantwoordelijk bent.

419
00:40:11,603 --> 00:40:14,437
En daar ben ik je wat voor schuldig.

420
00:40:14,439 --> 00:40:18,375
Je vroeg aan mij een manier om eruit te stappen.
Ik heb er toegang tot.

421
00:40:20,846 --> 00:40:24,483
Als je maar weet dat ik niet wil
dat je er een einde aan maakt.

422
00:40:26,084 --> 00:40:29,686
Maar het klinkt voor mij
alsof je er klaar voor bent.

423
00:41:15,919 --> 00:41:17,113
Alles in orde?

424
00:41:17,116 --> 00:41:18,816
Waar is Sasha?

425
00:41:22,741 --> 00:41:24,841
Er is hier iemand.

426
00:41:51,536 --> 00:41:53,603
Rustig.

427
00:41:53,605 --> 00:41:55,772
Hij zegt dat hij ons wil helpen.

428
00:42:02,314 --> 00:42:04,547
Is dat zo?

429
00:42:06,451 --> 00:42:08,685
Wil je helpen?

430
00:42:08,687 --> 00:42:10,386
Dat wil ik.

431
00:42:33,965 --> 00:42:36,208
Op je knie�n.

432
00:42:41,466 --> 00:42:46,568
Vertaling:
Quality over Quantity (QoQ) Release

433
00:42:46,964 --> 00:42:51,006
Download deze ondertitel op:
- www.OpenSubtitles.org -

