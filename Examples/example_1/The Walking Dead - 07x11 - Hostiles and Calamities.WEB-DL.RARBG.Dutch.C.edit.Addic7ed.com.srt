1
00:02:03,812 --> 00:02:08,281
Alsjeblieft, niet doen.
O, God, nee.

2
00:02:08,283 --> 00:02:11,151
Ik haat dit, alsjeblieft.

3
00:02:56,945 --> 00:02:59,399
Welkom thuis.

4
00:03:17,383 --> 00:03:21,312
Er zijn er veel meer in de bibliotheek.

5
00:03:23,088 --> 00:03:26,523
Jullie hebben een bibliotheek?

6
00:03:26,525 --> 00:03:30,227
Jouw vriend Daryl heeft het niet kunnen zien.

7
00:03:31,330 --> 00:03:35,186
Is hij ontsnapt?
- Ja.

8
00:03:35,187 --> 00:03:37,495
Weet je waar hij is?
- Nee.

9
00:03:38,938 --> 00:03:41,761
Anders zou ik het je wel vertellen.

10
00:03:44,710 --> 00:03:49,012
Vonda N. Mclntyre.
Dit zal voldoende zijn.

11
00:03:50,583 --> 00:03:53,778
Heb je honger? Ik haal wel iets.
Wat wil je?

12
00:03:54,019 --> 00:03:57,821
Wat ik wil?
- Wat wil je eten?

13
00:03:57,823 --> 00:04:00,524
Alles?
- Ja.

14
00:04:00,526 --> 00:04:03,460
Echt? Alles wat ik wil?
- Ja.

15
00:04:03,462 --> 00:04:05,562
Je kunt alles krijgen.
Wat wil je?

16
00:04:05,564 --> 00:04:09,199
Kreeft?
- Nee, dit is geen restaurant.

17
00:04:09,201 --> 00:04:11,602
Heb je pasta en tomatensaus?

18
00:04:11,604 --> 00:04:14,872
Wil je oranje of rood?

19
00:04:14,874 --> 00:04:17,608
Oranje.
- Goed.

20
00:04:18,811 --> 00:04:21,912
En augurken? Die vind ik lekker.

21
00:04:21,914 --> 00:04:23,680
Die zijn op.

22
00:04:23,682 --> 00:04:27,417
En chips?
- Dat hebben we wel.

23
00:04:27,419 --> 00:04:29,086
Nummer 42 maakt ze.

24
00:04:29,088 --> 00:04:32,623
Worden personen hier bij een nummer genoemd?

25
00:04:32,625 --> 00:04:34,625
De arbeiders wel.

26
00:04:34,627 --> 00:04:37,044
Maken ze de chips met de hand?

27
00:04:38,071 --> 00:04:40,631
Ja, ze hebben een ketel...
- Nee, dank je.

28
00:04:42,668 --> 00:04:46,370
Ik ben straks terug.
Geniet van je nieuwe plek.

29
00:05:29,801 --> 00:05:32,181
<b>Ga nu</b>

30
00:06:37,455 --> 00:06:42,233
Quality over Quantity (QoQ) Releases
The Walking Dead S07E11: Hostiles and Calamities

31
00:06:54,515 --> 00:06:57,582
Goedemorgen, zonnetje.

32
00:06:57,584 --> 00:07:00,875
Is het nog net zo knus als de vorige keer?

33
00:07:01,055 --> 00:07:03,655
Je zat lang achter de verkeerde kant van de deur...

34
00:07:03,657 --> 00:07:07,277
na je domme reisje met de vrouw en Tina.

35
00:07:08,262 --> 00:07:10,495
Laten we er nu over praten.

36
00:07:10,497 --> 00:07:15,300
We zochten een paar uur lang
naar jouw gefaalde project...

37
00:07:15,302 --> 00:07:17,759
we zochten overal...

38
00:07:17,760 --> 00:07:20,697
precies rond de tijd dat ik jou hierin stopte...

39
00:07:20,698 --> 00:07:25,075
en toen ik terugkwam,
besefte ik...

40
00:07:25,076 --> 00:07:28,714
dat ik een vrouw tekort komt.

41
00:07:30,918 --> 00:07:34,753
Welke?
- Sherry.

42
00:07:37,291 --> 00:07:40,625
Weet je daar meer van, Dwighty boy?

43
00:07:41,611 --> 00:07:46,231
Want ik moet zeggen
dat het wel heel toevallig is.

44
00:07:46,233 --> 00:07:51,778
Dat ze wegging nadat Daryl ervandoor ging.

45
00:07:51,779 --> 00:07:54,706
Hij heeft er niet veel voor hoeven te doen.

46
00:07:54,708 --> 00:07:58,710
Nee, iemand deed de deur voor hem open.

47
00:08:02,916 --> 00:08:05,200
Het was Sherry niet.

48
00:08:14,928 --> 00:08:19,564
Dwighty boy, was jij het?

49
00:08:21,802 --> 00:08:24,102
Werkte het andersom?

50
00:08:24,104 --> 00:08:27,773
Jij moest hem breken.
Heeft hij jou gebroken?

51
00:08:28,501 --> 00:08:33,690
Je hebt aardig wat moeten doorstaan.

52
00:08:34,148 --> 00:08:37,994
Kom je tegen mij in opstand, Dwighty?

53
00:08:38,018 --> 00:08:41,787
Zie je de dingen nu anders?

54
00:08:48,595 --> 00:08:50,575
Na dit alles...

55
00:08:51,665 --> 00:08:55,524
ervoor en erna...

56
00:08:56,085 --> 00:08:59,771
na alles...

57
00:09:02,176 --> 00:09:04,609
wie ben je, Dwight?

58
00:09:07,381 --> 00:09:09,181
Ik ben Negan.

59
00:09:46,220 --> 00:09:49,178
Daryl is niet zoals jou.

60
00:09:50,357 --> 00:09:52,020
Hij is...

61
00:09:53,335 --> 00:09:54,526
emotioneel.

62
00:09:55,442 --> 00:09:58,063
Dus of hij is onderweg naar huis...

63
00:09:58,065 --> 00:10:02,667
of hij komt terug
om er nog meer van ons te vermoorden.

64
00:10:02,669 --> 00:10:05,249
Waarschijnlijk ons.

65
00:10:06,039 --> 00:10:10,475
Hoe dan ook, we vinden hem wel.

66
00:10:13,080 --> 00:10:17,640
Denk je te weten waar Sherry naartoe ging?

67
00:10:19,786 --> 00:10:24,282
Ja.
- Breng haar terug.

68
00:10:25,674 --> 00:10:27,259
Zoek het uit.

69
00:10:33,066 --> 00:10:38,517
Hecht hem.
Herstel wat je kunt herstellen.

70
00:10:42,709 --> 00:10:46,702
Je denkt niet dat ze het deed?
- Jij wel?

71
00:10:46,703 --> 00:10:50,541
Ik heb haar een beetje leren kennen.
Niet goed.

72
00:10:50,542 --> 00:10:53,151
Maar goed genoeg om te weten
wat er gebeurde.

73
00:10:53,153 --> 00:10:57,088
Ze zag Daryl en in welke staat hij was.

74
00:10:57,090 --> 00:10:58,657
Ze liet hem gaan.

75
00:10:58,659 --> 00:11:02,093
Ze was zwak.
Ze heeft een groot hart.

76
00:11:02,886 --> 00:11:06,631
Jij werd geslagen en in een cel gegooid,
niet terecht, als je het mij vraagt.

77
00:11:07,416 --> 00:11:09,287
En je gaat gewoon weer verder.

78
00:11:10,048 --> 00:11:13,390
Misschien mag je ooit
een buitenpost voor hem runnen.

79
00:11:13,391 --> 00:11:17,306
Jij snapt het.
Ik denk graag dat ik dat ook doe.

80
00:11:24,918 --> 00:11:27,152
Het type met een onbaatzuchtige, gevoelige ziel...

81
00:11:27,154 --> 00:11:30,739
die met Negan trouwt om haar man te redden?

82
00:11:31,024 --> 00:11:35,927
Dat soort mensen kunnen hier niet meer zijn.

83
00:11:39,333 --> 00:11:40,832
Bedankt.

84
00:11:51,044 --> 00:11:53,178
Probeer alleen maar te helpen.

85
00:12:56,922 --> 00:13:01,375
Spullen die mensen laten groeien, maken
en verzamelen.

86
00:13:01,514 --> 00:13:04,232
Bier, brood, je hoofd laten bewerken.

87
00:13:04,352 --> 00:13:05,449
Wat bewerken?

88
00:13:05,451 --> 00:13:06,851
Kapper.

89
00:13:06,853 --> 00:13:09,620
Jij hebt er wel een nodig.

90
00:13:14,060 --> 00:13:18,629
We gebruiken een puntensysteem.
Je bent er nu ��n van ons, niet van hen.

91
00:13:18,631 --> 00:13:21,666
Zij eten stront, wij hebben het betere.

92
00:13:21,668 --> 00:13:24,302
Schrijf op wat je pakte
of wat zij deden...

93
00:13:24,561 --> 00:13:26,661
en hoeveel het waard was
en onderteken met je naam.

94
00:13:26,663 --> 00:13:28,231
Dat is het.

95
00:13:33,427 --> 00:13:36,261
Moet je geluksdag zijn.

96
00:13:36,710 --> 00:13:38,530
Zijn deze zelfgemaakt?
- Ja.

97
00:13:43,349 --> 00:13:46,660
Zitten veel goede zelfgemaakte spullen tussen.

98
00:13:48,488 --> 00:13:50,688
Nee, dank je.

99
00:13:56,296 --> 00:13:58,101
Als je iets wilt...

100
00:13:59,065 --> 00:14:00,980
dan pak je het.

101
00:14:03,303 --> 00:14:05,636
We laten Simon eerst een kijkje nemen...

102
00:14:05,638 --> 00:14:09,507
eerst de goede agent manier proberen,
kijken hoe ver we komen.

103
00:14:09,509 --> 00:14:12,643
Daar is hij.
De man van het uur.

104
00:14:12,645 --> 00:14:14,697
Kom hier, grote jongen.

105
00:14:21,323 --> 00:14:25,756
Wees niet onbeleefd, klootzak.
Zeg hallo.

106
00:14:27,393 --> 00:14:30,194
Hallo.

107
00:14:30,196 --> 00:14:33,001
Heb je een naam, klootzak?

108
00:14:33,933 --> 00:14:35,366
Eugene.

109
00:14:37,170 --> 00:14:40,297
Nu wij. Wie zijn jullie?

110
00:14:40,338 --> 00:14:42,234
Ik ben Negan.

111
00:14:46,412 --> 00:14:48,779
Eugene...

112
00:14:49,916 --> 00:14:52,950
ik weet dat je Lucille nog wel kent.

113
00:14:54,053 --> 00:14:59,026
Zie je dit?
Je moet wel heel goed kijken.

114
00:14:59,792 --> 00:15:03,227
Dat, mijn vriend,
is de kogel die jij maakte.

115
00:15:03,229 --> 00:15:06,130
Onder normale omstandigheden...

116
00:15:06,132 --> 00:15:11,464
zou ik het je telkens weer laten zien.

117
00:15:13,806 --> 00:15:19,544
Maar, Eugene, ik wil alleen maar weten
of je een slimmerik bent.

118
00:15:19,546 --> 00:15:23,114
Weet je dingen?

119
00:15:25,752 --> 00:15:28,019
Beantwoord de vraag.

120
00:15:31,024 --> 00:15:34,926
Ik ben inderdaad een slimmerik.

121
00:15:34,928 --> 00:15:37,984
Ik leerde mijzelf om kogels te maken.

122
00:15:38,155 --> 00:15:44,605
Ik vond een werkplaats met de nodige...

123
00:15:46,606 --> 00:15:50,408
Ik lees veel en...

124
00:15:50,410 --> 00:15:55,346
Ook al heb ik geen fotografisch geheugen...

125
00:15:55,348 --> 00:15:58,049
Ik doe niets gehaast en ben niet karig.

126
00:15:58,051 --> 00:16:00,051
Maar als er kennis wordt gedeeld,
pak ik dat op.

127
00:16:02,689 --> 00:16:06,824
Je bent echt een klootzak.

128
00:16:13,499 --> 00:16:16,434
Nee, dat ben ik niet.

129
00:16:20,306 --> 00:16:26,043
Ik heb een master in biochemie,
immunologie en microbiologie...

130
00:16:26,045 --> 00:16:28,813
en ik heb mijn doctoraat gehaald,
wat van mij een doctor maakt.

131
00:16:28,815 --> 00:16:30,481
Voor de uitbraak...

132
00:16:30,483 --> 00:16:34,185
maakte ik deel uit van een team van tien personen
van het Human Genome Project...

133
00:16:34,187 --> 00:16:39,857
werkte onder Dr. T. Brooks Ellis,
cre�erde ziektes om ziektes te bestrijden.

134
00:16:39,859 --> 00:16:43,394
Vuur met vuur bestrijden.

135
00:16:43,396 --> 00:16:47,632
De borrels op het werk waren...

136
00:16:55,241 --> 00:16:59,076
Goed, Dr. Slimmerik.

137
00:16:59,078 --> 00:17:03,352
Dit moet je makkelijk kunnen oplossen.

138
00:17:03,353 --> 00:17:06,350
Ik heb hier veel gratis arbeiders bij het hek...

139
00:17:06,352 --> 00:17:09,520
levende dode eikels
die helpen om het gespuis weg te houden.

140
00:17:09,522 --> 00:17:12,923
Het probleem is, ze houden het niet vol.
Ze vallen uit elkaar.

141
00:17:12,925 --> 00:17:18,107
Net als dat hoopje stront daar.

142
00:17:18,998 --> 00:17:21,766
Dus, Dr. Slimmerik...

143
00:17:21,768 --> 00:17:26,137
hoe zorgen we dat ze blijven staan?

144
00:17:39,719 --> 00:17:44,722
Je smelt hier toch dingen?

145
00:17:46,125 --> 00:17:50,780
Ik zag dat jullie
een werkende ijzersmelterij hebben.

146
00:17:50,864 --> 00:17:51,931
En?

147
00:17:53,533 --> 00:17:56,737
Je bezit al de middelen
om je probleem op te lossen.

148
00:17:57,470 --> 00:18:00,137
Stap ��n: Smelt metaal.

149
00:18:00,139 --> 00:18:04,783
Stap twee: Giet het over de walker heen
als ze de ketens aanraken.

150
00:18:05,445 --> 00:18:07,745
Het vloeibare metaal zal hard worden...

151
00:18:07,747 --> 00:18:10,948
waarbij het lichaam van de walker
bij elkaar wordt gehouden...

152
00:18:10,950 --> 00:18:13,951
en ze zitten vast aan het hek.

153
00:18:13,953 --> 00:18:18,155
Bonuspunten omdat hun hoofden worden beschermd
tegen hoofdtrauma...

154
00:18:18,157 --> 00:18:20,958
van vijanden en calamiteiten.

155
00:18:26,165 --> 00:18:30,783
Verdorie. Dat is het gaafste
wat ik ooit in mijn leven heb gehoord.

156
00:18:31,104 --> 00:18:35,973
Dat is niet alleen praktisch,
maar ook stoer.

157
00:18:38,277 --> 00:18:43,347
Kijk jou nu eens, Dr. Slimmerik.

158
00:18:45,251 --> 00:18:50,187
Deed je dit soort dingen ook voor Rick?

159
00:18:54,627 --> 00:18:58,694
Zijn verlies, onze winst.

160
00:18:59,198 --> 00:19:02,397
Ik wil je een bonus geven.

161
00:19:02,502 --> 00:19:05,836
Ik kreeg deze augurken.

162
00:19:10,443 --> 00:19:12,109
Nee.

163
00:19:12,111 --> 00:19:18,449
Als teken van mijn waardering
stuur ik een paar van mijn vrouwen naar je toe.

164
00:19:18,451 --> 00:19:20,384
Laat je een goede tijd beleven.

165
00:19:20,386 --> 00:19:24,188
Ik hoef me hier geen zorgen meer om te maken.

166
00:19:24,190 --> 00:19:27,391
Maar wie weet hoe slim je echt bent?

167
00:19:27,393 --> 00:19:33,473
Geen seks. Dat mag echt niet.

168
00:19:33,566 --> 00:19:38,702
Maar je kunt wel een etentje krijgen,
wat drankjes, lol hebben.

169
00:19:38,704 --> 00:19:42,239
Er is niets zoals mooie vrouwen
die lekker ruiken...

170
00:19:42,241 --> 00:19:45,476
om je weer menselijk te voelen.

171
00:19:46,437 --> 00:19:49,246
Daar weet ik niets vanaf.

172
00:19:50,650 --> 00:19:55,835
Zei je vrouwen? Zoals in meervoud?
- Dat zei ik inderdaad.

173
00:19:58,257 --> 00:20:03,059
Wat zegt Dr. Slimmerik tegen zijn
nieuwe beste vriend in de wereld?

174
00:20:07,867 --> 00:20:10,100
Wat zegt hij dan?

175
00:20:10,738 --> 00:20:14,071
Dank je. Echt heel erg...

176
00:20:14,073 --> 00:20:18,309
eerlijk en uit het hart.
Dank je.

177
00:20:21,080 --> 00:20:22,880
Goed.

178
00:20:24,225 --> 00:20:27,084
Waarom ga je geen plezier maken?

179
00:20:49,233 --> 00:20:50,978
Waar kijk ik eigenlijk naar?

180
00:20:51,035 --> 00:20:53,869
Het wezen dat op een insect lijkt
wordt een Yar genoemd.

181
00:20:53,871 --> 00:20:55,627
Het probeert wraak te nemen...

182
00:20:55,707 --> 00:20:59,247
op de kwaadaardige Qotile
voor de vernietiging van Razak IV.

183
00:21:02,084 --> 00:21:04,388
Misschien moet je het rustig aan doen, Amber.

184
00:21:04,887 --> 00:21:07,888
Misschien moet ik dat niet doen, Frankie.

185
00:21:09,325 --> 00:21:12,526
We kunnen iets anders spelen.
Warlords?

186
00:21:12,528 --> 00:21:14,895
Je kunt het met z'n vieren spelen
en het is wel gaaf.

187
00:21:14,897 --> 00:21:17,929
Wat jij wilt.
Dit is jouw avond.

188
00:21:21,770 --> 00:21:25,402
Wil je een massage?
Negan is er gek op.

189
00:21:25,403 --> 00:21:29,676
En ik was een fysiotherapeut.
Voordat alles gebeurde.

190
00:21:37,673 --> 00:21:41,722
Ik waardeer het gebaar en jullie toewijding
voor jullie opdracht...

191
00:21:41,724 --> 00:21:45,926
maar ik weet dat jullie hier
niet uit vrije wil zijn.

192
00:21:45,928 --> 00:21:50,063
Videogames zijn voor mij een manier
om te laten zien hoe je plezier kunt hebben.

193
00:21:50,065 --> 00:21:53,025
Wil je nog meer popcorn?

194
00:21:55,538 --> 00:21:59,640
Omdat dit Negans idee was,
betekent niet dat we hier niet willen zijn.

195
00:21:59,642 --> 00:22:03,249
Ik ben al blij met een intelligent gesprek.

196
00:22:03,479 --> 00:22:09,716
Ik denk dat een gesprek acceptabel is
onder de huidige omstandigheden.

197
00:22:09,718 --> 00:22:12,119
Waar wil je over praten?

198
00:22:12,121 --> 00:22:17,558
Wat dacht je van het Human Genome Project?
Ik kan daar de hele avond over praten.

199
00:22:22,965 --> 00:22:26,967
Eerlijk gezegd, viel mijn werk bij het HGP...

200
00:22:26,969 --> 00:22:32,139
onder het Black Box programma,
waarvan ik de inhoud niet mag bespreken.

201
00:22:32,141 --> 00:22:37,010
En als ik het wel kon,
dan zouden jullie het niet begrijpen.

202
00:22:37,012 --> 00:22:38,946
Beledigde hij ons, Tanya?

203
00:22:38,948 --> 00:22:42,583
Zo bedoelde ik het niet.
Het was gewoon een feit.

204
00:22:42,585 --> 00:22:47,854
Mijn intelligentie is getest.
Ik ben een geval apart.

205
00:22:47,856 --> 00:22:52,593
Ben je zo iemand die een bom kan maken
van bleek en een tandenstoker?

206
00:22:52,595 --> 00:22:53,810
Natuurlijk niet.

207
00:22:53,930 --> 00:22:58,398
Daar heb je droge gist, waterstofperoxide
en een beetje vloeibare zeep...

208
00:22:58,400 --> 00:23:03,203
misschien een beetje gootsteenontstopper,
wat ballonnen of zoiets...

209
00:23:03,205 --> 00:23:05,305
en nog wat huishoudspullen.

210
00:23:07,959 --> 00:23:09,343
Serieus?

211
00:23:09,345 --> 00:23:12,012
Zo serieus als bloedvergiftiging.

212
00:23:12,014 --> 00:23:13,280
Echt?

213
00:23:31,634 --> 00:23:33,660
Dit is zo stom.

214
00:23:33,965 --> 00:23:36,385
Ik hoop dat ik het tegendeel kan laten zien.

215
00:23:36,496 --> 00:23:39,230
Gewoon slaven?
- Pardon?

216
00:23:39,232 --> 00:23:41,572
Het zal wel.

217
00:23:46,006 --> 00:23:52,177
Dr. Eugene, maakte je net helium
van toiletspullen?

218
00:23:52,179 --> 00:23:54,245
Waterstof.

219
00:23:59,038 --> 00:24:01,866
Het is goed, Keno.
We zijn in orde.

220
00:24:08,086 --> 00:24:11,629
Rustig maar.
Je bent er nu een van ons.

221
00:24:20,040 --> 00:24:25,043
Ik moet bekennen dat ik misschien overdreef
dat ik dit classificeerde als het maken van een bom.

222
00:24:25,045 --> 00:24:30,404
Als de waterstofperoxide afbreekt
komt er een energie van warmte vrij.

223
00:24:30,407 --> 00:24:33,694
Dr. Eugene, kun je ons wat gaafs laten zien?

224
00:24:43,063 --> 00:24:44,629
Ik ga deze kaars aansteken.

225
00:24:44,631 --> 00:24:46,771
Cool.
- Eindelijk.

226
00:24:47,667 --> 00:24:50,057
Nee, ik bedoel, ik ga deze kaars aansteken.

227
00:25:51,331 --> 00:25:55,133
Schat?

228
00:26:14,954 --> 00:26:21,059
<i>D, we zeiden altijd dat als we elkaar kwijtraakten
dat ik hier op je zou wachten.</i>

229
00:26:21,061 --> 00:26:25,530
<i>Dan kwam je opdagen met bier en knabbels.
Weet je dat nog?</i>

230
00:26:25,532 --> 00:26:29,400
<i>Ik wel.
Jij waarschijnlijk niet meer.</i>

231
00:26:36,514 --> 00:26:42,914
<i>Je zei altijd toen we begonnen te daten,
je niet vertelde, dat je een slecht geheugen had.</i>

232
00:26:42,916 --> 00:26:46,217
<i>Je werd er zo gefrustreerd van...</i>

233
00:26:46,219 --> 00:26:50,254
<i>dat je de goede dagen niet zou herinneren...</i>

234
00:26:50,264 --> 00:26:52,351
<i>de bijzondere dagen.</i>

235
00:27:00,367 --> 00:27:02,433
<i>Ik vond het erg voor je.</i>

236
00:27:03,345 --> 00:27:07,572
<i>Ik herinner me dat je zei
dat jij je aan zoveel wilde vasthouden...</i>

237
00:27:07,574 --> 00:27:10,141
<i>en dat het dan weg zou zijn.</i>

238
00:27:10,143 --> 00:27:15,313
<i>Maar je hebt geluk dat jij je
de dingen niet herinnert, D.</i>

239
00:27:18,385 --> 00:27:21,352
<i>Ik wou dat ik nu op je kon wachten.</i>

240
00:27:21,354 --> 00:27:24,589
<i>Maar ik weet niet of je met mij meegaat...</i>

241
00:27:24,591 --> 00:27:27,815
<i>of dat je mij mee neemt daarheen...</i>

242
00:27:27,818 --> 00:27:29,922
<i>of dat je me vermoordt.</i>

243
00:27:30,132 --> 00:27:34,624
<i>Jij wilde niet in die wereld leven,
maar door mij moest je.</i>

244
00:27:36,002 --> 00:27:40,438
<i>Ik heb dit gedaan omdat ik niet wilde
dat je zou sterven.</i>

245
00:27:40,440 --> 00:27:46,844
<i>Maar nu heb je gedood,
en je bent geworden wat je nooit wilde zijn...</i>

246
00:27:46,846 --> 00:27:49,280
<i>en dat is mijn fout.</i>

247
00:27:50,242 --> 00:27:52,350
<i>Jij was beter als mij.</i>

248
00:27:52,352 --> 00:27:54,619
<i>De meeste zijn dat.</i>

249
00:27:55,469 --> 00:28:00,209
<i>Ik liet Daryl gaan, omdat hij mij eraan herinnerde
wie jij ooit was.</i>

250
00:28:00,827 --> 00:28:03,027
<i>En ik wilde het je laten vergeten.</i>

251
00:28:12,272 --> 00:28:16,674
<i>Ik denk dat ik het hier niet ga redden,
maar je hebt het mis.</i>

252
00:28:16,676 --> 00:28:21,913
<i>Daar zijn is niet beter als dood zijn.</i>

253
00:28:21,915 --> 00:28:23,614
<i>Het is erger.</i>

254
00:28:23,616 --> 00:28:28,519
<i>Ik hoop dat jij je dat realiseert,
en dat je wegkomt.</i>

255
00:28:28,521 --> 00:28:34,992
<i>Ik hoop dat je de goede dagen herinnert,
al is het er maar ��n, maar...</i>

256
00:28:34,994 --> 00:28:37,295
<i>ik denk niet dat je dat zal doen.</i>

257
00:28:37,297 --> 00:28:40,998
<i>Ik denk dat je dit nooit zal lezen.</i>

258
00:28:41,000 --> 00:28:43,367
<i>Ik hield van wie je was.</i>

259
00:28:43,369 --> 00:28:47,538
<i>Het spijt me,
dat ik je heb gemaakt tot wat je nu bent.</i>

260
00:28:47,540 --> 00:28:50,719
<i>Vaarwel, schat.</i>

261
00:29:50,436 --> 00:29:55,640
We horen dat je Yars en Qotile
aan het spelen bent, Eugene.

262
00:29:57,587 --> 00:30:02,647
Er was mij verteld
dat gisteravond een uitzondering was.

263
00:30:02,649 --> 00:30:04,949
Hij heeft ons niet gestuurd.

264
00:30:07,186 --> 00:30:09,086
We hebben je hulp nodig.

265
00:30:10,657 --> 00:30:13,891
Jij hebt Amber gisteravond gezien.

266
00:30:14,991 --> 00:30:17,194
Ze is alleen aan het drinken en huilen.

267
00:30:20,166 --> 00:30:22,066
Ze wilde dit niet.

268
00:30:22,737 --> 00:30:26,735
Wij hebben ons ervoor opgegeven.
Het is beter voor ons.

269
00:30:27,032 --> 00:30:29,006
Zij moest.

270
00:30:31,177 --> 00:30:36,480
Haar moeder had medicijnen nodig.
Ze kan niet werken.

271
00:30:38,603 --> 00:30:44,371
Amber dacht dat ze daarmee kon leven.

272
00:30:48,012 --> 00:30:49,697
Maar dat kan ze niet.

273
00:30:49,817 --> 00:30:53,405
Ze heeft ons gevraagd
om te helpen om er een eind aan te maken.

274
00:30:55,716 --> 00:30:58,988
Jullie hebben niet aangeboden...

275
00:30:58,990 --> 00:31:01,357
om geestelijke gezondheidzorg te geven?

276
00:31:01,359 --> 00:31:04,406
Nee.
Ben je serieus?

277
00:31:04,729 --> 00:31:07,415
Er is hier een fysiotherapeut.
Die zijn daar bekend mee.

278
00:31:07,835 --> 00:31:10,266
Zo'n een is er hier niet.

279
00:31:11,436 --> 00:31:14,659
Dus dit is het.

280
00:31:14,907 --> 00:31:17,674
Ze wil gewoon wat innemen.

281
00:31:17,676 --> 00:31:21,311
Ze wil slapen en niet meer wakker worden.

282
00:31:21,313 --> 00:31:24,314
Dat zou heel onverantwoordelijk zijn.

283
00:31:24,316 --> 00:31:30,520
Ze sterft die nacht in haar slaap,
ze transformeert, terwijl de rest slaapt...

284
00:31:30,522 --> 00:31:32,188
Dat regelen we wel.

285
00:31:32,190 --> 00:31:37,694
We hoopten dat jij iets voor haar kan maken.
Want wij weten dat jij dat kunt.

286
00:31:37,696 --> 00:31:41,097
Ik weet dat het krankzinnig klinkt,
en je kent ons haast niet...

287
00:31:41,099 --> 00:31:44,701
maar we zien dat jij een goede man bent.

288
00:31:47,309 --> 00:31:50,140
En daar zijn er niet veel meer van.

289
00:31:54,446 --> 00:31:57,781
De waarheid is dat ik geen goede man ben.

290
00:32:00,152 --> 00:32:04,955
Ik ben niet rechtvaardig, neutraal of chaotisch,
niets van dat allemaal.

291
00:32:11,964 --> 00:32:14,164
Dus jij zegt dat je het niet kunt?

292
00:32:17,637 --> 00:32:20,434
Gebrek aan vermogen is niet het probleem.

293
00:32:20,436 --> 00:32:26,923
Ik kan iets in elkaar fixen
met het gif wat ik hier heb, geen probleem...

294
00:32:27,043 --> 00:32:31,579
Help ons dan.
- Een pil, een injectie, wat dan ook.

295
00:32:31,581 --> 00:32:33,414
We hebben er twee nodig.

296
00:32:34,421 --> 00:32:37,151
Want we weten niet wanneer
we onze kans krijgen.

297
00:32:37,153 --> 00:32:40,539
Ze zal het doen,
met of zonder onze hulp.

298
00:32:40,659 --> 00:32:44,925
Ze zal lijden,
en misschien raakt er nog iemand gewond.

299
00:32:46,148 --> 00:32:49,697
Jij bent een goede man, Eugene.

300
00:32:53,375 --> 00:32:57,037
Dat moet je zijn.

301
00:33:01,366 --> 00:33:03,438
Hoeveel weegt ze?

302
00:33:04,108 --> 00:33:06,909
Geen idee, waarschijnlijk 60...

303
00:33:07,364 --> 00:33:10,231
Waarom?

304
00:33:10,233 --> 00:33:12,901
Als je me haar exacte gewicht geeft...

305
00:33:12,903 --> 00:33:16,304
garandeer ik je een opium voor een pijnloze dood.

306
00:33:16,306 --> 00:33:18,239
Zo snel mogelijk.

307
00:33:45,148 --> 00:33:46,914
Meer heb ik niet.

308
00:33:46,916 --> 00:33:50,718
Kom anders straks terug.
Dan spelen we een potje poker met de anderen.

309
00:33:50,720 --> 00:33:52,053
Ik probeer je erbij te krijgen.

310
00:33:52,055 --> 00:33:54,289
Maak je een grap? Ik speel geen poker.

311
00:33:54,291 --> 00:33:57,012
Ik moet wat gaten graven voor
een schutting in de ochtend.

312
00:33:57,060 --> 00:34:00,395
Ik wil graag een blisterverpakking
van je meest sterke capsules.

313
00:34:01,898 --> 00:34:04,465
Er is niet voor niks een rij.

314
00:34:04,467 --> 00:34:08,503
Hebben ze dat niet waar je vandaan komt?
Weet je niet hoe het werkt?

315
00:34:08,505 --> 00:34:11,072
Nee, ik ben bekend...
- Ga erin staan, lul.

316
00:34:27,157 --> 00:34:28,923
Wat is je nummer?

317
00:34:30,527 --> 00:34:33,528
Wat is die van jou?
- 16.

318
00:34:33,530 --> 00:34:37,699
Goed, nummer 16.
Mijn naam is Dr. Eugene Porter.

319
00:34:37,701 --> 00:34:43,604
Ik ben onlangs benoemd als
hoofdingenieur van deze faciliteit.

320
00:34:43,606 --> 00:34:45,440
Ik rapporteer direct aan Negan...

321
00:34:45,442 --> 00:34:48,109
wat betekent dat jij
aan mij rapporteert.

322
00:34:53,316 --> 00:34:56,451
De capsules, en snel.

323
00:34:56,453 --> 00:34:58,052
Ik wist het niet.

324
00:34:58,054 --> 00:35:02,757
Er zijn zoveel nieuwe gezichten, ik...

325
00:35:18,308 --> 00:35:20,708
En dit wil ik ook.

326
00:35:20,710 --> 00:35:22,210
En dit.

327
00:35:24,214 --> 00:35:26,748
Ik weet niet hoe je dit noemt.

328
00:35:26,750 --> 00:35:29,016
Ik noem het een Gremblygunk.

329
00:35:59,147 --> 00:36:00,282
Heb je haar gevonden?

330
00:36:04,053 --> 00:36:05,820
Waar is ze?

331
00:36:05,822 --> 00:36:07,622
Ik heb haar vermoord.

332
00:36:12,262 --> 00:36:15,029
Ze rende van me weg
in een zooi doden...

333
00:36:15,031 --> 00:36:19,834
dus ik heb het snel gedaan.

334
00:36:21,337 --> 00:36:23,514
Het doet nog steeds pijn.

335
00:36:23,517 --> 00:36:27,809
Het voelt beter met een verband.
- Daar had ik het niet over.

336
00:36:29,446 --> 00:36:31,643
Ik zeggen, vergeet haar maar...

337
00:36:32,126 --> 00:36:35,862
ik heb dat niemand zien doen,
na wat ze had gedaan.

338
00:36:38,288 --> 00:36:40,321
We hebben allemaal dingen gedaan.

339
00:36:40,323 --> 00:36:43,191
Ja, maar voordat we hier kwamen...

340
00:36:43,193 --> 00:36:45,226
voordat we het begrepen...

341
00:36:45,228 --> 00:36:47,662
waren we lafaards.

342
00:36:50,467 --> 00:36:54,869
We hoeven geen groot hart te hebben.

343
00:36:54,871 --> 00:36:58,172
Onthoud dat.

344
00:37:06,216 --> 00:37:10,518
Wat is dit?
- Daar kom je wel achter.

345
00:38:00,436 --> 00:38:06,040
Jij wilt dit echt zien.

346
00:38:13,483 --> 00:38:14,949
Waarom?

347
00:38:14,951 --> 00:38:16,651
Ik heb niks gedaan.

348
00:38:17,340 --> 00:38:22,156
Ik vond dit leuke souvenirtje
verstopt in je bureau.

349
00:38:22,158 --> 00:38:24,126
<b>Vaarwel, lieverd.</b>
- Ik weet niet wat dat is.

350
00:38:45,782 --> 00:38:47,181
Jij...

351
00:38:47,183 --> 00:38:50,912
liet de deur open
en liet mijn puppy ontsnappen.

352
00:38:54,190 --> 00:38:57,525
Jij wist dat Sherry haatte dat Daryl hier was...

353
00:38:57,527 --> 00:39:03,197
dus jij liet hem eruit voor haar,
om de held te zijn. Zodat jij in kon trekken.

354
00:39:03,199 --> 00:39:08,002
Dat is pas gluiperig.

355
00:39:08,004 --> 00:39:11,606
Dat heb ik niet gedaan. Ze wilde vluchten...
- Is ze gevlucht?

356
00:39:11,608 --> 00:39:14,275
Weet je waarom ze is gevlucht?

357
00:39:14,277 --> 00:39:18,846
Omdat ze wist dat ik haar
de schuld zou geven, wat...

358
00:39:18,848 --> 00:39:20,514
ik heb gedaan.

359
00:39:20,516 --> 00:39:25,019
Maar Sherry heeft Dwighty boy
het hele verhaal verteld...

360
00:39:25,021 --> 00:39:27,418
voordat ze verscheurd werd.

361
00:39:27,857 --> 00:39:31,826
Een super geile meid, keihard vermoord...

362
00:39:31,828 --> 00:39:35,830
door je hebzuchtige, piepkleine lul.

363
00:39:35,832 --> 00:39:37,644
Dat is niet waar.
Dwight?

364
00:39:39,068 --> 00:39:41,869
Hij liegt erover.
Ik zou dat nooit doen.

365
00:39:41,871 --> 00:39:43,904
Waarom zou hij dat doen?

366
00:39:43,906 --> 00:39:48,542
Waarom zou hij met opzet jou pijn doen?
Sherry is er niet meer.

367
00:39:48,544 --> 00:39:50,177
En als hij liegt...

368
00:39:50,179 --> 00:39:53,681
en ze is daar ergens, zal ik haar vinden.

369
00:39:53,683 --> 00:39:58,552
En dan zal ik de andere kant
van zijn gezicht eraf branden tot hij sterft.

370
00:39:58,554 --> 00:40:02,857
Dus wat is zijn voordeel?

371
00:40:05,294 --> 00:40:08,062
Nee.

372
00:40:08,064 --> 00:40:10,765
Ik ken Dwighty boy.

373
00:40:10,767 --> 00:40:13,067
Alles wat hij nodig heeft...

374
00:40:13,069 --> 00:40:16,137
was nog een nacht in het gat...

375
00:40:16,139 --> 00:40:19,306
om alles op een rijtje te zetten.

376
00:40:19,308 --> 00:40:22,743
Het werkte eerder,
en dat zal het weer doen.

377
00:40:22,745 --> 00:40:25,279
Nietwaar, Dwight?

378
00:40:25,281 --> 00:40:26,747
Ja.

379
00:40:39,228 --> 00:40:44,398
Alsjeblieft.

380
00:40:44,400 --> 00:40:48,495
Verbrand me alsjeblieft niet.

381
00:40:53,168 --> 00:40:55,135
Je weet dat ik dit haat.

382
00:40:55,137 --> 00:40:59,706
Zeg gewoon dat je het hebt gedaan,
en dat het je spijt, dan hoeft dit allemaal niet.

383
00:40:59,708 --> 00:41:04,687
Ja, ik heb het allemaal gedaan.

384
00:41:04,867 --> 00:41:08,260
Het spijt me zo.

385
00:41:10,686 --> 00:41:14,234
Het spijt me zo.

386
00:41:24,733 --> 00:41:26,633
Meer hoefde je niet te zeggen.

387
00:41:26,635 --> 00:41:31,239
Dat was het enige.

388
00:42:13,751 --> 00:42:17,092
Het goede is dat we een reserve Dr Carson hebben.

389
00:42:17,095 --> 00:42:21,088
Ik had nooit aan je moeten twijfelen, Dwighty boy.

390
00:42:21,650 --> 00:42:26,564
Sherry was mijn favoriet.
Het spijt me.

391
00:42:26,567 --> 00:42:27,963
Mij niet.

392
00:42:30,599 --> 00:42:32,699
Koud als ijs.

393
00:42:32,701 --> 00:42:35,001
Daar hou ik van.

394
00:42:55,342 --> 00:42:56,875
Binnen.

395
00:43:02,082 --> 00:43:04,583
Hallo.

396
00:43:04,585 --> 00:43:06,351
Alles goed?

397
00:43:07,721 --> 00:43:09,387
Beter dan dat.

398
00:43:10,783 --> 00:43:13,258
Je mag ook wel,
maar dat duurt nog wel even.

399
00:43:17,297 --> 00:43:20,132
Heb je de pillen gemaakt?

400
00:43:20,134 --> 00:43:21,700
Ja.

401
00:43:21,702 --> 00:43:24,136
Maar je krijgt ze niet.

402
00:43:30,077 --> 00:43:32,042
Amber rekent op ons.

403
00:43:32,045 --> 00:43:34,780
We zeiden dat je ons zou helpen,
en ze zei...

404
00:43:34,782 --> 00:43:36,114
Op de rem, rooie.

405
00:43:36,116 --> 00:43:38,950
Beledig niet mijn intelligentie, Frankie.

406
00:43:38,952 --> 00:43:43,054
Zeg dat de pillen niet voor Amber zijn,
maar voor Negan.

407
00:43:43,056 --> 00:43:44,990
Daarom wilde je er twee.

408
00:43:47,094 --> 00:43:50,962
Heeft hij je vrienden niet vermoord?
- Meerdere.

409
00:43:51,535 --> 00:43:54,666
Maar wij ongeveer 30 van hem.

410
00:43:54,668 --> 00:43:57,169
Totale ommekeer, en dat alles.

411
00:43:57,171 --> 00:44:00,605
Geef ons de pillen,
anders vertellen we het tegen Negan.

412
00:44:00,607 --> 00:44:06,645
En zeggen dat het jouw idee was,
en dat wij je moesten helpen.

413
00:44:06,647 --> 00:44:09,390
Dat zou een tactische fout zijn van jullie kant.

414
00:44:09,393 --> 00:44:14,186
Ze geloven mij eerder,
net als Dwight met de dokter.

415
00:44:14,188 --> 00:44:16,521
Je bent vervangbaar voor hem.

416
00:44:16,523 --> 00:44:19,255
Ik niet.

417
00:44:19,258 --> 00:44:21,359
Je bent een lafaard.

418
00:44:24,364 --> 00:44:26,598
Je bent een lafaard.

419
00:44:27,247 --> 00:44:29,495
Dat heb je correct.

420
00:45:39,406 --> 00:45:43,508
Mag ik binnenkomen?

421
00:45:52,686 --> 00:45:54,886
Hoe vind je het hier?

422
00:45:56,690 --> 00:46:00,064
Behandelen we Dr. Slimmerik goed?

423
00:46:02,262 --> 00:46:03,962
Frankie en Tanya...

424
00:46:04,871 --> 00:46:09,263
zeggen alleen maar goede dingen over jou.

425
00:46:14,708 --> 00:46:18,310
Het is al goed. Ik begrijp het.

426
00:46:18,312 --> 00:46:24,565
Ik weet hoe moeilijk de veranderingen zijn
om bij het juiste team te zijn.

427
00:46:24,718 --> 00:46:29,087
Maar ik wil dat je iets goed begrijpt.

428
00:46:29,089 --> 00:46:34,326
Ik geef niet iedereen deze uitnodiging.

429
00:46:34,328 --> 00:46:40,598
En ik neem het niet licht op.

430
00:46:48,909 --> 00:46:53,144
Je hoeft niet meer bang te zijn.

431
00:46:53,146 --> 00:46:56,614
Je hoeft niet bang te zijn.

432
00:46:56,616 --> 00:47:01,186
Je hoeft alleen maar antwoord te geven
op ��n vraag.

433
00:47:01,188 --> 00:47:04,656
En dat is een flinke.

434
00:47:05,759 --> 00:47:06,992
Wie zijn...

435
00:47:06,994 --> 00:47:08,360
Ik ben Negan.

436
00:47:08,362 --> 00:47:12,888
Ik ben beslist de steenkoude Negan.

437
00:47:12,891 --> 00:47:15,767
Ik was hem al voordat ik jou ontmoette.

438
00:47:15,769 --> 00:47:19,938
Ik moest je alleen ontmoeten
om zeker te zijn.

439
00:47:19,940 --> 00:47:25,543
Ik ben Negan.

440
00:47:27,948 --> 00:47:29,948
Voorzichtig daarmee.

441
00:47:32,953 --> 00:47:38,590
Wil je je verbranden aan gesmolten metaal?
Want zo gebeurt dat zeker.

442
00:48:02,983 --> 00:48:05,717
Gezien ik mij...
- Hoor je nu bij ons?

443
00:48:05,719 --> 00:48:08,878
Dat ben ik. Net als jij.

444
00:48:10,020 --> 00:48:13,024
Ik weet niet of jij herinnert...
- Dat doe ik niet.

445
00:48:13,026 --> 00:48:14,885
Eugene.

446
00:48:15,640 --> 00:48:17,195
Jij bent Dwight.

447
00:48:22,636 --> 00:48:24,436
Wij zijn Negan.

448
00:48:27,841 --> 00:48:29,161
Ja.

449
00:48:32,376 --> 00:48:36,918
Vertaling:
Quality over Quantity (QoQ) Releases

450
00:48:37,145 --> 00:48:40,774
Download deze ondertitel op:
- www.OpenSubtitles.org -

