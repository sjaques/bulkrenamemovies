1
00:01:13,489 --> 00:01:15,723
Thanks for keeping watch last night.

2
00:01:15,725 --> 00:01:19,184
Oh, it's nothing.
I like it.

3
00:01:21,497 --> 00:01:24,200
Uh, you can keep watch tonight.

4
00:01:28,837 --> 00:01:30,903
We gonna win today?

5
00:01:30,905 --> 00:01:32,438
Oh, yeah.

6
00:01:32,440 --> 00:01:35,408
We're gonna need batteries
for that walkie, too.

7
00:01:35,410 --> 00:01:38,403
Get it working again.

8
00:02:12,940 --> 00:02:16,354
What is it?
What's that smile?

9
00:02:16,964 --> 00:02:20,283
Couple of days out,
all we got to show are

10
00:02:20,612 --> 00:02:22,151
two guns, dented cans of beans,

11
00:02:22,153 --> 00:02:24,287
and some football jerseys, pretty much.

12
00:02:24,289 --> 00:02:26,589
Pretty much.

13
00:02:26,591 --> 00:02:29,592
We're fightin' the fight.

14
00:02:29,594 --> 00:02:32,061
It's better.

15
00:02:43,307 --> 00:02:46,542
And if I hear one more
goddamn word about Fat Joey,

16
00:02:46,544 --> 00:02:48,310
I'm gonna smash somebody's teeth in.

17
00:02:48,312 --> 00:02:50,378
Good riddance to dead weight.

18
00:02:50,380 --> 00:02:51,747
No more mascot crap.

19
00:03:00,490 --> 00:03:02,289
They got good taste in pretzels.

20
00:03:02,291 --> 00:03:04,024
And batteries.

21
00:03:08,030 --> 00:03:11,531
Two days.
We're gonna need to get back.

22
00:03:15,170 --> 00:03:17,704
Rick...

23
00:03:17,706 --> 00:03:21,140
Day and a half more...
today and tomorrow.

24
00:03:23,479 --> 00:03:26,645
We can come out again.
We should get back.

25
00:03:26,647 --> 00:03:28,981
Just a little more.

26
00:03:28,983 --> 00:03:31,216
We're okay.

27
00:03:31,218 --> 00:03:34,052
We don't have to find them right now.

28
00:03:34,054 --> 00:03:36,254
Yeah, I know.

29
00:03:36,256 --> 00:03:38,691
Just a little more, okay?

30
00:03:44,531 --> 00:03:46,564
Okay.

31
00:03:46,566 --> 00:03:47,899
Okay.

32
00:04:34,431 --> 00:04:38,733
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

33
00:04:49,099 --> 00:04:51,166
Hey, I was just coming over to do that.

34
00:04:51,168 --> 00:04:53,601
Denise showed me how.

35
00:04:55,471 --> 00:04:57,105
I'm done.

36
00:04:58,175 --> 00:04:59,682
What?

37
00:05:00,343 --> 00:05:02,026
Nothing.

38
00:05:02,779 --> 00:05:04,526
Okay.

39
00:05:05,982 --> 00:05:07,885
It's gonna work out.

40
00:05:08,718 --> 00:05:10,758
What... What does
that even mean, Tara?

41
00:05:10,986 --> 00:05:12,720
It means we've got
the numbers to fight now.

42
00:05:12,722 --> 00:05:14,321
We just need to find some guns and...

43
00:05:14,323 --> 00:05:17,057
Do you know where we could find
that many guns today?

44
00:05:17,059 --> 00:05:21,061
This week?
This year?

45
00:05:21,063 --> 00:05:24,663
No. You don't.

46
00:05:24,665 --> 00:05:28,434
I can't just wait.

47
00:05:28,436 --> 00:05:31,838
I don't know about you, but I can't.

48
00:05:31,840 --> 00:05:35,908
We're gonna find the guns, okay?
And we're gonna fight.

49
00:05:35,910 --> 00:05:37,376
Look, it's not gonna be easy.

50
00:05:37,378 --> 00:05:40,846
It wasn't the last time at the outpost.

51
00:05:42,616 --> 00:05:45,650
Hey, maybe you could
just save all this for them.

52
00:05:50,824 --> 00:05:52,758
Here.

53
00:05:52,760 --> 00:05:56,394
It'll help with your scar.

54
00:05:59,966 --> 00:06:02,032
Keep it.

55
00:06:02,034 --> 00:06:03,533
We need guns.

56
00:06:03,535 --> 00:06:05,236
I'm gonna go find them.

57
00:06:53,817 --> 00:06:55,917
Shh.

58
00:06:55,919 --> 00:06:57,718
Shh.

59
00:07:15,670 --> 00:07:17,437
Damn it.

60
00:07:17,439 --> 00:07:19,172
What was it?

61
00:07:19,174 --> 00:07:20,340
It was a deer.

62
00:07:20,342 --> 00:07:25,110
I owe you a deer.

63
00:07:25,112 --> 00:07:26,745
From before.

64
00:07:26,747 --> 00:07:28,580
When they were outside the gate.

65
00:07:28,582 --> 00:07:31,016
Oh, yeah, you do.

66
00:07:41,328 --> 00:07:46,030
You see that?

67
00:07:46,032 --> 00:07:49,167
Yeah.

68
00:08:16,827 --> 00:08:19,862
It's guns like that.
That's why we're out here.

69
00:08:47,724 --> 00:08:51,926
Something serious happened here.

70
00:08:51,928 --> 00:08:53,693
A long time ago.

71
00:08:53,695 --> 00:08:57,264
Yeah.
These are serious rounds.

72
00:08:57,266 --> 00:09:00,434
Might be serious guns here, too.

73
00:09:07,909 --> 00:09:09,842
Let's get a better look.

74
00:09:09,844 --> 00:09:11,610
Yeah.

75
00:10:21,979 --> 00:10:23,778
Damn it.

76
00:10:29,453 --> 00:10:31,752
Watch your step.

77
00:10:31,754 --> 00:10:35,123
Maybe we shouldn't be up here too long.

78
00:10:35,125 --> 00:10:38,826
Well, let's see what we're gonna see.

79
00:10:55,044 --> 00:10:59,379
Soldiers, civilians, all those rounds...

80
00:10:59,381 --> 00:11:01,114
There was a fight.

81
00:11:01,116 --> 00:11:03,313
Maybe it was the walkers.

82
00:11:03,519 --> 00:11:05,555
Maybe just other people.

83
00:11:05,719 --> 00:11:09,071
They're still wearing their guns.

84
00:11:09,723 --> 00:11:11,766
I think this is it.

85
00:11:12,660 --> 00:11:16,028
I think it is, too.

86
00:11:16,030 --> 00:11:17,829
Think we can?

87
00:11:49,895 --> 00:11:51,695
Yeah, we can.

88
00:12:21,526 --> 00:12:22,893
Are you okay?

89
00:12:22,895 --> 00:12:24,661
Y-Yeah. Are you?

90
00:12:24,663 --> 00:12:26,729
I am.

91
00:12:26,731 --> 00:12:30,066
This is...
This is a sign, right?

92
00:12:32,770 --> 00:12:34,403
This is it.
It has to be.

93
00:12:36,540 --> 00:12:40,176
Oh-ho. It is.

94
00:12:44,681 --> 00:12:46,614
Well, they're ready.

95
00:12:46,616 --> 00:12:48,549
Let's eat.

96
00:13:18,479 --> 00:13:21,480
I didn't find any guns.

97
00:13:21,482 --> 00:13:23,449
In case you were wondering.

98
00:13:23,451 --> 00:13:27,320
I was out there all day
and not a damn one.

99
00:13:27,322 --> 00:13:31,457
Not a real one.

100
00:13:31,459 --> 00:13:34,993
Thing is, I had a gun.

101
00:13:34,995 --> 00:13:36,594
I was gonna use it to kill Negan.

102
00:13:36,596 --> 00:13:38,630
I was ready to.

103
00:13:38,632 --> 00:13:41,166
And if I had done it,
like I'd planned to,

104
00:13:41,168 --> 00:13:43,402
Negan would be dead right now.

105
00:13:43,404 --> 00:13:48,340
And, yeah, maybe I'd be dead,
too, but who gives a damn?

106
00:13:48,342 --> 00:13:51,775
Eugene would still be here.

107
00:13:51,777 --> 00:13:54,845
Olivia would still be alive.

108
00:13:54,847 --> 00:13:57,648
Spencer would still be alive.

109
00:13:57,650 --> 00:14:01,018
And now they're gone, and I'm here

110
00:14:01,020 --> 00:14:05,189
because I was stupid enough
to listen to you.

111
00:14:05,191 --> 00:14:08,658
But you were.

112
00:14:08,660 --> 00:14:11,094
And you did.

113
00:14:13,865 --> 00:14:17,700
You stand there telling people
about their lives.

114
00:14:19,838 --> 00:14:21,704
Only if they come here.

115
00:14:23,675 --> 00:14:25,941
Like you.

116
00:14:25,943 --> 00:14:27,642
Right now.

117
00:14:27,644 --> 00:14:31,646
You don't know shit about shit.

118
00:14:31,648 --> 00:14:34,216
You're right.

119
00:14:34,218 --> 00:14:35,985
I don't.

120
00:14:37,821 --> 00:14:41,857
I said that you weren't supposed to die

121
00:14:41,859 --> 00:14:43,557
and that you shouldn't do the thing

122
00:14:43,559 --> 00:14:46,527
that you were planning to do

123
00:14:46,529 --> 00:14:48,729
because we needed you...

124
00:14:48,731 --> 00:14:51,399
still do.

125
00:14:51,401 --> 00:14:54,235
Even a fool like me could see that.

126
00:14:54,237 --> 00:14:56,337
It's easier to be dead,

127
00:14:56,339 --> 00:14:58,039
and if it's my fault you're alive,

128
00:14:58,041 --> 00:14:59,806
well, I'm just gonna have
to live with that.

129
00:14:59,808 --> 00:15:02,842
I decided to meddle.

130
00:15:02,844 --> 00:15:05,645
But I did something I thought was right,

131
00:15:05,647 --> 00:15:09,849
and I knew the stakes were very high.

132
00:15:09,851 --> 00:15:13,387
You can certainly blame me for
the fact that you have a life,

133
00:15:13,389 --> 00:15:16,489
but after that what
are you going to do with it?

134
00:15:16,491 --> 00:15:21,793
How are you going to make
what needs to happen happen?

135
00:15:21,795 --> 00:15:26,098
Anything is possible
until your heart stops beating.

136
00:15:29,703 --> 00:15:33,371
Certainly more than yelling at a fool.

137
00:16:06,138 --> 00:16:08,637
What?

138
00:16:15,547 --> 00:16:18,081
How is it?

139
00:16:18,083 --> 00:16:20,783
It's good.

140
00:16:24,955 --> 00:16:26,882
So, tomorrow...

141
00:16:27,257 --> 00:16:30,351
...we go out there,
we get the weapons, right?

142
00:16:30,894 --> 00:16:34,030
We get Jadis and her people
to fight with us.

143
00:16:34,764 --> 00:16:37,832
We kill Negan and anyone else
we have to to win.

144
00:16:39,470 --> 00:16:43,571
And what happens after that?

145
00:16:43,573 --> 00:16:46,173
After that, we keep going.

146
00:16:46,175 --> 00:16:48,875
Yeah, but...

147
00:16:48,877 --> 00:16:51,878
Negan ordered the world his way.

148
00:16:51,880 --> 00:16:55,483
It's up to us to reorder it
once he's gone, right?

149
00:16:57,686 --> 00:17:00,686
All the different communities,

150
00:17:00,688 --> 00:17:03,856
they can figure it out together,
how to keep going together.

151
00:17:03,858 --> 00:17:07,026
Yeah, but somebody's gonna have
to make that happen.

152
00:17:07,028 --> 00:17:09,662
Somebody will need to be
in charge of that, right?

153
00:17:12,066 --> 00:17:17,068
It should be you.

154
00:17:17,070 --> 00:17:20,506
- Not me.
- Why not?

155
00:17:20,508 --> 00:17:21,906
You could do it.

156
00:17:21,908 --> 00:17:24,909
You'd be good at it.

157
00:17:24,911 --> 00:17:27,379
I mean, if it's something
that you wanted.

158
00:17:27,381 --> 00:17:34,585
Well, I don't think I do.
Or I would.

159
00:17:34,587 --> 00:17:40,024
But the two of us, you and me,

160
00:17:40,026 --> 00:17:43,994
reordering things together...

161
00:17:43,996 --> 00:17:47,898
I want that.

162
00:17:47,900 --> 00:17:51,668
If it's something you wanted.

163
00:17:54,206 --> 00:17:56,273
Yeah.

164
00:18:15,759 --> 00:18:17,759
I'm gonna eat five more
of these dinners, okay?

165
00:18:20,030 --> 00:18:23,031
You know...

166
00:18:23,033 --> 00:18:26,501
we don't have to get it done tomorrow.

167
00:18:26,503 --> 00:18:29,470
We can take our time.

168
00:18:29,472 --> 00:18:31,105
If we go another day,
another one after that,

169
00:18:31,107 --> 00:18:33,074
it's fine.

170
00:18:33,076 --> 00:18:36,711
I mean, the place is clear.

171
00:18:36,713 --> 00:18:39,214
It's locked up tight.

172
00:18:40,583 --> 00:18:43,783
We found the only way in, so...

173
00:18:46,061 --> 00:18:48,522
We should get back.

174
00:18:49,241 --> 00:18:51,342
If we're getting the guns,

175
00:18:52,060 --> 00:18:54,223
it doesn't matter
if it takes a little longer.

176
00:18:54,463 --> 00:18:56,545
Wait.
I've been, um...

177
00:18:56,832 --> 00:18:59,342
I've been waiting to show you this one.

178
00:19:00,066 --> 00:19:05,600
It's, um, chili and mac and cheese...

179
00:19:07,074 --> 00:19:08,941
together.

180
00:19:08,943 --> 00:19:10,175
Come on.

181
00:20:12,053 --> 00:20:13,687
Oh. Sorry.

182
00:20:15,558 --> 00:20:18,157
I was just thinking.

183
00:20:21,763 --> 00:20:23,997
Wh... You...
You want this?

184
00:20:23,999 --> 00:20:27,033
Congrats.

185
00:20:27,035 --> 00:20:28,467
It's yours.

186
00:20:28,469 --> 00:20:32,405
Someone gave it to me,
someone who helped me.

187
00:20:32,407 --> 00:20:35,774
She put her ass on the line for me.

188
00:20:35,776 --> 00:20:37,308
She saved my life, you know?

189
00:20:37,310 --> 00:20:40,311
She didn't have to.
But she did.

190
00:20:40,313 --> 00:20:41,747
That means something now...
it does.

191
00:20:41,749 --> 00:20:44,149
It has to.

192
00:20:46,119 --> 00:20:49,755
If I say something, you know...

193
00:20:49,757 --> 00:20:51,789
Rick won't leave there
without their guns.

194
00:20:51,791 --> 00:20:53,390
If... If we could
even get close enough,

195
00:20:53,392 --> 00:20:57,695
they'll start firing,
and we'll fire back.

196
00:21:01,133 --> 00:21:03,734
I mean, they should just fight with us.

197
00:21:03,736 --> 00:21:05,803
The Saviors killed their fathers

198
00:21:05,805 --> 00:21:09,940
and their sons and their brothers.

199
00:21:09,942 --> 00:21:11,775
They won't want to fight with us.

200
00:21:11,777 --> 00:21:13,409
They just want to hide.

201
00:21:15,914 --> 00:21:21,651
So they'll just fight us.

202
00:21:21,653 --> 00:21:25,621
I tell everyone about Oceanside,
that's what happens.

203
00:21:31,729 --> 00:21:34,663
What makes our life worth more
than theirs?

204
00:21:38,836 --> 00:21:40,002
Because we want to stop

205
00:21:40,004 --> 00:21:43,471
the people that are hurting us,

206
00:21:43,473 --> 00:21:46,173
who will hurt other people?

207
00:21:57,120 --> 00:21:58,920
God damn it.

208
00:22:33,387 --> 00:22:34,886
Look.

209
00:22:34,888 --> 00:22:38,023
The walkers in the field,
they're not closed in.

210
00:22:39,626 --> 00:22:41,126
Yeah.

211
00:22:41,128 --> 00:22:43,528
We got to take care of the ones out here

212
00:22:43,530 --> 00:22:46,330
and block that gap, too,
so we can take the rest slow.

213
00:22:49,402 --> 00:22:51,301
Seven, eight...

214
00:22:52,872 --> 00:22:55,706
nine...

215
00:22:55,708 --> 00:22:59,009
That car...
I can block it up.

216
00:22:59,011 --> 00:23:00,577
I can take out that one on the way.

217
00:23:00,579 --> 00:23:03,513
You draw the rest.

218
00:23:03,515 --> 00:23:05,248
You're leaving me eight.

219
00:23:08,119 --> 00:23:09,719
We could shoot them,

220
00:23:09,721 --> 00:23:11,253
but that would call the rest
from the field.

221
00:23:11,255 --> 00:23:14,256
This is about doing it quiet,
with the sword.

222
00:23:15,560 --> 00:23:17,326
You can handle eight.

223
00:23:21,332 --> 00:23:22,498
Hey.

224
00:23:55,297 --> 00:23:56,630
Hyah!

225
00:23:56,632 --> 00:23:57,832
Hyah!

226
00:24:57,322 --> 00:25:00,323
You got your eight walkers.
I can push.

227
00:25:00,325 --> 00:25:01,825
So can I.

228
00:25:01,827 --> 00:25:04,161
You just worry
about steering us in that gap.

229
00:25:07,198 --> 00:25:09,531
Shit.

230
00:25:09,533 --> 00:25:10,900
The brakes don't work.

231
00:25:36,026 --> 00:25:39,027
Michonne, you good?

232
00:25:39,029 --> 00:25:40,628
Yeah.

233
00:25:40,630 --> 00:25:41,995
You?

234
00:25:43,232 --> 00:25:45,265
Yeah.

235
00:25:45,267 --> 00:25:48,401
I think we overshot it.

236
00:25:48,403 --> 00:25:51,438
You think or you know?

237
00:25:53,943 --> 00:25:55,108
I know.

238
00:25:57,691 --> 00:25:59,487
It was a good plan, though.

239
00:25:59,714 --> 00:26:01,659
It was a great plan.

240
00:26:30,680 --> 00:26:32,780
We'll do it here.

241
00:26:32,782 --> 00:26:34,548
Yeah.

242
00:26:38,454 --> 00:26:40,387
Shit.
It's not gonna hold.

243
00:26:45,027 --> 00:26:47,327
There it goes!

244
00:27:27,934 --> 00:27:30,034
Split them off into smaller groups.

245
00:27:30,036 --> 00:27:31,936
The barriers might hold.

246
00:27:31,938 --> 00:27:33,938
You take slide, I take Ferris wheel?

247
00:27:33,940 --> 00:27:36,373
Or we could just go.

248
00:27:36,375 --> 00:27:38,275
- You want to go?
- Nah. We can do this.

249
00:27:38,277 --> 00:27:39,542
Yeah, I know we can.

250
00:28:38,935 --> 00:28:40,401
How you doing?!

251
00:28:40,403 --> 00:28:42,636
Eight more!

252
00:28:42,638 --> 00:28:44,437
How about you?

253
00:28:47,675 --> 00:28:49,108
10!

254
00:29:08,996 --> 00:29:10,762
Hyah!

255
00:29:47,332 --> 00:29:49,132
Rick!

256
00:30:07,184 --> 00:30:08,449
Rick!

257
00:31:16,248 --> 00:31:17,414
Michonne!

258
00:31:33,731 --> 00:31:34,897
Unh!

259
00:31:42,940 --> 00:31:46,942
I tried, but I still owe you one.

260
00:33:47,192 --> 00:33:50,033
I could've gone a couple more days.

261
00:33:50,361 --> 00:33:52,588
I would've liked that.

262
00:34:02,940 --> 00:34:05,040
I haven't been sleeping.

263
00:34:09,309 --> 00:34:11,814
Thinking about what we lost.

264
00:34:15,419 --> 00:34:19,220
Thinking about my friends.

265
00:34:19,222 --> 00:34:22,356
Glenn saved me.

266
00:34:22,358 --> 00:34:24,525
Right at the start.

267
00:34:24,527 --> 00:34:28,596
I couldn't save him.

268
00:34:28,598 --> 00:34:30,865
It's normal.

269
00:34:30,867 --> 00:34:33,433
I know that.

270
00:34:33,435 --> 00:34:36,884
Being stuck on it.

271
00:34:37,628 --> 00:34:39,639
We went through something.

272
00:34:39,641 --> 00:34:42,609
This...
This doesn't cure it.

273
00:34:42,611 --> 00:34:44,677
Rick, I'm sorry.

274
00:34:51,714 --> 00:34:55,749
We're gonna fight them.

275
00:34:55,751 --> 00:35:00,220
That's what happens next.

276
00:35:00,222 --> 00:35:02,388
And we're gonna lose people,

277
00:35:02,390 --> 00:35:04,625
maybe a lot of them,
maybe even each other.

278
00:35:09,463 --> 00:35:13,332
Even then, it'll be worth it.

279
00:35:15,069 --> 00:35:17,436
When I thought that...

280
00:35:23,210 --> 00:35:27,045
I can't lose you.

281
00:35:27,047 --> 00:35:29,447
You asked me

282
00:35:29,449 --> 00:35:35,120
what kind of life we had
just surrendering.

283
00:35:35,122 --> 00:35:39,791
It wasn't...
It wasn't a life.

284
00:35:39,793 --> 00:35:43,060
What we did back there,

285
00:35:43,062 --> 00:35:45,929
what we're doing now,

286
00:35:45,931 --> 00:35:48,331
making a future for Judith

287
00:35:48,333 --> 00:35:50,000
and for Glenn and Maggie's baby,

288
00:35:50,002 --> 00:35:53,370
fighting the fight, that's living.

289
00:35:53,372 --> 00:35:56,473
You showed me that.

290
00:35:56,475 --> 00:35:58,641
You can lose me.

291
00:35:58,643 --> 00:36:00,609
- No.
- Yes, you can.

292
00:36:00,611 --> 00:36:03,880
I can lose you.

293
00:36:03,882 --> 00:36:08,985
We can lose our friends, people we love.

294
00:36:08,987 --> 00:36:11,620
It's not about us anymore.

295
00:36:11,622 --> 00:36:16,024
It's about a future.

296
00:36:16,026 --> 00:36:17,793
And if it's me who doesn't make it,

297
00:36:17,795 --> 00:36:19,695
you're gonna have
to lead the others forward

298
00:36:19,697 --> 00:36:22,130
because you're the one who can.

299
00:36:25,235 --> 00:36:28,369
But...

300
00:36:28,371 --> 00:36:31,372
how do you know?

301
00:36:31,374 --> 00:36:34,075
Because...

302
00:36:34,077 --> 00:36:37,644
you led me here.

303
00:36:45,855 --> 00:36:48,589
Operational? All?

304
00:36:48,591 --> 00:36:50,623
To the best of my knowledge, yeah.

305
00:36:52,194 --> 00:36:56,096
May need some cleaning.
We found supplies.

306
00:36:56,098 --> 00:36:58,031
Expect us?

307
00:36:58,033 --> 00:37:00,266
We cleaned some.
We oiled some.

308
00:37:00,268 --> 00:37:02,401
You can do the rest.
We do this together.

309
00:37:02,403 --> 00:37:04,771
Yes, yes, but operational?

310
00:37:04,773 --> 00:37:07,472
Well, you can fire a few.
Try them out, if you like.

311
00:37:07,474 --> 00:37:09,775
- How many?
- 63.

312
00:37:09,777 --> 00:37:11,243
We made an inventory.

313
00:37:11,245 --> 00:37:13,645
No.

314
00:37:13,647 --> 00:37:15,580
You mean the inventory?

315
00:37:17,017 --> 00:37:19,417
Not enough. - What? Wha...
What are you talking about?

316
00:37:19,419 --> 00:37:21,686
You asked for a lot of guns.
That's what this is.

317
00:37:21,688 --> 00:37:24,956
Enough to fight your fight.
Us? Nearly twice.

318
00:37:24,958 --> 00:37:26,523
Need nearly twice.

319
00:37:26,525 --> 00:37:27,759
We've wasted enough time.

320
00:37:27,761 --> 00:37:29,160
Let's go.
Take our guns with us.

321
00:37:29,162 --> 00:37:30,628
No.

322
00:37:30,630 --> 00:37:34,198
Our guns to take.
Our deal still on.

323
00:37:37,637 --> 00:37:39,870
Not all of them.

324
00:37:39,872 --> 00:37:42,739
We're keeping 10
for ourselves... to find more.

325
00:37:42,741 --> 00:37:44,040
Five.

326
00:37:44,042 --> 00:37:45,408
- 10.
- 6.

327
00:37:45,410 --> 00:37:47,144
- 10.
- 9.

328
00:37:47,146 --> 00:37:48,912
And the cat back.

329
00:37:53,085 --> 00:37:54,617
20. I keep the cat.

330
00:37:54,619 --> 00:37:57,820
We get you the guns.
We fight together.

331
00:37:57,822 --> 00:38:00,455
Say yes.

332
00:38:03,761 --> 00:38:05,627
Yes.

333
00:38:05,629 --> 00:38:07,629
More soon, we'll fight.

334
00:38:33,155 --> 00:38:35,422
You get a few more days

335
00:38:35,424 --> 00:38:38,658
before "what happens next."

336
00:38:42,497 --> 00:38:43,897
Few more days?

337
00:38:43,899 --> 00:38:45,866
That's right.

338
00:38:45,868 --> 00:38:48,968
We'll find more, figure it out.

339
00:38:48,970 --> 00:38:51,337
Soon.

340
00:38:51,339 --> 00:38:53,973
In a few more days.

341
00:39:17,497 --> 00:39:19,664
Hey.

342
00:39:19,666 --> 00:39:21,032
Hey.

343
00:39:21,034 --> 00:39:23,133
You all right?

344
00:39:23,135 --> 00:39:26,337
Yeah, yeah, I was just... I was
just coming over to see you.

345
00:39:26,339 --> 00:39:28,005
What's up?

346
00:39:28,007 --> 00:39:29,773
Have you seen Rosita?

347
00:39:29,775 --> 00:39:32,642
She didn't show for her
guard shift this morning.

348
00:39:34,071 --> 00:39:36,383
She probably just went out
looking for more.

349
00:39:38,372 --> 00:39:39,949
Yeah.

350
00:39:41,686 --> 00:39:43,652
Why were you...

351
00:39:43,654 --> 00:39:45,688
Why were you coming by?

352
00:39:49,560 --> 00:39:53,096
I have something to tell you.

353
00:40:16,652 --> 00:40:18,419
Hi.

354
00:40:25,395 --> 00:40:26,928
Are you here...

355
00:40:26,930 --> 00:40:29,130
I'm here because I need your help.

356
00:40:38,907 --> 00:40:42,909
One condition...

357
00:40:42,911 --> 00:40:46,379
I get to take the shot.

358
00:40:48,282 --> 00:40:50,215
Okay.

359
00:40:54,322 --> 00:40:56,588
Did you bring any explosives
from the bridge?

360
00:40:56,590 --> 00:40:58,557
I thought about it.

361
00:40:58,559 --> 00:41:00,759
But I didn't want anyone finding
out and trying to stop me.

362
00:41:00,761 --> 00:41:02,995
I got something better, though.

363
00:41:17,777 --> 00:41:19,810
So you can take your shot.

364
00:41:28,653 --> 00:41:30,887
Rick and Michonne found guns.

365
00:41:35,194 --> 00:41:36,826
But they aren't ready to move.

366
00:41:36,828 --> 00:41:38,461
They can't.

367
00:41:38,463 --> 00:41:41,330
The need more guns,
more people, more time,

368
00:41:41,332 --> 00:41:44,666
more excuses.

369
00:41:44,668 --> 00:41:47,669
I memorized everything
Daryl and Carl told Rick

370
00:41:47,671 --> 00:41:50,006
about the inside of Negan's place.

371
00:41:50,008 --> 00:41:53,909
Jesus made me a map of the outside.

372
00:41:53,911 --> 00:41:56,077
It's far away.
It's well-defended.

373
00:41:56,079 --> 00:41:58,446
- There's only two of us.
- You changing your mind?

374
00:41:58,448 --> 00:42:00,548
I'm ready to kill him.

375
00:42:00,550 --> 00:42:03,084
But I need to make sure
you know what this means.

376
00:42:03,086 --> 00:42:04,352
Do you?

377
00:42:06,723 --> 00:42:09,266
They can't catch us alive.

378
00:42:11,005 --> 00:42:13,237
If they do, we give them something.

379
00:42:13,870 --> 00:42:15,894
It's a one-way ticket
for both of us.

380
00:42:19,101 --> 00:42:21,013
If it is both of us.

381
00:42:22,505 --> 00:42:23,970
It is.

382
00:42:24,424 --> 00:42:29,424
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

