1
00:01:32,900 --> 00:01:38,893
Quality over Quantity (QoQ) Releases
The Walking Dead S07E13: Bury Me Here

2
00:02:56,458 --> 00:02:58,458
Word wakker, jongen.

3
00:02:58,460 --> 00:03:03,430
Waarom moet het zo vroeg zijn?
- Heb vandaag veel te doen.

4
00:03:07,436 --> 00:03:10,660
Wil je weer gaan slapen?
- Echt niet.

5
00:03:11,573 --> 00:03:15,449
Toen je naar mij toekwam,
zei je dat jij je broer ervan langs wil geven.

6
00:03:16,979 --> 00:03:19,581
Is dat wat je echt wilt?

7
00:03:21,850 --> 00:03:24,735
Ja.
- Nee.

8
00:03:26,201 --> 00:03:28,902
Je wilt net zoals je broer zijn.

9
00:03:28,904 --> 00:03:32,246
Ja?
- Ja.

10
00:03:37,312 --> 00:03:41,287
Let op je voeten, want als die niet goed staan...
- Lukt het niet.

11
00:05:08,970 --> 00:05:11,844
We wilden net...
- Waar is Morgan?

12
00:05:12,173 --> 00:05:16,075
Gaat het wel?
- Waar is Morgan?

13
00:05:25,587 --> 00:05:28,869
Waarom bracht Jezus Daryl en alle anderen
naar het Koninkrijk?

14
00:05:31,226 --> 00:05:32,348
Ze...

15
00:05:35,597 --> 00:05:39,332
Ze wilden kijken of we samen kunnen werken.

16
00:05:42,671 --> 00:05:46,971
De Verlossers zijn er nog.
Ze vechten niet met het Koninkrijk, maar...

17
00:05:49,177 --> 00:05:53,479
Daryl zei dat ze hen hebben
verslagen in Alexandria. Is dat waar?

18
00:05:53,481 --> 00:05:56,215
Dat ze een deal met hen hebben gesloten?

19
00:05:56,217 --> 00:05:58,155
Is iedereen in orde?

20
00:06:00,388 --> 00:06:04,857
Vertel mij dat het waar is.
- Daar moet je met Daryl over praten.

21
00:06:09,498 --> 00:06:13,507
Jij wilde dat ik tegen niemand
iets over jou zei.

22
00:06:14,969 --> 00:06:20,126
Dat ik niet mocht zeggen waar je was
en ik deed wat je vroeg.

23
00:06:20,575 --> 00:06:24,377
Daryl vond je niet door mij.

24
00:06:24,379 --> 00:06:26,379
En wat er tussen jullie is gezegd...

25
00:06:26,381 --> 00:06:29,353
is tussen jullie gezegd.

26
00:06:32,921 --> 00:06:36,046
Ik ga met je mee naar Alexandria,
als je de reis wilt maken...

27
00:06:37,992 --> 00:06:40,059
als je met mij wilt praten.

28
00:06:45,934 --> 00:06:48,472
Je moet niet alleen gaan.

29
00:06:59,147 --> 00:07:00,919
Je vond toch wat je wilde?

30
00:07:02,250 --> 00:07:04,255
Je was bij iedereen weg.

31
00:07:07,055 --> 00:07:08,755
Was dat wat je wilde?

32
00:07:11,426 --> 00:07:13,772
Of was het te laat om er aan te ontsnappen?

33
00:07:22,403 --> 00:07:25,718
We kunnen nu naar Alexandria gaan.

34
00:07:26,541 --> 00:07:28,775
Als je dat wilt.

35
00:07:36,050 --> 00:07:37,550
Kan ik je ergens mee helpen?

36
00:07:37,552 --> 00:07:42,288
Nee.
- Mag ik met je meelopen naar het huisje?

37
00:07:42,290 --> 00:07:43,598
Nee.

38
00:07:44,759 --> 00:07:47,333
We hebben vandaag een levering,
maar ik kan het wel overslaan.

39
00:07:50,839 --> 00:07:55,668
Ik probeer te leren
en gezien wat jij daar deed...

40
00:07:55,670 --> 00:07:57,641
Er waren er vijf van hen.

41
00:07:59,307 --> 00:08:04,331
Ik zou het waarderen
als je het mij voor zou doen.

42
00:08:06,481 --> 00:08:09,315
Nee, ga naar je levering.

43
00:09:37,424 --> 00:09:40,825
Je mag mij benaderen, Nabila.
- Ik sta hier goed.

44
00:09:41,683 --> 00:09:44,184
Shiva mag jou wel.

45
00:09:45,732 --> 00:09:47,999
Daar ben ik juist bang voor.

46
00:09:51,104 --> 00:09:55,129
Zeg maar wat je hebt gevonden.
- Je zult het niet leuk vinden.

47
00:09:55,130 --> 00:09:57,803
Ik wacht niet graag op slecht nieuws.

48
00:09:58,779 --> 00:10:02,419
We hebben een klein probleem
dat binnen een week een groot probleem kan zijn.

49
00:10:02,883 --> 00:10:06,551
De meeste planten hebben last van snuitkevers.

50
00:10:07,220 --> 00:10:12,540
We moeten alles vernietigen
voordat de kevers bij de gewassen komen.

51
00:10:12,926 --> 00:10:15,660
Ik weet dat de koninklijke tuin
belangrijk voor je is.

52
00:10:15,662 --> 00:10:17,429
Het spijt me.

53
00:10:19,333 --> 00:10:23,635
Dank je, Nabila.
Doe wat er gedaan moet worden.

54
00:10:23,637 --> 00:10:26,738
Maar dit is het mooie, Uwe Majesteit:

55
00:10:26,740 --> 00:10:30,108
Je kunt het eruit trekken en eraf snijden.

56
00:10:30,110 --> 00:10:33,244
Je kunt het verbranden en allemaal weggooien.

57
00:10:33,246 --> 00:10:35,914
Maar als je wilt,
kan het allemaal weer terug groeien.

58
00:10:40,654 --> 00:10:46,218
Volgens mij heb ik in mijn broek geplast.
Ik ga nu.

59
00:11:06,813 --> 00:11:09,722
We gaan over vijf minuten weg.
- Goed.

60
00:11:12,686 --> 00:11:14,386
Bedankt hiervoor.

61
00:11:15,449 --> 00:11:16,988
Ik denk er nog steeds over na.

62
00:11:16,990 --> 00:11:20,545
"Een tegenstander verwonden,
is hetzelfde als jezelf verwonden.''

63
00:11:22,496 --> 00:11:24,663
Je raakt hoe dan ook gewond.

64
00:11:27,100 --> 00:11:30,382
Ik heb iets voor je.

65
00:11:30,871 --> 00:11:32,671
Bedankt.

66
00:11:42,082 --> 00:11:44,883
Ik vond het in een restaurant.

67
00:11:44,885 --> 00:11:48,053
Ik ken een meisje en zij herstelde het voor mij.

68
00:11:52,025 --> 00:11:56,561
Zo, ik werd het zat om naar die spijker te kijken.

69
00:12:00,100 --> 00:12:01,833
Wie is het meisje?

70
00:12:03,670 --> 00:12:07,739
We vertrekken over drie minuten, dus...

71
00:12:24,357 --> 00:12:26,524
Hij is te jong om een vader te zijn.

72
00:12:28,528 --> 00:12:30,061
Was jij er een?

73
00:12:31,364 --> 00:12:33,080
Godzijdank wel.

74
00:12:33,934 --> 00:12:39,306
Maar ik was niet te jong,
perfecte leeftijd, perfecte tijd.

75
00:12:40,340 --> 00:12:42,929
Ik had het perfecte gezin en leven.

76
00:12:44,745 --> 00:12:48,042
Ik vroeg niet om veel
en kreeg meer dan ik verdiende.

77
00:12:49,950 --> 00:12:51,750
En jij?

78
00:12:59,126 --> 00:13:03,045
Het spijt me van hoe het tussen ons ging.

79
00:13:04,531 --> 00:13:06,486
Ik denk dat je het fout hebt over het doden...

80
00:13:07,134 --> 00:13:09,956
en dat je denkt dat dit met de Verlossers
stand kan houden.

81
00:13:11,304 --> 00:13:13,464
Maar het spijt me.

82
00:13:14,641 --> 00:13:16,864
Ik weet dat je een goed mens bent.

83
00:13:22,582 --> 00:13:25,732
Er komen dagen dat je niet goed kunt zijn.

84
00:13:26,787 --> 00:13:30,655
Als dat gebeurt,
neem het jezelf dan niet kwalijk.

85
00:13:36,763 --> 00:13:40,309
Benjamin, het is tijd om te gaan.

86
00:13:42,068 --> 00:13:43,635
Tot later, kleine man.

87
00:13:45,939 --> 00:13:49,307
Laat de fruittaart staan.
- Echt?

88
00:13:49,309 --> 00:13:50,475
Jerry.

89
00:13:53,013 --> 00:13:54,345
Goed.

90
00:14:02,455 --> 00:14:04,189
Wie is het meisje?

91
00:14:17,337 --> 00:14:20,805
Blijf allemaal laag
en houd je wapens omhoog.

92
00:14:22,342 --> 00:14:26,424
Iemand blokkeerde de weg.
Kan niet voor iets goeds zijn.

93
00:14:27,514 --> 00:14:31,819
Uwe Majesteit, blijf in de wagen.

94
00:14:46,566 --> 00:14:49,334
Neem maar een kijkje,
maar houd je wapens op scherp.

95
00:15:02,249 --> 00:15:07,352
Naar rechts.
Ga richting het gebouw.

96
00:15:08,655 --> 00:15:11,256
Laten we de koning omcirkelen.

97
00:15:11,258 --> 00:15:13,157
Ik geef dekking.

98
00:15:25,872 --> 00:15:28,172
We gaan richting het gebouw.

99
00:15:34,347 --> 00:15:36,314
Benjamin, wapen omhoog.

100
00:15:39,719 --> 00:15:41,920
Is dit hoog genoeg?

101
00:15:41,922 --> 00:15:43,721
Je doet het prima.

102
00:16:11,348 --> 00:16:14,052
<b>BEGRAAF MIJ HIER</b>

103
00:16:19,793 --> 00:16:23,164
Deze wereld maakt mensen gek.

104
00:16:25,498 --> 00:16:29,067
Mensen hebben veel ellende meegemaakt...

105
00:16:29,069 --> 00:16:32,437
dictators, volkenmoord,
haat...

106
00:16:32,439 --> 00:16:35,340
terwijl anderen genoten van de vrijheid
op hetzelfde stuk land.

107
00:16:35,342 --> 00:16:39,077
Maar dit, hoe we nu moeten leven...

108
00:16:39,079 --> 00:16:42,267
Het is geluk dat we niet allemaal krankzinnig zijn.

109
00:16:42,983 --> 00:16:46,714
Het is geen geluk, Uwe Majesteit.
- Hoe bedoel je?

110
00:16:46,820 --> 00:16:49,793
De wereld drijft mensen nu tot waanzin.

111
00:16:50,757 --> 00:16:52,064
Maar...

112
00:16:53,893 --> 00:16:55,860
jij maakte een andere wereld voor ons.

113
00:17:30,864 --> 00:17:32,363
Jullie zijn laat.

114
00:17:32,365 --> 00:17:36,701
Als je naar ons wilt luisteren, onze weg...
- Ik wil het niet horen.

115
00:17:36,703 --> 00:17:38,036
Val de koning niet in de rede.

116
00:17:43,510 --> 00:17:46,347
Rattenkop.

117
00:17:46,479 --> 00:17:49,680
Ezekiel, er zijn geen koningen...

118
00:17:49,682 --> 00:17:52,950
presidenten of premiers.

119
00:17:52,952 --> 00:17:56,220
Dat is ook allemaal een sprookje.

120
00:17:56,222 --> 00:18:00,091
Kom niet bij ons aan met dat
'Uwe Hoogheid' onzin.

121
00:18:00,093 --> 00:18:05,830
Ik waardeer dat jullie leveren, maar...

122
00:18:05,832 --> 00:18:10,034
het is onnodig gespannen,
en dat maakt mij weer onnodig gespannen.

123
00:18:10,036 --> 00:18:12,403
Ik doe dit niet voor de stress.

124
00:18:12,405 --> 00:18:15,807
Nee, juist het tegendeel.

125
00:18:15,809 --> 00:18:18,910
Laten we het doen.
Heb je de levering van vandaag?

126
00:18:31,024 --> 00:18:33,281
Ik wil ook jullie wapens.

127
00:18:36,196 --> 00:18:38,229
Dat was niet de afspraak.

128
00:18:38,231 --> 00:18:43,272
Je hebt een keuze,
dezelfde die er al vanaf het begin is.

129
00:18:43,273 --> 00:18:47,124
Geef jullie wapens af
of probeer ze te gebruiken.

130
00:18:47,874 --> 00:18:49,907
Wat wordt het?

131
00:18:58,351 --> 00:19:00,718
We moeten ze geven, Uwe Majesteit.

132
00:19:00,720 --> 00:19:04,190
''We moeten ze geven, Uwe Majesteit.''

133
00:19:05,825 --> 00:19:09,410
Of misschien duw ik het door je strot.

134
00:19:10,196 --> 00:19:12,396
Geef Morgan zijn stok terug.

135
00:19:14,067 --> 00:19:18,302
Dan krijgen jullie de wapens.
En kunnen we allemaal weer verder gaan.

136
00:19:18,304 --> 00:19:21,172
Ezekiel...

137
00:19:21,174 --> 00:19:27,111
ik wil dat je begrijpt hoe ernstig
de situatie nu is.

138
00:19:27,113 --> 00:19:31,851
Ik gaf je een keuze.
Wat wordt het?

139
00:19:32,986 --> 00:19:39,090
Kom op, je weet het antwoord.
- Het is goed.

140
00:19:50,503 --> 00:19:52,436
Geef hen jullie wapens.

141
00:20:01,447 --> 00:20:06,211
We nemen jullie wapens in
omdat het zo emotioneel wordt.

142
00:20:06,212 --> 00:20:09,820
Jullie hebben bewezen dat
jullie daar niet goed mee omgaan.

143
00:20:09,822 --> 00:20:12,557
Je komt tekort.

144
00:20:12,559 --> 00:20:15,808
We zeiden 12, ik tel er 11.
- Er zijn er 12.

145
00:20:15,809 --> 00:20:17,395
Die zijn er niet.
- Tel opnieuw.

146
00:20:17,397 --> 00:20:18,863
Tel jij ze.

147
00:20:31,044 --> 00:20:33,911
Ik heb ze geteld.

148
00:20:33,913 --> 00:20:36,013
Dat heb ik gedaan. Dit is onmogelijk.

149
00:20:36,015 --> 00:20:38,549
Het is wel mogelijk.

150
00:20:38,551 --> 00:20:42,599
Het is echt.
Het gebeurt nu.

151
00:20:43,656 --> 00:20:48,326
En de problemen moeten nu stoppen.

152
00:20:48,328 --> 00:20:52,513
Je moet leren wat er op het spel staat,
dus wij zullen het je leren.

153
00:20:52,516 --> 00:20:56,867
Je hoeft helemaal niks te doen.
Wij verdubbelen het in het uur.

154
00:20:56,869 --> 00:21:02,340
Nu is het enige wat belangrijk is.

155
00:21:02,342 --> 00:21:06,043
En nu...

156
00:21:06,045 --> 00:21:08,713
hou jij je niet aan je afspraak.

157
00:21:09,916 --> 00:21:12,917
Zelfs nadat we erover hebben gesproken...

158
00:21:12,919 --> 00:21:17,052
keer op keer.

159
00:21:19,792 --> 00:21:22,950
We gaan het nu oplossen...

160
00:21:24,897 --> 00:21:27,131
nu.

161
00:21:27,133 --> 00:21:30,434
Nu bedoel je met 'nu'?

162
00:21:31,771 --> 00:21:34,538
Juist, nu.

163
00:22:00,500 --> 00:22:02,300
Schiet maar.

164
00:22:05,364 --> 00:22:06,570
<i>Nee.</i>

165
00:22:08,734 --> 00:22:10,119
<i>Nee.</i>

166
00:22:10,530 --> 00:22:13,464
Rustig iedereen.

167
00:22:13,912 --> 00:22:17,242
We moeten Benjamin
terug naar het Koninkrijk brengen.

168
00:22:19,245 --> 00:22:21,578
Geef die vent zijn klotestok.

169
00:22:21,580 --> 00:22:24,748
Ga de truck in.
En waag het eens om wat te zeggen.

170
00:22:27,653 --> 00:22:29,920
We moeten terug.
- Nee.

171
00:22:29,922 --> 00:22:35,125
Luister nu een keer.
Let op en luister.

172
00:22:35,127 --> 00:22:39,563
Je doet de leveringen op tijd,
elke keer en ze zullen volledig zijn.

173
00:22:39,565 --> 00:22:42,923
Met een 'A' of 'F', en geen I.

174
00:22:43,669 --> 00:22:47,905
Je laat me zien dat je me begrijpt
door me te brengen wat je me morgen schuldig bent.

175
00:22:47,907 --> 00:22:49,673
We moeten gaan. Hij bloedt dood.

176
00:22:49,675 --> 00:22:55,030
Een meloen, niet meer of minder.
Heb je me begrepen?

177
00:22:55,033 --> 00:22:56,680
Ik heb je begrepen.

178
00:22:57,020 --> 00:22:58,687
Lap hem op.

179
00:22:59,452 --> 00:23:01,485
We moeten naar Carol rijden, Diane.

180
00:23:01,487 --> 00:23:06,982
We laden hem vol met medische spullen.
Het bloeden moet stoppen of hij haalt het niet.

181
00:23:06,985 --> 00:23:09,159
We gaan.

182
00:23:13,691 --> 00:23:15,144
Stap in.

183
00:23:53,606 --> 00:23:57,975
Blijf bij me. Ik ben er.

184
00:24:00,346 --> 00:24:04,314
Het komt goed.

185
00:24:08,821 --> 00:24:13,557
Om je tegenstander te verwonden...

186
00:24:13,559 --> 00:24:16,727
is hetzelfde als jezelf verwonden.

187
00:24:46,625 --> 00:24:50,761
Sorry dat ik naar je toe ben gekomen.
Er was geen andere optie.

188
00:25:01,440 --> 00:25:03,285
Wacht, Morgan.

189
00:25:24,663 --> 00:25:26,459
Blijf uit mijn buurt.

190
00:25:29,201 --> 00:25:30,534
Luister gewoon.

191
00:25:40,112 --> 00:25:41,511
Ik weet het.

192
00:25:46,885 --> 00:25:48,719
Ik weet het.

193
00:25:50,522 --> 00:25:52,823
Genoeg.

194
00:26:09,208 --> 00:26:13,410
Hou je wapen omhoog, Benjamin.

195
00:26:34,672 --> 00:26:36,572
Ik had het moeten zijn.

196
00:26:39,244 --> 00:26:41,238
Dat zei Gavin...

197
00:26:43,448 --> 00:26:44,914
dat ik de eerste zou zijn.

198
00:26:44,916 --> 00:26:49,941
Ik probeerde het te stoppen.
Ik zou mijn leven geven om het je te laten zien.

199
00:26:51,055 --> 00:26:53,923
Dat zou vandaag gebeuren.

200
00:26:53,925 --> 00:26:56,726
Dat probeerde ik.

201
00:26:56,728 --> 00:27:02,698
En toen liep het helemaal anders
omdat we niks deden om hen tegen te houden.

202
00:27:02,700 --> 00:27:04,500
We hebben niks gedaan.

203
00:27:15,880 --> 00:27:17,647
Niets.

204
00:27:27,759 --> 00:27:32,995
Ik was in het kamp toen het begon.

205
00:27:32,997 --> 00:27:38,601
Het was gigantisch, eindeloos...

206
00:27:38,949 --> 00:27:45,341
een stad vol zeilen, urine en rooklucht,
en huilende baby's.

207
00:27:49,147 --> 00:27:51,499
De baby's huilden altijd.

208
00:27:56,487 --> 00:27:59,088
Ik wist dat ze problemen hadden...

209
00:27:59,090 --> 00:28:02,291
maar ik dacht dat ik ze niet
op hoefde te lossen.

210
00:28:02,293 --> 00:28:06,195
Ik dacht dat ze sterker en slimmer
waren als ik.

211
00:28:09,300 --> 00:28:11,967
Wie ben ik dan wel?

212
00:28:16,441 --> 00:28:18,638
Dus ik heb niets gedaan.

213
00:28:22,513 --> 00:28:25,214
En toen was er een gevecht...

214
00:28:25,216 --> 00:28:28,884
en een brand.

215
00:28:28,886 --> 00:28:30,408
En verloor ik mijn vrouw.

216
00:28:33,791 --> 00:28:39,603
Na drie dagen op de vlucht,
zonder eten of slaap...

217
00:28:40,465 --> 00:28:45,701
de gruwel en angst...

218
00:28:45,703 --> 00:28:48,637
verloor ik mijn kleine meid.

219
00:28:52,810 --> 00:28:54,677
Voor mijn neus...

220
00:29:03,721 --> 00:29:07,423
omdat ik niks deed.

221
00:29:09,827 --> 00:29:12,561
Omdat ik wachtte.

222
00:29:27,278 --> 00:29:29,345
Luister.

223
00:29:29,347 --> 00:29:32,581
Alsjeblieft.

224
00:29:32,583 --> 00:29:36,952
Luister naar me.

225
00:29:36,954 --> 00:29:40,556
We kunnen gebruiken, wat er net is gebeurd.

226
00:29:40,558 --> 00:29:44,827
We kunnen de Verlossers laten zien
dat we het begrijpen.

227
00:29:44,829 --> 00:29:48,364
We weten wat we moeten doen.

228
00:29:48,366 --> 00:29:50,644
Dat we weten hoe we door moeten gaan.

229
00:29:53,604 --> 00:29:56,605
En ze moeten ons geloven.

230
00:29:56,607 --> 00:30:00,883
We moeten iets doen, zodat ze ons geloven.

231
00:30:02,947 --> 00:30:06,977
En als we hun vertrouwen terug hebben...

232
00:30:07,718 --> 00:30:10,453
vermoorden we ze.

233
00:30:10,455 --> 00:30:12,788
Een eind aan hen maken.

234
00:30:12,790 --> 00:30:14,990
We sluiten ons aan bij Alexandria en de Hilltop...

235
00:30:14,992 --> 00:30:17,839
en we vermorzelen ze in ��n keer.

236
00:30:23,601 --> 00:30:25,347
Dit is het, Morgan...

237
00:30:28,806 --> 00:30:33,777
Je moet gaan doden.

238
00:30:37,181 --> 00:30:40,816
Anders kun je net zo goed zelfmoord plegen.

239
00:30:47,525 --> 00:30:49,758
Er moet iemand sterven.

240
00:30:49,760 --> 00:30:52,969
Ik probeerde diegene te zijn,
dat is niet gebeurd.

241
00:30:59,971 --> 00:31:05,173
Dus ik ga het leger leiden om de Verlossers
te vermorzelen en te vernietigen.

242
00:31:06,844 --> 00:31:11,780
Ik.

243
00:31:11,782 --> 00:31:13,733
Ik ga praten met Ezekiel.

244
00:31:15,319 --> 00:31:17,414
En vertel iedereen wat ik heb gedaan.

245
00:31:19,957 --> 00:31:23,125
En leef er de rest van mijn leven mee.

246
00:32:53,451 --> 00:32:55,200
Heb je het hem verteld?

247
00:32:58,556 --> 00:33:00,361
Heb je het gedaan?

248
00:33:00,725 --> 00:33:03,559
Niet nu.

249
00:33:03,561 --> 00:33:05,260
We hebben het erover als we terug zijn.

250
00:33:05,262 --> 00:33:06,677
Wat is er aan de hand?

251
00:33:06,797 --> 00:33:09,779
Het is beter om erover te praten
als we terug zijn, Uwe Majesteit.

252
00:33:12,837 --> 00:33:14,998
We gaan er nu over praten.

253
00:34:03,487 --> 00:34:05,354
Hoe is het met de knul?

254
00:34:21,906 --> 00:34:24,339
Hij is dood.

255
00:34:30,347 --> 00:34:34,550
Loop nu terug voordat ik je vermoord.

256
00:34:34,552 --> 00:34:39,331
E�n woord, een vieze blik
en ik vermoord je hier ter plekke.

257
00:34:40,558 --> 00:34:42,357
Lopen.

258
00:35:01,912 --> 00:35:03,712
Heb je het?

259
00:35:19,130 --> 00:35:22,798
Ik wilde zeggen
dat we het begrepen hebben.

260
00:35:22,800 --> 00:35:24,133
Helemaal.

261
00:35:27,938 --> 00:35:30,572
Stop ermee.
- Laat hem.

262
00:35:31,809 --> 00:35:33,709
Stop, Morgan.

263
00:36:26,030 --> 00:36:28,169
Het was allemaal opgezet.

264
00:36:32,503 --> 00:36:35,205
Hij blokkeerde gisteren de weg
zodat we te laat zouden zijn.

265
00:36:38,342 --> 00:36:40,080
Hij was het.

266
00:36:42,746 --> 00:36:47,049
En we hadden de spullen toen we weggingen...

267
00:36:47,051 --> 00:36:48,083
allemaal.

268
00:36:52,523 --> 00:36:57,920
Hij heeft er ��n uit de lading gehaald,
en verstopt toen we waren gestopt.

269
00:36:57,923 --> 00:37:03,232
Hij wilde wat uitlokken
tussen het Koninkrijk en de Verlossers.

270
00:37:18,783 --> 00:37:22,351
Ik wilde je laten zien...

271
00:37:22,553 --> 00:37:25,017
Ik wilde je laten zien
dat we het hebben begrepen.

272
00:37:27,124 --> 00:37:29,992
Dat we begrijpen dat we weten
wat we moeten doen...

273
00:37:29,994 --> 00:37:34,096
dat we weten dat we door moeten gaan.

274
00:37:40,771 --> 00:37:42,507
Mooi.

275
00:37:45,709 --> 00:37:49,678
Volgende week, zelfde tijd.

276
00:37:53,284 --> 00:37:57,019
Begrepen.

277
00:38:18,409 --> 00:38:21,109
Heeft hij dat gedaan?

278
00:38:21,111 --> 00:38:24,171
Hij wilde eerder sterven als ons.

279
00:38:24,348 --> 00:38:26,736
Hij wilde dat hij het was.

280
00:38:32,323 --> 00:38:35,991
Dacht dat hij kon kiezen,
en daarom moest Duane sterven.

281
00:38:39,296 --> 00:38:41,222
Duane?

282
00:38:55,646 --> 00:38:58,513
Benjamin, daarom moest Benjamin...

283
00:39:04,088 --> 00:39:06,788
Morgan...

284
00:39:06,790 --> 00:39:11,360
Laten we gaan.
- Dat was ik van plan.

285
00:39:11,362 --> 00:39:13,261
Je moet hier niet alleen zijn, Morgan.

286
00:39:13,263 --> 00:39:17,265
Ga maar. Niet nu.

287
00:39:17,267 --> 00:39:20,335
Benjamin zou niet...
- Ik zei niet nu.

288
00:39:23,307 --> 00:39:25,674
Ik zei...

289
00:39:32,483 --> 00:39:34,850
Ga gewoon.

290
00:41:40,223 --> 00:41:43,124
Wil je echt weten
wat er is gebeurd in Alexandria?

291
00:41:45,895 --> 00:41:49,430
Wat is er met jou gebeurd?

292
00:41:49,432 --> 00:41:51,465
Ik heb Richard vermoord.

293
00:41:54,470 --> 00:41:56,904
Ik heb hem gewurgd.

294
00:41:58,941 --> 00:42:01,709
Door hem is Benjamin dood, dus...

295
00:42:05,214 --> 00:42:08,883
Wil je weten wat er is gebeurd in Alexandria?

296
00:42:14,624 --> 00:42:16,423
Ja.

297
00:42:21,631 --> 00:42:24,668
Negan heeft Glenn en Abraham vermoord.

298
00:42:28,604 --> 00:42:31,539
Doodgeslagen met een honkbalknuppel.

299
00:42:36,479 --> 00:42:41,982
Nu hebben de Verlossers Alexandria,
Rick en de rest...

300
00:42:41,984 --> 00:42:45,948
alles wat ze doen is voor de Verlossers.

301
00:42:56,332 --> 00:42:58,699
En ze hebben er nog meer vermoord.

302
00:43:01,904 --> 00:43:05,806
Spencer, Olivia...

303
00:43:09,278 --> 00:43:13,414
Jezus heeft Rick en de rest
hier gebracht omdat...

304
00:43:13,416 --> 00:43:15,071
Rick wil met ze vechten.

305
00:43:21,791 --> 00:43:23,578
Je wilde het weten.

306
00:43:25,528 --> 00:43:27,402
Nu weet je het.

307
00:43:29,365 --> 00:43:31,532
Waar ga je heen?
- Ik ga ze vermoorden...

308
00:43:31,534 --> 00:43:33,334
stuk voor stuk.

309
00:43:33,336 --> 00:43:35,069
Waar?
- Ergens anders.

310
00:43:35,371 --> 00:43:38,551
Wacht...

311
00:43:44,146 --> 00:43:48,447
Je kunt gaan, of niet.

312
00:43:54,056 --> 00:43:55,856
Alsjeblieft.

313
00:44:00,229 --> 00:44:02,029
Alsjeblieft.

314
00:45:01,910 --> 00:45:03,294
Carol.

315
00:45:03,597 --> 00:45:05,492
Het spijt me.

316
00:45:06,662 --> 00:45:09,096
Ik wil je bedanken.

317
00:45:14,303 --> 00:45:16,225
Ik blijf hier.

318
00:45:18,240 --> 00:45:20,503
We moeten zorgen dat we er klaar voor zijn.

319
00:45:21,110 --> 00:45:23,110
We moeten vechten.

320
00:45:25,848 --> 00:45:27,921
Dat klopt.

321
00:45:29,785 --> 00:45:31,647
Maar niet vandaag.

322
00:46:39,187 --> 00:46:43,936
Vertaling:
Quality over Quantity (QoQ) Releases

323
00:46:44,128 --> 00:46:47,970
Download deze ondertitel op:
- www.OpenSubtitles.org -

