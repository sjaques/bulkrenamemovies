1
00:00:35,398 --> 00:00:39,056
Diane.
- Ik doe het.

2
00:00:47,210 --> 00:00:48,943
Goed schot.

3
00:00:48,945 --> 00:00:52,801
Mijn zus had zo'n jurk.
- Denk er niet over na.

4
00:01:08,770 --> 00:01:10,305
Mooi.

5
00:01:12,679 --> 00:01:15,030
Jullie zijn lekker op tijd.

6
00:01:24,081 --> 00:01:28,249
Nu, baas?
- Ja, Jerry.

7
00:01:35,725 --> 00:01:37,859
Het lijkt niet veel.

8
00:01:37,861 --> 00:01:41,832
Misschien moet je nog een keer kijken, Gavin.
Wij houden ons aan de afspraak.

9
00:01:48,462 --> 00:01:50,221
Je hebt gelijk.

10
00:01:50,673 --> 00:01:55,384
Het lijkt niet veel, maar 8, 9, 10.
Je hebt gelijk.

11
00:01:56,346 --> 00:01:59,113
Ik wil zijn pistool.

12
00:02:00,216 --> 00:02:05,238
Ik denk dat hij nooit een
pistool zou mogen hebben.

13
00:02:06,256 --> 00:02:11,181
Geen wapens voor stoute jongens.
- Jij sloeg mij eerst, eikel.

14
00:02:14,535 --> 00:02:15,816
Wat zei je daar?

15
00:02:21,630 --> 00:02:23,536
Hoe gaan we nu verder?

16
00:02:24,003 --> 00:02:26,121
Inderdaad, Uwe Majesteit.

17
00:02:27,073 --> 00:02:29,168
Hoe gaan we nu verder?

18
00:02:34,317 --> 00:02:37,738
Geef hem je pistool, Richard.

19
00:02:48,465 --> 00:02:50,346
Trek je terug, idioot.

20
00:02:55,205 --> 00:02:57,171
Stik er maar in, kloothommel.

21
00:03:08,184 --> 00:03:09,784
Ophouden.

22
00:03:13,356 --> 00:03:16,090
Ezekiel,
je weet dat ik dit niet kan gebruiken.

23
00:03:16,092 --> 00:03:18,192
Wij kunnen dat ook niet.

24
00:03:18,194 --> 00:03:21,970
Richard zal niet meer
bij onze toekomstige ontmoetingen zijn.

25
00:03:21,998 --> 00:03:25,042
Nee, Ezekiel, je moet hem meenemen.

26
00:03:25,216 --> 00:03:29,826
Want als dit niet stopt,
als het een echt probleem wordt...

27
00:03:30,822 --> 00:03:32,588
je herinnert toch wel wat ik heb gezegd?

28
00:03:32,609 --> 00:03:35,102
Hij staat als eerste op de lijst
om geslagen te worden.

29
00:03:35,241 --> 00:03:37,066
Ik deed het net bijna zelf.

30
00:03:42,018 --> 00:03:47,968
Ik weet dat het zijn fout niet is,
maar dit moet stoppen.

31
00:03:48,683 --> 00:03:50,135
Anders...

32
00:03:50,593 --> 00:03:55,752
loopt het misschien uit de hand.

33
00:03:56,199 --> 00:04:01,955
Jared, sta op.
Laten we gaan.

34
00:04:06,810 --> 00:04:11,494
Mag ik alsjeblieft mijn stok terug?

35
00:04:12,947 --> 00:04:15,330
Iemand gaf het aan mij.

36
00:04:16,252 --> 00:04:18,691
Iemand die overleden is.

37
00:04:18,922 --> 00:04:22,758
Gavin, laat mij hem slaan.
- Nee.

38
00:04:24,094 --> 00:04:29,495
Kijk gewoon om je heen, sensei.

39
00:04:30,266 --> 00:04:31,933
Laten we gaan.

40
00:05:00,863 --> 00:05:05,475
Mijn zus hield van die jurk.

41
00:05:12,575 --> 00:05:15,176
Je was daar vandaag heel snel mee.

42
00:05:15,311 --> 00:05:17,044
Indrukwekkend, Benjamin.

43
00:05:17,690 --> 00:05:19,614
Ik zei toch dat ik beter word.

44
00:05:19,616 --> 00:05:21,515
Je was een beetje te snel.

45
00:05:21,517 --> 00:05:25,516
Alleen omdat je weet hoe je moet vechten
betekent niet dat je het op moet zoeken.

46
00:05:25,546 --> 00:05:27,446
Het gebeurde gewoon.
Ik dacht niet na.

47
00:05:27,557 --> 00:05:29,057
Dat moet je wel.

48
00:05:29,518 --> 00:05:33,098
Vanaf nu zul je dat doen. Ok�?

49
00:05:35,632 --> 00:05:37,179
Ja.

50
00:05:39,306 --> 00:05:41,523
Richard.

51
00:05:43,104 --> 00:05:45,310
Wij praten later wel.

52
00:05:45,996 --> 00:05:47,613
Jerry.

53
00:05:50,132 --> 00:05:51,717
Jij bent goed met die stok, man.

54
00:05:55,967 --> 00:06:00,325
Wil je naar de ziekenboeg?
- Nee, ik ben in orde.

55
00:06:01,795 --> 00:06:03,662
Waar gingen jullie heen?

56
00:06:04,216 --> 00:06:07,620
Ik moet Daryl alleen spreken.
Is dat goed?

57
00:06:07,786 --> 00:06:10,942
Ik haal wel iets voor die wond.

58
00:06:13,792 --> 00:06:17,243
Je ging naar hen toe, is het niet?
- Ja.

59
00:06:17,396 --> 00:06:19,296
Hoort dat bij je deal?

60
00:06:23,902 --> 00:06:25,635
Wat is er mis met jou?

61
00:06:26,705 --> 00:06:28,763
Je bloedt.

62
00:06:31,143 --> 00:06:33,130
Ze deden dat jou aan.

63
00:06:33,178 --> 00:06:36,348
Je weet wat ze zijn.
- Ja.

64
00:06:40,586 --> 00:06:45,553
Als Carol hier was
en ze dat allemaal zag...

65
00:06:45,624 --> 00:06:49,993
als ze het wist van Abraham en Glenn...

66
00:06:51,425 --> 00:06:56,733
dan zou ze met ons daar naartoe gaan
om ze allemaal te vermoorden.

67
00:06:57,135 --> 00:06:58,724
Dat zou ze doen.

68
00:06:58,770 --> 00:07:01,468
En daarom is ze weggegaan.

69
00:07:35,374 --> 00:07:37,245
Ik oefen.

70
00:07:38,183 --> 00:07:40,963
Ik moet deze vaker gebruiken.

71
00:07:41,964 --> 00:07:46,042
De Verlossers zijn slim genoeg om te weten
dat ik geen wapen in hun buurt moet hebben.

72
00:07:52,391 --> 00:07:54,154
Morgan zei dat je een boogschutter bent.

73
00:08:07,399 --> 00:08:08,805
Waarom?

74
00:08:11,209 --> 00:08:13,415
Omdat we hetzelfde willen.

75
00:08:13,712 --> 00:08:15,701
Ik heb je hulp nodig.

76
00:09:01,544 --> 00:09:06,579
Quality over Quantity (QoQ) Releases
The Walking Dead S07E10: New Best Friends

77
00:09:13,241 --> 00:09:16,369
<i>We moeten Ezekiel overtuigen.</i>

78
00:09:16,478 --> 00:09:17,994
<i>Dit is het.</i>

79
00:09:18,080 --> 00:09:21,815
<i>Alexandria, de Hilltop,
en het Koninkrijk slaan eerst toe...</i>

80
00:09:21,817 --> 00:09:26,162
<i>raken ze hard
en dan roeien we de Verlossers uit.</i>

81
00:09:27,594 --> 00:09:32,255
<i>Houden veel goede mensen...</i>

82
00:09:33,696 --> 00:09:35,486
<i>we houden ze veilig.</i>

83
00:10:13,001 --> 00:10:17,686
Ze rijden over deze weg.
Als we auto's zien, zijn het de Verlossers.

84
00:10:18,530 --> 00:10:21,129
De laatste tijd komen ze in groepen van drie.

85
00:10:21,757 --> 00:10:24,430
Daarom heb ik jou nodig.
Ik kan ze niet alleen aan.

86
00:10:25,076 --> 00:10:27,254
We raken ze eerst met de wapens...

87
00:10:27,582 --> 00:10:29,182
en dan de Molotovs.

88
00:10:29,184 --> 00:10:31,835
Dan weer de wapens totdat ze dood zijn.

89
00:10:32,173 --> 00:10:35,055
Waarom het vuur?
- Het moet er slecht uitzien.

90
00:10:36,170 --> 00:10:40,360
We willen dat de Verlossers die het ontdekken
woedend zijn.

91
00:10:42,545 --> 00:10:45,489
Ik liet een spoor achter vanaf hier
tot waar ik de wapens verstopte...

92
00:10:45,490 --> 00:10:48,368
bij het huisje van iemand waar
Ezekiel om geeft.

93
00:10:49,425 --> 00:10:51,838
Wie is dat?
- Een eenling die hij ontmoette.

94
00:10:51,840 --> 00:10:55,842
Soms brengt hij eten.
- Waarom wonen ze niet in het Koninkrijk?

95
00:10:56,522 --> 00:10:59,546
Geen idee, ze woont daar,
ze zal daar sterven.

96
00:11:01,922 --> 00:11:03,249
Is het een vrouw?

97
00:11:03,251 --> 00:11:05,885
Maakt dat iets uit?
Ze heeft meer ballen dan ons.

98
00:11:08,256 --> 00:11:10,945
Ze zal hoe dan ook sterven.

99
00:11:10,946 --> 00:11:17,431
Als de Verlossers hun maatjes dood vinden
en ze kunnen een spoor volgen...

100
00:11:17,899 --> 00:11:21,767
gaan ze naar de wapens
en dan naar het huisje...

101
00:11:22,507 --> 00:11:24,124
en dan vallen ze die vrouw aan.

102
00:11:24,901 --> 00:11:26,025
Hoe heet ze?

103
00:11:26,026 --> 00:11:28,270
Misschien vermoorden ze haar,
misschien ook niet.

104
00:11:28,271 --> 00:11:31,632
Maar dan ziet Ezekiel wat hij moet doen.

105
00:11:31,633 --> 00:11:33,613
Hoe heet ze?

106
00:11:33,615 --> 00:11:37,417
Ze is taai, misschien overleeft ze het.
- Zeg verdomme haar naam.

107
00:11:46,094 --> 00:11:47,894
Carol.

108
00:11:47,896 --> 00:11:51,908
Ik hoopte dat je haar niet kende,
dacht niet dat het je iets kon schelen...

109
00:11:52,100 --> 00:11:54,634
omdat je weet wat er moet gebeuren.

110
00:11:54,636 --> 00:11:56,269
Nee.
- Misschien overleeft ze het.

111
00:11:56,271 --> 00:12:00,306
Op deze manier kan het lukken.

112
00:12:00,308 --> 00:12:04,677
Op deze manier komen we van de Verlossers af
en hebben we allemaal een toekomst.

113
00:12:05,543 --> 00:12:08,830
Ze woont daar alleen, wachtend op haar dood.
- Nee.

114
00:12:08,831 --> 00:12:13,135
Als we niets doen, zullen er meer mensen sterven,
mensen die willen leven.

115
00:12:13,221 --> 00:12:15,261
Blijf uit de buurt van Carol, hoor je me?

116
00:12:25,200 --> 00:12:27,133
Dat zijn ze.

117
00:12:30,338 --> 00:12:35,108
We kunnen wachten totdat het misgaat,
dat we mensen verliezen...

118
00:12:36,857 --> 00:12:39,406
of we doen het moeilijke...

119
00:12:40,961 --> 00:12:43,345
en kiezen zelf ons lot uit.

120
00:12:43,944 --> 00:12:46,812
Nee.
- Sorry.

121
00:13:15,171 --> 00:13:16,627
Er zullen er meer komen.

122
00:13:17,538 --> 00:13:21,651
Ze zullen ook weer terugkomen.

123
00:13:22,483 --> 00:13:24,883
We hebben nog een kans.

124
00:13:25,777 --> 00:13:28,166
Maar we hebben bijna geen tijd meer.

125
00:13:28,455 --> 00:13:31,483
Als jij en jouw mensen tegen de Verlossers
willen vechten...

126
00:13:32,159 --> 00:13:35,794
dan moeten jullie het snel doen
en jullie hebben het Koninkrijk nodig.

127
00:13:39,166 --> 00:13:43,528
Wat we moeten doen
vereist toch een opoffering.

128
00:13:45,472 --> 00:13:47,148
Mannen zoals wij...

129
00:13:49,085 --> 00:13:50,809
we zijn al zoveel verloren.

130
00:13:50,811 --> 00:13:53,045
Je kent mij niet.

131
00:13:53,047 --> 00:13:58,570
Ik weet doordat Carol daar alleen woont...

132
00:14:00,758 --> 00:14:03,755
ze nu al wel dood kan zijn.

133
00:14:06,393 --> 00:14:11,186
Als ze gewond raakt, sterft
of koorts krijgt...

134
00:14:11,187 --> 00:14:16,768
gedood wordt door een walker,
geraakt wordt door de bliksem...

135
00:14:16,770 --> 00:14:21,173
als er iets met haar gebeurt,
dan vermoord ik je.

136
00:14:29,416 --> 00:14:32,240
Ik zou sterven voor het Koninkrijk.

137
00:14:32,419 --> 00:14:34,118
Waarom doe je dat dan niet?

138
00:17:11,280 --> 00:17:14,127
Zijn jullie gelijken of hebben jullie een leider?

139
00:17:15,837 --> 00:17:17,070
Hij.

140
00:17:24,252 --> 00:17:28,092
Hoi, ik ben Rick.

141
00:17:30,225 --> 00:17:32,514
Wij bezitten jullie levens.

142
00:17:32,727 --> 00:17:35,355
Wil je ze terugkopen?

143
00:17:36,531 --> 00:17:38,691
Heb je iets?

144
00:17:39,985 --> 00:17:43,994
Jij hebt ��n van mijn mensen.
Gabriel.

145
00:17:44,405 --> 00:17:47,063
Ik wil hem eerst zien.
Daarna praten we.

146
00:18:21,075 --> 00:18:24,797
De spullen die je meenam van de boot
zijn ingenomen.

147
00:18:25,301 --> 00:18:27,405
Ik zag ze, dus pakten we de rest.

148
00:18:29,338 --> 00:18:30,720
En we namen hem mee.

149
00:18:32,311 --> 00:18:35,669
Dan weet je dat we niets hebben
om onze levens terug te kopen.

150
00:18:37,201 --> 00:18:40,989
Dat zul je snel hebben.
Niets.

151
00:18:41,217 --> 00:18:44,305
Want ik en mijn mensen zijn al van die groep...

152
00:18:44,387 --> 00:18:46,938
die de spullen van de boot innamen.

153
00:18:47,223 --> 00:18:50,922
Ze noemen zichzelf de Verlossers.
Zij bezitten onze levens.

154
00:18:51,794 --> 00:18:56,923
En als je ons vermoordt,
zal hij iets hen pakken.

155
00:19:00,169 --> 00:19:02,722
En ze zullen gaan zoeken.

156
00:19:05,025 --> 00:19:08,842
Je hebt maar twee opties
als het om de Verlossers gaat:

157
00:19:08,844 --> 00:19:13,340
Of ze vermoorden je
of ze bezitten je.

158
00:19:13,829 --> 00:19:15,723
Maar er is een uitweg.

159
00:19:17,099 --> 00:19:18,883
Voeg je bij ons.

160
00:19:18,884 --> 00:19:23,527
Voeg je bij ons
om tegen ze te vechten.

161
00:19:31,881 --> 00:19:33,447
Nee.

162
00:19:49,586 --> 00:19:51,565
Rosita, niet doen.

163
00:19:52,835 --> 00:19:56,570
Iedereen stoppen.
Wacht.

164
00:20:00,090 --> 00:20:03,505
Laat ons gaan of ik vermoord haar.

165
00:20:05,715 --> 00:20:09,316
Ga nu bij Tamiel weg.

166
00:20:10,046 --> 00:20:15,641
De Verlossers hebben andere gemeenschappen.

167
00:20:16,492 --> 00:20:22,722
Ze hebben eten, wapens, voertuigen en brandstof.

168
00:20:22,723 --> 00:20:24,932
Wat je maar wilt, de Verlossers hebben het.

169
00:20:39,644 --> 00:20:42,716
Laat Tamiel los.

170
00:20:57,319 --> 00:20:59,462
Zeg wat je wilt.

171
00:21:01,370 --> 00:21:05,545
Als we samenwerken,
kunnen we ze samen verslaan...

172
00:21:05,548 --> 00:21:09,201
en krijg je spullen die nu van hen zijn.

173
00:21:09,204 --> 00:21:15,349
Vechten met ons wordt beloond.
Meer dan je je voor kunt stellen.

174
00:21:15,351 --> 00:21:17,618
Ik wil nu wat.

175
00:21:19,728 --> 00:21:20,754
Rick...

176
00:21:21,364 --> 00:21:23,372
kan alles.

177
00:21:24,903 --> 00:21:26,452
Deze groep.

178
00:21:26,455 --> 00:21:31,398
Heeft mij hier gevonden,
ver van ons huis.

179
00:21:32,021 --> 00:21:36,403
Wat wil je hebben?
Zeg het, dan regelen we het voor je.

180
00:21:36,405 --> 00:21:38,730
We zullen je laten zien wat we kunnen.

181
00:21:39,608 --> 00:21:41,108
Nu.

182
00:21:51,420 --> 00:21:54,021
Tamiel, Brion...

183
00:21:54,023 --> 00:21:56,657
breng Rick naar boven...

184
00:22:05,358 --> 00:22:06,953
Het is goed.

185
00:22:25,586 --> 00:22:28,322
Ga hier zitten.

186
00:22:28,324 --> 00:22:30,057
Niks aan de hand.

187
00:22:33,863 --> 00:22:35,874
Wie zijn die mensen?

188
00:22:38,434 --> 00:22:40,956
Onze nieuwe vrienden, zeker?

189
00:22:44,506 --> 00:22:47,087
Waar brengen ze hem heen?

190
00:23:30,152 --> 00:23:33,553
Wij allemaal,
zijn hier sinds de verandering.

191
00:23:33,555 --> 00:23:37,190
We nemen.
En trekken ons er niks van aan.

192
00:23:37,192 --> 00:23:39,726
Dingen worden moeilijker.

193
00:23:41,270 --> 00:23:45,135
We openen blikjes,
soms is het verrot van binnen.

194
00:23:46,873 --> 00:23:52,143
De tijd ging voorbij.
Dingen veranderen weer.

195
00:23:53,075 --> 00:23:55,264
Dus misschien veranderen wij.

196
00:23:57,385 --> 00:23:59,002
Misschien.

197
00:24:00,946 --> 00:24:03,450
Ik wil weten of je het meent...

198
00:24:03,452 --> 00:24:06,553
en of je het waard bent.

199
00:24:15,319 --> 00:24:17,087
Wat heb je gedaan?

200
00:24:33,303 --> 00:24:34,718
Rick.

201
00:24:40,031 --> 00:24:42,164
Ik ben in orde.

202
00:26:14,943 --> 00:26:16,092
De muren.

203
00:26:16,094 --> 00:26:17,526
Gebruik ze.

204
00:27:00,958 --> 00:27:03,506
Geloof je ons nu?

205
00:27:05,528 --> 00:27:09,879
Vertel ons wat je wilt en we regelen het.

206
00:28:11,497 --> 00:28:13,196
Wapens.

207
00:28:13,656 --> 00:28:17,530
Heel veel.

208
00:28:18,533 --> 00:28:20,688
En dan vechten we met je mee.

209
00:28:38,225 --> 00:28:42,805
Weet je wat de winst is?
- Dat weet ik.

210
00:28:44,167 --> 00:28:46,451
De helft van de winst.

211
00:28:46,454 --> 00:28:47,543
Je krijgt 'n derde.

212
00:28:47,545 --> 00:28:51,233
En we zullen terugpakken
wat je van ons hebt gestolen.

213
00:28:53,368 --> 00:28:55,718
De helft.
- 'n Derde.

214
00:28:55,720 --> 00:28:57,153
De helft.

215
00:28:59,323 --> 00:29:01,473
'n Derde.

216
00:29:02,293 --> 00:29:05,757
'n Derde,
en we houden wat we hebben gestolen.

217
00:29:09,934 --> 00:29:13,602
De helft van de potten,
die we al hadden.

218
00:29:13,604 --> 00:29:17,173
Laatste kans. Ja?

219
00:29:18,843 --> 00:29:22,545
Zeg ja.

220
00:29:23,713 --> 00:29:25,550
Ja.

221
00:29:26,803 --> 00:29:32,051
En de wapens.

222
00:29:40,765 --> 00:29:45,847
Ik heb lang genoeg bij de boot gewacht.
Ik wil er wat voor.

223
00:29:45,850 --> 00:29:51,106
Dit is het dan.
Potten en wapens, wapens en potten.

224
00:29:52,092 --> 00:29:56,045
Heb je gewacht totdat iemand
die spullen van de boot ging halen?

225
00:29:56,666 --> 00:30:00,616
Lang.
We nemen. We doen geen moeite.

226
00:30:04,722 --> 00:30:09,902
Dat ding daarbinnen,
was dat om iemand zichzelf te laten bewijzen?

227
00:30:09,905 --> 00:30:13,162
Nee. Zijn naam was Winslow.

228
00:30:18,236 --> 00:30:20,469
Wat ga je doen met Gabriel?

229
00:30:21,860 --> 00:30:25,941
Ga.
Anders verloopt de deal.

230
00:30:25,943 --> 00:30:28,677
Snel.

231
00:30:31,656 --> 00:30:33,944
Hoe heet je?

232
00:30:34,252 --> 00:30:36,151
Jadis.

233
00:31:17,159 --> 00:31:18,737
We hebben een deal.

234
00:31:55,574 --> 00:31:59,587
Ik heb tegen Richard gezegd: geen bezoekjes meer.
- Daar ben ik me van bewust.

235
00:31:59,590 --> 00:32:03,346
Uw wens is eenzaamheid.
Daarom heb ik iets wat het vergemakkelijkt.

236
00:32:03,348 --> 00:32:06,536
Mijn mannen komen de doden wegwerken.

237
00:32:06,539 --> 00:32:10,020
De doden zijn onbedachtzaam voor degenen
die alleen willen zijn.

238
00:32:10,022 --> 00:32:15,158
Ik dacht dat onze inspanning
wel gewaardeerd zou worden.

239
00:32:15,160 --> 00:32:17,288
Jij bent degene die de deur geopend heeft.

240
00:32:17,996 --> 00:32:19,696
Je stond op mijn draad.

241
00:32:19,698 --> 00:32:22,499
Ik dacht dat ik hem op tijd had.
Sorry.

242
00:32:22,501 --> 00:32:25,135
Je had ze goed verborgen, dame.
- Noem haar geen 'dame.'

243
00:32:25,137 --> 00:32:28,872
Mevrouw. Miss...
- Hou je kop maar.

244
00:32:28,874 --> 00:32:30,774
Begrepen.

245
00:32:30,776 --> 00:32:33,577
Tot ziens, Uwe Majesteit.

246
00:32:33,579 --> 00:32:35,612
Wacht.

247
00:32:38,878 --> 00:32:41,852
Cobbler.

248
00:32:43,455 --> 00:32:47,924
Kevin zei dat u dat lekker vindt.
Het was voor als u de deur niet open zou doen.

249
00:32:55,143 --> 00:32:56,700
Ga.

250
00:34:01,900 --> 00:34:05,268
Jezus heeft ons naar het Koninkrijk gebracht.

251
00:34:05,270 --> 00:34:08,371
Morgan zei dat je gewoon weg was gegaan.

252
00:34:09,382 --> 00:34:11,210
Ik was hier.

253
00:34:11,910 --> 00:34:15,946
Ik zag je.

254
00:34:18,517 --> 00:34:22,552
Waarom ben je weggegaan?

255
00:34:29,494 --> 00:34:33,129
Ik moest.

256
00:34:47,713 --> 00:34:51,381
Ik heb wat gehoord toen ik op wacht stond,
binnen de muren.

257
00:34:51,383 --> 00:34:54,351
Ik ben de voorraadkamer ingegaan,
iemand besprong me.

258
00:34:54,353 --> 00:34:59,756
Ze was boos omdat ze de voorraad uit de boot
niet kreeg na het lange wachten.

259
00:34:59,758 --> 00:35:02,359
Dus ze liet me de rest inpakken.

260
00:35:02,361 --> 00:35:06,730
Ze zei dat het op de boot lag, dus ik hoopte...

261
00:35:06,732 --> 00:35:09,299
Nee. Je hoopte niet alleen.

262
00:35:09,301 --> 00:35:11,935
Je hebt ons hier gekregen.

263
00:35:15,941 --> 00:35:18,379
Ik begon het geloof te verliezen.

264
00:35:22,714 --> 00:35:25,849
Maar toen zag ik jou. Je knikte naar me.

265
00:35:25,851 --> 00:35:30,387
Alleen het feit al dat je wist dat ik niet weg was,
dat je mij zocht en vond...

266
00:35:30,389 --> 00:35:34,335
dat je die nummers zag, je leek zo...

267
00:35:34,338 --> 00:35:36,693
blij.

268
00:35:39,765 --> 00:35:42,432
We gaan het regelen.

269
00:35:42,434 --> 00:35:45,983
Maar het gaat nog moeilijk worden tot die tijd.

270
00:35:47,205 --> 00:35:49,184
We moeten volhouden.

271
00:35:49,775 --> 00:35:51,441
Ik wel.

272
00:35:51,443 --> 00:35:53,576
Bedankt.

273
00:36:00,919 --> 00:36:03,286
Waarom lachte je?

274
00:36:04,756 --> 00:36:08,458
Wat maakt je zo zeker?

275
00:36:11,405 --> 00:36:15,942
Iemand liet me zien dat vijanden
ook vrienden kunnen worden.

276
00:36:27,429 --> 00:36:29,846
We moeten niet teruggaan naar Alexandria.

277
00:36:29,848 --> 00:36:33,427
We moeten op zoek
naar wapens voor deze deal.

278
00:36:33,652 --> 00:36:35,652
Rick is gewond. Aaron ook.

279
00:36:35,654 --> 00:36:40,841
Ik ben veel zenuwachtiger voor wat Eric
gaat zeggen als hij mijn gezicht weer ziet.

280
00:36:41,059 --> 00:36:42,792
Mensen thuis hebben eten nodig.

281
00:36:42,794 --> 00:36:47,263
Rick wil dat we voorraad terugbrengen
en daarna hergroeperen, dus dat doen we dus.

282
00:36:48,333 --> 00:36:50,769
Dan ga ik alleen.

283
00:36:54,606 --> 00:36:58,041
We blijven bij elkaar, punt uit.

284
00:36:59,644 --> 00:37:02,045
Wat is je probleem?

285
00:37:02,047 --> 00:37:06,182
We zijn nu niet uit op een gevecht, Rosita.
We zorgen dat we daar klaar voor zijn.

286
00:37:06,184 --> 00:37:09,093
Het is altijd een gevecht, Tara.

287
00:37:09,584 --> 00:37:13,056
Ik wil niet dat iemand ons in de weg loopt
of ons vertraagt.

288
00:37:13,058 --> 00:37:16,659
Dat we mensen tegenhouden die van ons nemen,
of wij nemen het van andere mensen.

289
00:37:16,661 --> 00:37:18,900
Dat boeit mij niet.

290
00:37:19,498 --> 00:37:21,199
Wij zullen winnen.

291
00:37:21,366 --> 00:37:23,466
Word volwassen.

292
00:37:26,671 --> 00:37:28,605
Wapens.

293
00:37:28,607 --> 00:37:30,073
Snel.

294
00:37:30,075 --> 00:37:32,709
Snel of anders...

295
00:37:38,817 --> 00:37:43,086
Als we je gehecht hebben gaan we
op zoek naar wapens, toch?

296
00:37:43,088 --> 00:37:46,990
Dat klopt.
- Enig idee waar?

297
00:37:46,992 --> 00:37:50,493
Nee, maar dat heeft ons nooit tegengehouden.

298
00:37:50,496 --> 00:37:54,097
Jij bent verder als ons allemaal geweest, Tara.

299
00:37:54,099 --> 00:37:56,775
Vertel ons in ieder geval
waar we niet moeten kijken.

300
00:37:57,636 --> 00:38:00,804
Goed. Doe ik.

301
00:38:00,806 --> 00:38:02,608
We gaan.

302
00:38:03,708 --> 00:38:04,969
Wacht.

303
00:38:13,952 --> 00:38:16,619
Waarom ben je...

304
00:38:16,621 --> 00:38:19,155
Omdat we gewonnen hebben.

305
00:38:21,159 --> 00:38:23,293
En om degene te vervangen die je kwijt was.

306
00:38:26,731 --> 00:38:29,923
Goed. We gaan.

307
00:38:42,101 --> 00:38:44,036
Ik kon niet iedereen verliezen.

308
00:38:46,440 --> 00:38:48,640
Geen van allen.

309
00:38:52,056 --> 00:38:55,113
Ik kon jou ook niet verliezen.

310
00:39:01,597 --> 00:39:03,655
Ik kon ze niet vermoorden.

311
00:39:08,295 --> 00:39:10,539
Ik kon het wel.

312
00:39:11,060 --> 00:39:12,623
Ik zou het doen.

313
00:39:14,668 --> 00:39:20,305
Als ze nog iemand van onze mensen pijn zouden doen,
dan zou ik het doen.

314
00:39:25,512 --> 00:39:29,247
En daarna zou ik mezelf verliezen.

315
00:39:39,593 --> 00:39:41,927
De verlossers, zijn ze gekomen?

316
00:39:43,997 --> 00:39:45,778
Ja.

317
00:40:04,596 --> 00:40:07,168
Is er iemand gewond geraakt?

318
00:40:10,924 --> 00:40:12,958
Is iedereen in orde?

319
00:40:17,731 --> 00:40:19,706
Hebben de Verlossers...

320
00:40:29,743 --> 00:40:34,079
Is iedereen veilig thuis gekomen?

321
00:40:39,653 --> 00:40:41,525
Daryl...

322
00:40:48,395 --> 00:40:50,462
Ze zijn gekomen.

323
00:40:50,464 --> 00:40:52,880
We hebben ze allemaal te grazen genomen.

324
00:40:54,348 --> 00:40:58,770
We hebben een deal gesloten met de rest,
zoals Ezekiel.

325
00:40:59,840 --> 00:41:02,508
Iedereen is in orde.

326
00:41:04,287 --> 00:41:06,048
Iedereen is in orde.

327
00:41:14,321 --> 00:41:18,590
Gaan we nog eten, of moet je een koning zijn
om hier voedsel te krijgen?

328
00:41:21,629 --> 00:41:23,128
Hou je kop.

329
00:41:38,812 --> 00:41:40,345
Ezekiel...

330
00:41:40,347 --> 00:41:42,047
is hij in orde?

331
00:41:45,030 --> 00:41:47,174
Dat denk ik wel.

332
00:42:34,501 --> 00:42:39,070
Zorg goed voor jezelf.

333
00:43:13,807 --> 00:43:17,075
Je bent goed met haar.

334
00:43:18,645 --> 00:43:23,281
Ezekiel zal onder de indruk zijn.

335
00:43:23,283 --> 00:43:27,352
Iedereen die een tijger als huisdier heeft
kan het niet slecht doen.

336
00:43:29,723 --> 00:43:32,123
Hij is volgens Carol in orde.

337
00:43:32,125 --> 00:43:34,734
Ik heb haar gevonden in dat huisje.

338
00:43:35,562 --> 00:43:38,396
Wat ik zei...

339
00:43:39,617 --> 00:43:44,321
toen ik zei dat ze gewoon weg was gegaan...

340
00:43:46,443 --> 00:43:49,674
moest ik van haar zeggen.
- Ik begrijp het.

341
00:43:52,720 --> 00:43:54,942
We hebben het Koninkrijk nodig.

342
00:43:56,152 --> 00:43:58,629
Dat moet je laten gebeuren.

343
00:44:01,955 --> 00:44:04,122
Het spijt me.

344
00:44:04,124 --> 00:44:07,670
Dat meen ik echt...

345
00:44:10,597 --> 00:44:13,465
maar ik kan het niet zijn.

346
00:44:14,868 --> 00:44:19,596
Luister, waar jij je ook aan vastklampt...

347
00:44:21,041 --> 00:44:23,752
is allang weg.

348
00:44:24,845 --> 00:44:27,379
Word wakker.

349
00:44:33,787 --> 00:44:35,668
Jij bent hetzelfde als ik, Daryl.

350
00:44:35,956 --> 00:44:38,957
Je weet helemaal niks van mij.
- Dat doe ik wel.

351
00:44:38,959 --> 00:44:42,770
Omdat je Carol niet hebt verteld
wat er is gebeurd.

352
00:44:43,664 --> 00:44:47,524
Dat heb je niet gedaan, anders zou ze hier zijn.

353
00:44:49,249 --> 00:44:51,202
En daar ben ik blij om.

354
00:44:54,608 --> 00:44:57,265
Dan zie je dat we ons allemaal
ergens aan vastklampen.

355
00:45:04,566 --> 00:45:08,766
Ik ga morgenochtend terug naar de Hilltop,
en zorg dat ik er klaar voor ben.

356
00:45:38,660 --> 00:45:45,048
Vertaling:
Quality over Quantity (QoQ) Releases

357
00:45:45,248 --> 00:45:49,368
Download deze ondertitel op:
www.OpenSubtitles.org -

