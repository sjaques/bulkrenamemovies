1
00:01:18,990 --> 00:01:23,322
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

2
00:02:28,330 --> 00:02:30,864
God damn it.

3
00:02:41,075 --> 00:02:43,075
Wake it up, kid.

4
00:02:43,077 --> 00:02:45,878
Why does it have to be so early?

5
00:02:45,880 --> 00:02:48,047
Got a lot to do today.

6
00:02:52,053 --> 00:02:53,974
You want to go back to sleep?

7
00:02:54,094 --> 00:02:55,839
No way.

8
00:02:56,190 --> 00:02:58,363
You said you wanted to be able
to knock your brother

9
00:02:58,388 --> 00:03:00,535
on his butt when you first came to me.

10
00:03:02,315 --> 00:03:04,198
That really what you want?

11
00:03:06,467 --> 00:03:07,934
Yeah.

12
00:03:07,936 --> 00:03:10,064
No.

13
00:03:10,818 --> 00:03:13,273
You want to be like your brother.

14
00:03:13,521 --> 00:03:15,359
Yeah?

15
00:03:15,623 --> 00:03:18,062
Yeah.

16
00:03:19,560 --> 00:03:21,927
All right.

17
00:03:21,929 --> 00:03:23,695
Check your feet,

18
00:03:23,697 --> 00:03:27,332
- 'cause if your feet ain't right...
- Nothing's right.

19
00:03:27,334 --> 00:03:28,967
Huh.

20
00:04:52,019 --> 00:04:53,585
Hey!

21
00:04:53,587 --> 00:04:55,020
We were just about
to come out to clear the...

22
00:04:55,022 --> 00:04:56,788
Where's Morgan?

23
00:04:56,790 --> 00:04:58,023
You okay?

24
00:04:58,025 --> 00:05:00,692
Where is Morgan?

25
00:05:10,204 --> 00:05:11,737
Why did Jesus bring Daryl

26
00:05:11,739 --> 00:05:13,605
and everyone else to the Kingdom?

27
00:05:15,843 --> 00:05:20,212
They, um...

28
00:05:20,214 --> 00:05:21,913
They wanted to see if we could all start

29
00:05:21,915 --> 00:05:23,949
working together on things.

30
00:05:27,288 --> 00:05:29,154
The Saviors are still here.

31
00:05:29,156 --> 00:05:33,792
They're not fighting
with the Kingdom, but...

32
00:05:33,794 --> 00:05:36,528
Daryl said they beat them
back in Alexandria.

33
00:05:36,530 --> 00:05:38,096
Is that true?

34
00:05:38,098 --> 00:05:40,832
That they made a deal with them?

35
00:05:40,834 --> 00:05:45,003
Is everyone okay?

36
00:05:45,005 --> 00:05:46,772
Just tell me if it's true.

37
00:05:46,774 --> 00:05:49,474
You need to talk to Daryl about that.

38
00:05:54,315 --> 00:05:56,982
He wanted me to keep
the whole story about you

39
00:05:56,984 --> 00:05:59,584
from everybody.

40
00:05:59,586 --> 00:06:01,687
Told me not to say where you were,

41
00:06:01,689 --> 00:06:05,190
and I did what you asked.

42
00:06:05,192 --> 00:06:08,994
Daryl didn't find you because of me.

43
00:06:08,996 --> 00:06:10,996
And what was said between you and him

44
00:06:10,998 --> 00:06:14,132
was said between you and him.

45
00:06:17,538 --> 00:06:19,171
I will go with you to Alexandria,

46
00:06:19,173 --> 00:06:22,607
if you want to make the trip...

47
00:06:22,609 --> 00:06:24,676
if you want to talk to him.

48
00:06:30,551 --> 00:06:33,518
You shouldn't go alone.

49
00:06:43,764 --> 00:06:46,865
You found what you wanted, right?

50
00:06:46,867 --> 00:06:51,670
You got away from everyone.

51
00:06:51,672 --> 00:06:56,041
Is it what you wanted?

52
00:06:56,043 --> 00:06:59,177
Was it just too late to get away?

53
00:07:07,020 --> 00:07:11,156
Hey, we can leave
for Alexandria right now.

54
00:07:11,158 --> 00:07:13,392
Right now, if you want to.

55
00:07:20,667 --> 00:07:22,167
Is there anything I can help you with?

56
00:07:22,169 --> 00:07:24,870
- No.
- Well, can I just...

57
00:07:24,872 --> 00:07:26,905
If you don't mind, could I walk
back to the cottage with you?

58
00:07:26,907 --> 00:07:29,374
No!

59
00:07:29,376 --> 00:07:32,878
We have a drop today, but I can miss it.

60
00:07:32,880 --> 00:07:35,947
I just...

61
00:07:35,949 --> 00:07:37,282
I'm trying to learn,

62
00:07:37,284 --> 00:07:40,285
and seeing what you did here, I...

63
00:07:40,287 --> 00:07:43,922
There were five of them.

64
00:07:43,924 --> 00:07:45,689
I would just really appreciate it

65
00:07:45,714 --> 00:07:49,753
if you would just... let me see
how you do what you do.

66
00:07:51,121 --> 00:07:53,932
No.
Go do your drop.

67
00:07:58,305 --> 00:08:00,105
Okay.

68
00:09:22,041 --> 00:09:23,651
You may approach, Nabila.

69
00:09:23,776 --> 00:09:25,442
I'm fine where I am.

70
00:09:26,971 --> 00:09:30,018
Shiva likes you.

71
00:09:30,349 --> 00:09:32,616
That's what I'm afraid of.

72
00:09:35,721 --> 00:09:38,189
Tell me what you found.

73
00:09:38,191 --> 00:09:39,690
You're not gonna like it.

74
00:09:39,692 --> 00:09:43,394
I like waiting to hear bad news less.

75
00:09:43,396 --> 00:09:44,895
We got a little problem that could turn

76
00:09:44,897 --> 00:09:47,498
into a big problem inside of a week.

77
00:09:47,500 --> 00:09:51,835
Most of the plants down there,
they got weevils.

78
00:09:51,837 --> 00:09:53,637
We have to cut and burn everything

79
00:09:53,639 --> 00:09:57,541
before the weevils get to
the main crops in the courtyard.

80
00:09:57,543 --> 00:10:00,277
I know the royal garden is
important to you.

81
00:10:00,279 --> 00:10:02,046
I'm sorry.

82
00:10:03,950 --> 00:10:06,050
Thank you, Nabila.

83
00:10:06,052 --> 00:10:08,252
Do what must be done.

84
00:10:08,254 --> 00:10:11,355
Here's the beautiful thing,
Your Majesty...

85
00:10:11,357 --> 00:10:14,725
You can tear it out and cut it down.

86
00:10:14,727 --> 00:10:17,861
You can burn it and throw it all away.

87
00:10:17,863 --> 00:10:20,531
But if you want, it can all grow back.

88
00:10:25,271 --> 00:10:29,240
Okay.
I think I just pissed myself.

89
00:10:29,242 --> 00:10:32,009
I'm-a gonna go.

90
00:10:51,430 --> 00:10:53,297
We're at
"five minutes go."

91
00:10:53,299 --> 00:10:54,898
Okay.

92
00:10:54,900 --> 00:10:57,301
Um...

93
00:10:57,303 --> 00:10:59,003
thank you for this.

94
00:10:59,005 --> 00:11:01,605
I'm still thinking about it.

95
00:11:01,607 --> 00:11:07,111
"To injure an opponent is
to injure oneself."

96
00:11:07,113 --> 00:11:09,280
Get injured no matter what.

97
00:11:11,717 --> 00:11:15,486
I got something for you...
for the new place.

98
00:11:15,488 --> 00:11:17,288
Thanks.

99
00:11:25,264 --> 00:11:26,697
Wow.

100
00:11:26,699 --> 00:11:29,500
I found it at a restaurant
I was scavenging.

101
00:11:29,502 --> 00:11:32,670
I know a girl,
and she, uh, fixed it up for me.

102
00:11:36,642 --> 00:11:38,542
There.

103
00:11:38,544 --> 00:11:40,778
I was getting tired
of looking at that nail.

104
00:11:44,717 --> 00:11:46,450
So, who's the girl?

105
00:11:46,452 --> 00:11:48,285
Uh...

106
00:11:48,287 --> 00:11:50,754
We're at, like,
"three minutes go" right now,

107
00:11:50,756 --> 00:11:52,356
so...

108
00:12:08,974 --> 00:12:11,141
Kid's too young to be a father.

109
00:12:13,145 --> 00:12:14,678
Were you one?

110
00:12:15,981 --> 00:12:18,549
I was, thank God.

111
00:12:18,551 --> 00:12:20,050
But I wasn't too young...

112
00:12:20,052 --> 00:12:24,955
perfect age, perfect time.

113
00:12:24,957 --> 00:12:29,360
I had myself the perfect family
and the perfect life.

114
00:12:29,362 --> 00:12:32,696
I didn't ask for much,
and I got more than I deserved.

115
00:12:34,567 --> 00:12:36,367
And you?

116
00:12:43,743 --> 00:12:49,146
I'm sorry for how things have
been between you and me.

117
00:12:49,148 --> 00:12:51,749
I think you're wrong about killing...

118
00:12:51,751 --> 00:12:54,084
about how you think things can
last like this with the Saviors.

119
00:12:55,921 --> 00:12:59,256
But I'm sorry.

120
00:12:59,258 --> 00:13:01,825
I know you're a good man.

121
00:13:07,199 --> 00:13:11,402
The day's coming
where you can't be that good.

122
00:13:11,404 --> 00:13:13,537
When that happens,

123
00:13:13,539 --> 00:13:15,272
don't beat yourself up about it.

124
00:13:21,380 --> 00:13:24,882
Benjamin, it's time
for us to take our leave.

125
00:13:26,685 --> 00:13:28,252
See you, little man.

126
00:13:30,556 --> 00:13:31,889
Leave the cobbler.

127
00:13:31,891 --> 00:13:33,924
Really?

128
00:13:33,926 --> 00:13:35,092
Jerry.

129
00:13:37,630 --> 00:13:38,962
Fine.

130
00:13:47,072 --> 00:13:48,806
Who's the girl?

131
00:14:01,954 --> 00:14:05,422
Everyone stay down.
Get your guns up.

132
00:14:06,959 --> 00:14:09,059
Somebody blocked the road.

133
00:14:09,061 --> 00:14:12,129
Can't be for anything good.

134
00:14:12,131 --> 00:14:15,999
Your Majesty, stay inside.

135
00:14:31,183 --> 00:14:33,951
Take a look if you want,
but keep your guns up.

136
00:14:46,866 --> 00:14:48,999
Echelon right.

137
00:14:49,001 --> 00:14:51,969
Force towards the building over there.

138
00:14:53,272 --> 00:14:55,873
Let's close around the king.

139
00:14:55,875 --> 00:14:57,774
I got cover.

140
00:15:10,489 --> 00:15:12,789
Let's move towards the building.

141
00:15:18,964 --> 00:15:20,931
Benjamin, get your gun up.

142
00:15:24,336 --> 00:15:26,537
Think that's high enough?

143
00:15:26,539 --> 00:15:28,338
You're fine, man.

144
00:15:56,869 --> 00:15:58,669
Jesus...

145
00:16:04,410 --> 00:16:10,113
This world drives one mad.

146
00:16:10,115 --> 00:16:13,684
People have lived through
every kind of misery...

147
00:16:13,686 --> 00:16:17,054
tyrants, genocide,
systems of quiet hate...

148
00:16:17,056 --> 00:16:19,957
while others enjoy freedom
on the same piece of land.

149
00:16:19,959 --> 00:16:23,694
Yet this, how we must exist now...

150
00:16:23,696 --> 00:16:26,430
It is mere luck we are not all insane.

151
00:16:27,600 --> 00:16:29,766
It isn't luck, Your Majesty.

152
00:16:29,768 --> 00:16:31,435
How's that?

153
00:16:31,437 --> 00:16:35,372
The world does drive people crazy now.

154
00:16:35,374 --> 00:16:38,508
But...

155
00:16:38,510 --> 00:16:40,477
you made us another world.

156
00:17:15,481 --> 00:17:16,980
You're late.

157
00:17:16,982 --> 00:17:19,282
If you could indulge us
your pardon, our path...

158
00:17:19,284 --> 00:17:21,318
I don't want to hear about it.

159
00:17:21,320 --> 00:17:22,653
Don't interrupt the king.

160
00:17:23,922 --> 00:17:25,088
Unh!

161
00:17:28,127 --> 00:17:31,094
You rat-faced prick.

162
00:17:31,096 --> 00:17:34,297
Ezekiel, there aren't any kings,

163
00:17:34,299 --> 00:17:37,567
presidents, or prime ministers.

164
00:17:37,569 --> 00:17:40,837
And all that is a fairy tale, too.

165
00:17:40,839 --> 00:17:44,708
Don't bring that
"Your Highness" shit our way.

166
00:17:44,710 --> 00:17:50,447
Now, I appreciate that
you've been delivering, but...

167
00:17:50,449 --> 00:17:52,282
things have been unnecessarily tense,

168
00:17:52,284 --> 00:17:54,651
and that makes me unnecessarily tense.

169
00:17:54,653 --> 00:17:57,020
I didn't go this route for stress.

170
00:17:57,022 --> 00:18:00,424
No.
Just the opposite.

171
00:18:00,426 --> 00:18:03,527
So let's get to it.
Do you have today's offering?

172
00:18:15,641 --> 00:18:17,207
I want your guns, too.

173
00:18:20,813 --> 00:18:22,846
We did not agree to that.

174
00:18:22,848 --> 00:18:25,082
Well, you got a choice...

175
00:18:25,084 --> 00:18:27,350
Same one that's been there
since the beginning, I guess.

176
00:18:27,352 --> 00:18:32,489
You can give up your guns,
or you can try to use them.

177
00:18:32,491 --> 00:18:34,524
What's it gonna be?

178
00:18:42,968 --> 00:18:45,335
We should give them over, Your Majesty.

179
00:18:45,337 --> 00:18:50,440
"Ehh, we should
give 'em over, Your Majesty."

180
00:18:50,442 --> 00:18:55,011
Or maybe I'll just shove
this down your throat.

181
00:18:55,013 --> 00:18:57,013
Give Morgan his stick back.

182
00:18:58,684 --> 00:19:00,417
Then you can have the guns.

183
00:19:00,419 --> 00:19:02,919
And all of us can go on
with our day's journeys.

184
00:19:02,921 --> 00:19:05,789
Ezekiel...

185
00:19:05,791 --> 00:19:09,192
I need you to understand the gravity

186
00:19:09,194 --> 00:19:11,728
of what's happening here.

187
00:19:11,730 --> 00:19:13,597
I gave you a choice.

188
00:19:13,599 --> 00:19:17,601
What is it going to be?

189
00:19:17,603 --> 00:19:19,770
Come on.

190
00:19:19,772 --> 00:19:21,538
You know the answer.

191
00:19:21,540 --> 00:19:23,707
It's fine.

192
00:19:35,120 --> 00:19:37,053
Give them your guns.

193
00:19:46,064 --> 00:19:48,165
We took your guns just now

194
00:19:48,167 --> 00:19:50,167
because things are
about to get emotional.

195
00:19:50,169 --> 00:19:51,568
You guys have proven

196
00:19:51,570 --> 00:19:54,437
that you don't do so good
with emotional.

197
00:19:54,439 --> 00:19:57,174
You're short.

198
00:19:57,176 --> 00:19:58,775
We said 12.
I count 11.

199
00:19:58,777 --> 00:20:00,076
There are 12 there.

200
00:20:00,078 --> 00:20:02,012
- There aren't.
- Count again.

201
00:20:02,014 --> 00:20:03,480
You count them!

202
00:20:15,661 --> 00:20:18,528
I counted them.

203
00:20:18,530 --> 00:20:20,630
I did.
T-This is impossible.

204
00:20:20,632 --> 00:20:23,166
It's possible.

205
00:20:23,168 --> 00:20:25,368
It's real.

206
00:20:25,370 --> 00:20:28,271
It's happening now.

207
00:20:28,273 --> 00:20:32,943
And the problems have
to end... now.

208
00:20:32,945 --> 00:20:35,445
You have to learn the stakes here,

209
00:20:35,447 --> 00:20:39,149
so we're gonna teach you.
You don't have to do anything.

210
00:20:39,151 --> 00:20:41,484
We'll get you twice the tribute
in an hour.

211
00:20:41,486 --> 00:20:46,328
Right now... is
the only time that matters.

212
00:20:46,959 --> 00:20:49,683
And right now...

213
00:20:50,662 --> 00:20:53,330
you haven't held up
your side of the bargain.

214
00:20:54,331 --> 00:20:57,534
After we have talked about things,

215
00:20:57,536 --> 00:21:04,407
over and over again and again.

216
00:21:04,409 --> 00:21:09,512
So we're gonna deal with all of this...

217
00:21:09,514 --> 00:21:11,748
right now.

218
00:21:11,750 --> 00:21:15,051
Right now
as in... "right now"?

219
00:21:15,053 --> 00:21:16,386
Yeah.

220
00:21:16,388 --> 00:21:19,155
Right... now.

221
00:21:45,117 --> 00:21:46,917
Just do it.

222
00:21:48,253 --> 00:21:49,519
Okay.

223
00:21:49,521 --> 00:21:51,187
No!

224
00:21:52,574 --> 00:21:54,645
No!

225
00:21:54,647 --> 00:21:57,581
Everybody settle down!

226
00:21:58,508 --> 00:22:01,359
We need to get Benjamin
back to the Kingdom.

227
00:22:03,762 --> 00:22:06,095
Give the man his damn stick!

228
00:22:06,097 --> 00:22:09,265
Get in the truck!
Don't say a goddamn word!

229
00:22:12,170 --> 00:22:14,437
- We need to get back.
- No.

230
00:22:14,439 --> 00:22:16,239
Right now, you have to listen.

231
00:22:16,241 --> 00:22:19,642
For once, be present and listen.

232
00:22:19,644 --> 00:22:21,611
You will make your deliveries on time,

233
00:22:21,613 --> 00:22:24,080
every time, and they will be complete.

234
00:22:24,082 --> 00:22:26,716
It's A's or F's, no I's.

235
00:22:28,186 --> 00:22:29,986
You are gonna show me
that you understand

236
00:22:29,988 --> 00:22:32,422
by bringing the balance
of what you owe tomorrow.

237
00:22:32,424 --> 00:22:34,190
We need to go!
He's bleeding out!

238
00:22:34,192 --> 00:22:36,693
One cantaloupe, not more, not less!

239
00:22:36,695 --> 00:22:38,962
Do you understand?!

240
00:22:38,964 --> 00:22:41,197
Yes, I understand.

241
00:22:41,199 --> 00:22:42,599
Go patch him up.

242
00:22:42,601 --> 00:22:43,967
Okay.

243
00:22:43,969 --> 00:22:46,002
Dianne, we need to drive to Carol's!

244
00:22:46,004 --> 00:22:48,004
We loaded her up with medical supplies!

245
00:22:48,006 --> 00:22:49,372
We need to stop the bleeding.

246
00:22:49,374 --> 00:22:50,974
He's not gonna make it to the Kingdom.

247
00:22:50,976 --> 00:22:53,676
Aah!
Let's go!

248
00:22:53,678 --> 00:22:54,878
Up!

249
00:22:56,848 --> 00:22:58,781
Hey!
Get in!

250
00:23:38,123 --> 00:23:40,256
Stay with me.
Stay with me.

251
00:23:40,258 --> 00:23:42,492
I'm right here.
Stay with me.

252
00:23:44,863 --> 00:23:46,062
All right.
Okay.

253
00:23:46,064 --> 00:23:48,831
It's okay.

254
00:23:48,833 --> 00:23:53,336
It's okay.

255
00:23:53,338 --> 00:23:58,074
"T-To injure an opponent...

256
00:23:58,076 --> 00:24:01,244
is to injure yourself."

257
00:24:31,142 --> 00:24:33,476
I'm sorry for coming to you.

258
00:24:33,478 --> 00:24:35,278
We had no choice.

259
00:24:45,624 --> 00:24:47,423
Morgan, wait.

260
00:24:57,669 --> 00:24:59,736
Okay.

261
00:25:01,006 --> 00:25:03,106
Oh, God.

262
00:25:03,108 --> 00:25:05,174
Mm.

263
00:25:09,180 --> 00:25:10,346
Stay away from me.

264
00:25:13,518 --> 00:25:14,851
Just listen.

265
00:25:19,491 --> 00:25:22,458
No! No! Aah!

266
00:25:24,429 --> 00:25:25,828
I know!

267
00:25:28,033 --> 00:25:30,033
No! No! No!

268
00:25:31,202 --> 00:25:33,036
I know!

269
00:25:34,839 --> 00:25:37,140
Enough! No!

270
00:25:54,025 --> 00:25:56,483
Benjamin, get your gun up.

271
00:26:19,889 --> 00:26:21,789
It was supposed to be me.

272
00:26:24,461 --> 00:26:26,643
That's what Gavin said...

273
00:26:28,642 --> 00:26:29,977
that I'd be first.

274
00:26:30,002 --> 00:26:32,366
I was trying to stop it.

275
00:26:32,368 --> 00:26:35,266
I was gonna give my life to show you!

276
00:26:36,172 --> 00:26:38,049
That's what should've happened today.

277
00:26:39,042 --> 00:26:41,843
That's what I tried.

278
00:26:41,845 --> 00:26:43,811
And something else happened

279
00:26:43,813 --> 00:26:47,815
because we have done nothing
to stop them.

280
00:26:47,817 --> 00:26:49,617
We've done nothing!

281
00:27:00,997 --> 00:27:02,764
Nothing.

282
00:27:12,876 --> 00:27:18,112
I was in a camp when it started.

283
00:27:18,114 --> 00:27:23,718
It was huge... endless...

284
00:27:23,720 --> 00:27:27,221
a city of... of tarps
and latrines

285
00:27:27,223 --> 00:27:30,458
and wood smoke and babies crying.

286
00:27:34,264 --> 00:27:36,764
There were always babies crying.

287
00:27:41,604 --> 00:27:44,205
I knew they had problems,

288
00:27:44,207 --> 00:27:47,408
but I didn't think
they were mine to solve.

289
00:27:47,410 --> 00:27:51,312
I thought there were stronger
or smarter people there than me.

290
00:27:54,417 --> 00:27:57,084
W-Who the hell am I, right?

291
00:28:01,558 --> 00:28:07,628
So I did nothing.

292
00:28:07,630 --> 00:28:10,331
A-And there was a fight...

293
00:28:10,333 --> 00:28:14,001
and a fire.

294
00:28:14,003 --> 00:28:18,906
And I lost my wife.

295
00:28:18,908 --> 00:28:25,580
And after three days of running,
no food, and no sleep...

296
00:28:25,582 --> 00:28:30,818
of horror and terror...

297
00:28:30,820 --> 00:28:33,754
I lost my little girl.

298
00:28:37,927 --> 00:28:39,794
Right in front of me...

299
00:28:48,838 --> 00:28:52,540
because I didn't do something.

300
00:28:54,944 --> 00:28:57,678
Because I waited.

301
00:29:12,395 --> 00:29:14,462
Listen to me.

302
00:29:14,464 --> 00:29:17,698
Please.

303
00:29:17,700 --> 00:29:22,069
Just listen.

304
00:29:22,071 --> 00:29:25,673
We can use what's happened.

305
00:29:25,675 --> 00:29:29,944
We can show the Saviors that we get it.

306
00:29:29,946 --> 00:29:33,481
We understand what we need to do.

307
00:29:33,483 --> 00:29:38,719
We know how to go on.

308
00:29:38,721 --> 00:29:41,722
And they need to believe us.

309
00:29:41,724 --> 00:29:48,062
We have to do something
to make them believe us.

310
00:29:48,064 --> 00:29:52,833
And then, when we gain
their trust back...

311
00:29:52,835 --> 00:29:55,570
we kill them...

312
00:29:55,572 --> 00:29:57,905
end them.

313
00:29:57,907 --> 00:30:00,107
We join with Alexandria and the Hilltop,

314
00:30:00,109 --> 00:30:02,944
and we crush them in one fell swoop.

315
00:30:08,718 --> 00:30:13,921
But this is it, Morgan.

316
00:30:13,923 --> 00:30:18,593
You have... to... kill.

317
00:30:22,298 --> 00:30:25,933
Or else you might as well
just kill yourself.

318
00:30:32,642 --> 00:30:34,875
Someone had to die.

319
00:30:34,877 --> 00:30:38,086
I tried to be the one.
That didn't happen.

320
00:30:45,088 --> 00:30:47,088
So I'll be the one to lead our army

321
00:30:47,090 --> 00:30:51,959
to crush the Saviors...
destroy them.

322
00:30:51,961 --> 00:30:56,897
Me.

323
00:30:56,899 --> 00:31:00,434
I'll talk to Ezekiel.

324
00:31:00,436 --> 00:31:05,072
I'll tell everyone what I did.

325
00:31:05,074 --> 00:31:08,242
And I'll live the rest
of my life making up for it.

326
00:32:38,568 --> 00:32:42,203
Did you tell him?

327
00:32:43,673 --> 00:32:45,840
Did you?

328
00:32:45,842 --> 00:32:48,676
Not now.

329
00:32:48,678 --> 00:32:50,377
We'll talk about it when we get back.

330
00:32:50,379 --> 00:32:51,794
What is it?

331
00:32:51,914 --> 00:32:54,274
It's better to talk about it
when we get back, Your Majesty.

332
00:32:57,954 --> 00:33:00,855
We'll talk about it now.

333
00:33:48,604 --> 00:33:51,005
How's the kid?

334
00:34:07,023 --> 00:34:09,456
He's dead.

335
00:34:15,464 --> 00:34:19,667
Start walking back now
before I kill you.

336
00:34:19,669 --> 00:34:21,802
You say one word or throw one look,

337
00:34:21,804 --> 00:34:25,673
I'll kill you right here and now.

338
00:34:25,675 --> 00:34:27,474
Go!

339
00:34:47,029 --> 00:34:48,829
Do you have it?

340
00:35:04,247 --> 00:35:07,915
I just want to say we get it.

341
00:35:07,917 --> 00:35:09,250
We understand.

342
00:35:13,055 --> 00:35:15,689
- Cease this!
- Leave 'em be!

343
00:35:16,926 --> 00:35:18,826
Morgan! Stop!

344
00:35:18,828 --> 00:35:20,728
Morgan!

345
00:36:11,147 --> 00:36:14,815
He set it all up.

346
00:36:17,620 --> 00:36:23,457
He blocked the road yesterday,
made us late.

347
00:36:23,459 --> 00:36:27,861
It was all him.

348
00:36:27,863 --> 00:36:32,166
And we had your offering when we left...

349
00:36:32,168 --> 00:36:37,638
had it all.

350
00:36:37,640 --> 00:36:42,076
He took one
from the cargo, and he hid it...

351
00:36:42,078 --> 00:36:43,744
when we stopped.

352
00:36:43,746 --> 00:36:45,346
He wanted to get something started

353
00:36:45,348 --> 00:36:48,349
between the Kingdom and the Saviors.

354
00:36:55,024 --> 00:36:57,524
Hey.

355
00:36:57,526 --> 00:36:59,326
Hey!

356
00:37:04,100 --> 00:37:07,668
Wanted to show you...

357
00:37:07,670 --> 00:37:12,239
Wanted to show you that we get it...

358
00:37:12,241 --> 00:37:15,109
That we understand what it is
that we need to do...

359
00:37:15,111 --> 00:37:19,213
that we know how to go on.

360
00:37:25,888 --> 00:37:27,688
Good.

361
00:37:30,826 --> 00:37:34,795
Next week, same time.

362
00:37:38,401 --> 00:37:42,136
We... We understand.

363
00:38:03,526 --> 00:38:06,226
He... He did this?

364
00:38:06,228 --> 00:38:09,463
He wanted to die for us before.

365
00:38:09,465 --> 00:38:17,438
He wanted it to be him.

366
00:38:17,440 --> 00:38:21,108
Thought he could choose,
and that's why Duane had to die.

367
00:38:24,413 --> 00:38:25,679
"Duane"?

368
00:38:40,763 --> 00:38:43,630
Benjamin... Benjamin...
That's why Benjamin...

369
00:38:49,205 --> 00:38:51,905
Morgan...

370
00:38:51,907 --> 00:38:53,607
let us take our leave.

371
00:38:53,609 --> 00:38:55,998
I was supposed to.

372
00:38:56,479 --> 00:38:58,378
Morgan, you should not be alone here.

373
00:38:58,380 --> 00:39:00,092
Go.

374
00:39:00,583 --> 00:39:02,382
Not now.

375
00:39:02,384 --> 00:39:05,452
- Benjamin not... would not...
- Man, I said not now!

376
00:39:08,424 --> 00:39:10,791
I said...

377
00:39:17,600 --> 00:39:19,967
Y'all just go.

378
00:41:25,140 --> 00:41:28,041
Do you really want to
know what happened in Alexandria?

379
00:41:31,630 --> 00:41:33,885
What happened to you?

380
00:41:34,588 --> 00:41:36,382
Killed Richard.

381
00:41:39,577 --> 00:41:41,821
I strangled him.

382
00:41:43,858 --> 00:41:46,626
He's the one got Benjamin killed, so...

383
00:41:50,131 --> 00:41:53,800
Do you want to know
what happened in Alexandria?

384
00:41:59,541 --> 00:42:01,340
Yeah.

385
00:42:06,548 --> 00:42:09,007
Negan killed Glenn and Abraham.

386
00:42:11,265 --> 00:42:12,765
What?

387
00:42:13,521 --> 00:42:16,077
Beat them to death with a baseball bat.

388
00:42:21,396 --> 00:42:25,663
Now the Saviors have Alexandria,
and Rick and everybody else...

389
00:42:27,406 --> 00:42:30,921
everything they do is
for the Saviors now.

390
00:42:41,249 --> 00:42:43,616
And they killed more.

391
00:42:46,821 --> 00:42:50,077
Killed Spencer, Olivia...

392
00:42:54,195 --> 00:42:57,505
Jesus brought Rick
and the rest here because...

393
00:42:58,333 --> 00:43:01,169
Rick wants to fight them.

394
00:43:06,708 --> 00:43:10,443
You wanted to know.

395
00:43:10,445 --> 00:43:14,280
Now you do.

396
00:43:14,282 --> 00:43:16,449
- Where are you going?
- I'm gonna kill them...

397
00:43:16,451 --> 00:43:18,251
out there, one by one.

398
00:43:18,253 --> 00:43:19,986
But where?
Somewhere else.

399
00:43:19,988 --> 00:43:21,254
W-Wait.

400
00:43:21,256 --> 00:43:23,322
Wait. Just wait.
Wait!

401
00:43:29,063 --> 00:43:33,432
You can go... and not go.

402
00:43:38,973 --> 00:43:40,773
Please.

403
00:43:45,146 --> 00:43:46,946
Please.

404
00:44:46,904 --> 00:44:48,307
Carol.

405
00:44:48,473 --> 00:44:50,573
I'm sorry.

406
00:44:51,983 --> 00:44:54,013
I thank you.

407
00:44:59,602 --> 00:45:01,602
I'm gonna be here now.

408
00:45:03,157 --> 00:45:05,186
We have to get ready.

409
00:45:06,027 --> 00:45:08,027
We have to fight.

410
00:45:10,765 --> 00:45:14,700
We do.

411
00:45:14,702 --> 00:45:18,371
But not today.

412
00:46:20,798 --> 00:46:25,798
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

