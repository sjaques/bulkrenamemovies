1
00:00:34,916 --> 00:00:37,584
Diane.

2
00:00:37,586 --> 00:00:41,187
Got it.

3
00:00:46,728 --> 00:00:48,461
Good shot.

4
00:00:48,463 --> 00:00:51,221
My sister had that dress.

5
00:00:51,246 --> 00:00:53,113
Don't think about it.

6
00:01:09,417 --> 00:01:12,452
Nice.

7
00:01:13,755 --> 00:01:15,788
It's getting to be like clockwork.

8
00:01:23,899 --> 00:01:25,765
Now, boss?

9
00:01:25,767 --> 00:01:27,767
Yes, Jerry.

10
00:01:35,243 --> 00:01:37,277
It looks light.

11
00:01:37,279 --> 00:01:39,946
Perhaps you might look again, Gavin.

12
00:01:39,948 --> 00:01:41,981
We've met our obligations.

13
00:01:47,055 --> 00:01:48,521
Huh.

14
00:01:48,523 --> 00:01:50,089
You're right.

15
00:01:50,091 --> 00:01:54,194
Doesn't seem like much, but 8, 9, 10.

16
00:01:54,196 --> 00:01:55,762
You're right.

17
00:01:55,764 --> 00:01:58,531
I want his gun.

18
00:02:00,025 --> 00:02:05,135
In fact, I don't think this
asshole should have a gun ever.

19
00:02:05,674 --> 00:02:08,174
No guns for bad boys.

20
00:02:08,176 --> 00:02:11,177
You hit me first, prick.

21
00:02:15,450 --> 00:02:17,183
What's that?

22
00:02:19,554 --> 00:02:21,521
Okay.

23
00:02:21,523 --> 00:02:23,756
Where do we go from here?

24
00:02:23,758 --> 00:02:27,360
That's right, Your Majesty.

25
00:02:27,385 --> 00:02:30,674
Where do we go from here?

26
00:02:33,735 --> 00:02:37,937
Hand him your weapon, Richard.

27
00:02:48,283 --> 00:02:51,985
Stand down, idiot.

28
00:02:54,623 --> 00:02:56,589
Suck on it, you little shit.

29
00:02:56,591 --> 00:02:59,259
Richard!

30
00:02:59,261 --> 00:03:01,060
Aah!

31
00:03:07,802 --> 00:03:09,402
Stand down!

32
00:03:13,056 --> 00:03:15,790
Ezekiel, you know I can't have this.

33
00:03:15,815 --> 00:03:17,610
We can't have this.

34
00:03:17,612 --> 00:03:21,714
Richard will refrain from
attending our future exchanges.

35
00:03:21,716 --> 00:03:25,218
No. No, Ezekiel, you're gonna
keep bringing him.

36
00:03:25,220 --> 00:03:27,520
'Cause if this doesn't stop,

37
00:03:27,522 --> 00:03:30,523
if this starts becoming a real problem,

38
00:03:30,525 --> 00:03:32,625
you remember what I said.

39
00:03:32,627 --> 00:03:35,028
He is still batting first on the lineup.

40
00:03:35,030 --> 00:03:36,963
Almost did it myself just now.

41
00:03:40,230 --> 00:03:41,669
Hey.

42
00:03:42,136 --> 00:03:44,886
I know it's not his fault, but...

43
00:03:45,640 --> 00:03:48,675
this has got to stop.

44
00:03:48,677 --> 00:03:50,009
So...

45
00:03:50,011 --> 00:03:53,831
things might need to get a little...

46
00:03:54,215 --> 00:03:55,370
visceral.

47
00:03:57,026 --> 00:03:59,670
Jared, get the hell up!

48
00:03:59,732 --> 00:04:02,482
Let's get the hell out of here.

49
00:04:06,861 --> 00:04:08,561
Can I, um...

50
00:04:08,563 --> 00:04:11,367
Can I have my stick back, please?

51
00:04:12,757 --> 00:04:15,132
Someone gave it to me.

52
00:04:15,914 --> 00:04:18,257
Someone who's gone.

53
00:04:18,740 --> 00:04:20,473
Gavin, you got to let me
turn up this dipshit.

54
00:04:20,475 --> 00:04:23,096
No! No!

55
00:04:23,812 --> 00:04:25,155
Just...

56
00:04:25,822 --> 00:04:29,682
read the goddamn room, sensei.

57
00:04:29,684 --> 00:04:31,351
Let's go.

58
00:05:00,281 --> 00:05:01,614
Yeah.

59
00:05:02,852 --> 00:05:05,962
My sister loved that dress.

60
00:05:11,993 --> 00:05:14,727
You were quick with that today.

61
00:05:14,729 --> 00:05:16,462
Impressive, Benjamin.

62
00:05:16,464 --> 00:05:19,032
Told you I'm getting better.

63
00:05:19,034 --> 00:05:20,933
You were a trifle too quick.

64
00:05:22,250 --> 00:05:23,625
Just because you now know how to fight

65
00:05:23,804 --> 00:05:25,039
doesn't mean you should seek one.

66
00:05:25,064 --> 00:05:27,013
Yeah, but it just happened.
I wasn't thinking.

67
00:05:27,038 --> 00:05:28,870
You have to.

68
00:05:29,545 --> 00:05:32,157
Every time from
this time forward, so you shall.

69
00:05:32,182 --> 00:05:33,946
Yes?

70
00:05:35,284 --> 00:05:36,831
Yes.

71
00:05:39,254 --> 00:05:41,191
Richard.

72
00:05:42,606 --> 00:05:44,728
We shall speak later.

73
00:05:45,745 --> 00:05:47,511
Jerry.

74
00:05:49,824 --> 00:05:51,978
You're sick with this stick, man.

75
00:05:52,003 --> 00:05:53,464
Jerry!

76
00:05:55,744 --> 00:05:58,467
Hey, uh, do you want
to head to the infirmary?

77
00:05:58,510 --> 00:06:00,143
No. I'm okay.

78
00:06:00,145 --> 00:06:01,611
Hey.

79
00:06:01,613 --> 00:06:03,480
Where'd you go in them trucks?

80
00:06:04,034 --> 00:06:06,334
I need to speak to Daryl alone.

81
00:06:06,336 --> 00:06:07,602
That okay?

82
00:06:07,604 --> 00:06:10,105
Yeah, I'll get you something
for that cut.

83
00:06:10,107 --> 00:06:12,374
Yeah.

84
00:06:13,610 --> 00:06:15,710
You went to see them, right?

85
00:06:15,712 --> 00:06:17,212
Yeah.

86
00:06:17,214 --> 00:06:19,114
Part of your deal?

87
00:06:23,720 --> 00:06:25,453
What the hell's wrong with you?

88
00:06:26,523 --> 00:06:28,523
You're bleeding.

89
00:06:30,961 --> 00:06:32,994
They did that to you.

90
00:06:33,873 --> 00:06:36,045
- You know what they are.
- I do.

91
00:06:40,404 --> 00:06:43,605
You know, if Carol were here,

92
00:06:43,607 --> 00:06:45,440
she saw all that...

93
00:06:45,688 --> 00:06:48,166
if she knew about Abraham...

94
00:06:48,449 --> 00:06:50,115
and Glenn...

95
00:06:51,173 --> 00:06:54,312
she'd be leading us right to them,

96
00:06:54,929 --> 00:06:56,765
ready to kill them all.

97
00:06:56,953 --> 00:06:58,562
She would.

98
00:06:59,327 --> 00:07:01,562
And that's why she left, man.

99
00:07:35,192 --> 00:07:36,691
I'm practicing.

100
00:07:36,693 --> 00:07:40,161
Gonna have to start using these more.

101
00:07:40,163 --> 00:07:43,598
The Saviors are smart enough to know

102
00:07:43,600 --> 00:07:46,034
I shouldn't have a gun around them.

103
00:07:52,309 --> 00:07:54,072
Morgan said you're a bowman.

104
00:08:07,317 --> 00:08:08,723
Why?

105
00:08:11,127 --> 00:08:13,346
'Cause we want the same things.

106
00:08:14,424 --> 00:08:15,831
I need your help.

107
00:08:56,214 --> 00:09:04,214
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

108
00:09:13,559 --> 00:09:16,794
We need something to move Ezekiel.

109
00:09:16,819 --> 00:09:18,373
This is it.

110
00:09:18,398 --> 00:09:22,133
Alexandria, the Hilltop,
and the Kingdom hitting first,

111
00:09:22,135 --> 00:09:26,437
hitting hard, and then we wipe
the Saviors from the Earth.

112
00:09:26,439 --> 00:09:28,339
Keeping people...

113
00:09:28,341 --> 00:09:33,077
dozens and dozens and dozens
of good people...

114
00:09:33,079 --> 00:09:35,046
keeping them safe.

115
00:10:13,319 --> 00:10:14,819
They ride this road.

116
00:10:14,821 --> 00:10:17,388
If we see cars, it's the Saviors.

117
00:10:17,390 --> 00:10:21,092
They've been coming in packs
of two or three lately.

118
00:10:21,094 --> 00:10:22,727
That's why I need you.

119
00:10:22,729 --> 00:10:24,695
I can't take them alone.

120
00:10:24,697 --> 00:10:27,898
We're gonna hit them
with the guns first...

121
00:10:27,900 --> 00:10:29,500
and then the Molotovs.

122
00:10:29,502 --> 00:10:32,803
Then back to the guns
until they're dead.

123
00:10:32,805 --> 00:10:35,373
Why the fire?
Needs to look bad.

124
00:10:35,375 --> 00:10:38,542
The Saviors who discover what's left...

125
00:10:38,544 --> 00:10:40,678
we want them to be angry.

126
00:10:40,680 --> 00:10:44,915
I left a trail from here
to the weapons cache I planted,

127
00:10:44,917 --> 00:10:48,686
to the cabin of that someone
Ezekiel cares about.

128
00:10:48,688 --> 00:10:50,154
Who's that?

129
00:10:50,156 --> 00:10:52,156
It's just some loner he met.

130
00:10:52,158 --> 00:10:53,791
Sometimes he brings food.

131
00:10:53,793 --> 00:10:56,160
Why don't they live in the Kingdom?

132
00:10:56,162 --> 00:10:57,461
I don't know.

133
00:10:57,463 --> 00:10:59,864
She lives out there,
she'll die out there.

134
00:11:01,367 --> 00:11:03,567
It's a woman?

135
00:11:03,569 --> 00:11:06,203
What does that matter?
She's got more balls than you and me.

136
00:11:08,574 --> 00:11:10,841
She's gonna die either way.

137
00:11:10,843 --> 00:11:13,544
When the Saviors come
and find their buddies dead,

138
00:11:13,546 --> 00:11:16,013
if they know their elbow
from their asshole

139
00:11:16,015 --> 00:11:18,215
and can follow an obvious spoor,

140
00:11:18,217 --> 00:11:22,086
they're gonna go to the weapons
cache and then to the cabin,

141
00:11:22,088 --> 00:11:24,522
and they're gonna attack this woman.

142
00:11:24,524 --> 00:11:25,823
What's her name?

143
00:11:25,825 --> 00:11:28,292
Maybe they kill her, maybe they don't,

144
00:11:28,294 --> 00:11:31,395
but it's gonna show Ezekiel
what he needs to do.

145
00:11:31,397 --> 00:11:33,931
Her name.
What is it?

146
00:11:33,933 --> 00:11:36,233
She's tough.
Maybe she'll live.

147
00:11:36,235 --> 00:11:37,735
Say her damn name!

148
00:11:46,412 --> 00:11:48,212
Carol.

149
00:11:48,214 --> 00:11:50,281
I hoped you didn't know her,

150
00:11:50,283 --> 00:11:52,416
but I didn't think you'd care,

151
00:11:52,418 --> 00:11:54,952
'cause you know what needs to happen.

152
00:11:54,954 --> 00:11:56,587
No.
Maybe she'll live.

153
00:11:56,589 --> 00:11:58,589
Look, this...
this is how...

154
00:11:58,591 --> 00:12:00,624
this is how this could happen.

155
00:12:00,626 --> 00:12:02,626
This is how we can get rid
of the Saviors,

156
00:12:02,628 --> 00:12:04,995
how we all can have a future.

157
00:12:04,997 --> 00:12:07,631
She's living out there on her own,

158
00:12:07,633 --> 00:12:09,133
just waiting to die.
No!

159
00:12:09,135 --> 00:12:10,634
If we don't do anything,

160
00:12:10,636 --> 00:12:12,269
a hell of a lot more people
are gonna die,

161
00:12:12,271 --> 00:12:13,737
people who want to live!

162
00:12:13,739 --> 00:12:15,439
You stay the hell away
from Carol, you hear me?

163
00:12:25,518 --> 00:12:27,451
It's them.

164
00:12:30,656 --> 00:12:33,924
Look, we can wait for things to go bad,

165
00:12:33,926 --> 00:12:35,426
we lose people...

166
00:12:37,175 --> 00:12:41,277
or we can do the hard thing...

167
00:12:41,279 --> 00:12:43,663
and choose our fate for ourselves.

168
00:12:44,262 --> 00:12:45,628
No.

169
00:12:45,630 --> 00:12:47,130
Sorry.

170
00:13:15,489 --> 00:13:16,893
There'll be more.

171
00:13:16,895 --> 00:13:19,495
Or those...

172
00:13:19,497 --> 00:13:22,799
they're gonna ride back this way later.

173
00:13:22,801 --> 00:13:25,201
We'll have another chance.

174
00:13:25,203 --> 00:13:28,771
But we're running out of time.

175
00:13:28,773 --> 00:13:32,675
If you and your people want
to move against the Saviors...

176
00:13:32,677 --> 00:13:36,112
you need to do it soon,
and you need the Kingdom.

177
00:13:39,484 --> 00:13:45,788
What we have to do requires
sacrifice one way or another.

178
00:13:45,790 --> 00:13:47,690
Guys like us...

179
00:13:47,692 --> 00:13:51,127
we've already lost so much.

180
00:13:51,129 --> 00:13:53,363
You don't know me.

181
00:13:53,365 --> 00:13:54,731
I know...

182
00:13:54,733 --> 00:13:59,669
that Carol,
living on her own like that...

183
00:13:59,671 --> 00:14:04,073
she might as well be dead right now.

184
00:14:06,711 --> 00:14:09,145
She gets hurt, she dies,

185
00:14:09,147 --> 00:14:11,147
if she catches a fever,

186
00:14:11,149 --> 00:14:14,217
if she's taken out by a walker...

187
00:14:14,219 --> 00:14:17,086
if she gets hit by lightning...

188
00:14:17,088 --> 00:14:21,491
anything... anything happens
to her, I'll kill you.

189
00:14:29,616 --> 00:14:31,568
I would die for the Kingdom.

190
00:14:32,737 --> 00:14:34,436
Why don't you?

191
00:17:11,398 --> 00:17:15,174
Are you a collective, or does one lead?

192
00:17:15,955 --> 00:17:17,188
This.

193
00:17:24,370 --> 00:17:26,537
Hi.

194
00:17:26,539 --> 00:17:28,210
I'm Rick.

195
00:17:30,343 --> 00:17:32,843
We own your lives.

196
00:17:32,845 --> 00:17:36,647
You want to buy them back?

197
00:17:36,649 --> 00:17:38,949
Have anything?

198
00:17:38,951 --> 00:17:42,477
Well, you have one of my people.

199
00:17:42,597 --> 00:17:44,521
Gabriel.

200
00:17:44,523 --> 00:17:45,931
I want to see him first.

201
00:17:46,051 --> 00:17:47,383
Then we can talk.

202
00:18:21,193 --> 00:18:24,915
The boat things you took got taken.

203
00:18:25,419 --> 00:18:29,454
Saw them, so we took the rest.

204
00:18:29,456 --> 00:18:31,556
And we took him.

205
00:18:31,558 --> 00:18:33,925
Well, then you know we have nothing

206
00:18:33,927 --> 00:18:36,394
to buy back our lives with.

207
00:18:37,319 --> 00:18:41,333
That's what you'll have soon...
nothing.

208
00:18:41,335 --> 00:18:44,803
Because me and my people
already belong to that group

209
00:18:44,805 --> 00:18:47,339
who took those supplies from the boat.

210
00:18:47,341 --> 00:18:48,868
They're called the Saviors.

211
00:18:48,988 --> 00:18:51,910
They own our lives.

212
00:18:51,912 --> 00:18:54,346
And if you kill us,

213
00:18:54,348 --> 00:18:57,582
you'll be taking something from them.

214
00:19:00,287 --> 00:19:04,289
And they will come looking.

215
00:19:04,291 --> 00:19:08,960
You only have two options
when it comes to the Saviors...

216
00:19:08,962 --> 00:19:13,542
either they kill you or they own you.

217
00:19:13,947 --> 00:19:17,215
But there is a way out.

218
00:19:17,217 --> 00:19:18,950
Join us.

219
00:19:18,952 --> 00:19:23,588
Join us in fighting them.

220
00:19:31,999 --> 00:19:33,565
No.

221
00:19:49,704 --> 00:19:51,683
Rosita, don't!

222
00:19:52,953 --> 00:19:55,120
Everybody, stop!

223
00:19:55,122 --> 00:19:56,688
Just wait!

224
00:19:59,426 --> 00:20:03,195
Let us go, or I will kill her!

225
00:20:05,833 --> 00:20:09,434
Away from Tamiel now.

226
00:20:09,436 --> 00:20:12,003
The Saviors, they...

227
00:20:12,005 --> 00:20:16,608
they have other places,
other communities.

228
00:20:16,610 --> 00:20:18,777
They have things...

229
00:20:18,779 --> 00:20:22,280
food, weapons, vehicles, fuel.

230
00:20:22,282 --> 00:20:24,850
Whatever you want, the Saviors have it.

231
00:20:38,832 --> 00:20:42,834
Away from Tamiel.

232
00:20:55,983 --> 00:20:59,384
Your words now.

233
00:21:01,488 --> 00:21:05,257
If you join us
and we beat them together,

234
00:21:05,259 --> 00:21:08,693
you can have much of what's theirs.

235
00:21:08,695 --> 00:21:12,163
Fighting with us, you'll be rewarded,

236
00:21:12,165 --> 00:21:15,467
more than you can imagine.

237
00:21:15,469 --> 00:21:17,736
Want something now.

238
00:21:17,738 --> 00:21:20,872
Rick...

239
00:21:20,874 --> 00:21:24,342
can do anything.

240
00:21:24,344 --> 00:21:26,044
This group?

241
00:21:26,046 --> 00:21:31,516
They found me...
here, so far from our home.

242
00:21:31,518 --> 00:21:33,718
What do you need?

243
00:21:33,720 --> 00:21:36,521
Just tell us...
we'll get it for you.

244
00:21:36,523 --> 00:21:39,724
We'll show you what we can do.

245
00:21:39,726 --> 00:21:43,028
Now.

246
00:21:51,538 --> 00:21:54,139
Tamiel, Brion...

247
00:21:54,141 --> 00:21:56,775
show Rick Up Up Up.

248
00:22:04,318 --> 00:22:06,952
It's okay.

249
00:22:25,105 --> 00:22:27,138
Here.

250
00:22:27,140 --> 00:22:28,440
Sit.

251
00:22:28,442 --> 00:22:30,175
Thanks.
I'm okay.

252
00:22:33,981 --> 00:22:36,982
Who are these people?

253
00:22:38,552 --> 00:22:42,020
New best friends, I guess?

254
00:22:44,624 --> 00:22:47,183
Where are they taking him?

255
00:23:30,270 --> 00:23:33,671
All of us, here since the change.

256
00:23:33,673 --> 00:23:35,073
We take.

257
00:23:35,075 --> 00:23:37,308
We don't bother.

258
00:23:37,310 --> 00:23:39,844
Things grow harder.

259
00:23:39,846 --> 00:23:45,050
We open cans, sometimes inside's rotten.

260
00:23:45,052 --> 00:23:48,153
Time's passed.

261
00:23:48,155 --> 00:23:53,191
Things are changing again.

262
00:23:53,193 --> 00:23:57,095
So maybe we change.

263
00:23:57,097 --> 00:23:58,396
Maybe.

264
00:24:01,064 --> 00:24:03,568
Need to know you're real with this,

265
00:24:04,030 --> 00:24:06,225
that you're worth it.

266
00:24:06,673 --> 00:24:08,073
Unh!

267
00:24:15,237 --> 00:24:17,446
What did you do?!

268
00:24:32,746 --> 00:24:34,308
Rick!

269
00:24:39,949 --> 00:24:42,417
I'm all right.

270
00:25:09,178 --> 00:25:11,745
Rick!

271
00:25:40,176 --> 00:25:41,942
Aah!

272
00:25:41,944 --> 00:25:43,777
Aah!

273
00:26:05,902 --> 00:26:08,022
Aah!

274
00:26:14,243 --> 00:26:16,010
The walls!

275
00:26:16,012 --> 00:26:17,444
Use them!

276
00:27:01,276 --> 00:27:03,799
You believe us now?!

277
00:27:05,323 --> 00:27:07,828
Just tell us what you want...

278
00:27:08,276 --> 00:27:10,064
and we'll get it.

279
00:28:11,360 --> 00:28:13,237
Guns.

280
00:28:13,753 --> 00:28:15,104
A lot.

281
00:28:15,580 --> 00:28:17,180
A lot.

282
00:28:18,057 --> 00:28:20,310
And then we fight your fight.

283
00:28:37,963 --> 00:28:40,187
You know we will win?

284
00:28:40,189 --> 00:28:42,362
Oh, I know it.

285
00:28:43,784 --> 00:28:46,136
After, we get half of what's won.

286
00:28:46,161 --> 00:28:47,838
Oh, you'll get a third.

287
00:28:48,166 --> 00:28:50,964
And we're taking back
what you just stole from us.

288
00:28:53,168 --> 00:28:54,835
Half.

289
00:28:54,837 --> 00:28:56,036
A third.

290
00:28:56,038 --> 00:28:57,471
Half.

291
00:28:59,641 --> 00:29:01,541
A third.

292
00:29:02,611 --> 00:29:05,479
A third, and we keep what we stole.

293
00:29:10,252 --> 00:29:13,920
Half of the jars, the ones we took.

294
00:29:13,922 --> 00:29:15,989
One time, this time.

295
00:29:15,991 --> 00:29:17,491
Yes?

296
00:29:19,161 --> 00:29:22,863
Say yes.

297
00:29:22,865 --> 00:29:25,499
Yes.

298
00:29:25,501 --> 00:29:28,468
And the guns.

299
00:29:28,470 --> 00:29:31,838
And the guns.

300
00:29:41,083 --> 00:29:43,517
Waited by the boat long time.

301
00:29:43,519 --> 00:29:45,886
Want something for it.

302
00:29:45,888 --> 00:29:47,420
So it's this.

303
00:29:47,422 --> 00:29:51,424
Jars and guns, guns and jars.

304
00:29:51,426 --> 00:29:54,060
You waited for someone

305
00:29:54,062 --> 00:29:56,363
to get the supplies
off that boat for you?

306
00:29:56,365 --> 00:29:57,798
Long time.

307
00:29:57,800 --> 00:30:00,934
We take.
We don't bother.

308
00:30:05,040 --> 00:30:06,540
You had that...

309
00:30:06,542 --> 00:30:09,676
thing down there
for someone to prove themselves?

310
00:30:09,678 --> 00:30:11,178
No.

311
00:30:11,180 --> 00:30:13,480
His name was Winslow.

312
00:30:18,154 --> 00:30:20,387
What were you gonna do with Gabriel?

313
00:30:20,389 --> 00:30:22,055
Go.

314
00:30:22,057 --> 00:30:25,859
Deal expires.

315
00:30:25,861 --> 00:30:28,595
Soon.

316
00:30:30,883 --> 00:30:34,168
What's your name?

317
00:30:34,170 --> 00:30:36,069
Jadis.

318
00:31:16,512 --> 00:31:18,846
We have a deal.

319
00:31:55,269 --> 00:31:57,336
I told Richard no more visits.

320
00:31:57,361 --> 00:31:58,927
I am aware.

321
00:31:59,616 --> 00:32:01,226
Your desire is solitude.

322
00:32:01,398 --> 00:32:03,585
That's what I've ordered
to be facilitated.

323
00:32:04,632 --> 00:32:06,597
My men are here clearing the wasted.

324
00:32:06,622 --> 00:32:07,691
The dead are quite inconsiderate

325
00:32:07,716 --> 00:32:10,183
when it comes to those
who are wanting to be alone.

326
00:32:10,599 --> 00:32:12,751
I thought our efforts
would be quiet enough

327
00:32:12,776 --> 00:32:14,876
to fall beneath your notice.

328
00:32:14,878 --> 00:32:17,712
You're the one who opened the door.

329
00:32:17,714 --> 00:32:19,414
Tripped my wire.

330
00:32:19,416 --> 00:32:20,715
Thought I caught it in time.

331
00:32:21,401 --> 00:32:22,194
Sorry.

332
00:32:22,501 --> 00:32:23,734
You hid them well, lady.

333
00:32:23,759 --> 00:32:25,158
Don't call her "lady."

334
00:32:25,554 --> 00:32:28,590
- Ma'am, Ms., missus...
- You can shut up now.

335
00:32:28,592 --> 00:32:30,492
Copy.

336
00:32:30,494 --> 00:32:33,295
Goodbye, Your Majesty.

337
00:32:33,297 --> 00:32:35,330
Hold up.

338
00:32:39,106 --> 00:32:40,903
Cobbler.

339
00:32:43,295 --> 00:32:45,128
Kevin said you like it.

340
00:32:45,153 --> 00:32:47,642
Just in case you did open the door.

341
00:32:54,888 --> 00:32:56,418
Go.

342
00:33:53,977 --> 00:33:56,678
Okay.

343
00:33:56,680 --> 00:33:58,813
Oh.

344
00:34:01,618 --> 00:34:04,986
Jesus took us to the Kingdom.

345
00:34:04,988 --> 00:34:08,089
Morgan said you just left.

346
00:34:08,091 --> 00:34:11,626
I was out here.

347
00:34:11,628 --> 00:34:15,664
I saw you.

348
00:34:18,235 --> 00:34:22,270
Why'd you go?

349
00:34:29,212 --> 00:34:31,854
I had to.

350
00:34:47,821 --> 00:34:49,954
I heard something when I was on watch,

351
00:34:50,121 --> 00:34:51,999
from inside the wall.

352
00:34:52,024 --> 00:34:54,922
I went into the pantry,
one of them jumped me.

353
00:34:55,372 --> 00:34:58,196
She was angry because she didn't
get the supplies from the boat

354
00:34:58,221 --> 00:34:59,474
after all the waiting.

355
00:35:00,376 --> 00:35:02,977
So they made me pack up everything else.

356
00:35:03,737 --> 00:35:06,232
But she said they were
at the boat, so I hoped...

357
00:35:06,257 --> 00:35:09,775
No.
You didn't just hope.

358
00:35:09,800 --> 00:35:12,434
You got us here.

359
00:35:15,659 --> 00:35:19,661
I was beginning to lose faith.

360
00:35:22,432 --> 00:35:23,932
But then I saw you.

361
00:35:23,934 --> 00:35:25,567
And you nodded at me.

362
00:35:25,569 --> 00:35:28,866
I mean, just the fact
that you knew I didn't leave,

363
00:35:28,891 --> 00:35:32,244
that you searched and found me,
that you, seeing these numbers,

364
00:35:32,269 --> 00:35:34,118
you seemed so...

365
00:35:35,109 --> 00:35:36,426
glad.

366
00:35:39,483 --> 00:35:42,150
We will set things right.

367
00:35:42,152 --> 00:35:46,921
But things are gonna get
very hard before that time.

368
00:35:46,923 --> 00:35:49,491
We have to hold on.

369
00:35:49,493 --> 00:35:51,159
I will.

370
00:35:51,161 --> 00:35:53,294
Thank you.

371
00:36:00,637 --> 00:36:03,004
What made you smile?

372
00:36:04,474 --> 00:36:08,176
What made you so...
confident?

373
00:36:09,980 --> 00:36:16,551
Someone showed me
enemies can become friends.

374
00:36:28,153 --> 00:36:29,864
We shouldn't be
going back to Alexandria.

375
00:36:29,866 --> 00:36:33,668
We need to stay out and look
for the guns for this deal.

376
00:36:33,670 --> 00:36:35,670
Rick is hurt.
Aaron is hurt.

377
00:36:35,672 --> 00:36:38,473
I'm more nervous
about what Eric will say

378
00:36:38,475 --> 00:36:41,075
if he sees my face like this again.

379
00:36:41,077 --> 00:36:42,810
People back home need food.

380
00:36:42,812 --> 00:36:45,546
Rick wants to bring supplies
back and we regroup,

381
00:36:45,548 --> 00:36:47,281
so that's what we're doing.

382
00:36:48,351 --> 00:36:51,319
Then I'll go on my own.

383
00:36:54,624 --> 00:36:58,059
We are sticking together, and that's it.

384
00:36:59,662 --> 00:37:02,063
What is your problem?

385
00:37:02,065 --> 00:37:04,599
We're not looking
for a fight right now, Rosita!

386
00:37:04,601 --> 00:37:06,200
We're getting ready for one.

387
00:37:06,202 --> 00:37:08,669
It's always a fight, Tara.

388
00:37:08,671 --> 00:37:13,074
I'm not letting anyone
get in our way or slow us down.

389
00:37:13,076 --> 00:37:15,076
If we got to stop people
from taking from us

390
00:37:15,078 --> 00:37:16,677
or we got to take from other people,

391
00:37:16,679 --> 00:37:19,514
I don't care.

392
00:37:19,516 --> 00:37:21,382
We win.

393
00:37:21,384 --> 00:37:23,484
Grow up.

394
00:37:26,689 --> 00:37:28,623
Guns.

395
00:37:28,625 --> 00:37:30,091
Soon.

396
00:37:30,093 --> 00:37:32,727
Soon.
Or else.

397
00:37:38,835 --> 00:37:40,768
Once we get you stitched up,

398
00:37:40,770 --> 00:37:43,104
we'll go right back out
and find the guns, right?

399
00:37:43,106 --> 00:37:44,505
That's right.

400
00:37:44,507 --> 00:37:47,008
Do you have any idea where?

401
00:37:47,010 --> 00:37:50,411
No, but that's never stopped us before.

402
00:37:50,413 --> 00:37:54,115
Tara, you've been out
further than any of us.

403
00:37:54,117 --> 00:37:56,117
At least you can tell us
where not to look.

404
00:37:57,654 --> 00:37:59,420
Yeah.

405
00:37:59,422 --> 00:38:00,822
Sure thing.

406
00:38:00,824 --> 00:38:03,724
Let's go already.

407
00:38:03,726 --> 00:38:05,526
Hold... Hold on.

408
00:38:13,892 --> 00:38:16,559
Why are you...?

409
00:38:16,639 --> 00:38:19,173
Because we won.

410
00:38:21,177 --> 00:38:23,311
And to replace the one you lost.

411
00:38:26,567 --> 00:38:28,200
All right?

412
00:38:28,225 --> 00:38:29,692
Let's go.

413
00:38:42,219 --> 00:38:44,154
I couldn't lose anyone.

414
00:38:46,558 --> 00:38:48,758
I couldn't lose any of them.

415
00:38:52,174 --> 00:38:55,231
I couldn't lose you.

416
00:39:01,715 --> 00:39:03,773
I couldn't kill them.

417
00:39:08,413 --> 00:39:10,657
I could.

418
00:39:11,178 --> 00:39:12,741
I would.

419
00:39:14,786 --> 00:39:16,380
If they hurt any of our people...

420
00:39:16,500 --> 00:39:20,423
any more of them...
that's what I would do.

421
00:39:25,630 --> 00:39:29,365
And there wouldn't be
anything let of me after that.

422
00:39:39,711 --> 00:39:42,045
The Saviors...
did they come?

423
00:39:44,115 --> 00:39:45,896
Yeah.

424
00:40:04,870 --> 00:40:07,036
Did anyone get hurt?

425
00:40:11,042 --> 00:40:13,076
Is everybody okay?

426
00:40:17,849 --> 00:40:19,649
Did the Saviors...

427
00:40:29,861 --> 00:40:34,197
Is everybody back home okay?

428
00:40:39,771 --> 00:40:43,239
Daryl...

429
00:40:48,513 --> 00:40:50,580
They came.

430
00:40:50,582 --> 00:40:53,416
We got them all.

431
00:40:53,418 --> 00:40:58,888
Made a deal with
the rest of them, like Ezekiel.

432
00:41:00,358 --> 00:41:03,026
Everyone's all right.

433
00:41:03,028 --> 00:41:05,695
Everyone's all right.

434
00:41:14,439 --> 00:41:17,040
We gonna eat or... or I got to
be a king or something

435
00:41:17,042 --> 00:41:18,708
to get food around here?

436
00:41:22,247 --> 00:41:23,746
Shut up.

437
00:41:38,930 --> 00:41:40,463
Ezekiel...

438
00:41:40,465 --> 00:41:42,165
is he okay?

439
00:41:45,136 --> 00:41:47,370
Yeah, I think he is.

440
00:42:34,619 --> 00:42:39,188
Watch out for yourself, all right?

441
00:43:13,925 --> 00:43:17,193
Well, you're good with her.

442
00:43:18,763 --> 00:43:23,399
Ezekiel will be impressed.

443
00:43:23,401 --> 00:43:25,835
Well, figure any guy
that has a pet tiger

444
00:43:25,837 --> 00:43:27,470
can't be that bad.

445
00:43:29,841 --> 00:43:32,241
He's okay by Carol.

446
00:43:32,243 --> 00:43:35,678
Yeah, I found her,
out in that little house.

447
00:43:35,680 --> 00:43:38,514
Look, what I said...

448
00:43:38,516 --> 00:43:41,684
when I said she just...

449
00:43:41,686 --> 00:43:43,619
went away...

450
00:43:43,621 --> 00:43:47,757
it's what she told me to do.

451
00:43:47,759 --> 00:43:49,792
No, I get it.

452
00:43:51,896 --> 00:43:54,564
We need the Kingdom.

453
00:43:54,566 --> 00:43:58,468
You got to make that happen.

454
00:44:02,073 --> 00:44:04,240
I'm sorry.

455
00:44:04,242 --> 00:44:10,713
I mean, I... I really am,
but, uh...

456
00:44:10,715 --> 00:44:13,583
it can't be me.

457
00:44:14,986 --> 00:44:21,157
Look, whatever it is
you're holding on to...

458
00:44:21,159 --> 00:44:24,961
it's already gone, man.

459
00:44:24,963 --> 00:44:27,497
Wake the hell up.

460
00:44:33,905 --> 00:44:36,072
You're the same as me, Daryl.

461
00:44:36,074 --> 00:44:38,527
- You don't know shit about me.
- No, I do.

462
00:44:40,300 --> 00:44:43,780
'Cause you didn't tell Carol
what happened.

463
00:44:43,782 --> 00:44:48,518
You didn't,
'cause she'd be here otherwise.

464
00:44:48,520 --> 00:44:51,320
And I'm glad for that.

465
00:44:54,926 --> 00:44:57,860
See, we're all holding on to something.

466
00:45:03,802 --> 00:45:07,170
I'm going back to Hilltop in the morning

467
00:45:07,172 --> 00:45:08,771
and getting ready.

468
00:45:24,047 --> 00:45:29,047
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<b>WEB-DL resync by kinglouisxx</b>
<font color="#ec14bd">www.addic7ed.com</fo

