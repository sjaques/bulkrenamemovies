1
00:00:00,183 --> 00:00:03,485
<i>Wat voorafging:</i>

2
00:00:05,491 --> 00:00:09,693
Je moet gaan, voordat je ontdekt
hoe gevaarlijk wij allemaal zijn.

3
00:00:09,795 --> 00:00:10,928
Bedreigde je mij nu net?

4
00:00:16,469 --> 00:00:18,535
We gaan hier kogels fabriceren.

5
00:00:18,637 --> 00:00:22,339
Volgens het vraag en aanbod
zijn kogels nu het geld.

6
00:00:22,441 --> 00:00:24,608
Alles wat we hebben,
hebben we uit gevechten.

7
00:00:24,710 --> 00:00:28,378
Als we hun regels volgen,
hebben we een soort leven.

8
00:00:28,381 --> 00:00:29,480
Wat voor leven is dat?

9
00:02:57,636 --> 00:02:59,571
Hoe laat is het?

10
00:03:01,941 --> 00:03:04,576
Net half zes geweest.

11
00:03:06,111 --> 00:03:08,046
Laten we gaan.

12
00:03:30,303 --> 00:03:31,867
Dat zag ik gisteravond niet.

13
00:03:33,807 --> 00:03:35,317
Wat zag je niet?

14
00:03:42,181 --> 00:03:44,682
Ze zijn er nog steeds.

15
00:03:44,684 --> 00:03:47,007
Maar er lijkt een einde aan te komen.

16
00:03:47,454 --> 00:03:50,154
De hele verdomde nacht.

17
00:03:50,156 --> 00:03:54,692
Negan is wel slim genoeg
om ze niet in onze buurt te laten komen.

18
00:03:54,694 --> 00:03:59,097
Dan is Fat Joey tenminste ergens goed in.
- In wat? Taart eten?

19
00:04:00,165 --> 00:04:05,336
Hij plaatste explosieven op de Herdway.
Kleintjes, niets speciaals.

20
00:04:05,338 --> 00:04:07,839
Guerilla-zooi.

21
00:04:07,841 --> 00:04:12,617
Wat voor zooi?
- Zoals in oorlog, idioot.

22
00:04:12,618 --> 00:04:18,750
De explosieven die we vorige maand vonden,
de RPG's van de nieuwelingen.

23
00:04:18,752 --> 00:04:23,509
Misschien schakelt het ze uit.

24
00:04:24,991 --> 00:04:29,552
Er is een opening, we gaan.
- Eindelijk.

25
00:04:29,863 --> 00:04:31,561
Godzijdank.

26
00:05:33,892 --> 00:05:36,494
Ik maak een spoor.

27
00:05:36,496 --> 00:05:39,430
Ik denk dat we in de buurt zijn.

28
00:05:46,004 --> 00:05:49,073
We moeten uitstappen,
ze de rest van de weg volgen...

29
00:05:49,075 --> 00:05:50,374
kijken wat we kunnen ontdekken.

30
00:05:50,376 --> 00:05:51,943
Ik...

31
00:05:51,945 --> 00:05:53,144
Hoe?

32
00:05:53,146 --> 00:05:55,638
De val is normaal niet gevaarlijk.

33
00:05:55,982 --> 00:05:58,483
Maar als we ons verzetten.
Je moet je gewoon laten vallen.

34
00:05:58,485 --> 00:06:03,287
De truck rijdt langzaam.
We zullen in de blinde hoek zijn.

35
00:06:03,289 --> 00:06:07,458
Als ik word gepakt...
- Het komt goed, we moeten nu gaan.

36
00:06:08,427 --> 00:06:11,062
Doe het eerst voor.

37
00:06:27,479 --> 00:06:31,415
''Doe het eerst voor.''

38
00:06:54,004 --> 00:06:57,875
Jongens, laad alles uit en breng het naar binnen.

39
00:06:57,877 --> 00:07:00,845
Ik moet zelf ook wat ontladen.

40
00:07:00,847 --> 00:07:03,314
Negan, ik wil met je praten over de omleiding.

41
00:07:03,316 --> 00:07:06,117
Wat is ermee?
- Het is misgegaan.

42
00:07:06,119 --> 00:07:08,186
We zijn ermee bezig,
maar het is daar een zooitje.

43
00:07:08,188 --> 00:07:10,154
En wiens taak was dat?

44
00:07:12,091 --> 00:07:14,926
Verdorie.
Ik dacht dat ze het goed dicht hadden gemaakt.

45
00:07:14,928 --> 00:07:17,895
Geen zorgen.
Waar dit vandaan komt, is nog veel meer.

46
00:07:22,534 --> 00:07:25,036
Verdomme.

47
00:07:25,038 --> 00:07:28,472
Wat krijgen we nou?

48
00:07:30,509 --> 00:07:33,010
Stap achteruit.
Laat je wapens vallen.

49
00:07:35,547 --> 00:07:37,448
Ik wil alleen Negan.

50
00:07:37,450 --> 00:07:41,646
Hij vermoordde mijn vrienden.
Niemand anders hoeft te sterven.

51
00:07:48,927 --> 00:07:51,329
Verdorie.

52
00:07:53,899 --> 00:07:57,902
Jij bent schattig.

53
00:07:58,871 --> 00:08:02,073
Koos je dat wapen
omdat het er cool uitziet?

54
00:08:02,075 --> 00:08:04,876
Dat deed je, h�?

55
00:08:04,878 --> 00:08:07,612
Jongen, ik zal niet liegen...

56
00:08:07,614 --> 00:08:11,001
ik schrok me de tering.

57
00:08:16,356 --> 00:08:18,322
Jongen.

58
00:08:18,324 --> 00:08:20,424
Dwight.

59
00:08:20,426 --> 00:08:22,393
Laat hem gaan.

60
00:08:30,502 --> 00:08:33,437
Zo behandel je onze nieuwe gast toch niet?

61
00:08:36,208 --> 00:08:39,543
Kom, knul.
Ik geef je een rondleiding.

62
00:09:30,260 --> 00:09:36,743
Quality over Quantity (QoQ) Releases
The Walking Dead S07E07: Sing Me a Song

63
00:10:01,095 --> 00:10:04,864
Kom, knul.
Ik geef je een rondleiding.

64
00:10:05,932 --> 00:10:09,635
Je doet precies hetzelfde als je vader...

65
00:10:09,637 --> 00:10:12,505
alleen je doet het maar voor de helft goed...

66
00:10:12,507 --> 00:10:15,608
omdat je een oog mist.

67
00:10:19,079 --> 00:10:20,579
Echt?

68
00:10:20,581 --> 00:10:23,149
Je pakt mijn hand niet?

69
00:10:23,151 --> 00:10:27,186
Je hebt geluk dat je nog een hand hebt.

70
00:10:28,455 --> 00:10:32,858
Net zoals Daryl daar,
nu ik erover nadenk.

71
00:10:32,860 --> 00:10:37,463
Hoe gaat het daar, Daryl?
Is het al heet genoeg?

72
00:10:37,465 --> 00:10:40,166
Ja, dat zou lastig gaan met ��n arm.

73
00:10:48,809 --> 00:10:50,443
Slimme jongen.

74
00:10:50,445 --> 00:10:52,111
Kom mee.

75
00:10:53,225 --> 00:10:55,697
Dwighty-boy,
waarom haal jij Daryl niet...

76
00:10:55,783 --> 00:10:58,951
en breng hem naar de keuken.

77
00:10:58,953 --> 00:11:00,312
Nieuw plan, jongens.

78
00:11:00,388 --> 00:11:02,922
Laten we de doden verbranden,
en de truck later uitladen.

79
00:11:02,924 --> 00:11:07,760
Ik heb vandaag geen tijd
om met ��n van mijn vrouwen te neuken.

80
00:11:09,529 --> 00:11:11,564
Misschien wel eentje.

81
00:11:15,802 --> 00:11:17,670
Kom op.

82
00:11:17,672 --> 00:11:19,638
Wat ga je met mij doen?

83
00:11:27,547 --> 00:11:31,650
Nummer ��n:
Laat me geen andere indruk van jou krijgen.

84
00:11:31,652 --> 00:11:34,497
Je bent stoer.
Je bent nergens bang voor.

85
00:11:34,555 --> 00:11:36,723
Wees niet bang voor mij.
Dat is een teleurstelling.

86
00:11:36,758 --> 00:11:42,161
Nummer twee:
Wil je echt dat ik de verrassing verpest?

87
00:11:44,164 --> 00:11:45,716
Krijg wat, knul.

88
00:11:46,446 --> 00:11:48,047
Ik meen het.

89
00:11:48,870 --> 00:11:50,620
Krijg wat.

90
00:12:03,717 --> 00:12:06,252
Kijk dit eens.

91
00:12:18,231 --> 00:12:22,234
De Verlossers gingen op pad
en vochten met de doden...

92
00:12:22,236 --> 00:12:26,272
en kwamen terug met hele goede spullen.

93
00:12:26,274 --> 00:12:30,075
Dat kan ook van jullie zijn...

94
00:12:30,077 --> 00:12:35,714
als jullie hard werken en de regels volgen.

95
00:12:35,716 --> 00:12:40,982
Vandaag krijgt iedereen
verse groenten bij het avondeten.

96
00:12:41,189 --> 00:12:43,925
Geen punten voor nodig.

97
00:12:48,261 --> 00:12:50,596
Zie je dat?

98
00:12:50,598 --> 00:12:53,126
Respect.

99
00:12:53,601 --> 00:12:54,900
Cool, h�?

100
00:12:56,100 --> 00:12:58,203
Zitten ze nog steeds op hun knie�n?

101
00:12:59,105 --> 00:13:00,773
Ga maar weer verder.

102
00:13:07,147 --> 00:13:10,783
Ik wilde je wat laten zien
wat wel potentieel heeft.

103
00:13:10,785 --> 00:13:12,084
Ik weet waar we naartoe gaan.

104
00:13:12,086 --> 00:13:14,253
Ook al ben ik er heel zeker van...

105
00:13:14,255 --> 00:13:18,090
zou ik nalatig zijn als ik niet zou vertellen,
dat na het grondig te hebben geanalyseerd...

106
00:13:18,092 --> 00:13:24,129
denk ik dat we de jackpot hebben gevonden
voor deze taak.

107
00:13:24,131 --> 00:13:28,188
Wat?
- Spullen vinden voor Negan.

108
00:13:28,189 --> 00:13:31,370
Nee, we doen niets voor die<i> cabr�n.</i>

109
00:13:33,974 --> 00:13:36,909
Ga je geen spullen voor de Verlossers zoeken?

110
00:13:38,312 --> 00:13:39,368
Waarom niet?

111
00:13:41,248 --> 00:13:43,149
Je kunt niet tegen ze vechten, Rosita.

112
00:13:44,685 --> 00:13:46,819
Misschien moeten we allemaal samen reizen.

113
00:13:46,821 --> 00:13:48,104
Ik vind het goed.

114
00:13:48,255 --> 00:13:53,325
Als we er zijn, kunnen we met z'n vieren zoeken.
- We hebben geen hulp nodig.

115
00:13:53,327 --> 00:13:56,662
Ik weet dat je het niet leuk vindt
en dat je het niet wilt horen...

116
00:13:56,664 --> 00:13:59,164
maar we moeten ze bevoorraden.

117
00:13:59,166 --> 00:14:00,833
Het is net als de belasting.

118
00:14:05,205 --> 00:14:08,173
Je kunt wel boos tegen mij doen,
maar dit is niet mijn schuld.

119
00:14:08,175 --> 00:14:09,808
Rick deed dat.

120
00:14:11,678 --> 00:14:13,946
Denk je dat je het beter kunt?

121
00:14:13,948 --> 00:14:17,930
Ja.
- Tuurlijk.

122
00:14:17,985 --> 00:14:23,288
Ik wed dat je al een hele wagen vol met spullen
voor jezelf hebt verstopt.

123
00:14:24,691 --> 00:14:30,129
Ga het halen.
Betaal je belasting en laat mij met rust.

124
00:14:32,866 --> 00:14:33,966
Laten we gaan.

125
00:14:49,182 --> 00:14:53,933
Dames.
Let niet op de jongen.

126
00:14:56,923 --> 00:14:58,824
Ik weet het.

127
00:14:58,826 --> 00:15:02,985
Iedere vrouw waar jij vandaan komt
lijkt op een boekhoudster.

128
00:15:03,378 --> 00:15:06,705
Je wilt wel naar hun tieten kijken.

129
00:15:07,001 --> 00:15:11,504
Het is goed, ik vind het niet erg.
Zij ook niet. Ga je gang.

130
00:15:17,878 --> 00:15:22,815
Kan ik je even spreken, lieve vrouw?

131
00:15:26,753 --> 00:15:29,121
Maak het jezelf gemakkelijk.

132
00:15:50,009 --> 00:15:52,244
Wat is er met Amber en Mark gebeurd?

133
00:15:52,246 --> 00:15:56,081
Wat wij bespreken als jij er niet bent,
gaat je niets aan.

134
00:15:58,051 --> 00:16:00,819
Eerlijk.

135
00:16:06,893 --> 00:16:08,994
Wil je weten wat ik heb gehoord?

136
00:16:10,530 --> 00:16:15,234
Ik hoorde dat Mark zijn plicht verzaakte
om bij Amber te zijn.

137
00:16:15,236 --> 00:16:19,905
Ik moet weten of dat waar is.

138
00:16:25,613 --> 00:16:28,481
Er zijn regels om een reden.

139
00:16:28,783 --> 00:16:31,550
Niets doet er nog toe als je dood bent.

140
00:16:31,552 --> 00:16:37,322
Jij, lieverd, bent hier omdat je dat snapt.

141
00:16:43,296 --> 00:16:45,831
Ze maakte een fout.

142
00:16:45,833 --> 00:16:50,335
Wees niet te streng voor haar.
- Kalmeer.

143
00:16:55,408 --> 00:16:58,544
Heb ik ooit er ��n van jullie geslagen?

144
00:16:58,546 --> 00:17:02,981
Nee. Maar ik ken jou.

145
00:17:02,983 --> 00:17:05,350
Er zijn ergere dingen.

146
00:17:15,094 --> 00:17:19,598
Kijk jou nu eens.

147
00:17:22,203 --> 00:17:24,512
Wordt vervolgd.

148
00:17:55,468 --> 00:17:59,171
Amber, lieverd.

149
00:17:59,173 --> 00:18:05,010
Je weet dat ik hier niemand wil,
die hier niet wil zijn, h�?

150
00:18:07,782 --> 00:18:13,152
Dus als je wilt gaan en naar Mark toe wil,
dan kun je dat.

151
00:18:13,554 --> 00:18:17,256
Maar wat kun je niet doen?

152
00:18:18,224 --> 00:18:23,896
Vreemdgaan.
- Inderdaad.

153
00:18:23,898 --> 00:18:26,131
Je kunt niet...

154
00:18:27,667 --> 00:18:30,435
vreemdgaan.

155
00:18:34,041 --> 00:18:37,042
Er zijn er genoeg
die jouw plek in willen nemen...

156
00:18:37,044 --> 00:18:41,380
en er zijn wel wat vacatures.

157
00:18:41,382 --> 00:18:45,050
Wil je terug naar Mark en je moeder?

158
00:18:45,052 --> 00:18:48,387
Ik geef jullie allemaal hetzelfde werk.

159
00:18:48,389 --> 00:18:53,158
Nee. Ik blijf.

160
00:18:53,160 --> 00:18:55,460
Het spijt me.

161
00:18:56,607 --> 00:18:59,329
Je weet wat dat betekent, h�?

162
00:19:00,200 --> 00:19:04,269
Je weet wat dat betekent, h�?

163
00:19:05,638 --> 00:19:09,007
Ja.

164
00:19:10,410 --> 00:19:12,644
Ik hou van je, Negan.

165
00:19:12,646 --> 00:19:16,682
Natuurlijk doe je dat.

166
00:19:19,085 --> 00:19:22,421
Ik weet niet waarom je huilt.

167
00:19:22,423 --> 00:19:25,524
Je hebt het hier harstikke goed.

168
00:19:49,749 --> 00:19:53,218
Wil je Carson voor mij halen?

169
00:19:54,620 --> 00:19:57,055
Ja.
- Zag je dat?

170
00:19:57,057 --> 00:19:58,623
Ik was niet hard tegen haar...

171
00:19:58,625 --> 00:20:03,395
ook al ben ik over het algemeen erg hard.

172
00:20:03,397 --> 00:20:06,631
Je bent een klootzak.
- Dat weet ik.

173
00:20:06,633 --> 00:20:11,136
Maar het ergste is,
dat je me toch wel mag.

174
00:20:11,138 --> 00:20:14,740
Net zoals ik, ken je de waarheid.

175
00:20:55,681 --> 00:20:59,284
Carl, wil je dat dienblad pakken?

176
00:21:04,323 --> 00:21:05,624
Waarom is hij hier?

177
00:21:08,062 --> 00:21:11,548
Waar we hier over praten,
als je er niet bent...

178
00:21:12,466 --> 00:21:15,654
gaat je niets aan.

179
00:21:19,672 --> 00:21:25,917
Laat mij niet deze tandenstoker
in zijn enige oog steken.

180
00:21:27,213 --> 00:21:31,116
Ga met Dwight mee.
Hij geeft je een mop.

181
00:21:31,118 --> 00:21:32,584
Dwighty boy...

182
00:21:32,586 --> 00:21:34,820
zet dat fornuis aan.

183
00:21:34,822 --> 00:21:40,031
Ik kom er zo aan.
Tijd voor een kleine d�j� vu.

184
00:21:41,528 --> 00:21:45,163
Kom, knul.

185
00:22:17,296 --> 00:22:21,433
We hebben alleen maar vandaag
om iets te vinden.

186
00:22:21,435 --> 00:22:24,402
Morgen kunnen ze weer terug zijn.

187
00:22:24,404 --> 00:22:26,505
Nu ook.

188
00:22:32,378 --> 00:22:36,464
<b>LOOP DOOR
HIER VIND JE ALLEEN PROBLEMEN</b>

189
00:22:50,598 --> 00:22:53,280
Iemand haten, is dat een zonde?

190
00:22:54,935 --> 00:22:59,339
Nee, niet per s�.

191
00:22:59,341 --> 00:23:01,641
Gedachten zijn gewoon gedachten.

192
00:23:01,643 --> 00:23:05,478
Uiteindelijk gaat het om onze daden.

193
00:23:14,922 --> 00:23:16,407
Ik haat Rick.

194
00:23:18,260 --> 00:23:20,994
Goed, hij was een agent.

195
00:23:20,996 --> 00:23:23,463
Hij zou de leiding niet moeten hebben.

196
00:23:23,465 --> 00:23:27,705
Mijn moeder, ze werkte voor het Congres.

197
00:23:27,769 --> 00:23:30,904
Zij leidde mensen.
Ze bracht het beste in mensen naar boven.

198
00:23:30,906 --> 00:23:33,096
Rick bracht het beste in mij naar boven.

199
00:23:34,342 --> 00:23:37,543
Hoe? Door je te pesten?

200
00:23:40,582 --> 00:23:42,767
Een beetje.

201
00:23:43,251 --> 00:23:46,150
Maar meer door mij te inspireren.

202
00:23:47,322 --> 00:23:51,552
Hij hield niet alleen de mensen in leven.
Hij bracht ons allemaal samen.

203
00:23:51,593 --> 00:23:54,627
Hij is niet perfect,
maar hij vond zijn manier.

204
00:23:54,629 --> 00:23:58,031
Hij hield niet iedereen in leven, Gabriel.

205
00:23:58,033 --> 00:24:01,697
Mijn moeder, vader, mijn broer...
- Het was niet zijn schuld.

206
00:24:02,137 --> 00:24:05,471
Hij kwam hier en zij stierven.

207
00:24:05,473 --> 00:24:09,275
Of misschien stierven andere mensen niet,
omdat hij hier kwam.

208
00:24:09,277 --> 00:24:13,051
Misschien hadden we die buitenpost
niet aan moeten vallen en dan had Glenn...

209
00:24:15,549 --> 00:24:18,418
We kunnen daar niet aan denken.

210
00:24:18,420 --> 00:24:20,453
Dat horen we niet te doen.

211
00:24:22,656 --> 00:24:24,691
We zien hier nu...

212
00:24:24,693 --> 00:24:26,976
en we kunnen naar de toekomst kijken.

213
00:24:29,496 --> 00:24:31,531
Welke toekomst?

214
00:24:31,533 --> 00:24:34,000
Als hij de leiding heeft, is er geen toekomst.

215
00:24:36,170 --> 00:24:39,572
Het enige waar ik aan blijf denken, is dat...

216
00:24:39,574 --> 00:24:42,892
het misschien wel goed voor ons is...

217
00:24:44,179 --> 00:24:46,179
als hij nooit meer terugkomt.

218
00:24:51,151 --> 00:24:53,353
Stop de auto.

219
00:25:07,501 --> 00:25:12,506
Wat je nu zegt,
maakt je geen zondaar.

220
00:25:13,608 --> 00:25:17,043
Maar wel een verachtelijke klootzak.

221
00:25:18,345 --> 00:25:21,147
Alleen voor nu.

222
00:25:21,715 --> 00:25:24,550
Het hoeft niet voor altijd te zijn.

223
00:25:33,227 --> 00:25:36,095
Ik ga terug.

224
00:27:04,852 --> 00:27:07,253
Zijn al die vrouwen jouw...

225
00:27:07,255 --> 00:27:12,158
Echtgenotes? Ja. Wilde altijd al
verschillende vrouwen neuken.

226
00:27:12,160 --> 00:27:16,696
Waarom er maar bij ��n blijven?
Waarom de oude regels volgen?

227
00:27:16,698 --> 00:27:19,699
Waarom het leven niet beter maken?

228
00:27:19,701 --> 00:27:22,568
Daarover gesproken...

229
00:27:22,570 --> 00:27:24,337
ga zitten.

230
00:27:26,640 --> 00:27:29,242
Laten we beginnen.

231
00:27:29,244 --> 00:27:31,244
Beginnen met wat?

232
00:27:32,814 --> 00:27:36,849
Ik wil je beter leren kennen.

233
00:27:36,851 --> 00:27:38,918
Waarom?

234
00:27:38,920 --> 00:27:42,355
Het uitpraten. Je bent slim.

235
00:27:42,357 --> 00:27:47,160
Ik ga je vertellen hoe slim je bent,
voor het geval je het zelf niet weet.

236
00:27:47,162 --> 00:27:52,431
Ik verwachtte dat een kind van jouw leeftijd
zou mokken, helemaal niets zou doen.

237
00:27:52,433 --> 00:27:56,435
Behalve misschien huilen
omdat je het feest mist.

238
00:27:56,437 --> 00:28:00,840
Maar jij hebt een missie.

239
00:28:00,842 --> 00:28:03,776
Je vindt mij, doodde twee van mijn mannen...

240
00:28:03,778 --> 00:28:05,478
en je bent slim genoeg
om te weten...

241
00:28:05,480 --> 00:28:09,545
dat ik dat niet laat gaan.

242
00:28:14,622 --> 00:28:18,758
Ik kan het niet doen.

243
00:28:18,760 --> 00:28:21,794
Het is net alsof ik tegen
een verjaardagscadeau praat.

244
00:28:21,796 --> 00:28:26,383
Haal dat van je gezicht af.
Ik wil zien wat oma mij gaf.

245
00:28:27,368 --> 00:28:28,434
Nee.

246
00:28:28,436 --> 00:28:30,536
Twee mannen.

247
00:28:31,772 --> 00:28:35,408
Twee mannen.

248
00:28:35,410 --> 00:28:38,244
Straf.

249
00:28:38,946 --> 00:28:43,249
Wil je me echt boos maken?

250
00:29:11,279 --> 00:29:13,546
We zijn er bijna.

251
00:29:20,520 --> 00:29:23,608
Haal dat haar uit je gezicht.
Laat mij eens kijken.

252
00:29:30,732 --> 00:29:34,467
Dat is walgelijk.
Niet gek dat je het bedekt.

253
00:29:34,469 --> 00:29:37,703
Heb je het gezien?
Heb je in de spiegel gekeken?

254
00:29:37,705 --> 00:29:40,840
Dat is echt harstikke smerig.

255
00:29:40,842 --> 00:29:43,776
Ik zie je schedel.

256
00:29:43,778 --> 00:29:48,748
Ik wil het aanraken.
Mag dat?

257
00:30:05,065 --> 00:30:08,334
Jeetje, knul.

258
00:30:08,336 --> 00:30:09,902
Luister...

259
00:30:12,673 --> 00:30:13,950
Ik...

260
00:30:14,042 --> 00:30:18,911
Het is makkelijk om te vergeten
dat je nog maar een kind bent.

261
00:30:20,448 --> 00:30:23,350
Ik wilde je niet kwetsen ofzo.

262
00:30:24,492 --> 00:30:29,488
Ik was alleen maar aan het dollen.
- Vergeet het.

263
00:30:34,729 --> 00:30:36,996
Kom binnen.

264
00:30:36,998 --> 00:30:39,832
Sorry dat ik stoor, meneer,
maar...

265
00:30:39,834 --> 00:30:42,735
je liet Lucille bij de truck liggen.

266
00:30:42,737 --> 00:30:44,804
Meen je dat?

267
00:30:45,772 --> 00:30:46,839
Dat doe ik nooit.

268
00:30:46,841 --> 00:30:50,976
Als een kind met een machinegeweer schiet,
ben je wel even afgeleid.

269
00:30:53,680 --> 00:30:59,118
Maar alle grappen terzijde,
je ziet er wel stoer uit.

270
00:30:59,120 --> 00:31:00,920
Ik zou dat niet bedekken.

271
00:31:00,922 --> 00:31:05,124
Het scoort misschien niet bij de dames,
maar ik zweer je:

272
00:31:05,126 --> 00:31:08,394
Niemand zal je dwarszitten als je er zo uitziet.

273
00:31:08,396 --> 00:31:10,629
Echt niet.

274
00:31:14,368 --> 00:31:18,571
Fat Joseph, heb je haar helemaal
hierheen gedragen voor me?

275
00:31:18,573 --> 00:31:20,373
Ja, meneer.

276
00:31:27,381 --> 00:31:30,416
Was je aardig? Was je lief?

277
00:31:32,520 --> 00:31:35,454
Heb je haar behandeld als een dame?

278
00:31:35,456 --> 00:31:39,759
Ja, meneer.

279
00:31:40,093 --> 00:31:42,962
Heb je haar poesje water gegeven
als een dame?

280
00:31:47,535 --> 00:31:51,398
Ik neem je in de maling.
Een knuppel heeft geen poesje.

281
00:31:54,542 --> 00:31:56,575
Donder op.

282
00:32:00,782 --> 00:32:03,115
Zie je dat?

283
00:32:03,117 --> 00:32:05,084
Dat bedoel ik nou.

284
00:32:05,086 --> 00:32:08,087
Mannen slaan elkaars ballen aan fladderen.

285
00:32:08,089 --> 00:32:11,885
Dit had je vader je moeten leren.

286
00:32:17,732 --> 00:32:20,066
Wat doe je voor je plezier?

287
00:32:20,568 --> 00:32:22,835
Hou je van muziek?

288
00:32:27,974 --> 00:32:32,111
Ik wil dat je een liedje voor me zingt.

289
00:32:32,113 --> 00:32:33,512
Wat?

290
00:32:33,514 --> 00:32:37,049
Ja, je hebt twee van mijn mannen
neergemaaid met een machinegeweer.

291
00:32:37,051 --> 00:32:41,821
Daar wil ik wat voor terug.

292
00:32:41,823 --> 00:32:43,889
Zing een liedje voor me.

293
00:32:46,661 --> 00:32:50,596
Ik weet er geen ��n.
- Gelul.

294
00:32:50,598 --> 00:32:53,866
Wat zong je moeder altijd voor je?

295
00:32:53,868 --> 00:32:56,502
Wat zette je vader op in de auto?

296
00:33:06,012 --> 00:33:08,214
Begin met zingen.

297
00:33:23,698 --> 00:33:26,499
Doorgaan.

298
00:33:39,145 --> 00:33:44,283
Laat me je niet afleiden, jongeman.

299
00:34:02,969 --> 00:34:05,237
Dat was best wel goed.

300
00:34:05,239 --> 00:34:09,108
Lucille houdt ervan
als er voor haar wordt gezongen.

301
00:34:09,110 --> 00:34:14,179
Het enige waar ze meer van houdt,
dan iemands hersens inslaan.

302
00:34:14,181 --> 00:34:16,111
Gek, h�?

303
00:34:25,625 --> 00:34:28,694
Zong je moeder dat voor je?

304
00:34:28,696 --> 00:34:30,658
Waar is ze nu?

305
00:34:35,269 --> 00:34:38,003
Verdomme.

306
00:34:38,005 --> 00:34:40,639
Dood, h�?

307
00:34:43,076 --> 00:34:45,678
Heb je het zien gebeuren?

308
00:34:46,747 --> 00:34:50,015
Ik heb haar doodgeschoten...

309
00:34:50,017 --> 00:34:52,885
voordat ik kon...

310
00:34:54,988 --> 00:34:59,245
Geen wonder dat je
een geboren seriemoordenaar bent.

311
00:35:01,328 --> 00:35:05,831
Dat was trouwens een voorbeeld
van iemands ballen aan fladderen slaan.

312
00:35:11,838 --> 00:35:14,740
Kom op, knul, sta op.

313
00:35:14,742 --> 00:35:17,276
Het moet ondertussen klaar zijn.

314
00:35:17,278 --> 00:35:19,532
Wat is er klaar?

315
00:35:20,615 --> 00:35:22,815
Het ijzer.

316
00:35:43,771 --> 00:35:45,872
Hou even vast voor me.

317
00:35:52,913 --> 00:35:54,696
Jullie kennen de deal.

318
00:35:55,817 --> 00:36:00,687
Wat er gaat gebeuren
wordt een flinke opgave om naar te kijken.

319
00:36:00,689 --> 00:36:02,922
Ik wil het niet doen.

320
00:36:02,924 --> 00:36:07,827
Ik wou dat ik de regels kon negeren
en het laten gaan, maar dat gaat niet.

321
00:36:09,230 --> 00:36:11,331
Waarom?

322
00:36:11,333 --> 00:36:13,833
<i>De regels houden ons in leven.</i>

323
00:36:15,054 --> 00:36:18,754
En dat klopt.

324
00:36:21,042 --> 00:36:23,209
We overleven.

325
00:36:23,211 --> 00:36:25,812
Wij bieden bescherming aan de rest.

326
00:36:25,814 --> 00:36:30,817
Wij brengen de beschaving terug in de wereld.

327
00:36:30,819 --> 00:36:33,953
Wij zijn de Verlossers.

328
00:36:36,924 --> 00:36:39,426
Maar dat kunnen we niet zijn
zonder de regels.

329
00:36:39,428 --> 00:36:44,431
Door de regels werkt het.

330
00:36:44,433 --> 00:36:47,434
Ik weet dat het niet makkelijk is.

331
00:36:47,436 --> 00:36:52,939
Er is altijd werk.
En er is altijd een prijs.

332
00:36:52,941 --> 00:36:59,379
Als je probeert mij te belazeren,
eraan te ontkomen...

333
00:37:06,320 --> 00:37:10,256
dan is het ijzer voor jou.

334
00:37:18,100 --> 00:37:20,900
Opstaan.

335
00:37:33,981 --> 00:37:36,516
D...

336
00:37:54,268 --> 00:37:58,972
Mark, het spijt me.

337
00:37:58,974 --> 00:38:02,809
Maar het is wat het is.

338
00:38:44,452 --> 00:38:48,588
Dat was niet zo erg, h�?

339
00:38:52,359 --> 00:38:54,027
Jezus.

340
00:38:54,029 --> 00:38:56,095
Hij heeft in zijn broek gezeken.

341
00:39:04,371 --> 00:39:07,407
Maak het schoon.

342
00:39:11,345 --> 00:39:16,316
Doc, ik ben klaar.
Doe je ding.

343
00:39:20,621 --> 00:39:24,891
De zwakkeling is flauwgevallen.

344
00:39:25,192 --> 00:39:29,529
De rekening is betaald en we staan gelijk.
Alles is weer goed.

345
00:39:29,531 --> 00:39:33,233
Laat Mark zijn gezicht een herinnering
voor hem zijn...

346
00:39:33,235 --> 00:39:37,871
en voor de rest,
dat de regels belangrijk zijn.

347
00:39:41,242 --> 00:39:44,911
Ik hoop dat we allemaal
wat hebben geleerd vandaag...

348
00:39:44,913 --> 00:39:50,583
want ik wil dit nooit meer hoeven te doen.

349
00:39:58,259 --> 00:40:00,593
Gek dit, h�?

350
00:40:00,595 --> 00:40:03,745
Je denkt vast dat ik getikt ben.

351
00:40:04,065 --> 00:40:07,567
Kom maar mee.
Dan kijken we wat we met jou gaan doen.

352
00:42:11,526 --> 00:42:14,895
Ik wilde hier echt niet terugkomen...

353
00:42:22,170 --> 00:42:24,205
Nee.

354
00:42:25,941 --> 00:42:29,076
Ik vroeg je om een kogel te maken.

355
00:42:29,377 --> 00:42:31,579
En je zei ja tegen me.

356
00:42:31,581 --> 00:42:32,947
Dus maak een kogel voor me.

357
00:42:32,949 --> 00:42:35,850
Je hebt gelijk, ik zei dat ik het zou doen.

358
00:42:35,852 --> 00:42:38,152
Maar dat...

359
00:42:38,154 --> 00:42:40,120
was in het heetst van de strijd.

360
00:42:40,122 --> 00:42:44,525
De uren erna heb ik alle andere opties bekeken...

361
00:42:44,527 --> 00:42:50,132
en nagedacht over
wat er uiteindelijk zou gebeuren.

362
00:42:52,300 --> 00:42:56,537
Ik probeer je absoluut niet tegen te houden.

363
00:42:56,539 --> 00:42:59,006
Misschien je vertragen.

364
00:42:59,008 --> 00:43:02,376
Misschien een bepaald gevoel
van veiligheid geven.

365
00:43:02,378 --> 00:43:05,312
Ik wacht niet, Eugene.
- Dat was dom.

366
00:43:05,314 --> 00:43:10,517
<i>Pendejo,</i> je weet niet eens waar je over praat.
- Dat weet ik wel.

367
00:43:10,519 --> 00:43:13,420
Ik heb dit grondig geanalyseerd.

368
00:43:13,422 --> 00:43:17,057
Het maakt niet uit welke vaardigheden je bezit.

369
00:43:17,059 --> 00:43:20,160
Abraham had gelijk,
zij zijn in de meerderheid.

370
00:43:20,162 --> 00:43:25,332
Zijn spel, tenzij we het winnende punt maken,
met de man die de leiding heeft.

371
00:43:25,334 --> 00:43:28,435
E�n kogel zal niet werken.
In het beste geval schakel je hem uit.

372
00:43:28,437 --> 00:43:30,037
Dus maak een kogel voor me.

373
00:43:30,039 --> 00:43:34,208
Iemand moet de prijs betalen.

374
00:43:34,210 --> 00:43:36,944
Zelfs als je wilt...

375
00:43:36,946 --> 00:43:40,314
van alles wat we hebben gezien,
zou jij het kunnen zijn.

376
00:43:40,316 --> 00:43:44,084
Zal de man die onze redder doodde, sterven?

377
00:43:44,086 --> 00:43:47,021
Misschien.
- Maak dan een kogel voor me.

378
00:43:47,023 --> 00:43:50,124
Je praat dit niet uit mijn hoofd, Eugene.

379
00:43:50,126 --> 00:43:55,062
Je bent me dat schuldig, en hem ook.
Als er een prijs is, zal ik hem betalen.

380
00:43:55,064 --> 00:43:58,632
Maar zeg me niet dat je het niet doet.

381
00:44:00,135 --> 00:44:01,468
Je weet niks.

382
00:44:01,470 --> 00:44:04,605
Je doet niks.

383
00:44:05,106 --> 00:44:07,041
Je bent een lafaard.

384
00:44:07,409 --> 00:44:09,977
En je bent zwak.

385
00:44:09,979 --> 00:44:16,050
De enige reden dat je nog leeft is omdat je liegt
en dat mensen je zielig vinden.

386
00:44:21,189 --> 00:44:24,325
Dus doe deze ene keer...

387
00:44:24,327 --> 00:44:28,329
iets nuttigs...

388
00:44:29,464 --> 00:44:33,133
en maak een kogel voor me.

389
00:45:24,854 --> 00:45:27,789
Neem het jezelf niet kwalijk.

390
00:45:27,791 --> 00:45:29,791
Waarom zou ik?

391
00:45:31,896 --> 00:45:37,566
Hij vroeg je over Mark en Amber,
en je hebt ze verraden, toch?

392
00:45:38,566 --> 00:45:41,401
Je moest wel.

393
00:45:41,704 --> 00:45:43,828
Dat is niet wat er is gebeurd.

394
00:45:49,646 --> 00:45:52,581
Wat je ook helpt om 's nachts te slapen.

395
00:46:00,723 --> 00:46:04,192
Hoe slaap jij 's nachts?
- Dat doe ik niet.

396
00:46:04,194 --> 00:46:07,378
Ik blijf tot in de ochtend TV kijken.

397
00:46:11,701 --> 00:46:14,036
Toen we die deal met hem maakten...

398
00:46:14,038 --> 00:46:17,339
hebben wij dat besloten,
het had alleen met ons te maken.

399
00:46:17,341 --> 00:46:19,241
Nee.

400
00:46:19,243 --> 00:46:21,209
Als je nog steeds staat...

401
00:46:21,211 --> 00:46:24,646
dan is het altijd over iemand anders zijn rug.

402
00:46:30,353 --> 00:46:33,188
Je moet gaan.
Straks ziet iemand ons nog.

403
00:46:33,190 --> 00:46:35,290
We doen niks.

404
00:46:36,659 --> 00:46:40,629
Nee. Dat klopt.

405
00:46:52,375 --> 00:46:54,876
Mag ik het weer omdoen?

406
00:46:54,878 --> 00:46:57,179
Nee, absoluut niet.

407
00:46:57,181 --> 00:46:58,768
Waarom niet?

408
00:47:00,384 --> 00:47:03,852
Moet je die agressieveling zien.

409
00:47:03,854 --> 00:47:07,689
Het mag niet omdat ik nog niet klaar met je bent.

410
00:47:07,691 --> 00:47:14,162
Ik kijk graag naar die agressieve ogen,
dus het zit er niet in.

411
00:47:18,001 --> 00:47:19,234
Wat?

412
00:47:19,236 --> 00:47:22,170
Heb je iets te zeggen?

413
00:47:22,772 --> 00:47:24,773
Waarom heb je me niet gedood?

414
00:47:24,775 --> 00:47:28,010
Of mijn vader of Daryl?

415
00:47:28,778 --> 00:47:30,912
Daryl...

416
00:47:30,914 --> 00:47:34,016
wordt een goede soldaat voor mij.

417
00:47:34,018 --> 00:47:37,352
Hij denkt dat hij alles onder controle heeft...

418
00:47:37,354 --> 00:47:39,721
maar je hebt het zelf gezien.

419
00:47:39,723 --> 00:47:41,423
Je vader?

420
00:47:41,425 --> 00:47:43,925
Die heeft al geweldig spul geleverd.

421
00:47:43,927 --> 00:47:48,764
Maar jij...

422
00:47:48,766 --> 00:47:54,336
Nou, we zullen zien.

423
00:47:54,338 --> 00:47:58,006
Het is nuttiger om je te breken.

424
00:47:58,008 --> 00:48:01,410
Leuker ook.

425
00:48:01,412 --> 00:48:03,345
Je vindt dat dom?

426
00:48:03,347 --> 00:48:06,682
Ik denk dat je anders bent.

427
00:48:10,120 --> 00:48:13,188
Je bent een slimme knul.

428
00:48:13,190 --> 00:48:15,323
Wat denk jij wat ik moet doen?

429
00:48:15,325 --> 00:48:18,994
Je weet dat ik je niet kan laten gaan.

430
00:48:18,996 --> 00:48:21,463
Dus, moet ik je vermoorden?

431
00:48:21,465 --> 00:48:25,934
Je gezicht bewerken? Je arm eraf hakken?

432
00:48:25,936 --> 00:48:29,786
Zeg het maar.
Wat denk je zelf?

433
00:48:31,040 --> 00:48:35,696
Ik denk dat je uit het raam moet springen,
dan hoef ik je niet te doden.

434
00:48:40,117 --> 00:48:45,964
Kijk, dat is de knul die pas echt indruk maakt.

435
00:48:47,724 --> 00:48:51,927
Ik denk dat je niet zegt wat je met me gaat doen,
want je doet helemaal niks.

436
00:48:51,929 --> 00:48:55,297
Als je ons kende, als je iets wist...

437
00:48:55,299 --> 00:48:58,066
zou je ons vermoorden.

438
00:48:58,068 --> 00:49:00,335
Maar dat kun je niet.

439
00:49:03,907 --> 00:49:06,742
Misschien heb je gelijk.

440
00:49:06,744 --> 00:49:09,311
Misschien kan ik dat niet.

441
00:49:13,450 --> 00:49:16,184
Laten we een stukje gaan rijden, knul.

442
00:49:38,508 --> 00:49:40,909
Daryl.

443
00:49:44,013 --> 00:49:47,849
Je ziet er bezorgd uit,
dus ik breng die knul naar huis.

444
00:49:47,851 --> 00:49:50,786
Als je hem iets doet...
- Dwight.

445
00:49:51,988 --> 00:49:56,091
Daryl heeft een pauze nodig,
breng hem naar zijn kooi.

446
00:52:00,885 --> 00:52:03,520
Laat vallen.

447
00:52:07,625 --> 00:52:09,826
Ook je mes.

448
00:52:15,166 --> 00:52:17,734
En wil je mijn schoenen straks ook?

449
00:52:19,803 --> 00:52:22,972
Breng me naar Negan.

450
00:52:37,154 --> 00:52:39,889
Start hem. Rijden.

451
00:53:09,887 --> 00:53:14,057
Breng me naar Negan.

452
00:53:27,973 --> 00:53:29,839
Carl, waar is...
- Het gaat goed met Enid.

453
00:53:29,841 --> 00:53:32,909
Geweldig.

454
00:53:33,777 --> 00:53:35,244
Waar is Rick?

455
00:53:35,246 --> 00:53:36,346
Ik...

456
00:53:36,348 --> 00:53:39,349
Doet er niet toe.

457
00:53:39,351 --> 00:53:41,884
Waar is Rick?

458
00:53:42,286 --> 00:53:45,722
Hij zoekt buiten spullen voor je.

459
00:53:45,724 --> 00:53:49,392
Cool.
Dan wacht ik.

460
00:53:50,663 --> 00:53:56,199
Hij ging verder als normaal.
Ze zijn waarschijnlijk vandaag niet terug.

461
00:53:56,201 --> 00:53:58,101
We hebben haast geen spullen meer.

462
00:53:58,103 --> 00:54:00,370
We sterven haast van de honger.

463
00:54:02,306 --> 00:54:04,641
Sterven?

464
00:54:05,909 --> 00:54:07,877
Jij?

465
00:54:07,879 --> 00:54:12,715
Met 'haast' bedoel je 'niet echt.'

466
00:54:22,360 --> 00:54:23,993
Echt?

467
00:54:23,995 --> 00:54:28,965
Jullie hebben echt geen gevoel voor humor.

468
00:54:35,172 --> 00:54:37,073
Sorry.

469
00:54:39,777 --> 00:54:41,811
Wat was je naam ook alweer?

470
00:54:42,379 --> 00:54:45,081
Olivia.

471
00:54:45,083 --> 00:54:46,683
Juist.

472
00:54:46,685 --> 00:54:48,451
Olivia.

473
00:54:57,127 --> 00:55:02,865
Het spijt me dat ik zo grof tegen je was.

474
00:55:02,867 --> 00:55:04,834
En zo te zien ben ik hier nog wel even...

475
00:55:04,836 --> 00:55:08,705
in afwachting
tot jullie niet-gevreesde-leider terugkeert.

476
00:55:08,707 --> 00:55:11,774
En als je zou willen...

477
00:55:11,776 --> 00:55:14,744
zou het wel leuk zijn...

478
00:55:14,746 --> 00:55:16,913
als ik je flink zou naaien.

479
00:55:16,915 --> 00:55:21,350
Als je ermee akkoord gaat.

480
00:55:33,464 --> 00:55:37,300
Ik vind je nu 50% leuker.

481
00:55:39,236 --> 00:55:41,437
Als je het maar weet.

482
00:55:47,912 --> 00:55:53,950
Goed, ik ga met mijn poten omhoog
en wacht tot mijn spullen er zijn.

483
00:55:53,952 --> 00:55:55,351
Olivia...

484
00:55:55,353 --> 00:55:58,988
Wil jij als een braaf lammetje
wat limonade voor ons maken?

485
00:55:58,990 --> 00:56:01,958
Ik weet dat ik wat
van die poederzooi heb achtergelaten.

486
00:56:01,960 --> 00:56:03,126
Het moet met...

487
00:56:03,128 --> 00:56:04,761
Maak het.

488
00:56:08,766 --> 00:56:10,533
Maak het.

489
00:56:10,535 --> 00:56:13,169
Neem je tijd.

490
00:56:13,171 --> 00:56:15,805
En maak het lekker.

491
00:56:25,816 --> 00:56:27,250
Goed, knul.

492
00:56:27,252 --> 00:56:29,285
Geef me een rondleiding.

493
00:56:52,843 --> 00:56:54,577
Wat dacht je van deze?

494
00:56:54,579 --> 00:56:58,481
Het is maar een waterkoker.
- Meen je dat nu?

495
00:57:08,525 --> 00:57:12,137
Moet je dat leuke engeltje zien.

496
00:57:51,302 --> 00:57:53,437
Die gast is bewapend.

497
00:57:53,439 --> 00:57:57,174
Hij heeft eten, voorraad en munitie.

498
00:58:04,849 --> 00:58:11,300
"De enige manier dat je zo ver kan lezen voordat
je neergeschoten zou worden, is omdat ik dood ben."

499
00:58:31,844 --> 00:58:34,444
Ziet er naar uit
dat hij zijn voorraad heeft beschermd.

500
00:58:34,812 --> 00:58:37,581
Ziet er naar uit dat het nog steeds beschermd is.

501
00:58:39,617 --> 00:58:42,486
Alleen vandaag, toch?

502
00:59:04,909 --> 00:59:06,743
Bedankt.

503
00:59:10,248 --> 00:59:12,449
Het spijt me van wat ik heb gezegd.
- Dat negeer ik.

504
00:59:12,451 --> 00:59:14,317
Het spijt me.

505
00:59:14,319 --> 00:59:16,219
Nee.

506
00:59:16,654 --> 00:59:20,490
Misschien de woorden, maar niet de intentie.

507
00:59:20,492 --> 00:59:23,360
Je klinkt eerlijk,
maar dat is maar een vlooiencircus.

508
00:59:23,362 --> 00:59:24,761
Je hebt wat je wilt.

509
00:59:24,763 --> 00:59:27,931
Je zegt dit omdat je je slecht voelt,
maar er niet voor uit willen komen.

510
00:59:27,933 --> 00:59:31,334
Je meende het, je voelde het.
Dat is jouw waarheid.

511
00:59:31,669 --> 00:59:33,436
Eugene...

512
00:59:33,438 --> 00:59:35,839
Ik heb nu een momentje voor mezelf nodig.

513
00:59:54,725 --> 00:59:56,326
Gevonden wat je zocht?

514
00:59:56,328 --> 00:59:57,794
Het is goed gegaan.

515
00:59:59,797 --> 01:00:01,598
Bij mij ook.

516
01:00:07,438 --> 01:00:09,306
Hoe kom je hieraan?

517
01:00:13,344 --> 01:00:14,678
Latijns.

518
01:00:14,680 --> 01:00:16,580
Het was een moeilijke les.

519
01:00:16,582 --> 01:00:20,317
Mijn moeder zei dat moeilijk
ooit van pas zou komen.

520
01:00:20,319 --> 01:00:23,420
Het is een lijst van vangsten
van een dode vent met een plan.

521
01:00:23,422 --> 01:00:27,257
Ik heb medische benodigdheden,
waterzuiveraars, fakkels...

522
01:00:27,259 --> 01:00:29,759
En dat geef je allemaal aan hem?

523
01:00:29,761 --> 01:00:33,997
Dat is wat Rick wil dat we doen, toch?

524
01:00:35,466 --> 01:00:37,834
Dus dat ga ik doen.

525
01:00:37,836 --> 01:00:39,970
En ik ga meer doen als dat.

526
01:01:03,027 --> 01:01:05,295
Ze zijn er.

527
01:01:05,297 --> 01:01:07,397
Hij is hier.

528
01:01:10,702 --> 01:01:14,004
Deze kleine meid is waardevol.

529
01:01:16,742 --> 01:01:19,309
Hallo, buur.

530
01:01:19,311 --> 01:01:20,677
Waarom kom je straks niet langs?

531
01:01:20,679 --> 01:01:24,414
Misschien zetten we de BBQ buiten.

532
01:01:25,283 --> 01:01:28,485
Ik vind het wel wat hier.

533
01:01:30,489 --> 01:01:33,790
Misschien moet ik hier wel gewoon blijven.

534
01:01:35,426 --> 01:01:38,695
Ik zat te denken wat je eerder
tegen me zei, Carl.

535
01:01:40,831 --> 01:01:44,801
Misschien is het dom om jou
en je vader in leven te houden.

536
01:01:46,404 --> 01:01:49,306
Ik bedoel, waarom doe ik zo mijn best?

537
01:01:49,640 --> 01:01:55,045
Misschien moet ik jullie beiden
daar in dat perkje begraven.

538
01:01:59,051 --> 01:02:01,751
En dan kom ik gewoon in deze wijk wonen.

539
01:02:01,753 --> 01:02:03,353
Wat vind jij daarvan?

540
01:02:03,356 --> 01:02:06,274
Vertaling:
Quality over Quantity (QoQ) Releases

541
01:02:06,277 --> 01:02:06,796
Download deze ondertitel op:
- www.OpenSubtitles.org -

