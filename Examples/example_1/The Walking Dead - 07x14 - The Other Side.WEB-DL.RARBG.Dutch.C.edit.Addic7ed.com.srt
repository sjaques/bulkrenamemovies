1
00:05:24,178 --> 00:05:25,911
Hoi.

2
00:05:29,650 --> 00:05:32,867
Ben je hier...
- Omdat ik je hulp nodig heb.

3
00:06:16,122 --> 00:06:20,440
Quality over Quantity (QoQ) Releases
The Walking Dead S07E14: The Other Side

4
00:06:23,234 --> 00:06:25,535
Sorry dat ik je caravan overneem.

5
00:06:26,872 --> 00:06:30,037
Ik groeide met veel mensen om mij heen op.
Ik ben eraan gewend.

6
00:06:30,105 --> 00:06:34,741
Grote familie?
- Opvangtehuis.

7
00:06:36,011 --> 00:06:40,548
Maar zo is het hier niet.
Tenminste, niet de slechte dingen.

8
00:06:41,517 --> 00:06:44,068
Voor het eerst voel ik mij ergens thuis.

9
00:06:44,836 --> 00:06:47,688
Ervoor zorgen dat jij
en Sasha hier deel van uit zouden maken...

10
00:06:47,723 --> 00:06:49,969
liet mij hier ook een deel van uitmaken.

11
00:06:50,225 --> 00:06:54,127
Toen ik hier net was, was ik nooit hier.

12
00:06:55,531 --> 00:06:59,633
Ik vind het altijd moeilijk
om mij open te stellen...

13
00:06:59,668 --> 00:07:04,744
buren, vrienden, vriendjes.

14
00:07:10,063 --> 00:07:12,378
Je moet het een keer proberen.

15
00:07:13,314 --> 00:07:15,114
Zelfs al is het niet voor lang.

16
00:07:17,519 --> 00:07:21,321
Ik ga aan de smid vragen
of hij meer speren kan maken.

17
00:07:21,556 --> 00:07:24,606
Misschien kunnen we ze ruilen met het Koninkrijk
voor kogelwerende vesten.

18
00:07:25,326 --> 00:07:28,261
Eigenlijk hebben we een militaire uitrusting nodig.

19
00:08:09,135 --> 00:08:14,782
Ik zocht de vorige keer naar iets om te lezen.

20
00:08:19,846 --> 00:08:21,645
Je mag de kogels houden.

21
00:08:22,081 --> 00:08:25,482
Ik wist niet dat je een pistool had.
- Had ik ook niet.

22
00:08:26,586 --> 00:08:28,470
Maar nu wel.

23
00:08:29,488 --> 00:08:32,523
Sasha, doe het nu nog niet.

24
00:08:35,995 --> 00:08:38,362
Rosita kwam hier niet om de mensen te trainen.

25
00:08:38,397 --> 00:08:40,721
Jullie gaan allebei achter Negan aan.

26
00:08:41,734 --> 00:08:43,767
Maar dat kunnen jullie niet zonder hulp.

27
00:08:44,590 --> 00:08:46,192
Heel veel hulp.

28
00:08:46,195 --> 00:08:48,210
We hebben dit al besproken.

29
00:08:48,213 --> 00:08:52,790
Ik weet wat je denkt.
Ik waardeer dat.

30
00:08:52,812 --> 00:08:55,963
Maar ik verander niet van gedachten.

31
00:08:56,048 --> 00:08:57,954
Zij doet dat niet.

32
00:08:59,384 --> 00:09:02,359
Weet Maggie al dat Rosita hier is?

33
00:09:02,821 --> 00:09:07,391
Ik denk het niet.
Ik heb niets...

34
00:09:07,459 --> 00:09:10,060
Je moet het haar vertellen.

35
00:09:10,095 --> 00:09:11,428
Alles.

36
00:09:11,663 --> 00:09:14,131
Nee, nog niet.

37
00:09:16,001 --> 00:09:18,734
Ik bereid mij er nog steeds op voor.

38
00:09:20,505 --> 00:09:24,173
Maar Rosita gaat, met of zonder mij.

39
00:09:24,209 --> 00:09:25,908
Dus moet ze samen met mij gaan.

40
00:09:25,944 --> 00:09:27,276
Dan ga ik ook mee.
- Ik ook.

41
00:09:27,345 --> 00:09:28,845
Nee.

42
00:09:28,880 --> 00:09:32,348
De Hilltop moet voorbereid zijn op
wat er daarna gebeurt.

43
00:09:32,417 --> 00:09:33,983
Maggie heeft jullie nodig.

44
00:09:35,186 --> 00:09:37,386
Ze heeft jou ook nodig.

45
00:09:38,004 --> 00:09:40,807
Niet meer.

46
00:09:40,959 --> 00:09:45,565
Ze heeft alle anderen en zij hebben haar.

47
00:09:53,037 --> 00:09:56,972
Je kunt blijven, dat weet ik.

48
00:09:58,028 --> 00:10:03,479
Maar ik weet dat jij en zij dat niet doen,
maar ik wens dat je blijft.

49
00:10:03,514 --> 00:10:07,016
Want het is een lang leven
en dan niet meer.

50
00:10:07,051 --> 00:10:10,119
Je kunt alles meenemen wat je nodig hebt.

51
00:10:10,154 --> 00:10:13,622
Maar jij en Rosita moeten met Maggie praten.

52
00:10:13,658 --> 00:10:15,524
Dat zijn jullie haar wel schuldig.

53
00:10:30,874 --> 00:10:32,760
Luister...

54
00:10:33,109 --> 00:10:34,742
Enid...

55
00:10:36,013 --> 00:10:38,145
Maggie vertrouwt jou.

56
00:10:39,916 --> 00:10:43,563
Je moet haar beschermen.

57
00:10:43,586 --> 00:10:46,521
Zij is de toekomst van deze plek.

58
00:10:46,589 --> 00:10:48,155
Dat weet ik zeker.

59
00:10:50,894 --> 00:10:52,660
Net zoals jij.

60
00:10:58,701 --> 00:11:01,369
Bewaar dit voor mij.

61
00:11:03,373 --> 00:11:04,739
Het is voor de baby.

62
00:11:07,844 --> 00:11:11,111
Misschien kun jij het afmaken
terwijl ik weg ben.

63
00:11:17,053 --> 00:11:20,721
Sasha...
- Ja.

64
00:11:26,061 --> 00:11:28,991
Over tien minuten vertel ik Maggie
wat er aan de hand is.

65
00:11:30,933 --> 00:11:32,866
Kijk maar wat je daarmee doet.

66
00:11:35,638 --> 00:11:37,560
Ik doe wat je mij vroeg.

67
00:11:58,993 --> 00:12:02,094
<i>De Verlossers komen eraan.</i>

68
00:12:02,130 --> 00:12:03,296
We moeten naar Maggie.

69
00:12:03,331 --> 00:12:04,564
Waar is ze?
- Kom op.

70
00:12:12,269 --> 00:12:15,447
Waar verbergen we ons?
- Dat doen we niet.

71
00:12:21,615 --> 00:12:24,616
Ga erin, het gaat naar de andere kant.

72
00:12:24,685 --> 00:12:26,496
We moeten hier weg,
voordat Maggie dat doet.

73
00:12:38,599 --> 00:12:40,546
We halen het nooit op tijd.

74
00:12:41,101 --> 00:12:42,715
Kom op.

75
00:12:49,877 --> 00:12:52,777
Blijf daar beneden.
Ik houd ze wel weg.

76
00:12:52,813 --> 00:12:55,480
Het zijn niet dezelfde die naar Alexandria gaan.

77
00:13:27,313 --> 00:13:32,583
Simon. Hallo.
- Jij ook, Gregory.

78
00:13:32,651 --> 00:13:33,951
Hallo.

79
00:13:37,056 --> 00:13:41,992
We hadden jullie nog niet verwacht.
Wil je in mijn kantoor praten?

80
00:13:42,027 --> 00:13:46,397
Mijn mensen vonden een fles gin.
Jij houdt toch van gin?

81
00:13:46,632 --> 00:13:51,949
Ik ben een man die steeds van smaak verandert.

82
00:13:51,971 --> 00:13:53,737
Ik drink momenteel tequila.

83
00:13:55,273 --> 00:13:59,542
<i>A�ejo. Reposado.</i>
Kleine slokjes, mixen...

84
00:13:59,578 --> 00:14:02,289
<i>Un mundo de sabor.</i>

85
00:14:02,881 --> 00:14:05,597
Gin lijkt nu wel terpentijn.

86
00:14:06,384 --> 00:14:09,652
Het ging de vorige keer best snel.

87
00:14:09,688 --> 00:14:12,388
Ik ben anders dan anderen.

88
00:14:12,424 --> 00:14:17,427
Weet je wat? Ik respecteer dat.
Tequila.

89
00:14:17,462 --> 00:14:21,431
Dat onthoud ik.
- Geweldig.

90
00:14:22,100 --> 00:14:26,102
We hoeven niet te praten.
Hopelijk is dit een snel bezoek.

91
00:14:27,572 --> 00:14:29,672
Wat kan ik voor je doen?

92
00:14:29,807 --> 00:14:34,510
Dat is een geweldige vraag.
Helaas is het antwoord niet zo geweldig.

93
00:14:34,545 --> 00:14:37,246
Je hebt hier een zeker iemand...

94
00:14:37,281 --> 00:14:39,381
die nu Negans zekere iemand moet zijn.

95
00:14:39,417 --> 00:14:43,319
Wat je voor mij kunt doen
is mij in die richting sturen.

96
00:14:46,056 --> 00:14:48,557
Wie is het?

97
00:14:48,592 --> 00:14:51,560
Steek je wijsvinger op en ik vertel het je.

98
00:14:59,202 --> 00:15:00,802
Daryl.

99
00:15:21,307 --> 00:15:23,191
Heb jij die tunnel gegraven?

100
00:15:23,226 --> 00:15:26,428
Maggie dacht dat we een snelle uitweg nodig hadden.
Dus maakte ik er een.

101
00:15:26,463 --> 00:15:29,064
We zijn er weg, wat nu?

102
00:15:30,667 --> 00:15:32,200
Dat weet je.

103
00:15:55,081 --> 00:15:57,915
Als deze start, hevel ik benzine over.
Ik moet alleen iets vinden...

104
00:15:57,920 --> 00:15:59,387
Er is een lege waterfles.

105
00:16:02,437 --> 00:16:05,372
Ik dacht dat dit het was, maar...

106
00:16:14,182 --> 00:16:20,220
Vind je het leuk? Ik heb het gemaakt.
Auto is overleden.

107
00:17:07,258 --> 00:17:09,267
Waar heb je geleerd om bommen te ontmantelen?

108
00:17:11,004 --> 00:17:15,650
Zat je in het leger?
- Iemand die ik kende.

109
00:17:16,776 --> 00:17:18,309
Hij niet.

110
00:17:18,344 --> 00:17:20,511
Ik ben niet hier om je beter te leren kennen...

111
00:17:20,580 --> 00:17:22,847
dus of we praten over de missie
of helemaal niet.

112
00:17:29,088 --> 00:17:30,942
Goed.

113
00:17:38,564 --> 00:17:41,665
Jezus zei dat er gebouwen zijn
met twee, drie of vier verdiepingen.

114
00:17:41,701 --> 00:17:44,233
Net buiten het toevluchtsoord,
richting het oosten.

115
00:17:44,234 --> 00:17:49,240
Een ervan moet uitzicht hebben op de binnenplaats
waar de walkers zijn.

116
00:17:49,275 --> 00:17:51,942
We gaan het gebouw binnen, wachten
en schieten daarvandaan.

117
00:17:53,437 --> 00:17:56,947
Als we dichterbij moeten komen,
zijn er zwakke plekken op het terrein...

118
00:17:57,016 --> 00:18:00,884
waar we ons voordeel mee kunnen doen,
maar als het kan...

119
00:18:00,920 --> 00:18:03,820
zeg ik dat we erbuiten blijven
en het goed doen.

120
00:18:04,189 --> 00:18:06,957
Misschien komen we er dan levend weg.

121
00:18:17,436 --> 00:18:21,285
Heb je daar een probleem mee?
- Ik wil er zeker van zijn dat we hem doden.

122
00:18:21,486 --> 00:18:24,504
Als we missen, krijgen we geen andere kans.
- Ik mis niet.

123
00:18:24,543 --> 00:18:28,044
Zelfs als je hem raakt en hij sterft niet.
- Ik mis niet.

124
00:18:28,080 --> 00:18:31,014
Als we erheen gaan en worden gepakt...
- Ik word niet gepakt.

125
00:18:31,049 --> 00:18:32,615
Wat is er mis met jou?

126
00:18:34,152 --> 00:18:36,919
Als ze ons pakken, krijgen we geen kans meer.

127
00:18:36,955 --> 00:18:38,688
Want dan vermoorden ze ons.

128
00:18:39,824 --> 00:18:43,526
Hierbuiten leven we.
Dan krijgen we nog een kans.

129
00:18:59,444 --> 00:19:03,446
Er is niets mis met mij, Sasha.
Ik wil dat hij dood is.

130
00:19:03,815 --> 00:19:07,650
Als dit teveel voor je is en jij je wilt
terugtrekken, kun je dat nu doen.

131
00:19:07,685 --> 00:19:10,252
Ik ga nergens heen.
- Ik wil er niet meer over praten.

132
00:19:10,288 --> 00:19:12,721
Goed.
Misschien denk je er dan over na.

133
00:19:13,290 --> 00:19:15,257
Misschien verander je van gedachten...

134
00:19:15,292 --> 00:19:18,080
over wat ervoor nodig is
om te krijgen wat je wilt.

135
00:19:27,604 --> 00:19:32,006
Hoi, ik heb verse groenten.

136
00:19:32,075 --> 00:19:36,344
Stop. Het zijn moesgroenten.
Gebruik het hele woord.

137
00:19:36,379 --> 00:19:37,679
We hebben de tijd.

138
00:19:40,750 --> 00:19:44,352
Ik moet deze moesgroenten
naar jullie toe brengen...

139
00:19:44,387 --> 00:19:47,589
en de mand is best wel zwaar.

140
00:19:47,824 --> 00:19:51,283
Voor mij, bedoel ik.
Waarschijnlijk niet voor jou.

141
00:19:51,328 --> 00:19:55,129
Hier, zet het in de truck
en als je naar de tuin toe komt...

142
00:19:55,165 --> 00:19:57,665
kan ik de rest...
- Stop.

143
00:19:57,834 --> 00:20:01,469
Ik weet niet wie jij denkt dat ik ben
of wie wij zijn.

144
00:20:01,504 --> 00:20:04,038
Laad ze zelf maar in.
Ik heb het druk.

145
00:20:04,673 --> 00:20:06,974
Sorry.

146
00:20:07,009 --> 00:20:11,178
Raap dat op en ga weg.

147
00:20:14,683 --> 00:20:20,020
En geef dat nu aan mij.

148
00:20:20,055 --> 00:20:23,090
Laat het mij niet afpakken.

149
00:21:31,452 --> 00:21:34,357
Dr Harlan Carson, correct?

150
00:21:35,162 --> 00:21:38,396
Heb je een controle nodig?
- Waarschijnlijk.

151
00:21:38,432 --> 00:21:41,800
Het is lang geleden,
maar daarom ben ik niet hier.

152
00:21:42,697 --> 00:21:46,840
De laatste keer gaven we al de meeste medicijnen.

153
00:21:46,841 --> 00:21:50,742
Ja, maar daarom zijn we niet hier.

154
00:21:50,777 --> 00:21:54,246
Gefeliciteerd, je hebt promotie gemaakt.

155
00:21:54,281 --> 00:21:56,214
Negan wil dat je bij hem bent.

156
00:21:56,250 --> 00:21:58,350
Je bent een belangrijk man, Harlan.

157
00:22:00,253 --> 00:22:02,220
Je gaat met ons mee.

158
00:22:03,590 --> 00:22:07,659
Ik heb hier pati�nten die me nodig hebben.

159
00:22:07,694 --> 00:22:09,894
Waarom heeft Negan twee dokters nodig?

160
00:22:18,104 --> 00:22:21,541
Ik dacht dat mijn blik die informatie al gaf.

161
00:22:21,808 --> 00:22:24,175
Ze zeggen dat mijn gezicht al genoeg zegt.

162
00:22:24,243 --> 00:22:28,346
Maar zo niet, laat mij jouw vraag herhalen.
Waarom hebben we twee dokters nodig?

163
00:22:28,381 --> 00:22:31,345
Antwoord: dat hebben we niet.

164
00:22:36,322 --> 00:22:40,457
Dus mijn broer maakte iemand kwaad...

165
00:22:40,493 --> 00:22:41,764
wat zijn dood werd?

166
00:22:42,228 --> 00:22:46,698
Zoiets, het ging er heftig aan toe.

167
00:22:46,899 --> 00:22:48,616
Gecondoleerd.

168
00:22:49,802 --> 00:22:52,269
Maar het is tijd.

169
00:22:53,906 --> 00:22:55,906
Al mijn gegevens zijn in orde.

170
00:22:55,941 --> 00:22:58,709
Je moet regelingen treffen
voor medische zorg...

171
00:22:58,744 --> 00:23:01,245
zeker voor mijn lopende pati�nten.

172
00:23:01,280 --> 00:23:03,080
Dat doe ik.

173
00:23:04,983 --> 00:23:06,616
Ik moet inpakken.
- Nee.

174
00:23:06,752 --> 00:23:10,153
We hebben alles wat je nodig hebt,
en anders halen we het wel.

175
00:23:10,222 --> 00:23:12,122
Hou je van ijs?

176
00:23:12,190 --> 00:23:13,857
Wij hebben ijs.

177
00:23:13,892 --> 00:23:17,594
We hebben een dame die <i>cardamom gelato</i> maakt.

178
00:23:17,629 --> 00:23:18,995
Ik neem je niet in de maling.

179
00:23:19,064 --> 00:23:22,665
Bijna vergeten.
Ga zitten.

180
00:23:24,169 --> 00:23:26,736
Eerlijk is eerlijk.

181
00:23:26,772 --> 00:23:30,740
We laten je niet in de steek
als het gaat om medische oplossingen.

182
00:23:30,809 --> 00:23:32,108
Ga maar.

183
00:23:48,593 --> 00:23:51,261
Kan ik je even spreken, Simon?

184
00:23:57,815 --> 00:24:03,980
Ik heb bewezen dat ik voor Negan kan werken.
Met mij kan je alle kanten op...

185
00:24:03,983 --> 00:24:09,912
maar ik moet het vertrouwen houden bij mijn mensen,
anders weet je niet wie het overneemt.

186
00:24:10,047 --> 00:24:13,048
Iemand anders is niet zo meewerkend.

187
00:24:13,117 --> 00:24:16,051
Die hebben misschien gekke idee�n.

188
00:24:18,488 --> 00:24:23,072
Wie zou het over moeten nemen?
Welke gekke idee�n?

189
00:24:23,075 --> 00:24:26,194
Geen idee.
Ik bekijk elk scenario.

190
00:24:26,629 --> 00:24:32,533
Wat ik wil zeggen, ik ken de aard van mensen,
en ze zijn niet voorspelbaar.

191
00:24:34,304 --> 00:24:37,071
Ik begrijp je bezorgdheid.

192
00:24:37,106 --> 00:24:39,573
Ik ben er niet ongevoelig voor.

193
00:24:44,249 --> 00:24:48,449
Als je problemen hebt van die aard...

194
00:24:48,484 --> 00:24:50,851
kom dan naar mij toe.

195
00:24:51,120 --> 00:24:52,520
Wanneer je wilt.

196
00:24:52,555 --> 00:24:54,555
Ik weet niet waar ik heen moet gaan.

197
00:24:56,359 --> 00:25:00,394
Ik regel het zo...

198
00:25:03,165 --> 00:25:05,866
zeg tegen de bewaker wie je bent...

199
00:25:07,303 --> 00:25:09,403
en dan laat hij je binnen.

200
00:25:12,742 --> 00:25:15,809
Zolang je de boel niet in de maling neemt.

201
00:25:16,846 --> 00:25:18,645
Dat zal niet gebeuren.

202
00:25:21,493 --> 00:25:24,311
Ik wil alleen dat je het overweegt...

203
00:25:24,314 --> 00:25:25,725
Geweldig.

204
00:25:30,520 --> 00:25:33,462
We trekken een fles tequila open.

205
00:25:33,762 --> 00:25:35,395
Dan praten we verder.

206
00:25:35,430 --> 00:25:37,864
We komen er wel uit.

207
00:25:38,900 --> 00:25:42,458
Je bent een goede kracht voor ons,
stress is niet goed voor je.

208
00:25:43,538 --> 00:25:47,707
Ik zou niet zoveel stress hebben
als je mijn dokter...

209
00:25:47,776 --> 00:25:50,243
Begrepen.

210
00:25:50,278 --> 00:25:52,111
Ik zal het niet meer vragen.

211
00:26:46,883 --> 00:26:49,450
Hij is weg.

212
00:26:51,220 --> 00:26:53,517
Je wilde hem vermoorden.

213
00:26:54,590 --> 00:26:55,666
Hij zou ons ontdekken.

214
00:26:55,951 --> 00:26:59,427
Dat heeft hij niet gedaan.

215
00:27:01,998 --> 00:27:03,931
Hij verdiende de dood.

216
00:27:05,696 --> 00:27:09,758
Sinds je hier bent,
heb je geen woord tegen me gezegd.

217
00:27:11,793 --> 00:27:13,668
Wil je me aankijken?

218
00:27:14,430 --> 00:27:16,349
Alsjeblieft.

219
00:27:28,993 --> 00:27:30,581
Het spijt me.

220
00:27:32,355 --> 00:27:35,423
Het spijt me.

221
00:27:43,747 --> 00:27:46,135
Het was niet jouw fout.

222
00:27:47,401 --> 00:27:49,578
Wel.

223
00:27:50,453 --> 00:27:54,055
Nee, dat was het niet.

224
00:27:54,590 --> 00:27:58,492
Jij bent ��n van de goede dingen in de wereld.

225
00:27:58,527 --> 00:28:01,095
Dat dacht Glenn ook.

226
00:28:01,530 --> 00:28:03,196
En hij weet het...

227
00:28:04,073 --> 00:28:06,634
want hij was ook ��n van de goede dingen.

228
00:28:09,405 --> 00:28:12,939
En...

229
00:28:13,275 --> 00:28:16,309
Ik wilde die vent ook vermoorden.

230
00:28:16,345 --> 00:28:20,254
Ik zou hem vastbinden,
en kijken hoe hij zou sterven.

231
00:28:25,218 --> 00:28:27,440
Maar we moeten winnen.

232
00:28:36,164 --> 00:28:39,799
Help me daarmee.

233
00:29:50,103 --> 00:29:52,147
Dit werkt wel.

234
00:30:32,943 --> 00:30:34,057
Nu.

235
00:31:05,169 --> 00:31:06,437
Stap in.

236
00:32:23,681 --> 00:32:25,581
Al iets?

237
00:32:25,817 --> 00:32:29,569
Negan is nog steeds niet naar buiten gekomen.
Maar Eugene is er wel.

238
00:32:30,364 --> 00:32:32,167
Eugene?

239
00:32:32,475 --> 00:32:36,210
Het lijkt alsof hij de mensen opdrachten geeft.

240
00:32:37,847 --> 00:32:40,147
Misschien bespeelt hij ze.

241
00:32:43,887 --> 00:32:46,089
Wil je me die knopen ook leren?

242
00:32:46,122 --> 00:32:48,711
Ik weet er veel, maar niet die.

243
00:32:49,759 --> 00:32:51,525
Dat zal toch snel niks meer uitmaken.

244
00:32:53,863 --> 00:32:55,830
Misschien.

245
00:33:05,608 --> 00:33:08,075
Ik zal ze je leren.

246
00:33:08,111 --> 00:33:10,653
Dit is de eerste.

247
00:33:12,047 --> 00:33:14,615
Laat over je hand gaan, zoals dit.

248
00:33:15,061 --> 00:33:19,530
Dan pak je de bovenkant,
die naar beneden, dan omhoog...

249
00:33:19,533 --> 00:33:22,407
en dan erdoorheen, dan...

250
00:33:23,459 --> 00:33:28,729
Snap je hem?
- Laat mij eens proberen.

251
00:33:51,753 --> 00:33:53,686
Weet je...

252
00:33:53,722 --> 00:33:56,856
we hebben geluk dat je bij ons bent.

253
00:33:58,326 --> 00:34:00,391
Je weet alles.

254
00:34:18,318 --> 00:34:19,924
Johnny.

255
00:34:21,289 --> 00:34:23,258
Die heeft me geleerd bommen te maken.

256
00:34:23,361 --> 00:34:26,017
Overlever, weet te overleven.

257
00:34:27,933 --> 00:34:31,122
Marcus heeft me geleerd om auto's te repareren.
Hij was een monteur...

258
00:34:31,158 --> 00:34:33,525
en een Wannabee stunt-driver.

259
00:34:34,661 --> 00:34:36,541
En een eikel.

260
00:34:36,644 --> 00:34:39,864
Knopen heb ik geleerd van Chaser.

261
00:34:41,034 --> 00:34:43,368
Dat was natuurlijk niet zijn echte naam.

262
00:34:45,071 --> 00:34:47,633
Er waren nog anderen.

263
00:34:48,842 --> 00:34:51,345
Waren dat de mensen die je
verloren hebt op weg naar D.C?

264
00:34:51,548 --> 00:34:53,303
Hun niet.

265
00:34:53,506 --> 00:34:59,524
Een hoop wilden mij beschermen,
alsof ik niet voor mezelf kan zorgen.

266
00:35:01,087 --> 00:35:02,286
En dat kon ik niet.

267
00:35:03,856 --> 00:35:06,107
En ik haatte hoe dat voelde.

268
00:35:07,794 --> 00:35:09,527
Dus ik deed met ze mee.

269
00:35:09,862 --> 00:35:14,932
En ze hadden zelfs niet door dat ik alles
oppikte om het beter te doen.

270
00:35:18,704 --> 00:35:21,812
Toen ik alles wist ben ik hem gesmeerd.

271
00:35:26,211 --> 00:35:28,145
De seks was voor het plezier.

272
00:35:29,548 --> 00:35:33,884
Als de wereld voorbij is,
moet iedereen stoom afblazen.

273
00:35:40,793 --> 00:35:43,126
Was dat hoe het met hem was?

274
00:35:45,348 --> 00:35:47,081
Abraham?

275
00:35:51,570 --> 00:35:53,503
Nee.

276
00:35:56,274 --> 00:36:00,343
Ik viel op hem omdat hij zag
dat ik mijn eigen boontjes kon doppen.

277
00:36:00,378 --> 00:36:02,312
En ik keek nooit terug.

278
00:36:09,020 --> 00:36:11,754
Nee, dat deed ik niet.

279
00:36:12,290 --> 00:36:15,191
Toen het voorbij was.

280
00:36:26,637 --> 00:36:28,882
Weet je waar ik aan onderdoor ging?

281
00:36:30,474 --> 00:36:34,376
Toen we bij Alexandria aankwamen,
deed ik alsof alles goed was, en hij niet.

282
00:36:37,614 --> 00:36:39,481
En eigenlijk was die klootzak wel goed.

283
00:36:42,519 --> 00:36:44,786
En ik snapte er geen snars van.

284
00:36:44,821 --> 00:36:49,024
Daar leven, voelen dat we het gehaald hadden,
dat pikte ik niet op.

285
00:36:49,859 --> 00:36:52,927
Toen hij sprong...

286
00:36:57,500 --> 00:36:59,619
dacht ik dat ik jou ook haatte.

287
00:37:02,338 --> 00:37:06,240
Maar volgens mij haatte ik het
dat hij er eerder achter was gekomen.

288
00:37:13,983 --> 00:37:16,391
Ik heb dit nog nooit aan iemand verteld.

289
00:37:17,187 --> 00:37:19,620
Wie ik was, wat ik heb gedaan...

290
00:37:21,591 --> 00:37:24,125
Daar ben ik blij om...

291
00:37:24,160 --> 00:37:27,628
dat jij het gevoel had...

292
00:37:27,697 --> 00:37:31,566
dat je het kon.

293
00:37:35,391 --> 00:37:38,005
Ik ben stom geweest om zoveel tijd te verspillen.

294
00:37:47,283 --> 00:37:51,118
Ik kan Abraham nooit vertellen
dat ik gelukkig was als hij dat was.

295
00:38:01,464 --> 00:38:03,864
Was je gelukkig?

296
00:38:10,773 --> 00:38:13,018
Ik wel.

297
00:38:20,749 --> 00:38:22,716
Het was nog niet zijn tijd.

298
00:38:24,786 --> 00:38:27,554
Abraham zou het gevecht aangaan.

299
00:38:32,227 --> 00:38:35,728
En die klootzak met die knuppel
heeft dat ook weggenomen.

300
00:38:38,733 --> 00:38:42,502
Dat hij met zijn dood een punt zou maken.

301
00:38:51,813 --> 00:38:54,447
Ik denk dat we dat allemaal willen.

302
00:39:00,788 --> 00:39:03,422
Hoe het allemaal loopt...

303
00:39:03,491 --> 00:39:05,224
ik steun je.

304
00:39:05,293 --> 00:39:08,461
En ik jou.

305
00:39:14,435 --> 00:39:16,368
Denk je dat die lul er al is?

306
00:39:44,064 --> 00:39:47,098
Dr Carson, de dokter van Maggie.

307
00:39:47,133 --> 00:39:48,959
Ze hebben hem hierheen gebracht.

308
00:39:56,676 --> 00:39:58,676
Negan is uit zijn grot.

309
00:40:07,987 --> 00:40:10,354
Hij staat te dichtbij.

310
00:40:11,090 --> 00:40:14,758
Ik kan zo niet schieten.

311
00:40:16,562 --> 00:40:18,261
Zet de walkie aan.

312
00:40:27,039 --> 00:40:30,040
<i>Dit is Dr. Eugene Porter, hoofdontwikkelaar.</i>

313
00:40:30,075 --> 00:40:34,398
<i>Ik heb nog een tiental walkers nodig.
a.k.a. doden zoals je dat in de volksmond zegt...</i>

314
00:40:34,401 --> 00:40:36,742
<i>voor het protocol voor het hek PDQ.</i>

315
00:40:37,516 --> 00:40:38,849
<i>Eigenlijk, PFQ.</i>

316
00:40:39,117 --> 00:40:42,152
<i>Dit is voor Negan, wie ik ook ben...</i>

317
00:40:42,220 --> 00:40:46,056
<i>al duurt het de hele nacht om ze te krijgen,
dus ik heb gezonde mensen nodig...</i>

318
00:40:46,124 --> 00:40:49,459
<i>om de gebouwen te omsingelen
en leeg te trekken.</i>

319
00:40:49,494 --> 00:40:53,363
<i>Negan rust nu uit,
dus in de tussentijd...</i>

320
00:40:53,398 --> 00:40:59,235
<i>zullen alle vragen naar ondergetekende gaan,
Dr. Eugene Porter, hoofdontwikkelaar...</i>

321
00:40:59,271 --> 00:41:02,084
<i>ook wel bekend als Negan, wie ik ben.</i>

322
00:41:04,542 --> 00:41:06,437
Verandering van plannen.

323
00:41:11,916 --> 00:41:14,116
Dus we gaan naar binnen.

324
00:41:16,287 --> 00:41:18,187
Ik ben klaar.

325
00:41:33,748 --> 00:41:35,214
Binnen.

326
00:41:39,922 --> 00:41:41,554
Freddie zei dat je me wilde spreken?

327
00:41:41,623 --> 00:41:43,923
Ja, ga zitten.

328
00:41:49,764 --> 00:41:54,033
Het viel me op dat je de laatste tijd
minder moeite doet.

329
00:41:54,101 --> 00:41:57,569
En er zijn te veel mensen in je caravan.

330
00:41:57,605 --> 00:41:59,171
Brandpreventie.

331
00:41:59,506 --> 00:42:03,108
Er is genoeg ruimte hier in Barrington House
voor onze gasten.

332
00:42:03,143 --> 00:42:05,110
Nu we het daar toch over hebben...

333
00:42:05,279 --> 00:42:11,349
Ik heb werk voor de nieuwelingen,
wie wil blijven moeten aan de slag.

334
00:42:11,385 --> 00:42:13,485
Geen kostgangers meer.

335
00:42:13,520 --> 00:42:15,587
Je hebt ze Dr Carson mee laten nemen.

336
00:42:15,622 --> 00:42:17,856
Het verraste me dat je niet weer op je knie�n ging.

337
00:42:18,091 --> 00:42:21,660
Je moet echt bezorgd zijn
als je iedereen opsplitst.

338
00:42:23,530 --> 00:42:25,630
Zo'n toontje moet je niet opzetten naar mij.

339
00:42:25,633 --> 00:42:29,534
Wie weet wat er kan gebeuren
met al die Verlossers die af en toe binnenwippen.

340
00:42:30,369 --> 00:42:32,436
Bedreigde je me nu?

341
00:42:32,471 --> 00:42:35,072
Ik was sarcastisch.

342
00:42:35,107 --> 00:42:37,007
Nee, volgens mij bedreigde je me.

343
00:42:37,043 --> 00:42:40,944
Denk wat je wilt,
wat ik zeg is dat ik op mijn vrienden let...

344
00:42:40,980 --> 00:42:44,848
en ik kom er net achter
dat wij geen vrienden zijn.

345
00:42:45,284 --> 00:42:48,018
Binnen.

346
00:42:49,521 --> 00:42:51,755
Geweldig, tequila.

347
00:42:51,790 --> 00:42:54,591
Zou je hem voor me open willen trekken?

348
00:42:54,626 --> 00:42:58,361
Je mag gaan.
Laat hem de weg zien, Kal.

349
00:43:13,300 --> 00:43:14,643
Alles goed?

350
00:43:14,713 --> 00:43:16,788
Waar zijn Sasha en Rosita?

351
00:43:20,552 --> 00:43:22,218
Zie je dat daar?

352
00:43:22,254 --> 00:43:25,321
Dat is lak hebben aan het veiligheidsprotocol.

353
00:43:25,357 --> 00:43:27,965
Het moet helemaal kloppen.

354
00:43:27,968 --> 00:43:31,961
Wat er bij de ingang staat met de spijkers,
kettingen en hoofden is goed voor het gezicht.

355
00:43:32,030 --> 00:43:35,632
Het geeft een signaal naar indringers
van 'pleur niet alles op straat' soort.

356
00:43:37,569 --> 00:43:40,370
Maar we moeten hameren
op het echte veiligheidsprotocol...

357
00:43:40,405 --> 00:43:43,039
zeker op de punten wat bewezen troep is.

358
00:43:43,108 --> 00:43:44,407
Klopt.

359
00:43:44,642 --> 00:43:46,776
Daarom sta ik nu op wacht.

360
00:43:46,811 --> 00:43:50,479
Met alle respect voor die andere,
maar die kon zelfs geen pistool vasthouden.

361
00:43:54,191 --> 00:43:56,206
Opstaan. Je gaat eruit.

362
00:43:59,757 --> 00:44:02,882
Eugene, sta op.
We halen je hier weg.

363
00:44:10,455 --> 00:44:11,626
Nee.

364
00:44:12,128 --> 00:44:14,396
Ik ga niet mee.

365
00:44:16,602 --> 00:44:19,908
Ik heb je niet gevraagd om te komen, dus ga.

366
00:44:21,612 --> 00:44:24,112
Er kunnen mensen rondlopen.

367
00:44:35,892 --> 00:44:39,140
Die verdomde liegende zwakkeling...

368
00:44:42,283 --> 00:44:44,659
Ik ben er klaar mee. Ik ga naar binnen.

369
00:44:46,935 --> 00:44:49,670
Wacht. Dit is zo gebeurt.

370
00:44:49,706 --> 00:44:52,506
Er kunnen er meer komen.
Hou een oogje in het zeil.

371
00:45:00,149 --> 00:45:03,417
Nog even.

372
00:45:10,027 --> 00:45:12,860
Wat doe je in godsnaam?
- Ga.

373
00:45:12,895 --> 00:45:14,662
Het is nog niet je tijd.

374
00:45:14,697 --> 00:45:17,126
Daar heb ik een punt mee, toch?

375
00:45:18,301 --> 00:45:19,733
Ze hebben je nodig.

376
00:45:57,247 --> 00:46:00,648
Verdomme.

377
00:46:13,250 --> 00:46:18,468
Vertaling:
Quality over Quantity (QoQ) Releases

378
00:46:18,695 --> 00:46:22,285
Download deze ondertitel op:
- www.OpenSubtitles.org -

