#!/usr/bin/env python

import getopt
import re
import sys
from os import path, listdir, rename
import mimetypes


class Settings:
    dryRun = False
    verbose = False


class IndentTrace:
    current_print_lvl = 0
    def __init__(self):
        IndentTrace.current_print_lvl += 1
    def __del__(self):
        IndentTrace.current_print_lvl -= 1

class Data:
    dirName=""

SUBTYPES = ["sub", "srt"]

LANGUAGES = {'nl': "Dutch",
             'en': "English",
             'fr': "French",
             'de': "German",
             'es': "Spanish"}

SUBVERSIONS =  [ "720p", "1080p"
               , "[h33t]"
               , "Addic7ed.com"
               , "AVS", "[PRiME]"
               , "aXXo"
               , "BATV", "BATV[ettv]"
               , "BD-RIP", "BDRip", "BD-R"
               , "BluRay"
               , "C.edit"
               , "CONVOY"
               , "DD5.1"
               , "DEMAND"
               , "DEFLATE"
               , "DVDRip", "DVDSCR"
               , "ettv", "[ettv]"
               , "ExYu"
               , "FLEET"
               , "GECKOS"
               , "HBO"
               , "HD"
               , "HEVC"
               , "H.264", "H264", "x264", "x264-FS"
               , "HI"
               , "iExTV"
               , "INTERNAL"
               , "KILLERS", "Killers", "KIILERS"
               , "LOL"
               , "monkee", "monkee[ettv]"
               , "NF"
               , "NTb"
               , ".orig"
               , "pir8"
               , "PROPER", "Proper"
               , "PSA"
               , "QCF"
               , "RARBG"
               , "REPACK"
               , "ShAaNiG"
               , "SKGTV"
               , "TURBO"
               , "updated"
               , "WEB-DL", "Web-DL", "WEBRip", "WebRip"
               , "ZT"
               ]


def usage():
    print "This script will rename all subtitles and movies within this directory"
    print "batchRenameMovies.py [file|directory]"
    print "Options:"
    print "-d|--dry-run :   run without changing any input files"
    print "-h|--help    :   prints this help content"
    print "-v|--verbose :   verbose mode"


def readArguments(argv):
    try:
        unixOptions = "hdv:"
        gnuOptions = ["help", "dry-run=", "verbose"]
        opts, args = getopt.getopt(argv, unixOptions, gnuOptions)
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        if opt in ("-d", "--dry-run"):
            print("Dry run enable")
            Settings.dryRun = True
        if opt in ("-v", "--verbose"):
            Settings.verbose = True

    print "Args = " + str(args)
    if not args:
        print "No files or directory given, process current directory"
        return getFilesFromDir('.')
    elif len(args) == 1: # either 1 file or 1 directory
        name = args[0]
        if path.isdir(name):
            Data.dirName = name
            print "Process directory: " + name
            return getFilesFromDir(name)
        elif isinstance(name, basestring) and path.isfile(name):
            Data.dirName = path.dirname(name)
            print "Process file: " + name
            return getSubFiles(name) + [path.basename(name)]
        else:
            print "Not a valid filename! " + ' '.join(args)
    elif len(args) > 1: # multiple files or directories
        Data.dirName = path.dirname(args[0])
        print "Process files: " + ' '.join(args)
        return getSubFiles(args[0]) + [path.basename(arg) for arg in args]
    else:
        print "Nothing to be done, no input dir or files."
        return []


def printWithLvl(full_trace_str):
    print '   ' * IndentTrace.current_print_lvl + str(full_trace_str)


def debugPrint(full_trace_str):
    getframe_expr = 'sys._getframe({}).f_code.co_name'
    nameOfCallerFunction = eval(getframe_expr.format(2))
    if Settings.verbose:
        print nameOfCallerFunction + full_trace_str


def isVideo(ext):
    mimeType = mimetypes.guess_type("dummy"+ext)
    return ( mimeType and mimeType[0] and ("video" in mimeType[0]) )


def isSubtitle(ext):
    return any(subtype in ext for subtype in SUBTYPES)


def getFilesFromDir(dirName):
    return [f for f in listdir(dirName) if path.isfile(path.join(dirName, f))]


def getSubFiles(filename):
    allFiles = [ f for f in listdir(path.dirname(filename)) if isSubtitle(path.splitext(f)[1]) ]
    return allFiles

def getDirName(filename):
    return path.dirname(path.abspath(filename))


def renameFiles(files):
    movieFiles, subFiles = getVideosAndSubs(files)
    renameInGroups(movieFiles, subFiles)


def renameInGroups(movies, subs):
    for idx, movie in enumerate(movies):
        IndentTrace.current_print_lvl += 1
        printWithLvl("\n"+str(idx + 1) + ". Process movie: " + movie.filename)
        printWithLvl(movie.episodeInfo)
        infoFound = False
        allEpisodeInfo = {}
        for sub in subs:
            if sub.isDefined() and (sub.getUniqueEpisodeId() == movie.getUniqueEpisodeId()):
                allEpisodeInfo = sub.episodeInfo
                printWithLvl("Found sub: " + str(allEpisodeInfo))
                sub.rename(allEpisodeInfo)
                infoFound = True
        if infoFound:
            movie.rename(allEpisodeInfo)
        else:
            printWithLvl("Not a suitable subtitle found")
        IndentTrace.current_print_lvl -= 1



def parseLanguage(string):
    # type: (string) -> string
    for key, value in LANGUAGES.items():
        if ('.' + str(key).lower()) in string.lower() or \
           ('.' + str(value).lower()) in string.lower():
            return key
    return ''


def getTitle(string):
    languageKeys = ['.'+l for l in LANGUAGES.keys()]
    returnString = removeSubstrings(string, SUBVERSIONS + languageKeys + LANGUAGES.values())
    return returnString.replace("- ", '').strip()


def removeSubstrings(string, list):
    returnString = string
    for substring in list:
        foundAtIdx = returnString.find(substring)
        if foundAtIdx == -1: continue
        returnString = returnString[:foundAtIdx-1]
    return returnString


class File(object):
    fileNbr = 0

    def __init__(self, filename, ext):
        File.fileNbr += 1
        self.filename = filename
        self.extension = ext
        self.collectInfo()

    def collectInfo(self):
        for style in [CommonTvShowStyle(self.filename),
                      Addic7edStyle(self.filename),
                      MovieStyle(self.filename)]:
            self.episodeInfo = style.parse()
            self.style = style
            if self.episodeInfo: return
        raise StandardError("No format style found for file: " + self.filename + ". Skip it.")

    def getUniqueEpisodeId(self):
        """ Create a string from the TV show + the season + the episode """
        return self.style.createUniqueId(self.episodeInfo)

    def isDefined(self):
        return hasattr(self, 'episodeInfo')

    def rename(self, newFilename):
        oldFile = Data.dirName + '/' + self.filename + self.extension
        newFile = Data.dirName + '/' + newFilename + self.extension
        try:
            if not Settings.dryRun: rename(oldFile, newFile)
            printWithLvl("==> " + oldFile + " renamed to:")
            printWithLvl("    " + newFile)
        except OSError as e:
            printWithLvl("==> Filename " + oldFile + " could not be renamed! Error: " + str(e))


class Video(File):
    def rename(self, info):
        newFilename = self.style.createNewFileName(info)
        super(Video, self).rename(newFilename)


class Subtitle(File):
    def rename(self, info):
        newFilename = self.style.createNewFileName(info) +'.'+ parseLanguage(self.filename)
        return super(Subtitle, self).rename(newFilename)


# Abstract class
class FormatStyle:
    def parse(self):
        raise NotImplementedError

    @staticmethod
    def createUniqueId(info):
        raise NotImplementedError

    @staticmethod
    def createNewFileName(fullInfo):
        raise NotImplementedError


class TvShowStyle(FormatStyle):
    def parse(self):
        raise NotImplementedError

    @staticmethod
    def createUniqueId(info):
        if info['tvShow'] is None:
            raise ValueError("No tv show found!")
        return info['tvShow'].lower() + ' ' + CommonTvShowStyle.createSxEx(info)

    @staticmethod
    def createSx(season):
        return "S" + ("%02d" % season)

    @staticmethod
    def createEx(episode):
        return "E" + ("%02d" % episode)

    @staticmethod
    def createSxEx(info):
        return CommonTvShowStyle.createSx(info['season']) + CommonTvShowStyle.createEx(info['episode'])

    @staticmethod
    def createNewFileName(fullInfo):
        titleStr = (" - " + fullInfo['title']) if 'title' in fullInfo else ""
        return fullInfo['tvShow'] + ' ' + CommonTvShowStyle.createSxEx(fullInfo) + titleStr


class MovieStyle(FormatStyle):
    def __init__(self, filename):
        self.filename = filename

    def parse(self):
        self.year = self.parseYear()

    def parseYear(self):
        try:
            regexGroups = re.search("[.(\[]\d{4}[.)\]]", self.filename).groups()
        except AttributeError:
            return None
        return regexGroups

    @staticmethod
    def createUniqueId(info):
        if not info or 'title' not in info:
            raise ValueError("No movie title found!")
            return ""
        return info['title'].lower() + ' ' + (info['year'] if info['year'] is not None else "")


class Addic7edStyle(TvShowStyle):
    """ Form: <TV Show> - SxEx - (<title>.)720p.WEB-DL.x264.ShAaNiG """

    def __init__(self, filename):
        self.delimiter = " - "
        self.filename = filename

    def parse(self):
        parts = self.filename.split(self.delimiter, 3)
        if len(parts) < 2: return {}
        info = {}
        info['tvShow'] = parts[0].replace(" -", '').replace('.', ' ').strip()
        if len(parts) > 1:
            SxE = self.parseSxE(parts[1])
        if SxE:
            info['season'] = int(SxE[0])
            info['episode'] = int(SxE[1])
        if len(parts) > 2:
            info['title'] = getTitle(parts[2])
        return info

    def parseSxE(self, string):
        try:
            regexGroups = re.search("(\d+).?(\d+)", string).groups()  # .? == possible char (e.g. 'x')
        except AttributeError:
            return None
        return regexGroups


class CommonTvShowStyle(TvShowStyle):
    """ Form: <TV Show>.Sx(x)Ex.(<title>).720p.WEB-DL.x264.ShAaNiG
     or form: <TV Show> Sx(x)Ex (<title>).nl
    """

    def __init__(self, filename):
        self.filename = filename

    def parse(self):
        SxEx = self.parseSxEx(self.filename)
        if not SxEx: return {}
        info = {}
        info['season'] = int(SxEx[0])
        info['episode'] = int(SxEx[1])

        parts = self.filename.split(CommonTvShowStyle.createSx(info['season']))
        if len(parts) > 1:
            info['tvShow'] = parts[0].replace(" -", '').replace('.', ' ').strip()

        parts = self.filename.split(CommonTvShowStyle.createEx(info['episode']))
        if len(parts) > 1:
            afterEx = parts[1]
            info['title'] = getTitle(afterEx[1:])
            return info
        else:
            return {}

    def parseSxEx(self, string):
        try:
            regexGroups = re.search("S(\d+).?E(\d+)", string).groups()
        except AttributeError:
            return None
        return regexGroups

    # NOT WORKING, RETURNS \d and \d INSTEAD
    def parseTvShow(self, string):
        try:
            regexGroups = re.search("^.*?(?=S(\d+).?E(\d+))", string).string
        except AttributeError:
            return None
        return regexGroups


def getVideosAndSubs(allFiles = None):
    movies = []
    subs = []
    if allFiles is None or len(allFiles) == 0:
        print "No files given!"
        return movies, subs
    for f in allFiles:
        if f.startswith('.'): continue
        filenameParts = path.splitext(f)
        filename = filenameParts[0]
        ext = filenameParts[1]
        if isVideo(ext):
            try:
                movies.append(Video(filename, ext))
            except StandardError:
                print "Not able to find a format style for video: " + filename
                continue
        elif isSubtitle(ext):
            subs.append(Subtitle(filename, ext))
    return movies, subs


if __name__ == "__main__":
    allFiles = readArguments(sys.argv[1:])
    renameFiles(allFiles)
